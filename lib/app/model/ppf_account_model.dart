// To parse this JSON data, do
//
//     final ppfAccountModel = ppfAccountModelFromJson(jsonString);

import 'dart:convert';

PpfAccountModel ppfAccountModelFromJson(String str) => PpfAccountModel.fromJson(json.decode(str));

String ppfAccountModelToJson(PpfAccountModel data) => json.encode(data.toJson());

class PpfAccountModel {
  PpfAccountModel({
    this.responsePpfList,
    this.responseStatus,
    this.responseMsg,
  });

  final List<ResponsePpfList>? responsePpfList;
  final int? responseStatus;
  final String? responseMsg;

  factory PpfAccountModel.fromJson(Map<String, dynamic> json) {
    try{
    return  PpfAccountModel(
        responsePpfList: List<ResponsePpfList>.from(json["response_ppf_list"].map((x) => ResponsePpfList.fromJson(x))),
        responseStatus: json["response_status"],
        responseMsg: json["response_msg"],
      );
    }catch(e){
     return PpfAccountModel(
        responsePpfList: [],
        responseStatus: json["response_status"],
        responseMsg: json["response_msg"],
      );
    }
  }

  Map<String, dynamic> toJson() => {
    "response_ppf_list": List<dynamic>.from(responsePpfList!.map((x) => x.toJson())),
    "response_status": responseStatus,
    "response_msg": responseMsg,
  };
}

class ResponsePpfList {
  ResponsePpfList({
    this.id,
    this.accountNo,
    this.statementDate,
    this.deposite,
    this.remark,
  });

  final String? id;
  final String? accountNo;
  final DateTime? statementDate;
  final String? deposite;
  final String? remark;

  factory ResponsePpfList.fromJson(Map<String, dynamic> json) => ResponsePpfList(
    id: json["id"],
    accountNo: json["account_no"],
    statementDate: DateTime.parse(json["statement_date"]),
    deposite: json["deposite"],
    remark: json["remark"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "account_no": accountNo,
    "statement_date": "${statementDate!.year.toString().padLeft(4, '0')}-"
        "${statementDate!.month.toString().padLeft(2, '0')}-${statementDate!.day.toString().padLeft(2, '0')}",
    "deposite": deposite,
    "remark": remark,
  };
}
