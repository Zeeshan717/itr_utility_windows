// To parse this JSON data, do
//
//     final userClientList = userClientListFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

UserClientList userClientListFromJson(String str) => UserClientList.fromJson(json.decode(str));

String userClientListToJson(UserClientList data) => json.encode(data.toJson());

class UserClientList {
  UserClientList({
    required this.clientArray,
    required this.success,
    required this.responseMsg,
  });

  List<ClientArray> clientArray;
  int success;
  String responseMsg;

  factory UserClientList.fromJson(Map<String, dynamic> json) => UserClientList(
    clientArray: List<ClientArray>.from(json["client_array"].map((x) => ClientArray.fromJson(x))),
    success: json["success"],
    responseMsg: json["response_msg"],
  );

  Map<String, dynamic> toJson() => {
    "client_array": List<dynamic>.from(clientArray.map((x) => x.toJson())),
    "success": success,
    "response_msg": responseMsg,
  };
}

class ClientArray {
  ClientArray({
    required this.clientId,
    required this.clientName,
    required this.firmName,
    required this.firmOwnerName,
    required this.clientEmail,
    required this.clientDob,
    required this.phoneNo,
    required this.distributorAddress,
    required this.placeOfSupply,
    required this.gstinNo,
    required this.panNo,
    required this.bankName,
    required this.accountNo,
    required this.ifscNo,
    required this.proprietorName,
    required this.proprietorAadharNumber,
    required this.proprietorFathersName,
    required this.firmType,
  });

  String clientId;
  String clientName;
  String firmName;
  String firmOwnerName;
  String clientEmail;
  String clientDob;
  String phoneNo;
  String distributorAddress;
  String placeOfSupply;
  String gstinNo;
  String panNo;
  String bankName;
  String accountNo;
  String ifscNo;
  String proprietorName;
  String proprietorAadharNumber;
  String proprietorFathersName;
  String firmType;

  factory ClientArray.fromJson(Map<String, dynamic> json) => ClientArray(
    clientId: json["client_id"],
    clientName: json["client_name"],
    firmName: json["firm_name"],
    firmOwnerName: json["firm_owner_name"],
    clientEmail: json["client_email"],
    clientDob: json["client_dob"],
    phoneNo: json["phone_no"],
    distributorAddress: json["distributor_address"],
    placeOfSupply: json["place_of_supply"],
    gstinNo: json["gstin_no"],
    panNo: json["pan_no"],
    bankName: json["bank_name"],
    accountNo: json["account_no"],
    ifscNo: json["ifsc_no"],
    proprietorName: json["proprietor_name"],
    proprietorAadharNumber: json["proprietor_aadhar_number"],
    proprietorFathersName: json["proprietor_fathers_name"],
    firmType: json["firm_type"],
  );

  Map<String, dynamic> toJson() => {
    "client_id": clientId,
    "client_name": clientName,
    "firm_name": firmName,
    "firm_owner_name": firmOwnerName,
    "client_email": clientEmail,
    "client_dob": clientDob,
    "phone_no": phoneNo,
    "distributor_address": distributorAddress,
    "place_of_supply": placeOfSupply,
    "gstin_no": gstinNo,
    "pan_no": panNo,
    "bank_name": bankName,
    "account_no": accountNo,
    "ifsc_no": ifscNo,
    "proprietor_name": proprietorName,
    "proprietor_aadhar_number": proprietorAadharNumber,
    "proprietor_fathers_name": proprietorFathersName,
    "firm_type": firmType,
  };
}
