// To parse this JSON data, do
//
//     final deductionModel = deductionModelFromJson(jsonString);

import 'dart:convert';

DeductionModel deductionModelFromJson(String str) => DeductionModel.fromJson(json.decode(str));

String deductionModelToJson(DeductionModel data) => json.encode(data.toJson());

class DeductionModel {
  DeductionModel({
    required this.the80C,
    required this.the80Ccd,
    required this.the80D,
    required this.the80G,
    required this.the80Tta,
    required this.other,
    required this.baseUrl,
    required this.epfPf,
    required this.lic,
    required this.nationalSaving,
    required this.tuitionFees,
    required this.seniorCitizenSaving,
    required this.repaymentOfPrinciple,
    required this.sukanyaSamridhiScheme,
    required this.regFeesStampDuty,
    required this.date80C,
    required this.remark80C,
    required this.attachmentPath80C,
    required this.donation,
    required this.date80Ccd,
    required this.remark80Ccd,
    required this.attachmentPath80Ccd,
    required this.parents,
    required this.self,
    required this.date80D,
    required this.remark80D,
    required this.attachmentPath80D,
    required this.range,
    required this.otherByCheque,
    required this.inCash,
    required this.date80G,
    required this.remark80G,
    required this.attachmentPath80G,
    required this.val80Tta,
    required this.date80Tta,
    required this.remark80Tta,
    required this.attachmentPath80Tta,
    required this.dateOther,
    required this.remarkOther,
    required this.attachmentPathOther,
    required this.responseStatus,
    required this.responseMsg,
  });

  String the80C;
  String the80Ccd;
  String the80D;
  String the80G;
  String the80Tta;
  String other;
  String baseUrl;
  String epfPf;
  String lic;
  String nationalSaving;
  String tuitionFees;
  String seniorCitizenSaving;
  String repaymentOfPrinciple;
  String sukanyaSamridhiScheme;
  String regFeesStampDuty;
  DateTime date80C;
  String remark80C;
  String attachmentPath80C;
  String donation;
  DateTime date80Ccd;
  String remark80Ccd;
  String attachmentPath80Ccd;
  String parents;
  String self;
  DateTime date80D;
  String remark80D;
  String attachmentPath80D;
  String range;
  String otherByCheque;
  String inCash;
  DateTime date80G;
  String remark80G;
  String attachmentPath80G;
  String val80Tta;
  DateTime date80Tta;
  String remark80Tta;
  String attachmentPath80Tta;
  DateTime dateOther;
  String remarkOther;
  String attachmentPathOther;
  int responseStatus;
  String responseMsg;

  factory DeductionModel.fromJson(Map<String, dynamic> json) => DeductionModel(
    the80C: json["80c"]==null?"0":json["80c"],
    the80Ccd: json["80ccd"]==null?"0":json["80ccd"],
    the80D: json["80d"]==null?"0":json["80d"],
    the80G: json["80g"]==null?"0":json["80g"],
    the80Tta: json["80tta"]==null?"0":json["80tta"],
    other: json["other"]==null?"0":json["other"],
    baseUrl: json["base_url"],
    epfPf: json["epf_pf"]==null?"0":json["epf_pf"],
    lic: json["lic"]==null?"0":json["lic"],
    nationalSaving: json["national_saving"]==null?"0":json["national_saving"],
    tuitionFees: json["tuition_fees"]==null?"0":json["tuition_fees"],
    seniorCitizenSaving: json["senior_citizen_saving"]==null?"0":json["senior_citizen_saving"],
    repaymentOfPrinciple: json["repayment_of_principle"]==null?"0":json["repayment_of_principle"],
    sukanyaSamridhiScheme: json["sukanya_samridhi_scheme"]==null?"0":json["sukanya_samridhi_scheme"],
    regFeesStampDuty: json["reg_fees_stamp_duty"]==null?"0":json["reg_fees_stamp_duty"],
    date80C:json["date_80c"]==null?DateTime.parse("0000-00-00"): DateTime.parse(json["date_80c"]),
    remark80C: json["remark_80c"]==null?"0":json["remark_80c"],
    attachmentPath80C: json["attachment_path_80c"]==null?"":json["attachment_path_80c"],
    donation: json["donation"]==null?"0":json["donation"],
    date80Ccd: json["date_80ccd"] == null?DateTime.parse("0000-00-00"): DateTime.parse(json["date_80ccd"]),
    remark80Ccd: json["remark_80ccd"]==null?"":json["remark_80ccd"],
    attachmentPath80Ccd: json["attachment_path_80ccd"]??"",
    parents: json["parents"]==null?"":json["parents"],
    self: json["self"]==null?"":json["self"],
    date80D: json["date_80d"] ==null?DateTime.parse("0000-00-00"): DateTime.parse(json["date_80d"]),
    remark80D: json["remark_80d"]==null?"":json["other"],
    attachmentPath80D: json["attachment_path_80d"]==null?"":json["other"],
    range: json["range"]==null?"":json["other"],
    otherByCheque: json["other_by_cheque"]==null?"":json["other"],
    inCash: json["in_cash"]==null?"":json["other"],
    date80G: json["date_80g"] == null ?DateTime.parse("0000-00-00"):DateTime.parse(json["date_80g"]),
    remark80G: json["remark_80g"]==null?"":json["other"],
    attachmentPath80G: json["attachment_path_80g"]==null?"":json["attachment_path_80g"],
    val80Tta: json["val_80tta"]==null?"":json["other"],
    date80Tta: json["date_80tta"]==null?DateTime.parse("0000-00-00"):DateTime.parse(json["date_80tta"]),
    remark80Tta: json["remark_80tta"]==null?"":json["other"],
    attachmentPath80Tta: json["attachment_path_80tta"]==null?"":json["other"],
    dateOther:json["date_other"] == null ?DateTime.parse("0000-00-00"):DateTime.parse(json["date_other"]),
    remarkOther: json["remark_other"]==null?"":json["other"],
    attachmentPathOther: json["attachment_path_other"]==null?"":json["other"],
    responseStatus: json["response_status"],
    responseMsg: json["response_msg"],
  );

  Map<String, dynamic> toJson() => {
    "80c": the80C,
    "80ccd": the80Ccd,
    "80d": the80D,
    "80g": the80G,
    "80tta": the80Tta,
    "other": other,
    "base_url": baseUrl,
    "epf_pf": epfPf,
    "lic": lic,
    "national_saving": nationalSaving,
    "tuition_fees": tuitionFees,
    "senior_citizen_saving": seniorCitizenSaving,
    "repayment_of_principle": repaymentOfPrinciple,
    "sukanya_samridhi_scheme": sukanyaSamridhiScheme,
    "reg_fees_stamp_duty": regFeesStampDuty,
    "date_80c": "${date80C.year.toString().padLeft(4, '0')}-${date80C.month.toString().padLeft(2, '0')}-${date80C.day.toString().padLeft(2, '0')}",
    "remark_80c": remark80C,
    "attachment_path_80c": attachmentPath80C,
    "donation": donation,
    "date_80ccd": "${date80Ccd.year.toString().padLeft(4, '0')}-${date80Ccd.month.toString().padLeft(2, '0')}-${date80Ccd.day.toString().padLeft(2, '0')}",
    "remark_80ccd": remark80Ccd,
    "attachment_path_80ccd": attachmentPath80Ccd,
    "parents": parents,
    "self": self,
    "date_80d": "${date80D.year.toString().padLeft(4, '0')}-${date80D.month.toString().padLeft(2, '0')}-${date80D.day.toString().padLeft(2, '0')}",
    "remark_80d": remark80D,
    "attachment_path_80d": attachmentPath80D,
    "range": range,
    "other_by_cheque": otherByCheque,
    "in_cash": inCash,
    "date_80g": "${date80G.year.toString().padLeft(4, '0')}-${date80G.month.toString().padLeft(2, '0')}-${date80G.day.toString().padLeft(2, '0')}",
    "remark_80g": remark80G,
    "attachment_path_80g": attachmentPath80G,
    "val_80tta": val80Tta,
    "date_80tta": "${date80Tta.year.toString().padLeft(4, '0')}-${date80Tta.month.toString().padLeft(2, '0')}-${date80Tta.day.toString().padLeft(2, '0')}",
    "remark_80tta": remark80Tta,
    "attachment_path_80tta": attachmentPath80Tta,
    "date_other": "${dateOther.year.toString().padLeft(4, '0')}-${dateOther.month.toString().padLeft(2, '0')}-${dateOther.day.toString().padLeft(2, '0')}",
    "remark_other": remarkOther,
    "attachment_path_other": attachmentPathOther,
    "response_status": responseStatus,
    "response_msg": responseMsg,
  };
}
