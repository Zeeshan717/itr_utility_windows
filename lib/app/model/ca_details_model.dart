// To parse this JSON data, do
//
//     final caDetailModel = caDetailModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

CaDetailModel caDetailModelFromJson(String str) => CaDetailModel.fromJson(json.decode(str));

String caDetailModelToJson(CaDetailModel data) => json.encode(data.toJson());

class CaDetailModel {
  CaDetailModel({
    required this.responseCaList,
    required this.responseStatus,
    required this.responseMsg,
  });

  List<ResponseCaList> responseCaList;
  int responseStatus;
  String responseMsg;

  factory CaDetailModel.fromJson(Map<String, dynamic> json) => CaDetailModel(
    responseCaList: List<ResponseCaList>.from(json["response_ca_list"].map((x) => ResponseCaList.fromJson(x))),
    responseStatus: json["response_status"],
    responseMsg: json["response_msg"],
  );

  Map<String, dynamic> toJson() => {
    "response_ca_list": List<dynamic>.from(responseCaList.map((x) => x.toJson())),
    "response_status": responseStatus,
    "response_msg": responseMsg,
  };
}

class ResponseCaList {
  ResponseCaList({
    required this.caId,
    required this.caFirmName,
    required this.caName,
    required this.designation,
    required this.mno,
    required this.frnno,
    required this.panCard,
    required this.address,
    required this.city,
    required this.district,
    required this.state,
    required this.pinCode,
    required this.status,
  });

  String caId;
  String caFirmName;
  String caName;
  String designation;
  String mno;
  String frnno;
  String panCard;
  String address;
  String city;
  String district;
  String state;
  String pinCode;
  String status;

  factory ResponseCaList.fromJson(Map<String, dynamic> json) => ResponseCaList(
    caId: json["ca_id"],
    caFirmName: json["ca_firm_name"],
    caName: json["ca_name"],
    designation: json["designation"],
    mno: json["mno"],
    frnno: json["frnno"],
    panCard: json["pan_card"],
    address: json["address"],
    city: json["city"],
    district: json["district"],
    state: json["state"],
    pinCode: json["pin_code"],
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "ca_id": caId,
    "ca_firm_name": caFirmName,
    "ca_name": caName,
    "designation": designation,
    "mno": mno,
    "frnno": frnno,
    "pan_card": panCard,
    "address": address,
    "city": city,
    "district": district,
    "state": state,
    "pin_code": pinCode,
    "status": status,
  };
}
