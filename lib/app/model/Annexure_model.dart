class CRModel {
  String head;
  String amount;

  CRModel({required this.head, required this.amount});
}

class DRModel {
  String drname;
  String dramount;

  DRModel({required this.drname, required this.dramount});
}

class LiabilityModel {
  String lname;
  String lamount;
  String key;
  LiabilityModel(this.key, this.lname, this.lamount);
}

class MoreAssetsModel {
  List<AssetModel> assetHead;
  String total;
  String key;
  MoreAssetsModel(this.key, this.assetHead, this.total);
}

class AssetModel {
  String assetname;
  String assetamount;
  String key;
  AssetModel(this.key, this.assetname, this.assetamount);
}

class MoreLiabilitiesModel {
  List<LiabilityModel> liabilityhead;
  String total;
  String key;
  MoreLiabilitiesModel(this.key, this.liabilityhead, this.total);
}

class FixedAssetModel {
  String name;
  String amount;
  String key;
  FixedAssetModel(this.key, this.name, this.amount);
}

class SundryDebtorModel {
  String name;
  String amount;
  String key;
  SundryDebtorModel(this.key, this.name, this.amount);
}

class CashAndBankBalanceModel {
  String name;
  String amount;
  String key;
  CashAndBankBalanceModel(this.key, this.name, this.amount);
}

class LoansAndAdvancesModel {
  String name;
  String amount;
  String key;
  LoansAndAdvancesModel(this.key, this.name, this.amount);
}

class ProvisionsModel {
  String name;
  String amount;
  String key;
  ProvisionsModel(this.key, this.name, this.amount);
}

class DutiesAndTaxesModel {
  String name;
  String amount;
  String key;
  DutiesAndTaxesModel(this.key, this.name, this.amount);
}

class ClosingStockModel {
  String name;
  String amount;
  String key;
  ClosingStockModel(this.key, this.name, this.amount);
}
