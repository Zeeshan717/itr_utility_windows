// To parse this JSON data, do
//
//     final annexureModel = annexureModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

AnnexureModel annexureModelFromJson(String str) => AnnexureModel.fromJson(json.decode(str));

String annexureModelToJson(AnnexureModel data) => json.encode(data.toJson());

class AnnexureModel {

  AnnexureModel({
    required this.id,
    required this.financialYear,
    required this.distributorId,
    required this.insertedBy,
    required this.annexureJson,
    required this.insertedAt,
    required this.responseStatus,
    required this.responseMsg,
  });

  String id;
  String financialYear;
  String distributorId;
  String insertedBy;
  AnnexureJson annexureJson;
  String insertedAt;
  int responseStatus;
  String responseMsg;

  factory AnnexureModel.fromJson(Map<String, dynamic> json) => AnnexureModel(
        id: json["id"]??"",
        financialYear: json["financial_year"]??"",
        distributorId: json["distributor_id"]??"",
        insertedBy: json["inserted_by"]??"",
        annexureJson: AnnexureJson.fromJson(json["annexure_json"]),
        insertedAt: json["inserted_at"]??"",
        responseStatus: json["response_status"]??"",
        responseMsg: json["response_msg"]??"",
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "financial_year": financialYear,
        "distributor_id": distributorId,
        "inserted_by": insertedBy,
        "annexure_json": annexureJson.toJson(),
        "inserted_at": insertedAt,
        "response_status": responseStatus,
        "response_msg": responseMsg,
      };
}

class AnnexureJson {
  AnnexureJson({
    required this.liabilities,
    required this.assets,
  });

  Liabilities liabilities;
  Assets assets;

  factory AnnexureJson.fromJson(Map<String, dynamic> json) => AnnexureJson(
        liabilities: Liabilities.fromJson(json["liabilities"]),
        assets: Assets.fromJson(json["Assets"]),
      );

  Map<String, dynamic> toJson() => {
        "liabilities": liabilities.toJson(),
        "Assets": assets.toJson(),
      };
}

class Assets {
  Assets({
    required this.fixedAssets,
    required this.sundryDebtors,
    required this.closingStock,
    required this.cashAndBank,
    required this.loansAndAndvaces,
    required this.moreAssets,
  });

  FixedAssets fixedAssets;
  SundryDebtors sundryDebtors;
  ClosingStock closingStock;
  CashAndBank cashAndBank;
  LoansAndAndvaces loansAndAndvaces;
  List<More> moreAssets;

  factory Assets.fromJson(Map<String, dynamic> json) => Assets(
        fixedAssets: FixedAssets.fromJson(json["fixed_assets"]),
        sundryDebtors: SundryDebtors.fromJson(json["sundry_debtors"]),
        closingStock: ClosingStock.fromJson(json["closing_stock"]),
        cashAndBank: CashAndBank.fromJson(json["cash_and_bank"]),
        loansAndAndvaces: LoansAndAndvaces.fromJson(json["loans_and_andvaces"]),
        moreAssets: List<More>.from(json["more_assets"].map((x) => More.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "fixed_assets": fixedAssets.toJson(),
        "sundry_debtors": sundryDebtors.toJson(),
        "closing_stock": closingStock.toJson(),
        "cash_and_bank": cashAndBank.toJson(),
        "loans_and_andvaces": loansAndAndvaces.toJson(),
        "more_assets": List<dynamic>.from(moreAssets.map((x) => x.toJson())),
      };
}

class CashAndBank {
  CashAndBank({
    required this.total,
    required this.sNo,
    required this.cashList,
  });

  String total;
  String sNo;
  List<List<String>> cashList;

  factory CashAndBank.fromJson(Map<String, dynamic> json) => CashAndBank(
        total: json["total"],
        sNo: json["s_no"],
        cashList: List<List<String>>.from(json["cash_list"].map((x) => List<String>.from(x.map((x) => x)))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "s_no": sNo,
        "cash_list": List<dynamic>.from(cashList.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}

class ClosingStock {
  ClosingStock({
    required this.total,
    required this.sNo,
    required this.closingStockList,
  });

  String total;
  String sNo;
  List<List<String>> closingStockList;

  factory ClosingStock.fromJson(Map<String, dynamic> json) => ClosingStock(
        total: json["total"],
        sNo: json["s_no"],
        closingStockList: List<List<String>>.from(json["closing_stock_list"].map((x) => List<String>.from(x.map((x) => x)))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "s_no": sNo,
        "closing_stock_list": List<dynamic>.from(closingStockList.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}

class FixedAssets {
  FixedAssets({
    required this.total,
    required this.sNo,
    required this.fixedAssetList,
  });

  String total;
  String sNo;
  List<List<String>> fixedAssetList;

  factory FixedAssets.fromJson(Map<String, dynamic> json) => FixedAssets(
        total: json["total"],
        sNo: json["s_no"],
        fixedAssetList: List<List<String>>.from(json["fixed_asset_list"].map((x) => List<String>.from(x.map((x) => x)))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "s_no": sNo,
        "fixed_asset_list": List<dynamic>.from(fixedAssetList.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}

class LoansAndAndvaces {
  LoansAndAndvaces({
    required this.total,
    required this.sNo,
    required this.loansList,
  });

  String total;
  String sNo;
  List<List<String>> loansList;

  factory LoansAndAndvaces.fromJson(Map<String, dynamic> json) => LoansAndAndvaces(
        total: json["total"],
        sNo: json["s_no"],
        loansList: List<List<String>>.from(json["loans_list"].map((x) => List<String>.from(x.map((x) => x)))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "s_no": sNo,
        "loans_list": List<dynamic>.from(loansList.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}

class More {
  More({
    required this.headName,
    required this.total,
    required this.sNo,
    required this.list,
  });

  String headName;
  String total;
  String sNo;
  List<List<String>> list;

  factory More.fromJson(Map<String, dynamic> json) => More(
        headName: json["head_name"],
        total: json["total"],
        sNo: json["s_no"],
        list: List<List<String>>.from(json["list"].map((x) => List<String>.from(x.map((x) => x)))),
      );

  Map<String, dynamic> toJson() => {
        "head_name": headName,
        "total": total,
        "s_no": sNo,
        "list": List<dynamic>.from(list.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}

class SundryDebtors {
  SundryDebtors({
    required this.total,
    required this.sNo,
    required this.debtorsList,
  });

  String total;
  String sNo;
  List<List<String>> debtorsList;

  factory SundryDebtors.fromJson(Map<String, dynamic> json) => SundryDebtors(
        total: json["total"],
        sNo: json["s_no"],
        debtorsList: List<List<String>>.from(json["debtors_list"].map((x) => List<String>.from(x.map((x) => x)))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "s_no": sNo,
        "debtors_list": List<dynamic>.from(debtorsList.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}

class Liabilities {
  Liabilities({
    required this.capitalAC,
    required this.provision,
    required this.dutiesTaxes,
    required this.moreLiablites,
  });

  CapitalAC capitalAC;
  Provision provision;
  DutiesTaxes dutiesTaxes;
  List<More> moreLiablites;

  factory Liabilities.fromJson(Map<String, dynamic> json) => Liabilities(
        capitalAC: CapitalAC.fromJson(json["capital_a/c"]),
        provision: Provision.fromJson(json["provision"]),
        dutiesTaxes: DutiesTaxes.fromJson(json["duties&taxes"]),
        moreLiablites: List<More>.from(json["more_liablites"].map((x) => More.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "capital_a/c": capitalAC.toJson(),
        "provision": provision.toJson(),
        "duties&taxes": dutiesTaxes.toJson(),
        "more_liablites": List<dynamic>.from(moreLiablites.map((x) => x.toJson())),
      };
}

class CapitalAC {
  CapitalAC({
    required this.sNo,
    required this.openingBalance,
    required this.interest,
    required this.tdsFy,
    required this.netProfit,
    required this.moreCrList,
    required this.totalCr,
    required this.tds,
    required this.moreDrList,
    required this.closingBal,
  });

  String sNo;
  String openingBalance;
  String interest;
  String tdsFy;
  String netProfit;
  List<List<String>> moreCrList;
  String totalCr;
  String tds;
  List<List<String>> moreDrList;
  String closingBal;

  factory CapitalAC.fromJson(Map<String, dynamic> json) => CapitalAC(
        sNo: json["s_no"],
        openingBalance: json["opening_balance"],
        interest: json["interest"],
        tdsFy: json["tds_fy"],
        netProfit: json["net_profit"],
        moreCrList: List<List<String>>.from(json["more_cr_list"].map((x) => List<String>.from(x.map((x) => x)))),
        totalCr: json["total_cr"],
        tds: json["tds"],
        moreDrList: List<List<String>>.from(json["more_dr_list"].map((x) => List<String>.from(x.map((x) => x)))),
        closingBal: json["closing_bal"],
      );

  Map<String, dynamic> toJson() => {
        "s_no": sNo,
        "opening_balance": openingBalance,
        "interest": interest,
        "tds_fy": tdsFy,
        "net_profit": netProfit,
        "more_cr_list": List<dynamic>.from(moreCrList.map((x) => List<dynamic>.from(x.map((x) => x)))),
        "total_cr": totalCr,
        "tds": tds,
        "more_dr_list": List<dynamic>.from(moreDrList.map((x) => List<dynamic>.from(x.map((x) => x)))),
        "closing_bal": closingBal,
      };
}

class DutiesTaxes {
  DutiesTaxes({
    required this.total,
    required this.sNo,
    required this.dutyList,
  });

  String total;
  String sNo;
  List<List<String>> dutyList;

  factory DutiesTaxes.fromJson(Map<String, dynamic> json) => DutiesTaxes(
        total: json["total"],
        sNo: json["s_no"],
        dutyList: List<List<String>>.from(json["duty_list"].map((x) => List<String>.from(x.map((x) => x)))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "s_no": sNo,
        "duty_list": List<dynamic>.from(dutyList.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}

class Provision {
  Provision({
    required this.total,
    required this.sNo,
    required this.provList,
  });

  String total;
  String sNo;
  List<List<String>> provList;

  factory Provision.fromJson(Map<String, dynamic> json) => Provision(
        total: json["total"],
        sNo: json["s_no"],
        provList: List<List<String>>.from(json["prov_list"].map((x) => List<String>.from(x.map((x) => x)))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "s_no": sNo,
        "prov_list": List<dynamic>.from(provList.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}
