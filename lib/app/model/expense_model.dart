// To parse this JSON data, do
//
//     final expenseModel = expenseModelFromJson(jsonString);

import 'dart:convert';

ExpenseModel expenseModelFromJson(String str) => ExpenseModel.fromJson(json.decode(str));

String expenseModelToJson(ExpenseModel data) => json.encode(data.toJson());

class ExpenseModel {
  ExpenseModel({
    required this.responseExpenseList,
    required this.responseStatus,
    required this.responseMsg,
  });

  List<ResponseExpenseList> responseExpenseList;
  int responseStatus;
  String responseMsg;

  factory ExpenseModel.fromJson(Map<String, dynamic> json) {
    try{
      return ExpenseModel(
        responseExpenseList: List<ResponseExpenseList>.from(json["response_expense_list"].map((x) => ResponseExpenseList.fromJson(x))),
        responseStatus: json["response_status"],
        responseMsg: json["response_msg"],
      );
    }catch(e){
      return ExpenseModel(
        responseExpenseList: [],
        responseStatus: json["response_status"],
        responseMsg: json["response_msg"],
      );
    }

  }


  Map<String, dynamic> toJson() => {
        "response_expense_list": List<dynamic>.from(responseExpenseList.map((x) => x.toJson())),
        "response_status": responseStatus,
        "response_msg": responseMsg,
      };

  @override
  String toString() {
    return 'ExpenseModel{responseExpenseList: $responseExpenseList, responseStatus: $responseStatus, responseMsg: $responseMsg}';
  }
}

class ResponseExpenseList {
  ResponseExpenseList({
     this.expenseId,
     this.financialYear,
     this.name,
     this.date,
     this.baseUrl,
     this.attachment,
     this.amount,
     this.remark,
  });

  String? expenseId;
  String? financialYear;
  String? name;
  DateTime? date;
  String? baseUrl;
  String? attachment;
  String? amount;
  String? remark;

  factory ResponseExpenseList.fromJson(Map<String, dynamic> json) => ResponseExpenseList(
        expenseId: json["expense_id"],
        financialYear: json["financial_year"],
        name: json["name"],
        date: DateTime.parse(json["date"]),
        baseUrl: json["base_url"],
        attachment: json["attachment"],
        amount: json["amount"],
        remark: json["remark"],
      );

  Map<String, dynamic> toJson() => {
        "expense_id": expenseId,
        "financial_year": financialYear,
        "name": name,
        "date": "${date!.year.toString().padLeft(4, '0')}-${date!.month.toString().padLeft(2, '0')}-${date!.day.toString().padLeft(2, '0')}",
        "base_url": baseUrl,
        "attachment": attachment,
        "amount": amount,
        "remark": remark,
      };
}
