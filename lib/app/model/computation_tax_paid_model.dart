class ComputationTaxPaidModel {
  String tpName;
  String tpTan;
  String tpToi;
  String tpDy;
  String tpBrought;
  String tpTds;
  String tpIncomefw;
  String tpBalance;
  String taxkey;
  ComputationTaxPaidModel(this.taxkey,this.tpName, this.tpTan, this.tpToi,this.tpDy,this.tpBrought,this.tpTds,this.tpIncomefw,this.tpBalance);
}