class DepreciationModel {
  String assetName = '';
  String depRate = '';
  String openingAmount = '';
  String purchaseAmountMore180Days = '';
  String purchaseAmountLess180Days = '';
  String deduction = '';
  String total = '';
  String depreciation = '';
  String closingBalance = '';
  String clientId = '';

  DepreciationModel(this.assetName, this.depRate, this.openingAmount, this.purchaseAmountMore180Days, this.purchaseAmountLess180Days, this.deduction,
      this.total, this.depreciation, this.closingBalance, this.clientId);
}
