// To parse this JSON data, do
//
//     final investmentModel = investmentModelFromJson(jsonString);

import 'dart:convert';

InvestmentModel investmentModelFromJson(String str) => InvestmentModel.fromJson(json.decode(str));

String investmentModelToJson(InvestmentModel data) => json.encode(data.toJson());

class InvestmentModel {
    InvestmentModel({
        required this.responseInvestmentList,
        required this.responseStatus,
        required this.responseMsg,
    });

    List<ResponseInvestmentList> responseInvestmentList;
    int responseStatus;
    String responseMsg;

    factory InvestmentModel.fromJson(Map<String, dynamic> json) {
        try{
           return  InvestmentModel(
                responseInvestmentList: List<ResponseInvestmentList>.from(json["response_investment_list"].map((x) => ResponseInvestmentList.fromJson(x))),
                responseStatus: json["response_status"],
                responseMsg: json["response_msg"],
            );
        }catch(e){
           return InvestmentModel(
                responseInvestmentList:[],
                responseStatus: json["response_status"],
                responseMsg: json["response_msg"],
            );
        }
    }


    Map<String, dynamic> toJson() => {
        "response_investment_list": List<dynamic>.from(responseInvestmentList.map((x) => x.toJson())),
        "response_status": responseStatus,
        "response_msg": responseMsg,
    };
}

class ResponseInvestmentList {
    ResponseInvestmentList({
        required this.investmentId,
        required this.financialYear,
        required this.investmentType,
        required this.name,
        required this.date,
        required this.baseUrl,
        required this.attachment,
        required this.amount,
        required this.remark,
    });

    String investmentId;
    String financialYear;
    String investmentType;
    String name;
    DateTime date;
    String baseUrl;
    String attachment;
    String amount;
    String remark;

    factory ResponseInvestmentList.fromJson(Map<String, dynamic> json) => ResponseInvestmentList(
        investmentId: json["investment_id"],
        financialYear: json["financial_year"],
        investmentType: json["investment_type"],
        name: json["name"],
        date: DateTime.parse(json["date"]),
        baseUrl: json["base_url"],
        attachment: json["attachment"],
        amount: json["amount"],
        remark: json["remark"],
    );

    Map<String, dynamic> toJson() => {
        "investment_id": investmentId,
        "financial_year": financialYear,
        "investment_type": investmentType,
        "name": name,
        "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "base_url": baseUrl,
        "attachment": attachment,
        "amount": amount,
        "remark": remark,
    };
}
