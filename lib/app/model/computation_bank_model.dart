class ComputationBankModel {
  String cbKey;
  String cbName;
  String cbifsc;
  String cbaccount;

  ComputationBankModel(this.cbKey, this.cbName, this.cbifsc, this.cbaccount);
}
