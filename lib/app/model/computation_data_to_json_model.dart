// To parse this JSON data, do
//
//     final computationDataToJson = computationDataToJsonFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

class ComputationDataToJson {
  ComputationDataToJson({
    required this.type,
    required this.address,
    required this.contact,
    required this.email,
    required this.pan,
    required this.father,
    required this.account,
    required this.fileStatus,
    required this.cstatus,
    required this.gender,
    required this.ifsc,
    required this.aadhar,
    required this.gtotal,
    required this.rBefore,
    required this.rAfter,
    required this.salary,
    required this.houseIncome,
    required this.businessProfession,
    required this.capitalGain,
    required this.otherIncome,
    required this.totalDeduction,
    required this.incomeTax,
    required this.tds,
    required this.tdsTotal,
    required this.bank,
  });

  String type;
  String address;
  String contact;
  String email;
  String pan;
  String father;
  String account;
  String fileStatus;
  String cstatus;
  String gender;
  String ifsc;
  String aadhar;
  String gtotal;
  String rBefore;
  String rAfter;
  IncomeType salary;
  IncomeType houseIncome;
  IncomeType businessProfession;
  IncomeType capitalGain;
  IncomeType otherIncome;
  IncomeType totalDeduction;
  List<String> incomeTax;
  List<List<String>> tds;
  String tdsTotal;
  List<List<String>> bank;

  Map<String, dynamic> toJson() => {
        "type": type,
        "address": address,
        "contact": contact,
        "email": email,
        "pan": pan,
        "father": father,
        "account": account,
        "file_status": fileStatus,
        "cstatus": cstatus,
        "gender": gender,
        "ifsc": ifsc,
        "aadhar": aadhar,
        "gtotal": gtotal,
        "r_before": rBefore,
        "r_after": rAfter,
        "salary": salary.toJson(),
        "house_income": houseIncome.toJson(),
        "business_profession": businessProfession.toJson(),
        "capital_gain": capitalGain.toJson(),
        "other_income": otherIncome.toJson(),
        "total_deduction": totalDeduction.toJson(),
        "income_tax": List<dynamic>.from(incomeTax.map((x) => x)),
        "tds": List<dynamic>.from(tds.map((x) => List<dynamic>.from(x.map((x) => x)))),
        "tds_total": tdsTotal,
        "bank": List<dynamic>.from(bank.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}

class IncomeType {
  IncomeType({
    required this.total,
    required this.list,
  });

  String total;
  List<List<String>> list;

  Map<String, dynamic> toJson() => {
        "total": total,
        "list": List<dynamic>.from(list.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}
