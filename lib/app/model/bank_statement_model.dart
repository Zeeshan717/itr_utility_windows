// To parse this JSON data, do
//
//     final bankStatementModel = bankStatementModelFromJson(jsonString);

import 'dart:convert';

BankStatementModel bankStatementModelFromJson(String str) => BankStatementModel.fromJson(json.decode(str));

String bankStatementModelToJson(BankStatementModel data) => json.encode(data.toJson());

class BankStatementModel {
  BankStatementModel({
    required this.responseBankList,
    required this.responseStatus,
    required this.responseMsg,
  });

  List<ResponseBankList> responseBankList;
  int responseStatus;
  String responseMsg;

  factory BankStatementModel.fromJson(Map<String, dynamic> json) {
    try{
     return BankStatementModel(
        responseBankList: List<ResponseBankList>.from(json["response_bank_list"].map((x) => ResponseBankList.fromJson(x))),
        responseStatus: json["response_status"],
        responseMsg: json["response_msg"],
      );
    }catch (e) {
     return BankStatementModel(
        responseBankList: [],
        responseStatus: json["response_status"],
        responseMsg: json["response_msg"],
      );
    }
  }

  Map<String, dynamic> toJson() => {
    "response_bank_list": List<dynamic>.from(responseBankList.map((x) => x.toJson())),
    "response_status": responseStatus,
    "response_msg": responseMsg,
  };
}

class ResponseBankList {
  ResponseBankList({
    required this.bankId,
    required this.bankStatus,
    required this.bankName,
    required this.accountNo,
    required this.accountType,
    required this.fromDate,
    required this.toDate,
    required this.financialYear,
    required this.baseUrl,
    required this.bankStatement,
    required this.interestPaid,
    required this.interestReceived,
    required this.charges,
    required this.other,
  });

  String bankId;
  String bankStatus;
  String bankName;
  String accountNo;
  String accountType;
  DateTime fromDate;
  DateTime toDate;
  String financialYear;
  String baseUrl;
  String bankStatement;
  String interestPaid;
  String interestReceived;
  String charges;
  String other;

  factory ResponseBankList.fromJson(Map<String, dynamic> json) => ResponseBankList(
    bankId: json["bank_id"],
    bankStatus: json["bank_status"],
    bankName: json["bank_name"],
    accountNo: json["account_no"],
    accountType: json["account_type"],
    fromDate: DateTime.parse(json["from_date"]),
    toDate: DateTime.parse(json["to_date"]),
    financialYear: json["financial_year"],
    baseUrl: json["base_url"],
    bankStatement: json["bank_statement"],
    interestPaid: json["interest_paid"],
    interestReceived: json["interest_received"],
    charges: json["charges"],
    other: json["other"],
  );

  Map<String, dynamic> toJson() => {
    "bank_id": bankId,
    "bank_status": bankStatus,
    "bank_name": bankName,
    "account_no": accountNo,
    "account_type": accountType,
    "from_date": "${fromDate.year.toString().padLeft(4, '0')}-${fromDate.month.toString().padLeft(2, '0')}-${fromDate.day.toString().padLeft(2, '0')}",
    "to_date": "${toDate.year.toString().padLeft(4, '0')}-${toDate.month.toString().padLeft(2, '0')}-${toDate.day.toString().padLeft(2, '0')}",
    "financial_year": financialYear,
    "base_url": baseUrl,
    "bank_statement": bankStatement,
    "interest_paid": interestPaid,
    "interest_received": interestReceived,
    "charges": charges,
    "other": other,
  };
}
