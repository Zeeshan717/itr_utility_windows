// To parse this JSON data, do
//
//     final depreciationDetailModel = depreciationDetailModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

DepreciationDetailModel depreciationDetailModelFromJson(String str) => DepreciationDetailModel.fromJson(json.decode(str));

String depreciationDetailModelToJson(DepreciationDetailModel data) => json.encode(data.toJson());

class DepreciationDetailModel {
  DepreciationDetailModel({
    required this.responseDepreciationList,
    required this.responseStatus,
    required this.responseMsg,
  });

  List<ResponseDepreciationList> responseDepreciationList;
  int responseStatus;
  String responseMsg;

  factory DepreciationDetailModel.fromJson(Map<String, dynamic> json) => DepreciationDetailModel(
        responseDepreciationList:
            List<ResponseDepreciationList>.from(json["response_depreciation_list"].map((x) => ResponseDepreciationList.fromJson(x))),
        responseStatus: json["response_status"],
        responseMsg: json["response_msg"],
      );

  Map<String, dynamic> toJson() => {
        "response_depreciation_list": List<dynamic>.from(responseDepreciationList.map((x) => x.toJson())),
        "response_status": responseStatus,
        "response_msg": responseMsg,
      };
}

class ResponseDepreciationList {
  ResponseDepreciationList({
    required this.depreciationId,
    required this.headCategory,
    required this.assetName,
    required this.openingBalance,
    required this.purchaseDate,
    required this.purchaseAmount,
    required this.saleDate,
    required this.saleAmount,
    required this.depreciationRate,
    required this.totalAssets,
    required this.depreciationAmount,
    required this.closingBalance,
    required this.insertedAt,
  });

  String depreciationId;
  String headCategory;
  String assetName;
  String openingBalance;
  String purchaseDate;
  String purchaseAmount;
  String saleDate;
  String saleAmount;
  String depreciationRate;
  String totalAssets;
  String depreciationAmount;
  String closingBalance;
  String insertedAt;

  factory ResponseDepreciationList.fromJson(Map<String, dynamic> json) => ResponseDepreciationList(
        depreciationId: json["depreciation_id"],
        headCategory: json["head_category"],
        assetName: json["asset_name"],
        openingBalance: json["opening_balance"],
        purchaseDate: json["purchase_date"],
        purchaseAmount: json["purchase_amount"],
        saleDate: json["sale_date"],
        saleAmount: json["sale_amount"],
        depreciationRate: json["depreciation_rate"],
        totalAssets: json["total_assets"],
        depreciationAmount: json["depreciation_amount"],
        closingBalance: json["closing_balance"],
        insertedAt: json["inserted_at"],
      );

  Map<String, dynamic> toJson() => {
        "depreciation_id": depreciationId,
        "head_category": headCategory,
        "asset_name": assetName,
        "opening_balance": openingBalance,
        "purchase_date": purchaseDate,
        "purchase_amount": purchaseAmount,
        "sale_date": saleDate,
        "sale_amount": saleAmount,
        "depreciation_rate": depreciationRate,
        "total_assets": totalAssets,
        "depreciation_amount": depreciationAmount,
        "closing_balance": closingBalance,
        "inserted_at": insertedAt,
      };
}
