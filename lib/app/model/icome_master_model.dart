// To parse this JSON data, do
//
//     final incomeMasterModel = incomeMasterModelFromJson(jsonString);

import 'dart:convert';

IncomeMasterModel incomeMasterModelFromJson(String str) => IncomeMasterModel.fromJson(json.decode(str));

String incomeMasterModelToJson(IncomeMasterModel data) => json.encode(data.toJson());

class IncomeMasterModel {
  IncomeMasterModel({
    required this.incomeId,
    required this.salary,
    required this.hp,
    required this.capital,
    required this.bAndP,
    required this.other,
    required this.agriculture,
    required this.financialYear,
    required this.baseUrl,
    required this.detailedResponse,
    required this.responseStatus,
    required this.responseMsg,
  });

  String incomeId;
  String salary;
  String hp;
  String capital;
  String bAndP;
  String other;
  String agriculture;
  String financialYear;
  String baseUrl;
  List<DetailedResponse> detailedResponse;
  int responseStatus;
  String responseMsg;

  factory IncomeMasterModel.fromJson(Map<String, dynamic> json) => IncomeMasterModel(
    incomeId: json["income_id"],
    salary: json["salary"],
    hp: json["hp"],
    capital: json["capital"],
    bAndP: json["b_and_p"],
    other: json["other"],
    agriculture: json["agriculture"],
    financialYear: json["financial_year"],
    baseUrl: json["base_url"],
    detailedResponse: List<DetailedResponse>.from(json["detailed_response"].map((x) => DetailedResponse.fromJson(x))),
    responseStatus: json["response_status"],
    responseMsg: json["response_msg"],
  );

  Map<String, dynamic> toJson() => {
    "income_id": incomeId,
    "salary": salary,
    "hp": hp,
    "capital": capital,
    "b_and_p": bAndP,
    "other": other,
    "agriculture": agriculture,
    "financial_year": financialYear,
    "base_url": baseUrl,
    "detailed_response": List<dynamic>.from(detailedResponse.map((x) => x.toJson())),
    "response_status": responseStatus,
    "response_msg": responseMsg,
  };
}

class DetailedResponse {
  DetailedResponse({
    required this.detailId,
    required this.incomeHead,
    required this.documentPath,
    required this.amount,
    required this.remark,
    required this.insertedBy,
  });

  String detailId;
  String incomeHead;
  String documentPath;
  String amount;
  String remark;
  String insertedBy;

  factory DetailedResponse.fromJson(Map<String, dynamic> json) => DetailedResponse(
    detailId: json["detail_id"],
    incomeHead: json["income_head"],
    documentPath: json["document_path"],
    amount: json["amount"],
    remark: json["remark"],
    insertedBy: json["inserted_by"],
  );

  Map<String, dynamic> toJson() => {
    "detail_id": detailId,
    "income_head": incomeHead,
    "document_path": documentPath,
    "amount": amount,
    "remark": remark,
    "inserted_by": insertedBy,
  };

}
