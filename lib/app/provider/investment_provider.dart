import 'package:flutter/cupertino.dart';
import 'package:get_storage/get_storage.dart';

import '../model/investment_model.dart';
import '../services/api.dart';

class InvestmentProvider with ChangeNotifier{
  Future<InvestmentModel>? investmentModel;
  GetStorage storage  = GetStorage();

  void updateValue(){
      investmentModel =  Api.getInvestmentApi(storage.read("client_id").toString(),
        storage.read("finacial_year").toString(), storage.read("admin_id").toString());
      notifyListeners();
  }
}