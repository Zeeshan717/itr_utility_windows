import 'package:accountx/app/services/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:get_storage/get_storage.dart';

import '../model/expense_model.dart';

class UpdateProvider with ChangeNotifier{
  Future<ExpenseModel>? getExpenseApiResponse;
  GetStorage storage = GetStorage();
  void updateValue(){
    getExpenseApiResponse =  Api.getExpenseApi(
        storage.read("client_id").toString(),
        storage.read("finacial_year").toString(),
        storage.read("admin_id").toString());
    notifyListeners();
  }
}