import 'package:flutter/cupertino.dart';
import 'package:get_storage/get_storage.dart';

import '../model/ppf_account_model.dart';
import '../model/ppf_account_model.dart';
import '../services/api.dart';

class PpfProvider with ChangeNotifier {
  Future<PpfAccountModel>? investmentModel;
  GetStorage box = GetStorage();

  void updateValue() {
    investmentModel = Api.getPPFAccount(box.read("client_id").toString(),
        box.read("admin_id").toString(), box.read("finacial_year").toString());
    notifyListeners();
  }
}
