

import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

import '../model/bank_statement_model.dart';
import '../services/api.dart';

class BankStatementProvider with ChangeNotifier{
  Future<BankStatementModel>? bankStatementModel;
  GetStorage box = GetStorage();

 void updateValue() {
       bankStatementModel  =  Api.getBankStatementApi(box.read("client_id").toString(),
       box.read("loginValue")["admin_id"].toString(),
       box.read("finacial_year").toString());
       notifyListeners();
 }

}