// ignore_for_file: file_names

import 'package:accountx/app/modules/homePage/homeController.dart';
import 'package:get/instance_manager.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<HomeController>(HomeController());
  }
}
