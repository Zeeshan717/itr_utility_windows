// ignore_for_file: file_names, must_be_immutable, avoid_print, sized_box_for_whitespace, avoid_function_literals_in_foreach_calls

import 'dart:io';
import 'dart:io';

import 'package:accountx/app/model/ca_details_model.dart';
import 'package:accountx/app/model/user_client_model.dart';
import 'package:accountx/app/modules/bank_statement_page/bank_statement_view.dart';
import 'package:accountx/app/modules/calculators_page/calculators_controller.dart';
import 'package:accountx/app/modules/calculators_page/calculators_view.dart';
import 'package:accountx/app/modules/deduction_page/deduction_view.dart';
import 'package:accountx/app/modules/expenses_page/expenses_view.dart';
import 'package:accountx/app/modules/financial_page/annexure_page/annexure_view.dart';
import 'package:accountx/app/modules/financial_page/balance_sheet_page/balance_sheet_view.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_view.dart';
import 'package:accountx/app/modules/financial_page/computation_page/total_computation_view.dart';
import 'package:accountx/app/modules/financial_page/financial_view.dart';
import 'package:accountx/app/modules/financial_page/profit_and_loss_page/profit_and_loss_view.dart';
import 'package:accountx/app/modules/incomeMasterPage/incomeMasterView.dart';
import 'package:accountx/app/modules/investment_page/investment_view.dart';
import 'package:accountx/app/modules/ppf_account_page/ppf_account_view.dart';
import 'package:accountx/app/modules/taxCalPage/taxController.dart';
import 'package:accountx/app/provider/bank_statement_provider.dart';
import 'package:accountx/app/provider/ppf_provider.dart';

import 'package:accountx/app/services/api.dart';
import 'package:accountx/widgets/sidebar.dart';
import 'package:date_format/date_format.dart';
import 'package:desktop_window/desktop_window.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';
import 'package:window_manager/window_manager.dart';

import '../../provider/investment_provider.dart';
import '../../provider/update_provider.dart';
import '../depreciationCalPage/depreciation_view.dart';
import '../expenses_page/expenses_details.dart';
import 'homeController.dart';

class HomeView extends StatefulWidget {

  var controller = Get.put(TaxController());
  var computaionController = Get.put(ComputationController());
  GetStorage storage = GetStorage();

  // var contoller = Get.put(CalculatorsController());
  int index = 0;

  bool isFinancialSelected = false;

  HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> with WindowListener {


  var clientName = "";
  var caName = "";

  List<Widget> screens = [
    CalculatorView(),
    IncomeMaster(),
    BankStatementView(),
    PPFAccount(),
    DeductionView(),
    InvestmentView(),
    ExpensesView(),
    FinancialView(),
  ];

  List<Widget> financialList = [
    ProfitAndLossView(),
    TotalComputationView(),
    ComputationView(),
    AnnexureView(),
    BalanceSheetView(),
  ];

  // const HomeView({ Key? key }) : super(key: key);
  final HomeController _homeController = Get.find();
  DateTime? currentBackPressTime;
  @override
  void initState() {
    WindowManager.instance.addListener(this);
    super.initState();
  }

   List<String> getYearList(){
     int currentYear =DateTime.now().year;
     List<String> year =[];
     for(int i=0;i<5;i++){
       year.add("${currentYear-i}-${currentYear-(i-1)}");
     }
     return year;
   }

  GetStorage box = GetStorage();
  @override
  Widget build(BuildContext context) {

    var loginValue = _homeController.box.read('loginValue');
    String clientId = _homeController.box.read("client_id").toString();
    print("----admin-----" + loginValue['admin_id']);
    print("----type----" + loginValue['type']);
    print("---userRole--------" + loginValue['user_role']);
    print("-----mangaer_detail------" + loginValue['manager_detail']);

    return FutureBuilder<UserClientList>(
      future: Api.UserClientListApi(
          loginValue['admin_id'], loginValue['type'], loginValue['imei_no'], loginValue['user_role'], loginValue['manager_detail']),
      builder: (context, snapshot) {

        if (snapshot.hasData) {
          EasyLoading.dismiss();
          _homeController.clientListForDraft = snapshot.data!.clientArray.map((e) => e.clientId+"-"+e.clientName).toList();
          return WillPopScope(
            onWillPop: () {
              return onWillPop(context);
            },
            child: Scaffold(
              drawer: const SideBar(),
              appBar: AppBar(
                elevation: 0,
                backgroundColor: const Color(0xff0B28BE),
                title: const Text(
                  "Hello ! AccountX",
                  style: TextStyle(fontSize: 16),
                ),
                actions: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          loginValue["admin_name"],
                          style: const TextStyle(fontSize: 15),
                        ),
                        Text(
                          loginValue['user_role'],
                          style: const TextStyle(fontSize: 13),
                        )
                      ],
                    ),
                  )
                ],
              ),
              body: MediaQuery.of(context).size.width<900?(clientName.toString().isEmpty && caName.toString().isEmpty ?
              Container(
                color: Colors.white24,
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text("Please Select Client Name And C.A.",style: TextStyle(
                        fontSize: 18
                    ),)
                  ],
                ),
              ) : Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: !widget.isFinancialSelected ? screens[widget.index] : financialList[widget.index],
              )):
              Row(
                children: [
                  Container(
                    width:  MediaQuery.of(context).size.width-(MediaQuery.of(context).size.width*(60/100)),
                    height: MediaQuery.of(context).size.height,
                    color: const Color(0xff0B28BE),
                    child: Column(
                      children: [
                        Expanded(
                          child: Container(
                            color: Colors.white,
                            /*decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(18),
                                  topRight: Radius.circular(18),
                                )),*/
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            child: SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 24,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      const SizedBox(
                                        width: 20,
                                      ),
                                      Flexible(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            const Text("Select Client Name"),
                                            const SizedBox(
                                              height: 4,
                                            ),
                                            Container(
                                              width: MediaQuery.of(context).size.width,
                                              height: 40,
                                               child: DropdownSearch<String>(
                                                popupProps: const PopupProps.dialog(showSearchBox: true),
                                                items: snapshot.data!.clientArray.map((e) => e.clientName+" -- "+e.clientId+" -- "+e.gstinNo).toList(),
                                                onChanged: (dropDownvalue) {
                                                var  value = dropDownvalue!.split(" -- ")[0];
                                                  _homeController.box.write("finacial_year", "2021-2022");
                                                  snapshot.data!.clientArray.forEach((element) {
                                                    clientName = element.clientName;
                                                    if (element.clientName == value.toString()) {
                                                      _homeController.box.write('client_id', element.clientId);
                                                      _homeController.box.write('client_name', element.clientName);
                                                      _homeController.box.write('place_of_supply', element.placeOfSupply);
                                                      _homeController.box.write('firm_name', element.firmName);
                                                      _homeController.box.write('phone_no', element.phoneNo);
                                                      _homeController.box.write('distributor_address', element.distributorAddress);
                                                      _homeController.selectedClientNameValue = value!;
                                                      print("inside dropDown"+_homeController.selectedClientNameValue.toString());

                                                    }
                                                  });

                                                  print("-------Cleint Id(Home View)------" + _homeController.box.read('client_id').toString());

                                                  print("-------Cleint Name(Home View)------" + _homeController.box.read('client_name').toString());

                                                  print("-------Cleint Phone (Home View) ------" + _homeController.box.read('phone_no').toString());

                                                  print("-------Cleint Address (Home View) ------" +
                                                      _homeController.box.read('distributor_address').toString());
                                                  box.write("deductionvalue", "");
                                                  var a = box.read("deductionvalue");
                                                  box.write("ca_name","");
                                                _homeController.getProfitAndLossApiCall().then((value) => setState((){}));
                                                _homeController.getAnnexureApiCall().then((value) => setState((){}));
                                                _homeController.getComputaionApiCall().then((value) => setState((){}));
                                                 ProfitAndLossView().callback();
                                                IncomeMaster().callBackouter();
                                                ExpenseDetails().callBackOuter();
                                                DeductionView().updateUi();
                                                Provider.of<UpdateProvider>(context,listen: false).updateValue();
                                                Provider.of<InvestmentProvider>(context,listen: false).updateValue();
                                                Provider.of<PpfProvider>(context,listen: false).updateValue();
                                                Provider.of<BankStatementProvider>(context,listen: false).updateValue();
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 16,
                                      ),
                                      /*Flexible(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            // const Text("Select Financial Year"),
                                            // const SizedBox(
                                            //   height: 4,
                                            // ),
                                            // Container(
                                            //   width: MediaQuery.of(context).size.width,
                                            //   height: 40,
                                            //   child: DropdownSearch<String>(
                                            //     enabled: true,
                                            //     // selectedItem: "2021-2022",
                                            //     items: const ["2021-2022", "2022-2023"],
                                            //     onChanged: (value) {
                                            //       _homeController.box
                                            //           .write("finacial_year", value);
                                            //       _homeController
                                            //           .selectedFinancialYear = value!;
                                            //     },
                                            //   ),
                                            // ),
                                            const Text("Select C.A. Name"),
                                            const SizedBox(
                                              height: 4,
                                            ),
                                            FutureBuilder<CaDetailModel>(
                                              future: Api.CaDetailsApi("68"),
                                              builder: (context, snapshot) {
                                                if (snapshot.hasData) {
                                                  return Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    height: 40,
                                                    child: DropdownSearch<String>(
                                                      popupProps: const PopupProps.dialog(showSearchBox: true),
                                                      items: snapshot.data!.responseCaList.map((e) => e.caName).toList(),
                                                      onChanged: (value) {
                                                        snapshot.data!.responseCaList.forEach((element) {
                                                          caName = element.caName;
                                                          if (element.caName == value) {
                                                            showDialog(
                                                                context: context,
                                                                builder: (context) {
                                                                  return AlertDialog(
                                                                    alignment: Alignment.center,
                                                                    backgroundColor: Colors.white,
                                                                    insetPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 120),
                                                                    contentPadding: EdgeInsets.zero,
                                                                    content: Column(
                                                                      children: [
                                                                        Container(
                                                                          width: MediaQuery.of(context).size.width,
                                                                          // height: 400,
                                                                          child: SingleChildScrollView(
                                                                            child: Column(
                                                                              children: [
                                                                                Container(
                                                                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                                                                  width: MediaQuery.of(context).size.width,
                                                                                  color: const Color(0xff17E4),
                                                                                  height: 50,
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                    children: [
                                                                                      const Text(""),
                                                                                      const Text(
                                                                                        "Verify C.A.",
                                                                                        style: TextStyle(fontSize: 16, color: Colors.white),
                                                                                      ),
                                                                                      InkWell(
                                                                                          onTap: () {
                                                                                            Get.back();
                                                                                          },
                                                                                          child: const Icon(
                                                                                            Icons.clear,
                                                                                            color: Colors.white,
                                                                                          ))
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                Padding(
                                                                                  padding: const EdgeInsets.all(8.0),
                                                                                  child: Column(
                                                                                    children: [
                                                                                      //EPF,PF
                                                                                      const SizedBox(
                                                                                        height: 20,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text(
                                                                                            "CA Name: ",
                                                                                            style: TextStyle(fontSize: 13),
                                                                                          ),
                                                                                          Text(element.caName)
                                                                                        ],
                                                                                      ),
                                                                                      //LIC
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Firm Name: ", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.caFirmName)
                                                                                        ],
                                                                                      ),
                                                                                      //National Saving
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Designation: ",
                                                                                              style: TextStyle(fontSize: 13)),
                                                                                          Text(element.designation)
                                                                                        ],
                                                                                      ),
                                                                                      //Tuition Fee for Children
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Phone No:", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.mno)
                                                                                        ],
                                                                                      ),
                                                                                      //Senior Citizen Saving
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Firm No: ", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.frnno)
                                                                                        ],
                                                                                      ),
                                                                                      //Repayment of Principal
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("pan_card: ", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.panCard)
                                                                                        ],
                                                                                      ),
                                                                                      //Sukanya Samridhi Scheme
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Address: ", style: TextStyle(fontSize: 13)),
                                                                                          Container(
                                                                                            width: 150,
                                                                                            child: Text(
                                                                                              element.address +
                                                                                                  "," +
                                                                                                  element.city +
                                                                                                  ", " +
                                                                                                  element.district +
                                                                                                  ", " +
                                                                                                  element.state +
                                                                                                  ", " +
                                                                                                  element.pinCode,
                                                                                              style: TextStyle(fontSize: 13),
                                                                                            ),
                                                                                          )
                                                                                        ],
                                                                                      ),
                                                                                      //Registration Fees
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),

                                                                                      //Attachment
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                Padding(
                                                                                  padding: const EdgeInsets.all(8.0),
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                                                    children: [
                                                                                      Flexible(
                                                                                        child: Container(
                                                                                            width: MediaQuery.of(context).size.width,
                                                                                            height: 40,
                                                                                            child: ElevatedButton(
                                                                                                style: ElevatedButton.styleFrom(
                                                                                                    primary: Color(0xff1739E4)),
                                                                                                onPressed: () {

                                                                                                  box.write("ca_name",element.caName);
                                                                                                  box.write("ca_address",element.address);
                                                                                                  box.write("ca_firm",element.caFirmName);
                                                                                                  box.write("ca_city",element.city);
                                                                                                  Get.back();
                                                                                                },
                                                                                                child: Text("OK"))),
                                                                                      ),
                                                                                      // SizedBox(
                                                                                      //     width: 160,
                                                                                      //     height: 40,
                                                                                      //     child: ElevatedButton(
                                                                                      //         style: ElevatedButton.styleFrom(
                                                                                      //             primary: Color(0xff1739E4)),
                                                                                      //         onPressed: () {},
                                                                                      //         child: Text("Confirm")))
                                                                                    ],
                                                                                  ),
                                                                                )
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  );
                                                                });

                                                            _homeController.box.write("CurrentlySelectedCA", element);
                                                            _homeController.selectedCaNameValue = value!;
                                                          }
                                                        });

                                                        print("---------Selected Ca---------------" +
                                                            _homeController.box.read('CurrentlySelectedCA').toString());
                                                      },
                                                    ),

                                                  );
                                                }
                                                return Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  height: 40,
                                                  child: DropdownSearch<String>(
                                                    popupProps: const PopupProps.dialog(showSearchBox: true),
                                                    items: [],
                                                    onChanged: (value) {

                                                    },
                                                  ),
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 20,
                                      )*/
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                 /*     const SizedBox(
                                        width: 20,
                                      ),
                                      Flexible(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            const Text("Select Client Name"),
                                            const SizedBox(
                                              height: 4,
                                            ),
                                            Container(
                                              width: MediaQuery.of(context).size.width,
                                              height: 40,
                                              child: DropdownSearch<String>(
                                                popupProps: const PopupProps.dialog(showSearchBox: true),
                                                items: snapshot.data!.clientArray.map((e) => e.clientName+"--"+e.clientId+"--"+e.gstinNo).toList(),
                                                onChanged: (value) {
                                                  _homeController.box.write("finacial_year", "2021-2022");
                                                  snapshot.data!.clientArray.forEach((element) {
                                                    clientName = element.clientName;
                                                    if (element.clientName == value.toString()) {
                                                      _homeController.box.write('client_id', element.clientId);
                                                      _homeController.box.write('client_name', element.clientName);
                                                      _homeController.box.write('place_of_supply', element.placeOfSupply);
                                                      _homeController.box.write('firm_name', element.firmName);
                                                      _homeController.box.write('phone_no', element.phoneNo);
                                                      _homeController.box.write('distributor_address', element.distributorAddress);
                                                      _homeController.selectedClientNameValue = value!;

                                                    }
                                                  });

                                                  print("-------Cleint Id(Home View)------" + _homeController.box.read('client_id').toString());

                                                  print("-------Cleint Name(Home View)------" + _homeController.box.read('client_name').toString());

                                                  print("-------Cleint Phone (Home View) ------" + _homeController.box.read('phone_no').toString());

                                                  print("-------Cleint Address (Home View) ------" +
                                                      _homeController.box.read('distributor_address').toString());
                                                  box.write("deductionvalue", "");
                                                  var a = box.read("deductionvalue");

                                                  box.write("ca_name","");
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),*/
                                      const SizedBox(
                                        width: 20,
                                      ),
                                      Flexible(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                          /*  const Text("Select Financial Year"),
                                            const SizedBox(
                                              height: 4,
                                            ),
                                            Container(
                                              width: MediaQuery.of(context).size.width,
                                              height: 40,
                                              child: DropdownSearch<String>(
                                                enabled: true,
                                                // selectedItem: "2021-2022",
                                                items: const ["2021-2022", "2022-2023"],
                                                onChanged: (value) {
                                                  _homeController.box
                                                      .write("finacial_year", value);
                                                  _homeController
                                                      .selectedFinancialYear = value!;
                                                },
                                              ),
                                            ),*/
                                            const Text("Select C.A. Name"),
                                            const SizedBox(
                                              height: 4,
                                            ),
                                            FutureBuilder<CaDetailModel>(
                                              future: Api.CaDetailsApi(
                                              box.read("loginValue")["admin_id"].toString()),
                                              builder: (context, snapshot) {
                                               if (snapshot.hasData) {
                                                  return Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    height: 40,
                                                    child: DropdownSearch<String>(
                                                      popupProps: const PopupProps.menu(),
                                                      items: snapshot.data!.responseCaList.where((element) => element.status == "Active" ).toList().map((e) => e.caName).toList(),
                                                      onChanged: (value) {
                                                        snapshot.data!.responseCaList.forEach((element) {
                                                          caName = element.caName;
                                                          if (element.caName == value) {
                                                            showDialog(
                                                                context: context,
                                                                builder: (context) {
                                                                  return AlertDialog(
                                                                    alignment: Alignment.center,
                                                                    backgroundColor: Colors.white,
                                                                    insetPadding: EdgeInsets.symmetric(horizontal: 400, vertical: 120),
                                                                    contentPadding: EdgeInsets.zero,
                                                                    title: Column(
                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                      children: [
                                                                        Text("C.A. Details"),
                                                                        Divider()
                                                                      ],
                                                                    ),

                                                                    content: Column(
                                                                      children: [
                                                                        Container(
                                                                          //color:Colors.red,
                                                                          width: MediaQuery.of(context).size.width,
                                                                          child: SingleChildScrollView(
                                                                            child: Column(
                                                                              children: [
                                                                                /*  Container(
                                                                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                                                                  width: MediaQuery.of(context).size.width,
                                                                                  color: const Color(0xff17E4),
                                                                                  height: 50,
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                    children: [
                                                                                      const Text(""),
                                                                                      const Text(
                                                                                        "Verify C.A.",
                                                                                        style: TextStyle(fontSize: 16, color: Colors.white),
                                                                                      ),
                                                                                      InkWell(
                                                                                          onTap: () {
                                                                                            Get.back();
                                                                                          },
                                                                                          child: const Icon(
                                                                                            Icons.clear,
                                                                                            color: Colors.white,
                                                                                          ))
                                                                                    ],
                                                                                  ),
                                                                                ),*/
                                                                                Padding(
                                                                                  padding: const EdgeInsets.symmetric(horizontal: 24),
                                                                                  child: Column(
                                                                                    children: [
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text(
                                                                                            "CA Name: ",
                                                                                            style: TextStyle(fontSize: 13),
                                                                                          ),
                                                                                          Text(element.caName)
                                                                                        ],
                                                                                      ),
                                                                                      //LIC
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Firm Name: ", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.caFirmName,)
                                                                                        ],
                                                                                      ),
                                                                                      //National Saving
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Designation: ",
                                                                                              style: TextStyle(fontSize: 13)),
                                                                                          Text(element.designation)
                                                                                        ],
                                                                                      ),
                                                                                      //Tuition Fee for Children
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Phone No:", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.mno)
                                                                                        ],
                                                                                      ),
                                                                                      //Senior Citizen Saving
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Firm No: ", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.frnno)
                                                                                        ],
                                                                                      ),
                                                                                      //Repayment of Principal
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("pan_card: ", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.panCard)
                                                                                        ],
                                                                                      ),
                                                                                      //Sukanya Samridhi Scheme
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Address: ", style: TextStyle(fontSize: 13)),
                                                                                          Container(
                                                                                            width: 150,
                                                                                            child: Text(
                                                                                              element.address +
                                                                                                  "," +
                                                                                                  element.city +
                                                                                                  ", " +
                                                                                                  element.district +
                                                                                                  ", " +
                                                                                                  element.state +
                                                                                                  ", " +
                                                                                                  element.pinCode,
                                                                                              style: TextStyle(fontSize: 13),
                                                                                            ),
                                                                                          )
                                                                                        ],
                                                                                      ),
                                                                                      //Registration Fees
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),

                                                                                      //Attachment
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),

                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                    actions: [
                                                                      Padding(
                                                                        padding: const EdgeInsets.all(8.0),
                                                                        child: Row(
                                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                                          children: [
                                                                            Container(
                                                                                width: 130,
                                                                                // width: MediaQuery.of(context).size.width,
                                                                                height: 40,
                                                                                child: ElevatedButton(
                                                                                    style: ElevatedButton.styleFrom(
                                                                                        primary: Color(0xff1739E4)),
                                                                                    onPressed: () {

                                                                                      Get.back();
                                                                                    },
                                                                                    child: Text("Cancel"))),
                                                                            SizedBox(width: 10,),
                                                                            Container(
                                                                                width: 130,
                                                                                //  width: MediaQuery.of(context).size.width,
                                                                                height: 40,
                                                                                child: ElevatedButton(
                                                                                    style: ElevatedButton.styleFrom(
                                                                                        primary: Color(0xff1739E4)),
                                                                                    onPressed: () {

                                                                                      box.write("ca_name",element.caName);
                                                                                      box.write("ca_address",element.address);
                                                                                      box.write("ca_firm",element.caFirmName);
                                                                                      box.write("ca_city",element.city);
                                                                                      Get.back();
                                                                                    },
                                                                                    child: Text("OK"))),

                                                                            // SizedBox(
                                                                            //     width: 160,
                                                                            //     height: 40,
                                                                            //     child: ElevatedButton(
                                                                            //         style: ElevatedButton.styleFrom(
                                                                            //             primary: Color(0xff1739E4)),
                                                                            //         onPressed: () {},
                                                                            //         child: Text("Confirm")))
                                                                          ],
                                                                        ),
                                                                      )
                                                                    ],
                                                                    actionsPadding: EdgeInsets.zero,
                                                                  );
                                                                });
                                                          /*  showDialog(context: context, builder: (context){
                                                              return SimpleDialog(
                                                                children: [
                                                                Padding(
                                                                  padding: const EdgeInsets.all(20.0),
                                                                  child: Column(
                                                                    children: [
                                                                      Text("Chartered accountant details confirmation"),

                                                                      Row(
                                                                        children: [
                                                                          Icon(Icons.person),
                                                                          SizedBox(
                                                                            width: 16,
                                                                          ),
                                                                          Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                              Text("Name",style: TextStyle(
                                                                                  color: Colors.grey
                                                                              ),),
                                                                              Text("Raghav Kandelwal",style: TextStyle(
                                                                                  fontWeight: FontWeight.bold
                                                                              ),)
                                                                            ],
                                                                          )
                                                                        ],
                                                                      )
                                                                    ],
                                                                  ),
                                                                )
                                                                ],
                                                              );
                                                            });
                                                            _homeController.box.write("CurrentlySelectedCA", element);
                                                            _homeController.selectedCaNameValue = value!;*/
                                                          }
                                                        });

                                                        print("---------Selected Ca---------------" +
                                                            _homeController.box.read('CurrentlySelectedCA').toString());
                                                      },
                                                    ),

                                                  );
                                                }
                                                return Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  height: 40,
                                                  child: DropdownSearch<String>(
                                                    popupProps: const PopupProps.dialog(showSearchBox: true),
                                                    items: [],
                                                    onChanged: (value) {

                                                    },
                                                  ),
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 20,
                                      ),
                                      Flexible(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            const Text("Select Financial Year"),
                                            const SizedBox(
                                              height: 4,
                                            ),
                                            Container(
                                              width: MediaQuery.of(context).size.width,
                                              height: 40,
                                              child: DropdownSearch<String>(
                                               // popupProps: const PopupProps.dialog(showSearchBox: true),
                                                enabled: true,
                                                // selectedItem: "2021-2022",
                                                items: getYearList(),
                                                onChanged: (value) {
                                                  print("--selected financial year------> $value");
                                                  _homeController.box.write("selected_finacila_year",value);
                                                  DepreciationCalculator  depreciationCalculator =  DepreciationCalculator();
                                                  depreciationCalculator.callState();
                                                  ProfitAndLossView().callback();
                                                  setState(() {

                                                  });
                                                 // _homeController.box.write("finacial_year", "2021-2022");
                                                  // _homeController.box
                                                  //     .write("finacial_year", value);
                                                  // _homeController
                                                  //     .selectedFinancialYear = value!;
                                                },
                                              ),
                                            ),
                                          /*  const Text("Select C.A. Name"),
                                            const SizedBox(
                                              height: 4,
                                            ),
                                            FutureBuilder<CaDetailModel>(
                                              future: Api.CaDetailsApi("68"),
                                              builder: (context, snapshot) {
                                                if (snapshot.hasData) {
                                                  return Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    height: 40,
                                                    child: DropdownSearch<String>(
                                                      popupProps: const PopupProps.dialog(showSearchBox: true),
                                                      items: snapshot.data!.responseCaList.map((e) => e.caName).toList(),
                                                      onChanged: (value) {
                                                        snapshot.data!.responseCaList.forEach((element) {
                                                          caName = element.caName;
                                                          if (element.caName == value) {
                                                            showDialog(
                                                                context: context,
                                                                builder: (context) {
                                                                  return AlertDialog(
                                                                    alignment: Alignment.center,
                                                                    backgroundColor: Colors.white,
                                                                    insetPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 120),
                                                                    contentPadding: EdgeInsets.zero,
                                                                    content: Column(
                                                                      children: [
                                                                        Container(
                                                                          width: MediaQuery.of(context).size.width,
                                                                          // height: 400,
                                                                          child: SingleChildScrollView(
                                                                            child: Column(
                                                                              children: [
                                                                                Container(
                                                                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                                                                  width: MediaQuery.of(context).size.width,
                                                                                  color: const Color(0xff17E4),
                                                                                  height: 50,
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                    children: [
                                                                                      const Text(""),
                                                                                      const Text(
                                                                                        "Verify C.A.",
                                                                                        style: TextStyle(fontSize: 16, color: Colors.white),
                                                                                      ),
                                                                                      InkWell(
                                                                                          onTap: () {
                                                                                            Get.back();
                                                                                          },
                                                                                          child: const Icon(
                                                                                            Icons.clear,
                                                                                            color: Colors.white,
                                                                                          ))
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                Padding(
                                                                                  padding: const EdgeInsets.all(8.0),
                                                                                  child: Column(
                                                                                    children: [
                                                                                      //EPF,PF
                                                                                      const SizedBox(
                                                                                        height: 20,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text(
                                                                                            "CA Name: ",
                                                                                            style: TextStyle(fontSize: 13),
                                                                                          ),
                                                                                          Text(element.caName)
                                                                                        ],
                                                                                      ),
                                                                                      //LIC
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Firm Name: ", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.caFirmName)
                                                                                        ],
                                                                                      ),
                                                                                      //National Saving
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Designation: ",
                                                                                              style: TextStyle(fontSize: 13)),
                                                                                          Text(element.designation)
                                                                                        ],
                                                                                      ),
                                                                                      //Tuition Fee for Children
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Phone No:", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.mno)
                                                                                        ],
                                                                                      ),
                                                                                      //Senior Citizen Saving
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Firm No: ", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.frnno)
                                                                                        ],
                                                                                      ),
                                                                                      //Repayment of Principal
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("pan_card: ", style: TextStyle(fontSize: 13)),
                                                                                          Text(element.panCard)
                                                                                        ],
                                                                                      ),
                                                                                      //Sukanya Samridhi Scheme
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text("Address: ", style: TextStyle(fontSize: 13)),
                                                                                          Container(
                                                                                            width: 150,
                                                                                            child: Text(
                                                                                              element.address +
                                                                                                  "," +
                                                                                                  element.city +
                                                                                                  ", " +
                                                                                                  element.district +
                                                                                                  ", " +
                                                                                                  element.state +
                                                                                                  ", " +
                                                                                                  element.pinCode,
                                                                                              style: TextStyle(fontSize: 13),
                                                                                            ),
                                                                                          )
                                                                                        ],
                                                                                      ),
                                                                                      //Registration Fees
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),

                                                                                      //Attachment
                                                                                      const SizedBox(
                                                                                        height: 10,
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                Padding(
                                                                                  padding: const EdgeInsets.all(8.0),
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                                                    children: [
                                                                                      Flexible(
                                                                                        child: Container(
                                                                                            width: MediaQuery.of(context).size.width,
                                                                                            height: 40,
                                                                                            child: ElevatedButton(
                                                                                                style: ElevatedButton.styleFrom(
                                                                                                    primary: Color(0xff1739E4)),
                                                                                                onPressed: () {

                                                                                                  box.write("ca_name",element.caName);
                                                                                                  box.write("ca_address",element.address);
                                                                                                  box.write("ca_firm",element.caFirmName);
                                                                                                  box.write("ca_city",element.city);
                                                                                                  Get.back();
                                                                                                },
                                                                                                child: Text("OK"))),
                                                                                      ),
                                                                                      // SizedBox(
                                                                                      //     width: 160,
                                                                                      //     height: 40,
                                                                                      //     child: ElevatedButton(
                                                                                      //         style: ElevatedButton.styleFrom(
                                                                                      //             primary: Color(0xff1739E4)),
                                                                                      //         onPressed: () {},
                                                                                      //         child: Text("Confirm")))
                                                                                    ],
                                                                                  ),
                                                                                )
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  );
                                                                });

                                                            _homeController.box.write("CurrentlySelectedCA", element);
                                                            _homeController.selectedCaNameValue = value!;
                                                          }
                                                        });

                                                        print("---------Selected Ca---------------" +
                                                            _homeController.box.read('CurrentlySelectedCA').toString());
                                                      },
                                                    ),

                                                  );
                                                }
                                                return Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  height: 40,
                                                  child: DropdownSearch<String>(
                                                    popupProps: const PopupProps.dialog(showSearchBox: true),
                                                    items: [],
                                                    onChanged: (value) {

                                                    },
                                                  ),
                                                );
                                              },
                                            ),*/
                                          ],
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 16,
                                      ),
                                    ],
                                  ),

                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        InkWell(
                                          onTap: () {
                                            print("-------this is working-----22-nov");
                                            print(_homeController.selectedClientNameValue.toString());
                                            print(_homeController.selectedFinancialYear.toString());

                                            if (_homeController.selectedClientNameValue == "" || _homeController.selectedFinancialYear == "") {
                                              ScaffoldMessenger.of(context).showSnackBar(
                                                const SnackBar(content: Text('Please Select Client Name and Financial Year')),
                                              );
                                            } else {
                                             setState(() {
                                               widget.isFinancialSelected = false;
                                               widget.index =0;
                                             });
                                       /*       Get.toNamed("/CalculatorView");*/
                                            }
                                          },
                                          child: Card(
                                            elevation: 5,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: widget.index==0 && widget.isFinancialSelected == false?Border(
                                                  bottom: BorderSide( //                   <--- right side
                                                    color: Color(0xff0B28BE),
                                                    width: 5.0,
                                                  ),
                                                ):null,),
                                              alignment: Alignment.center,
                                              width: 100,
                                              height: 100,
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  SvgPicture.asset(
                                                    "images/calculator.svg",
                                                    width: 20,
                                                    height: 20,
                                                  ),
                                                  const SizedBox(
                                                    height: 15,
                                                  ),
                                                  const Text(
                                                    "Calculator",
                                                    style: TextStyle(color: Color(0xff656A87), fontSize: 11, fontWeight: FontWeight.bold),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            if (_homeController.selectedClientNameValue == "" || _homeController.selectedFinancialYear == "") {
                                              ScaffoldMessenger.of(context).showSnackBar(
                                                const SnackBar(content: Text('Please Select Client Name and Financial Year')),
                                              );
                                            } else {
                                              setState(() {
                                                widget.isFinancialSelected = false;
                                                widget.index =1;
                                              });
                                              // Get.toNamed("/incomemaster");
                                            }
                                          },
                                          child: Card(
                                              elevation: 5,
                                              child: Container(
                                                  decoration: BoxDecoration(
                                                    border: widget.index==1&& widget.isFinancialSelected == false?   Border(
                                                      bottom: BorderSide( //                   <--- right side
                                                        color: Color(0xff0B28BE),
                                                        width: 5.0,
                                                      ),
                                                    ):null,),
                                                  alignment: Alignment.center,
                                                  height: 100,
                                                  width: 100,
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: [
                                                        SvgPicture.asset(
                                                          "images/income.svg",
                                                          width: 25,
                                                        ),
                                                        const SizedBox(
                                                          height: 5,
                                                        ),
                                                        const Text(
                                                          'Income Master',
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(color: Color(0xff656A87), fontSize: 11, fontWeight: FontWeight.bold),
                                                        ),
                                                      ],
                                                    ),
                                                  ))),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            if (_homeController.selectedClientNameValue == "" || _homeController.selectedFinancialYear == "") {
                                              ScaffoldMessenger.of(context).showSnackBar(
                                                const SnackBar(content: Text('Please Select Client Name and Financial Year')),
                                              );
                                            } else {
                                              setState(() {
                                                widget.isFinancialSelected = false;
                                                widget.index =2;
                                              });
                                              // Get.toNamed("/bankstatement");
                                            }
                                          },
                                          child: Card(
                                            elevation: 5,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: widget.index==2&& widget.isFinancialSelected == false?   Border(
                                                  bottom: BorderSide( //                   <--- right side
                                                    color: Color(0xff0B28BE),
                                                    width: 5.0,
                                                  ),
                                                ):null,),
                                              alignment: Alignment.center,
                                              width: 100,
                                              height: 100,
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  SvgPicture.asset(
                                                    "images/bank.svg",
                                                    width: 25,
                                                  ),
                                                  const SizedBox(
                                                    height: 15,
                                                  ),
                                                  const Text(
                                                    "Bank Statement",
                                                    style: TextStyle(
                                                        color: Color(
                                                          0xff656A87,
                                                        ),
                                                        fontSize: 11,
                                                        fontWeight: FontWeight.bold),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            if (_homeController.selectedClientNameValue == "" || _homeController.selectedFinancialYear == "") {
                                              ScaffoldMessenger.of(context).showSnackBar(
                                                const SnackBar(content: Text('Please Select Client Name and Financial Year')),
                                              );
                                            } else {
                                              setState(() {
                                                widget.isFinancialSelected = false;
                                                widget.index =3;
                                              });
                                              // Get.toNamed("/ppfaccount");
                                            }
                                          },
                                          child: Card(
                                            elevation: 5,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: widget.index==3 && widget.isFinancialSelected == false?   Border(
                                                  bottom: BorderSide( //                   <--- right side
                                                    color: Color(0xff0B28BE),
                                                    width: 5.0,
                                                  ),
                                                ):null,),
                                              alignment: Alignment.center,
                                              width: 100,
                                              height: 100,
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  SvgPicture.asset(
                                                    "images/ppf.svg",
                                                    width: 25,
                                                  ),
                                                  const SizedBox(
                                                    height: 15,
                                                  ),
                                                  const Text(
                                                    "PPF Account",
                                                    style: TextStyle(color: Color(0xff656A87), fontSize: 11, fontWeight: FontWeight.bold),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        InkWell(
                                            onTap: () {
                                              if (_homeController.selectedClientNameValue == "" || _homeController.selectedFinancialYear == "") {
                                                ScaffoldMessenger.of(context).showSnackBar(
                                                  const SnackBar(content: Text('Please Select Client Name and Financial Year')),
                                                );
                                              } else {
                                                setState(() {
                                                  widget.isFinancialSelected = false;
                                                  widget.index =4;
                                                });
                                                // Get.toNamed("/deduction");
                                              }
                                            },
                                            child: Card(
                                              elevation: 5,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  border: widget.index==4 && widget.isFinancialSelected == false?   Border(
                                                    bottom: BorderSide( //                   <--- right side
                                                      color: Color(0xff0B28BE),
                                                      width: 5.0,
                                                    ),
                                                  ):null,),
                                                alignment: Alignment.center,
                                                width: 100,
                                                height: 100,
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    SvgPicture.asset(
                                                      "images/deduction.svg",
                                                      width: 25,
                                                    ),
                                                    const SizedBox(
                                                      height: 5,
                                                    ),
                                                    const Text(
                                                      "Deduction",
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(color: Color(0xff656A87), fontSize: 11, fontWeight: FontWeight.bold),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )),
                                        InkWell(
                                            onTap: () {
                                              if (_homeController.selectedClientNameValue == "" || _homeController.selectedFinancialYear == "") {
                                                ScaffoldMessenger.of(context).showSnackBar(
                                                  const SnackBar(content: Text('Please Select Client Name and Financial Year')),
                                                );
                                              } else {
                                                setState(() {
                                                  widget.isFinancialSelected = false;
                                                  widget.index =5;
                                                });
                                                // Get.toNamed("/investment");
                                              }
                                            },
                                            child: Card(
                                              elevation: 5,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  border: widget.index==5 && widget.isFinancialSelected == false?   Border(
                                                    bottom: BorderSide( //                   <--- right side
                                                      color: Color(0xff0B28BE),
                                                      width: 5.0,
                                                    ),
                                                  ):null,),
                                                alignment: Alignment.center,
                                                width: 100,
                                                height: 100,
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    SvgPicture.asset(
                                                      "images/investment.svg",
                                                      width: 25,
                                                    ),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    const Text(
                                                      "Investment",
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(color: Color(0xff656A87), fontSize: 11, fontWeight: FontWeight.bold),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )),
                                        InkWell(
                                            onTap: () {
                                              if (_homeController.selectedClientNameValue == "" || _homeController.selectedFinancialYear == "") {
                                                ScaffoldMessenger.of(context).showSnackBar(
                                                  const SnackBar(content: Text('Please Select Client Name and Financial Year')),
                                                );
                                              } else {
                                                setState(() {
                                                  widget.isFinancialSelected = false;
                                                  widget.index =6;
                                                });
                                                // Get.toNamed("/expenses");
                                              }
                                            },
                                            child: Card(
                                              elevation: 5,
                                              child: Container(
                                               // color: widget.index == 6?Colors.indigo.withOpacity(.1):Colors.white,
                                                decoration: BoxDecoration(
                                                  border: widget.index==6 && widget.isFinancialSelected == false?   Border(
                                                    bottom: BorderSide( //                   <--- right side
                                                      color: Color(0xff0B28BE),
                                                      width: 5.0,
                                                    ),
                                                ):null,),
                                                alignment: Alignment.center,
                                                width: 100,
                                                height: 100,
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    SvgPicture.asset(
                                                      "images/expenses.svg",
                                                      width: 25,
                                                    ),
                                                    const SizedBox(
                                                      height: 5,
                                                    ),
                                                    const Text(
                                                      "Expenses",
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(color: Color(0xff656A87), fontSize: 11, fontWeight: FontWeight.bold),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )),
                                        InkWell(
                                          onTap:null,
                                          child: Card(
                                            elevation: 0,
                                            child: Container(
                                              alignment: Alignment.center,
                                              width: 100,
                                              height: 100,
                                              // child: Column(
                                              //   mainAxisAlignment: MainAxisAlignment.center,
                                              //   crossAxisAlignment: CrossAxisAlignment.center,
                                              //   children: [
                                              //     SvgPicture.asset(
                                              //       "images/ppf.svg",
                                              //       width: 25,
                                              //     ),
                                              //     const SizedBox(
                                              //       height: 15,
                                              //     ),
                                              //     const Text(
                                              //       "PPF Account",
                                              //       style: TextStyle(color: Color(0xff656A87), fontSize: 11, fontWeight: FontWeight.bold),
                                              //     ),
                                              //   ],
                                              // ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Financial statement",
                                          style: TextStyle(color: Color(0xff656A87)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(12),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        InkWell(
                                            onTap: () {
                                              var caname=box.read("ca_name")==null?"":box.read("ca_name");
                                              if (_homeController.selectedClientNameValue == "" || _homeController.selectedFinancialYear == "" ||
                                                  caname=="") {
                                                ScaffoldMessenger.of(context).showSnackBar(
                                                  const SnackBar(content: Text('Please Select Client Name and Financial Year')),
                                                );
                                              } else {
                                                widget.index = 0;
                                                widget.isFinancialSelected = true;
                                                setState(() {

                                                });
                                                // Get.to(FinancialView());
                                              }
                                            },
                                            child: Card(
                                              elevation: 5,
                                              child: Container(
                                                alignment: Alignment.center,
                                                width: 100,
                                                height: 100,
                                                decoration: BoxDecoration(
                                                  border: widget.isFinancialSelected == true ?   Border(
                                                    bottom: BorderSide( //                   <--- right side
                                                      color: Color(0xff0B28BE),
                                                      width: 5.0,
                                                    ),
                                                  ):null,),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    SvgPicture.asset(
                                                      "images/financialstatement.svg",
                                                      width: 25,
                                                    ),
                                                    const SizedBox(
                                                      height: 5,
                                                    ),
                                                    const Text(
                                                      "Financial Statement",
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(color: Color(0xff656A87), fontSize: 11, fontWeight: FontWeight.bold),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )),
                                      ],
                                    ),
                                  ),

                                  Visibility(
                                    visible: widget.isFinancialSelected,
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: MediaQuery.of(context).size.height/2.5,
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 10),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      widget.index = 0;
                                                    });
                                                    // Get.to(ProfitAndLossView());
                                                  },
                                                  child: Card(
                                                    elevation: 5,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        border: widget.index==0 ?   Border(
                                                          bottom: BorderSide( //                   <--- right side
                                                            color: Color(0xff0B28BE),
                                                            width: 5.0,
                                                          ),
                                                        ):null,),
                                                      alignment: Alignment.center,
                                                      width: 100,
                                                      height: 100,
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [
                                                         Visibility(
                                                           visible:_homeController.isDraftedpl ,
                                                           child:
                                                           Tooltip(
                                                             message: 'UpdateBy- ${_homeController.plUpdateBy}\nUpdateAt- ${_homeController.plUpdateAt}',
                                                             triggerMode: TooltipTriggerMode.tap, // ensures the label appears when tapped
                                                             preferBelow: false, // use this if you want the label above the widget
                                                             child: Row(
                                                               mainAxisAlignment: MainAxisAlignment.end ,
                                                               children: [
                                                                 Card(
                                                                 child: Container(
                                                                     padding: EdgeInsets.all(2),
                                                                     color: Colors.green ,
                                                                     child: Text("Drafted",style: TextStyle(fontSize: 10,color: Colors.white,fontWeight: FontWeight.bold),)),
                                                               ),],
                                                             ),
                                                           )
                                                           ,),
                                                          SvgPicture.asset(
                                                            "images/profitandloss.svg",
                                                            width: 30,
                                                          ),
                                                          Text(
                                                            "Profit And Loss Account",
                                                            textAlign: TextAlign.center,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    var deductvalue = widget.storage.read("deductionvalue");
                                                    // print("  TTTTTTTTTTT-------------- $deductvalue");

                                                    if (deductvalue.toString().isNotEmpty) {
                                                      setState(() {
                                                        widget.index = 1;
                                                      });
                                                    /*  Get.to(TotalComputationView());*/
                                                    } else {
                                                      Get.defaultDialog(
                                                          title: "Alert",
                                                          titleStyle: TextStyle(fontSize: 16),
                                                          content: Flexible(
                                                              child: Text(
                                                                "Please go to the calculator page",
                                                                style: TextStyle(fontSize: 13),
                                                              )),
                                                          actions: [
                                                            SizedBox(
                                                              height: 30,
                                                              child: ElevatedButton(
                                                                  style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
                                                                  onPressed: () {
                                                                    Get.back();
                                                                  },
                                                                  child: Text("OK")),
                                                            )
                                                          ]);
                                                    }
                                                  },
                                                  child: Card(
                                                    elevation: 5,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        border: widget.index==1 ?   Border(
                                                          bottom: BorderSide( //                   <--- right side
                                                            color: Color(0xff0B28BE),
                                                            width: 5.0,
                                                          ),
                                                        ):null,),
                                                      alignment: Alignment.center,
                                                      width: 100,
                                                      height: 100,
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [

                                                          SvgPicture.asset(
                                                            "images/computation.svg",
                                                            width: 30,
                                                          ),
                                                          Text(
                                                            "Total Computation",
                                                            textAlign: TextAlign.center,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    widget.computaionController.computationTaxPaidList.clear();
                                                    widget.computaionController.computationBankList.clear();

                                                    setState(() {
                                                      widget.index = 2;
                                                    });

                                                    // Get.to(ComputationView());
                                                  },
                                                  child: Card(
                                                    elevation: 5,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        border: widget.index==2 ?   Border(
                                                          bottom: BorderSide( //                   <--- right side
                                                            color: Color(0xff0B28BE),
                                                            width: 5.0,
                                                          ),
                                                        ):null,),
                                                      alignment: Alignment.center,
                                                      width: 100,
                                                      height: 100,
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [
                                                          Visibility(
                                                            visible:_homeController.isDraftedComputation ,
                                                            child:
                                                            Tooltip(
                                                              message: 'UpdateBy- ${_homeController.comUpdateBy}\nUpdateAt- ${_homeController.comUpdateAt}',
                                                              triggerMode: TooltipTriggerMode.tap, // ensures the label appears when tapped
                                                              preferBelow: false, // use this if you want the label above the widget
                                                              child: Row(
                                                                mainAxisAlignment: MainAxisAlignment.end ,
                                                                children: [ Card(
                                                                  child: Container(
                                                                      padding: EdgeInsets.all(2),
                                                                      color: Colors.green ,
                                                                      child: Text("Drafted",style: TextStyle(fontSize: 10,color: Colors.white,fontWeight: FontWeight.bold),)),
                                                                ),],
                                                              ),
                                                            ),),
                                                          SvgPicture.asset(
                                                            "images/totalcomputation.svg",
                                                            width: 30,
                                                          ),
                                                          Text(
                                                            "Computation\n  ",
                                                            textAlign: TextAlign.center,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {

                                                    setState(() {
                                                      widget.index = 3;
                                                    });

                                                    // Get.to(AnnexureView());

                                                  },
                                                  child: Card(

                                                    elevation: 5,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        border: widget.index==3 ?   Border(
                                                          bottom: BorderSide( //                   <--- right side
                                                            color: Color(0xff0B28BE),
                                                            width: 5.0,
                                                          ),
                                                        ):null,),
                                                      alignment: Alignment.center,
                                                      width: 100,
                                                      height: 100,
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [
                                                          Visibility(
                                                            visible:_homeController.isDraftedAnnexure ,
                                                            child:
                                                            Tooltip(
                                                              message: 'UpdateBy- ${_homeController.annexureUpdateBy}\nUpdateAt- ${_homeController.annexureUpdateAt}',
                                                              triggerMode: TooltipTriggerMode.tap, // ensures the label appears when tapped
                                                              preferBelow: false, // use this if you want the label above the widget
                                                              child: Row(
                                                                mainAxisAlignment: MainAxisAlignment.end ,
                                                                children: [ Card(
                                                                  child: Container(
                                                                      padding: EdgeInsets.all(2),
                                                                      color: Colors.green ,
                                                                      child: Text("Drafted",style: TextStyle(fontSize: 10,color: Colors.white,fontWeight: FontWeight.bold),)),
                                                                ),],
                                                              ),
                                                            ),),
                                                          SvgPicture.asset(
                                                            "images/annexure.svg",
                                                            width: 30,
                                                          ),
                                                          Text(
                                                            "Annexure\n  ",
                                                            textAlign: TextAlign.center,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 10),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      widget.index = 4;
                                                    });
                                                    // Get.toNamed("/balancesheet");
                                                  },
                                                  child: Card(

                                                    elevation: 5,
                                                    child: Container(

                                                      decoration: BoxDecoration(
                                                        border: widget.index == 4 ?   Border(
                                                          bottom: BorderSide( //                   <--- right side
                                                            color: Color(0xff0B28BE),
                                                            width: 5.0,
                                                          ),
                                                        ):null,),
                                                      alignment: Alignment.center,
                                                      width: 100,
                                                      height: 100,
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [
                                                          Visibility(
                                                            visible:_homeController.isDraftedBs ,
                                                            child:
                                                            Tooltip(
                                                              message: 'UpdateBy- ${_homeController.bsUpdateBy}\nUpdateAt- ${_homeController.bsUpdateAt}',
                                                              triggerMode: TooltipTriggerMode.tap, // ensures the label appears when tapped
                                                              preferBelow: false, // use this if you want the label above the widget
                                                              child: Row(
                                                                mainAxisAlignment: MainAxisAlignment.end ,
                                                                children: [ Card(
                                                                  child: Container(
                                                                      padding: EdgeInsets.all(2),
                                                                      color: Colors.green ,
                                                                      child: Text("Drafted",style: TextStyle(fontSize: 10,color: Colors.white,fontWeight: FontWeight.bold),)),
                                                                ),],
                                                              ),
                                                            ),),
                                                          SvgPicture.asset(
                                                            "images/balancesheet.svg",
                                                            width: 30,
                                                          ),
                                                          Text(
                                                            "Balance Sheet\n  ",
                                                            textAlign: TextAlign.center,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: null,
                                                  child: Card(
                                                    elevation: 0,
                                                    child: Container(

                                                      alignment: Alignment.center,
                                                      width: 100,
                                                      height: 100,
                                                      /*child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [
                                                          SvgPicture.asset(
                                                            "images/balancesheet.svg",
                                                            width: 30,
                                                          ),
                                                          Text(
                                                            "Balance Sheet",
                                                            textAlign: TextAlign.center,
                                                          )
                                                        ],
                                                      ),*/
                                                    ),
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: null,
                                                  child: Card(
                                                    elevation: 0,
                                                    child: Container(
                                                      alignment: Alignment.center,
                                                      width: 100,
                                                      height: 100,
                                                      /*child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [
                                                          SvgPicture.asset(
                                                            "images/balancesheet.svg",
                                                            width: 30,
                                                          ),
                                                          Text(
                                                            "Balance Sheet",
                                                            textAlign: TextAlign.center,
                                                          )
                                                        ],
                                                      ),*/
                                                    ),
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: null,
                                                  child: Card(
                                                    elevation: 0,
                                                    child: Container(
                                                      alignment: Alignment.center,
                                                      width: 100,
                                                      height: 100,
                                                      /*child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [
                                                          SvgPicture.asset(
                                                            "images/balancesheet.svg",
                                                            width: 30,
                                                          ),
                                                          Text(
                                                            "Balance Sheet",
                                                            textAlign: TextAlign.center,
                                                          )
                                                        ],
                                                      ),*/
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          /*SizedBox(
                                            height: 20,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 10),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [

                                                InkWell(
                                                  onTap: null,
                                                  child: Card(
                                                    elevation: 0,
                                                    child: Container(
                                                      alignment: Alignment.center,
                                                      width: 100,
                                                      height: 100,
                                                      *//*child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset("images/balance.svg",width: 30,),
                            Text("Balance Sheet",textAlign: TextAlign.center,)
                          ],
                        ),*//*
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),*/
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                   clientName.toString().isEmpty && caName.toString().isEmpty ?
                   Container(
                     color: Colors.white,
                     alignment: Alignment.center,
                     width: MediaQuery.of(context).size.width-(MediaQuery.of(context).size.width)*(40/100),
                     height: MediaQuery.of(context).size.height,
                     child: Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       crossAxisAlignment: CrossAxisAlignment.center,
                       children: [
                         Text("Please Select Client Name And C.A.",style: TextStyle(
                           fontSize: 18
                         ),)
                       ],
                     ),
                   ) : Container(
                     color: Colors.white,
                    width: MediaQuery.of(context).size.width-(MediaQuery.of(context).size.width)*(40/100),
                    height: MediaQuery.of(context).size.height,
                    // color: Colors.white,
                    child: !widget.isFinancialSelected ? screens[widget.index] : financialList[widget.index],
                  ),
                ],
              ),
            ),
          );
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          EasyLoading.show(status: "Loading");
          return Container();
        } else {
          EasyLoading.dismiss();
          Future(() {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    content: Container(
                      height: 100,
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            "images/no_wifi.png",
                            width: 50,
                            color: Colors.blue,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Text(
                            "No Data Internet Connection or Cellular Data Found!",
                            style: TextStyle(color: Colors.red, fontSize: 15),
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    ),
                    actions: [
                      TextButton(
                          onPressed: () {
                            exit(0);
                          },
                          child: const Text("OK"))
                    ],
                  );
                });
          });
        }
        return const Center(
          child: Text(
            "Some error occurred!",
            style: TextStyle(color: Colors.red, fontSize: 18),
          ),
        );
      },
    );
  }
/*
  @override
  void onWindowFocus() {
    setState(() {});
  }

  @override
  void onWindowMaximize() {
    WindowManager.instance.unmaximize();
  }
*/

  Future<bool> onWillPop(context) {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null || now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      // ScaffoldMessenger.of(context).showSnackBar(snackBar)
      Fluttertoast.showToast(msg: "double click to exit");

      return Future.value(false);
    }
    return Future.value(true);
  }
}

