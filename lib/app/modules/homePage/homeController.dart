// ignore_for_file: file_names

import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get_storage/get_storage.dart';

import '../../services/api.dart';

class HomeController extends GetxController {
  GetStorage box = GetStorage();

  List<String> clientListForDraft = [];
  var selectedClientNameValue = "";
  var selectedCaNameValue = "";
  var selectedFinancialYear = "2021-2022";

  bool isDraftedpl = false;
  bool isDraftedBs = false;
  bool isDraftedComputation = false;
  bool isDraftedAnnexure = false;

  var plUpdateBy = "";
  var plUpdateAt = "";

  var bsUpdateBy = "";
  var bsUpdateAt = "";

  var comUpdateBy = "";
  var comUpdateAt = "";

  var annexureUpdateBy = "";
  var annexureUpdateAt = "";




  Future<void> getProfitAndLossApiCall() async {
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var finacialYear = box.read("finacial_year");
    await Api.getProfitAndLossApi(clientId, finacialYear, adminId).then((value) {
      print("get profit and loss working");
      try {
        if (value["response_p_and_l"].toString().isNotEmpty) {
           isDraftedpl = true;
           plUpdateBy = value["response_p_and_l"][0]["inserted_by"];
           plUpdateAt = value["response_p_and_l"][0]["updated_at"];
           print("------client name list--------------"+clientListForDraft.toString());
        /*   clientListForDraft.forEach((element) {
             if(element.contains(value["response_p_and_l"][0]["inserted_by"].toString())){
               plUpdateBy = element.split("-")[1].toString();
             }
           });*/
        }else{
           isDraftedpl = false;
        }
      } catch (e) {
        isDraftedpl = false;
        print('Exception on GetProfit ${e}');
      }
    });

  }

  Future getAnnexureApiCall() async {
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var finacialYear = box.read("finacial_year");
    try{
      await Api.getAnnexurApi(clientId, finacialYear, adminId).then((value) {
        print("get profit and loss working");
        try {
          print(value.id.toString()+"------------*********--------");
          if (value.id.toString().isNotEmpty || value.id != null) {

            isDraftedAnnexure = true;
            isDraftedBs = true;

            annexureUpdateBy = value.insertedBy;
            annexureUpdateAt = value.insertedAt;

            bsUpdateBy = value.insertedBy;
            bsUpdateAt = value.insertedAt;

          }else{
            isDraftedAnnexure = false;
            isDraftedBs = false;
          }
        } catch (e) {
          isDraftedAnnexure = false;
          isDraftedBs = false;
        }
      });
    }catch(e){
      isDraftedAnnexure = false;
      isDraftedBs = false;
    }

  }

  Future getComputaionApiCall() async {

    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var finacialYear = box.read("finacial_year");

   try{
     await Api.getComputationApi(clientId, finacialYear, adminId).then((value) {
       try {
         if (value["id"].toString().isNotEmpty) {
           isDraftedComputation = true;
           comUpdateBy = value["inserted_by"];
           comUpdateAt = value["inserted_at"];
         }else{
           isDraftedComputation = false;
         }
       } catch (e) {
         isDraftedComputation = false;
         print('Exception on GetProfit ${e}');
       }
     });
   }catch(e){
     isDraftedComputation = false;
   }
  }



  // void userData() async {
  //   await Api.UserClientListApi("68", "super_admin", "1", "Admin", "85");
  // }
}
