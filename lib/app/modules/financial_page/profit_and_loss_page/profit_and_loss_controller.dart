import 'dart:convert';

import 'package:accountx/app/model/expense_model.dart';
import 'package:accountx/app/services/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:get_storage/get_storage.dart';

class ProfitAndLossAccountController extends GetxController {

  List<ResponseExpenseList> responseList = [];
  List<ResponseExpenseList> newList = [];
  GetStorage box = GetStorage();
  double closingStock = 0.0;
  double grossProfit = 0.0;
  double perecntage = 0.0;
  double totalGrossProfit = 0.0;
  double amountH = 0.0;
  var totalClosingStock = 0.0.obs;
  void onInit() {
    expensesController.text = "0.0";
    super.onInit();
  }

  TextEditingController expensesController = TextEditingController();
  TextEditingController openingStockController = TextEditingController();
  TextEditingController salesController = TextEditingController();
  TextEditingController purchaseController = TextEditingController();
  TextEditingController grossProfitController = TextEditingController();
  TextEditingController commissionController = TextEditingController();
  TextEditingController interestOnTDSController = TextEditingController();
  TextEditingController netProfitController = TextEditingController();
  TextEditingController closingStockController = TextEditingController();
  TextEditingController depreciationController = TextEditingController();
  TextEditingController commTradingController = TextEditingController();
  TextEditingController commPaidToRetailController = TextEditingController();
  TextEditingController newHead = TextEditingController();
  TextEditingController newAmount = TextEditingController();

TextEditingController salaryController = TextEditingController();
TextEditingController hpController = TextEditingController();
TextEditingController capitalController = TextEditingController();
TextEditingController bandpController = TextEditingController();
TextEditingController otherController = TextEditingController();
TextEditingController agricultureController = TextEditingController();

 var isalaryController = "0".obs;
 var ihpController = "0".obs;
 var icapitalController = "0".obs;
 var ibandpController = "0".obs;
 var iotherController = "0".obs;
 var iagricultureController = "0".obs;

  // grossProfitResult() {
  //   grossProfitController.text = (double.parse(salesController.text) * (1 / 100)).toString();
  //   totalGrossProfit = double.parse(salesController.text) + double.parse(grossProfitController.text);
  // }

  closingStockResult() {
    closingStock = double.parse(salesController.text) -
        (double.parse(openingStockController.text) + double.parse(purchaseController.text) + double.parse(grossProfitController.text));
    closingStockController.text = closingStock.toString();
  }

  void netProfitResult() {
    var expenses = double.parse(expensesController.text.isEmpty ? "0.0" : expensesController.text);
    var grossProfit = double.parse(grossProfitController.text.isEmpty ? "0.0" : grossProfitController.text);
    var commission = double.parse(commissionController.text.isEmpty ? "0.0" : commissionController.text);
    var interestOnTDS = double.parse(interestOnTDSController.text.isEmpty ? "0.0" : interestOnTDSController.text);
    var dep = double.parse(depreciationController.text.isEmpty ? "0.0" : depreciationController.text);
    var totalSalary =  double.parse(salaryController.text.isEmpty?"0.0":salaryController.text) +
        double.parse(hpController.text.isEmpty?"0.0":hpController.text) +
        double.parse(capitalController.text.isEmpty?"0.0":capitalController.text)+
        double.parse(bandpController.text.isEmpty?"0.0":bandpController.text)+
        double.parse(otherController.text.isEmpty?"0.0":otherController.text)+
        double.parse(agricultureController.text.isEmpty?"0.0":agricultureController.text);
    amountH = 0.0;
    for (int j = 0; j < newList.length; j++) {
      amountH += double.parse(newList[j].amount.toString());
    }
    netProfitController.text = ((grossProfit + commission + interestOnTDS + amountH+totalSalary) - (expenses + dep)).toString();
   
  }

  void getClosingStock() {
    var op = double.parse(openingStockController.text.isEmpty ? "0.0" : openingStockController.text);
    var sales = double.parse(salesController.text.isEmpty ? "0.0" : salesController.text);
    var purchage = double.parse(purchaseController.text.isEmpty ? "0.0" : purchaseController.text);
    var gp = double.parse(grossProfitController.text.isEmpty ? "0.0" : grossProfitController.text);

    var cta = double.parse(commTradingController.text.isEmpty ? "0.0" : commTradingController.text);
    var cpr = double.parse(commPaidToRetailController.text.isEmpty ? "0.0" : commPaidToRetailController.text);

    totalClosingStock.value = (op + purchage + gp + cpr) - (sales+cta);

    closingStockController.text = double.parse(totalClosingStock.value.toString()).toStringAsFixed(2);
  }

  void profitAndLossApiCall() async {
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var finacialYear = box.read("finacial_year");
    var map2 = {};
    map2 = Map.fromIterable(newList, key: (e) => e.name, value: (e) => e.amount);
    EasyLoading.show(status: "");
    await Api.profitAndLossApi(
            clientId,
            adminId,
            finacialYear,
            openingStockController.text,
            salesController.text,
            purchaseController.text,
            closingStockController.text,
            grossProfitController.text,
            expensesController.text,
            commissionController.text,
            interestOnTDSController.text,
            netProfitController.text,
            depreciationController.text,

            jsonEncode(map2).toString(), 
            commTradingController.text,
            commPaidToRetailController.text)
          

        .then((value) {
      if (value["response_status"].toString() == "1") {
        EasyLoading.dismiss();
        Get.defaultDialog(content: Text(value["message"]), actions: [
          ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
              onPressed: (() {
                Get.back();
              }),
              child: Text("Ok"))
        ]);
      } else {
        EasyLoading.dismiss();
        Get.defaultDialog(content: Text(value["message"]), actions: [
          ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
              onPressed: (() {
                Get.back();
              }),
              child: Text("Ok"))
        ]);
      }
    });
  }

  Future<void> getProfitAndLossApiCall() async {
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var finacialYear = box.read("finacial_year");
    print('--------getProfitAndLossApiCall()----------------${clientId}, ${adminId}, ${finacialYear}');
    await Api.getProfitAndLossApi(clientId, finacialYear, adminId).then((value) {
      print("-------zucol------------------------------------------------------"+clientId.toString());
      print("-------zucol------------------------------------------------------"+finacialYear.toString());
      print("-------zucol------------------------------------------------------"+adminId.toString());
      print("--------1 Feb--getProfitAndLossApiCall() response---------------"+value.toString());
      try {

        if (value["response_p_and_l"].toString().isNotEmpty) {
          openingStockController.text = value["response_p_and_l"][0]["opening_stock"];
          salesController.text = value["response_p_and_l"][0]["sale"];
          purchaseController.text = value["response_p_and_l"][0]["purchase"];
          closingStockController.text = value["response_p_and_l"][0]["closing_stock"];
          expensesController.text = value["response_p_and_l"][0]["expenses"];
          grossProfitController.text = value["response_p_and_l"][0]["gross_profit"];
          commissionController.text = value["response_p_and_l"][0]["commission"];
          interestOnTDSController.text = value["response_p_and_l"][0]["interest_on_tds"];
          netProfitController.text = value["response_p_and_l"][0]["net_profit"];
          depreciationController.text = value["response_p_and_l"][0]["depreciation"];
          commPaidToRetailController.text = value["response_p_and_l"][0]["commission_paid_to_retailer"];
          commTradingController.text = value["response_p_and_l"][0]["commission_of_trading_ac"];
          // print('newlist ${jsonDecode(value["response_p_and_l"][0]["head_json"])}"))}');
          //newList =  jsonDecode(value["response_p_and_l"][0]["head_json"]).toList((e)=>ResponseExpenseList(name:"${e.key}",amount:"${e.value}"));

        }else{

        }
      } catch (e) {
        openingStockController.text = "0";
        salesController.text = "0";
        purchaseController.text = "0";
        closingStockController.text = "0";
        expensesController.text = "0";
        grossProfitController.text = "0";
        commissionController.text = "0";
        interestOnTDSController.text = "0";
        netProfitController.text = "0";
        depreciationController.text = "0";
        commPaidToRetailController.text = "0";
        commTradingController.text = "0";
        print('Exception on GetProfit ${e}');
      }

    });
  }

  Future incomeMasterDetailsCall() async {
    print("IncomeMasterDetailsCall working");
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read('finacial_year');
    await Api.getIcomeMasterApi(clientId.toString(), adminId.toString(), financialYear.toString()).then((value1) {
      print("----getIcomeMasterApi response----"+value1.toString());
      if (value1['response_status'] == 1) {
        print("........>>>>>>> income master details: inBlock: status success");
        isalaryController.value = value1["salary"]??"0";
        ihpController.value = value1["hp"]??"0";
        icapitalController.value = value1["capital"]??"0";
        ibandpController.value = value1["b_and_p"]??"0";
        iotherController.value = value1["other"]??"0";
        iagricultureController.value = value1["agriculture"]??"0";

        salaryController.text = value1["salary"]??"0";
        hpController.text = value1["hp"]??"0";
        capitalController.text = value1["capital"]??"0";
        bandpController.text = value1["b_and_p"]??"0";
        otherController.text = value1["other"]??"0";
        agricultureController.text = value1["agriculture"]??"0";
      //  EasyLoading.dismiss();
      } else {
        print("........>>>>>>> income master details: outBlock: status failed");
       // EasyLoading.dismiss();
      }
    });
  }
}

extension ListFromMap<Key, Element> on Map<Key, Element> {
  List<T> toList<T>(
          T Function(MapEntry<Key, Element> entry) getElement) =>
      entries.map(getElement).toList();

}
