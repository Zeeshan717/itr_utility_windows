import 'package:accountx/app/modules/financial_page/profit_and_loss_page/profit_and_loss_controller.dart';
import 'package:get/get.dart';

class ProfitAndLossAccountBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ProfitAndLossAccountController());
  }
}
