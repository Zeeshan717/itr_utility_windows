import 'dart:convert';
import 'dart:io';

import 'package:accountx/app/model/expense_model.dart';

import 'package:accountx/app/modules/financial_page/profit_and_loss_page/profit_and_loss_controller.dart';

import 'package:accountx/app/services/api.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart' hide Column, Row;

import '../../../provider/update_provider.dart';

class ProfitAndLossView extends StatefulWidget {
   ProfitAndLossView({Key? key}) : super(key: key);
  _ProfitAndLossViewState profitAndLossViewState =_ProfitAndLossViewState();
  @override
  State<ProfitAndLossView> createState() => _ProfitAndLossViewState();
  void callback(){
    profitAndLossViewState.customCallBack();
  }
}

class _ProfitAndLossViewState extends State<ProfitAndLossView> {
  String? name;
  String? amount;
  var controller = Get.put(ProfitAndLossAccountController());
  GetStorage storage = GetStorage();
  Future<ExpenseModel>? getExpenseApiResponse;
  @override
  void initState() {
    super.initState();
    print("_ProfitAndLossViewState working ");
    print("--------->>>> initState rebuils");
    controller.getProfitAndLossApiCall().then((value) => setState(() {}));
   /*  getExpenseApiResponse =
         Api.getExpenseApi(
         storage.read("client_id").toString(),
         storage.read("finacial_year").toString(),
         storage.read("admin_id").toString());*/
    controller.incomeMasterDetailsCall().then((value) => setState(() {}));
  }

   customCallBack() async{
    await controller.getProfitAndLossApiCall();
/*    getExpenseApiResponse =  Api.getExpenseApi(
        storage.read("client_id").toString(),
        storage.read("finacial_year").toString(),
        storage.read("admin_id").toString());*/
    await controller.incomeMasterDetailsCall();
  //  UpdateProvider().changeValue;


  }
  double sum = 0.0;
  double totAmount = 0.0;
  double totAmount2 = 0.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          Tooltip(
            message: 'Add',
            child: IconButton(
                onPressed: () {
                  Get.defaultDialog(
                      barrierDismissible: false,
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      title: "Add New Head",
                      titleStyle: TextStyle(fontSize: 15),
                      content: Column(
                        children: [
                          SizedBox(
                            height: 30,
                            child: TextField(
                              keyboardType: TextInputType.name,
                              controller: controller.newHead,
                              decoration: InputDecoration(
                                  hintText: "Enter Head Name",
                                  contentPadding: EdgeInsets.all(5),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: BorderSide(
                                          color: Colors.grey, width: 1))),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            height: 30,
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: controller.newAmount,
                              decoration: InputDecoration(
                                  hintText: "Enter Amount",
                                  contentPadding: EdgeInsets.all(5),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: BorderSide(
                                          color: Colors.grey, width: 1))),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                      actions: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            TextButton(
                                onPressed: () {
                                  controller.newHead.clear();
                                  controller.newAmount.clear();
                                  setState(() {});
                                  Get.back();

                                  // FocusManager.instance.primaryFocus?.unfocus();
                                },
                                child: Text("Cancel")),
                            TextButton(
                                onPressed: () {
                                  controller.newList.add(ResponseExpenseList(
                                    name: controller.newHead.text,
                                    amount: controller.newAmount.text,
                                  ));
                                  controller.netProfitResult();
                                  setState(() {});
                                  Get.back();

                                  controller.newHead.clear();
                                  controller.newAmount.clear();
                                },
                                child: Text("OK")),
                          ],
                        )
                      ]);
                },
                icon: Image.asset(
                  "images/add.png",
                  color: Colors.black,
                  width: 25,
                )),
          ),
          SizedBox(
            width: 20,
          )
        ],
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text(
          "Profit And Loss Account",
          style: TextStyle(fontSize: 16, color: Colors.black),
        ),
      ),
      body:  Consumer<UpdateProvider>(
        builder: (context, updateProvider, child) =>  Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Form(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Column(
                        children: [
                          //1
                          Row(
                            children: [
                              Flexible(
                                child: Container(
                                  height: 40,
                                  child: TextFormField(
                                    onChanged: (value) {
                                      // controller.closingStockResult();
                                      controller.getClosingStock();
                                      controller.netProfitResult();
                                      setState(() {});
                                    },
                                    controller: controller.openingStockController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Opening Stock",
                                        labelStyle: const TextStyle(fontSize: 15),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1))),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                child: Container(
                                  height: 40,
                                  child: TextFormField(
                                    onChanged: (value) {
                                      controller.getClosingStock();
                                      controller.netProfitResult();
                                      setState(() {});
                                    },
                                    controller: controller.salesController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Sales",
                                        labelStyle: const TextStyle(fontSize: 15),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1))),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          //2
                          Row(
                            children: [
                              Flexible(
                                child: Container(
                                  height: 40,
                                  child: TextFormField(
                                    onChanged: (value) {
                                      controller.getClosingStock();
                                      controller.netProfitResult();
                                      setState(() {});
                                    },
                                    controller: controller.purchaseController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Purchase",
                                        labelStyle: const TextStyle(fontSize: 15),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1))),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                child: Container(
                                  height: 40,
                                  child: TextFormField(
                                    onChanged: (value) {
                                      controller.getClosingStock();
                                      controller.netProfitResult();
                                      setState(() {});
                                    },
                                    controller: controller.commTradingController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Commission of Trading Account",
                                        labelStyle: const TextStyle(fontSize: 15),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1))),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Flexible(
                                child: Container(
                                  height: 40,
                                  child: TextFormField(
                                    onChanged: (value) {
                                      controller.getClosingStock();
                                      controller.netProfitResult();
                                      setState(() {});
                                    },
                                    controller: controller.commPaidToRetailController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Commission paid to Retailers",
                                        labelStyle: const TextStyle(fontSize: 15),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1))),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                child: Container(
                                  height: 40,
                                  child: TextFormField(
                                    readOnly: true,
                                    controller: controller.closingStockController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Closing Stock",
                                        labelStyle: const TextStyle(fontSize: 15),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1))),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          //3
                          Row(
                            children: [
                              Flexible(
                                child: Container(
                                  height: 40,
                                  child: TextFormField(
                                    onChanged: (value) {
                                      // print("--------");
                                      controller.getClosingStock();
                                      controller.netProfitResult();
                                      setState(() {});
                                    },
                                    controller: controller.grossProfitController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Gross Profit",
                                        labelStyle: const TextStyle(fontSize: 15),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1))),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                child: Container(
                                  height: 40,
                                  child: TextFormField(
                                    onChanged: (value) {
                                      controller.netProfitResult();
                                      controller.netProfitResult();
                                      setState(() {});
                                    },
                                    readOnly: true,
                                    controller: controller.expensesController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Expenses",
                                        labelStyle: const TextStyle(fontSize: 15),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1))),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          //4
                          Row(
                            children: [
                              Flexible(
                                child: Container(
                                  height: 40,
                                  child: TextFormField(
                                    onChanged: (value) {
                                      controller.netProfitResult();
                                      controller.netProfitResult();
                                      setState(() {});
                                    },
                                    controller: controller.commissionController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Commission",
                                        labelStyle: const TextStyle(fontSize: 15),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1))),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                child: Container(
                                  height: 40,
                                  child: TextFormField(
                                    onChanged: (value) {
                                      controller.netProfitResult();
                                      controller.netProfitResult();
                                      setState(() {});
                                    },
                                    controller: controller.interestOnTDSController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Interest on TDS",
                                        labelStyle: const TextStyle(fontSize: 15),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.grey, width: 1))),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          //5
                        ],
                      ),
                    )),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: List.generate(controller.newList.length, (index) {
                        TextEditingController newHead2 = TextEditingController();
                        TextEditingController newAmount2 =
                        TextEditingController();

                        newHead2.text = controller.newList[index].name.toString();
                        newAmount2.text =
                            controller.newList[index].amount.toString();

                        name = newHead2.text;
                        amount = newAmount2.text;

                        return Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 5),
                                    height: 40,
                                    child: TextFormField(
                                      controller: newHead2,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                        /* labelText:
                                                    controller.newList[index].name,
                                                labelStyle:
                                                    const TextStyle(fontSize: 15),*/
                                          enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                              BorderRadius.circular(5),
                                              borderSide: const BorderSide(
                                                  color: Colors.grey, width: 1)),
                                          focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                              BorderRadius.circular(5),
                                              borderSide: const BorderSide(
                                                  color: Colors.grey, width: 1))),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Flexible(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 5),
                                    height: 40,
                                    child: TextFormField(
                                      controller: newAmount2,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                        /*   labelText: newList[index].amount,
                                                  labelStyle: const TextStyle(fontSize: 15),*/
                                          enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                              BorderRadius.circular(5),
                                              borderSide: const BorderSide(
                                                  color: Colors.grey, width: 1)),
                                          focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                              BorderRadius.circular(5),
                                              borderSide: const BorderSide(
                                                  color: Colors.grey, width: 1))),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                IconButton(
                                  onPressed: () {
                                    print("Delete Button");
                                    controller.newList.removeWhere((item) =>
                                    item.name ==
                                        controller.newList[index].name);
                                    controller.netProfitResult();
                                    setState(() {});
                                  },
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                )
                              ],
                            ),
                          ],
                        );
                      }),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: [
                      Flexible(
                        child: Container(
                          height: 40,
                          child: TextFormField(
                            controller: controller.netProfitController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: "Net Profit",
                                labelStyle: const TextStyle(fontSize: 15),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Colors.grey, width: 1)),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Colors.grey, width: 1))),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        child: Container(
                          height: 40,
                          child: TextFormField(
                            controller: controller.depreciationController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: "Depreciation",
                                labelStyle: const TextStyle(fontSize: 15),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Colors.grey, width: 1)),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Colors.grey, width: 1))),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        padding: EdgeInsets.all(8.0),
                        color: Color(0xff1739E4),
                        child: Text(
                          "Expense Details",
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                        )),
                  ],
                ),
                FutureBuilder<ExpenseModel>(
                    future: updateProvider.getExpenseApiResponse,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        sum = 0;
                        controller.responseList =
                            snapshot.data!.responseExpenseList;
                        for (int i = 0;
                        i < snapshot.data!.responseExpenseList.length;
                        i++) {
                          sum += double.parse(snapshot
                              .data!.responseExpenseList[i].amount
                              .toString());
                        }
                        controller.expensesController.text = sum.toString();

                        print("working");
                        return Column(
                          children: [
                            DataTable2(
                                dataRowHeight: 22,
                                columnSpacing: 8,
                                horizontalMargin: 12,
                                minWidth: 200,
                                columns: const [
                                  DataColumn2(label: Center(child: Text("Name"))),
                                  DataColumn2(
                                      label: Center(child: Text("Amount"))),
                                ],
                                rows: List.generate(
                                    snapshot.data!.responseExpenseList.length,
                                        (index) => DataRow(cells: [
                                      DataCell(Center(
                                          child: Text(
                                            snapshot.data!
                                                .responseExpenseList[index].name
                                                .toString(),
                                            style: const TextStyle(
                                              fontSize: 13,
                                            ),
                                            textAlign: TextAlign.center,
                                          ))),
                                      DataCell(Center(
                                          child: Text(
                                            snapshot.data!
                                                .responseExpenseList[index].amount
                                                .toString(),
                                            style: const TextStyle(
                                              fontSize: 13,
                                            ),
                                            textAlign: TextAlign.center,
                                          )))
                                    ]))),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        );
                      }
                      // else if (snapshot.connectionState == ConnectionState.waiting) {
                      //   // EasyLoading.show(status: "Loading");
                      //   print("ConnectionState.waiting");
                      //   return Container();
                      // }
                      else {
                        return Column(
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "No Expense Data Found\nPlease go to The Expense Page",
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                        );
                      }
                    }),
                //Expense List

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        padding: EdgeInsets.all(8.0),
                        color: Color(0xff1739E4),
                        child: Text(
                          "Income Details",
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                        )),
                  ],
                ),
                Obx(()=>Column(
                  children: [
                    DataTable2(
                        dataRowHeight: 22,
                        columnSpacing: 8,
                        horizontalMargin: 12,
                        minWidth: 200,
                        columns: const [
                          DataColumn2(label: Center(child: Text("Name"))),
                          DataColumn2(label: Center(child: Text("Amount"))),
                        ],
                        rows: [
                          DataRow(cells: [
                            DataCell(Center(
                                child: Text(
                                  "Salary",
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                ))),
                            DataCell(Center(
                                child: Text(
                                  controller.isalaryController.value,
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                )))
                          ]),
                          DataRow(cells: [
                            DataCell(Center(
                                child: Text(
                                  "HP",
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                ))),
                            DataCell(Center(
                                child: Text(
                                  controller.ihpController.value,
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                )))
                          ]),
                          DataRow(cells: [
                            DataCell(Center(
                                child: Text(
                                  "Capital",
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                ))),
                            DataCell(Center(
                                child: Text(
                                  controller.icapitalController.value,
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                )))
                          ]),
                          DataRow(cells: [
                            DataCell(Center(
                                child: Text(
                                  "B&P",
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                ))),
                            DataCell(Center(
                                child: Text(
                                  controller.ibandpController.value,
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                )))
                          ]),
                          DataRow(cells: [
                            DataCell(Center(
                                child: Text(
                                  "Other",
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                ))),
                            DataCell(Center(
                                child: Text(
                                  controller.iotherController.value,
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                )))
                          ]),
                          DataRow(cells: [
                            DataCell(Center(
                                child: Text(
                                  "Agriculture",
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                ))),
                            DataCell(Center(
                                child: Text(
                                  controller.iagricultureController.value,
                                  style: const TextStyle(
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                )))
                          ])
                        ]),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ),),

                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: 40,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: const Color(0xff1739E4)),
                                onPressed: () async {
                                  if (controller
                                      .openingStockController.text.isEmpty &&
                                      controller.salesController.text.isEmpty &&
                                      controller
                                          .purchaseController.text.isEmpty &&
                                      controller
                                          .commissionController.text.isEmpty &&
                                      controller
                                          .interestOnTDSController.text.isEmpty &&
                                      controller
                                          .depreciationController.text.isEmpty) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                        SnackBar(
                                            content: Text(
                                                "Please fill all the fields!!")));
                                  } else {
                                    controller.profitAndLossApiCall();

                                    controller.netProfitResult();
                                  }
                                },
                                child: const Text("Submit")),
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Tooltip(
                          message: 'Generate Excel',
                          child: InkWell(
                            onTap: () async {
                              double grossTotal = 0.0;
                              grossTotal = double.parse(
                                  controller.salesController.text) +
                                  double.parse(
                                      controller.closingStockController.text) +
                                  double.parse(
                                      controller.commTradingController.text);

                              double netTotal = 0.0;

                              final Workbook workbook = Workbook();
                              final Worksheet sheet = workbook.worksheets[0];

                              Style globalStyle = workbook.styles.add('style');

                              globalStyle.fontName = 'Times New Roman';

                              globalStyle.fontColor = '#000000';
                              globalStyle.fontSize = 14;

                              final Range range = sheet.getRangeByName('A8:D8');
                              range.cellStyle.borders.all.color = '#000000';
                              range.cellStyle.borders.top.lineStyle =
                                  LineStyle.thin;
                              range.cellStyle.borders.bottom.lineStyle =
                                  LineStyle.thin;

                              final Range range2 = sheet.getRangeByName('B14');
                              range2.cellStyle.borders.all.color = '#000000';
                              range2.cellStyle.borders.top.lineStyle =
                                  LineStyle.thin;

                              range2.cellStyle.borders.bottom.lineStyle =
                                  LineStyle.double;

                              final Range range3 = sheet.getRangeByName('D14');
                              range3.cellStyle.borders.all.color = '#000000';
                              range3.cellStyle.borders.top.lineStyle =
                                  LineStyle.thin;
                              range3.cellStyle.borders.bottom.lineStyle =
                                  LineStyle.double;

                              sheet.name = "Profit And Loss Account";
                              storage.read("firm_name").toString() == null
                                  ? sheet.getRangeByName('B2').setText("")
                                  : sheet.getRangeByName('B2').setText(storage
                                  .read("firm_name")
                                  .toString() +
                                  "(Prop.  ${storage.read("client_name").toString()})");
                              sheet.getRangeByName('B2').cellStyle.bold = true;
                              sheet.getRangeByName('B2').cellStyle = globalStyle;

                              storage.read("distributor_address").toString() ==
                                  null
                                  ? sheet.getRangeByName('B3').setText("1")
                                  : sheet.getRangeByName('B3').setText(storage
                                  .read("distributor_address")
                                  .toString());

                              sheet.getRangeByName('B3').cellStyle = globalStyle;

                              sheet
                                  .getRangeByName('B6')
                                  .setText("TRADING AND PROFIT AND"
                                  " LOSS A/C FOR THE YEAR ENDED 31/03/2022");
                              sheet
                                  .getRangeByName('A8')
                                  .setText("PARTICULARS      ");
                              sheet.getRangeByName('A8').cellStyle.bold = true;
                              sheet.getRangeByName('A8').columnWidth = 35;

                              sheet.getRangeByName('B8').setText("AMOUNT");
                              sheet.getRangeByName('B8').cellStyle.bold = true;
                              sheet.getRangeByName('B8').columnWidth = 12.5;

                              sheet.getRangeByName('C8').setText("PARTCULARS");
                              sheet.getRangeByName('C8').cellStyle.bold = true;
                              sheet.getRangeByName('C8').columnWidth = 35;

                              sheet.getRangeByName('D8').setText("AMOUNT");
                              sheet.getRangeByName('D8').cellStyle.bold = true;
                              sheet.getRangeByName('D8').columnWidth = 12.5;

                              sheet
                                  .getRangeByName('A9')
                                  .setText("To OPENING STOCK");
                              sheet.getRangeByName('B9').setText(
                                  "₹ " + controller.openingStockController.text);

                              sheet.getRangeByName('A10').setText("To PURCHASE");
                              sheet.getRangeByName('B10').setText(
                                  "₹ " + controller.purchaseController.text);

                              sheet
                                  .getRangeByName('A11')
                                  .setText("Comm Paid to Retailer ");
                              sheet.getRangeByName('B11').setText("₹ " +
                                  controller.commPaidToRetailController.text);
                              sheet
                                  .getRangeByName('A12')
                                  .setText("To GROSS PROFIT");
                              sheet.getRangeByName('B12').setText(
                                  "₹ " + controller.grossProfitController.text);

                              sheet.getRangeByName('C9').setText("By SALES");
                              sheet.getRangeByName('D9').setText(
                                  "₹ " + controller.salesController.text);
                              sheet
                                  .getRangeByName('C10')
                                  .setText("By Commision Of Trading A/c");
                              sheet.getRangeByName('D10').setText(
                                  "₹ " + controller.commTradingController.text);

                              sheet
                                  .getRangeByName('C12')
                                  .setText("By Closing Stock");
                              sheet.getRangeByName('D12').setText(
                                  "₹ " + controller.closingStockController.text);

                              sheet
                                  .getRangeByName('B14')
                                  .setText("₹ " + grossTotal.toString());
                              sheet.getRangeByName('B14').cellStyle.bold = true;
                              sheet.getRangeByName('B14').columnWidth = 20;

                              sheet
                                  .getRangeByName('D14')
                                  .setText("₹ " + grossTotal.toString());
                              sheet.getRangeByName('D14').cellStyle.bold = true;
                              sheet.getRangeByName('D14').columnWidth = 20;
                              int i = 0;

                              for (i = 0;
                              i < controller.responseList.length;
                              i++) {
                                sheet.getRangeByName('A${16 + i}').setText("To " +
                                    controller.responseList[i].name.toString());
                                sheet.getRangeByName('B${16 + i}').setText("₹ " +
                                    controller.responseList[i].amount.toString());
                              }
                              sheet
                                  .getRangeByName('A${16 + i}')
                                  .setText('To Depreciation');
                              sheet.getRangeByName('B${16 + i}').setText(
                                  '₹ ' + controller.depreciationController.text);

                              sheet
                                  .getRangeByName('C16')
                                  .setText("By GROSS PROFIT");
                              sheet.getRangeByName('D16').setText(
                                  "₹ " + controller.grossProfitController.text);

                              sheet
                                  .getRangeByName('C17')
                                  .setText("By COMMISSION");
                              sheet.getRangeByName('D17').setText(
                                  "₹ " + controller.commissionController.text);

                              sheet
                                  .getRangeByName('C18')
                                  .setText("By INTEREST ON TDS");

                              sheet.getRangeByName('D18').setText(
                                  "₹ " + controller.interestOnTDSController.text);
//---------------------------------------code updated on  1 Feb 2023--------------------------------------------------------------------------
                              sheet.getRangeByName('C19').setText("By Salary");
                              sheet.getRangeByName('D19').setText(
                                  "₹ " + controller.salaryController.text);

                              sheet.getRangeByName('C20').setText("By Hp");
                              sheet
                                  .getRangeByName('D20')
                                  .setText("₹ " + controller.hpController.text);

                              sheet.getRangeByName('C21').setText("By Capital");
                              sheet.getRangeByName('D21').setText(
                                  "₹ " + controller.capitalController.text);

                              sheet.getRangeByName('C22').setText("By B&P");
                              sheet.getRangeByName('D22').setText(
                                  "₹ " + controller.bandpController.text);

                              sheet.getRangeByName('C23').setText("By Other");
                              sheet.getRangeByName('D23').setText(
                                  "₹ " + controller.otherController.text);
                              sheet
                                  .getRangeByName('C24')
                                  .setText("By Agriculture");
                              sheet.getRangeByName('D24').setText(
                                  "₹ " + controller.bandpController.text);
//---------------------------------------code-end-------------------------------------------------------------------------
                              print("New List : ---- " +
                                  controller.newList.length.toString());
                              controller.amountH = 0.0;
                              for (int j = 0;
                              j < controller.newList.length;
                              j++) {
                                controller.amountH += double.parse(
                                    controller.newList[j].amount.toString());
                                sheet.getRangeByName('C${19 + j}').setText("By " +
                                    controller.newList[j].name.toString());
                                sheet.getRangeByName('D${19 + j}').setText("₹ " +
                                    controller.newList[j].amount.toString());

                                print(" Amount H: ------- " +
                                    controller.amountH.toString());
                              }
                              controller.netProfitResult();
                              if (double.parse(
                                  controller.netProfitController.text) >=
                                  0.0) {
                                netTotal = double.parse(
                                    controller.expensesController.text) +
                                    double.parse(
                                        controller.netProfitController.text) +
                                    double.parse(
                                        controller.depreciationController.text);
                              } else {
                                netTotal = double.parse(
                                    controller.expensesController.text);
                              }

                              if (double.parse(
                                  controller.netProfitController.text) >=
                                  0.0) {
                                sheet
                                    .getRangeByName('A${16 + i + 2}')
                                    .setText("To NET PROFIT");
                                sheet
                                    .getRangeByName('A${16 + i + 2}')
                                    .setText("To NET PROFIT");
                                sheet
                                    .getRangeByName('A${16 + i + 2}')
                                    .cellStyle
                                    .bold = true;
                                sheet.getRangeByName('B${16 + i + 2}').setText(
                                    "₹ " + controller.netProfitController.text);
                              } else {
                                sheet
                                    .getRangeByName('C${16 + i + 2}')
                                    .setText("By NET LOSS");
                                sheet
                                    .getRangeByName('C${16 + i + 2}')
                                    .setText("By NET LOSS");
                                sheet
                                    .getRangeByName('C${16 + i + 2}')
                                    .cellStyle
                                    .bold = true;

                                sheet.getRangeByName('D${16 + i + 2}').setText(
                                    "₹ " +
                                        controller.netProfitController.text
                                            .split("-")[1]);
                              }

                              final Range range4 =
                              sheet.getRangeByName('B${17 + i + 2}');

                              range4.cellStyle.borders.all.color = '#000000';
                              range4.cellStyle.borders.top.lineStyle =
                                  LineStyle.thin;
                              range4.cellStyle.borders.bottom.lineStyle =
                                  LineStyle.double;

                              sheet
                                  .getRangeByName('B${17 + i + 2}')
                                  .setText("₹ " + netTotal.toString());

                              final Range range5 =
                              sheet.getRangeByName('D${17 + i + 2}');
                              range5.cellStyle.borders.all.color = '#000000';
                              range5.cellStyle.borders.top.lineStyle =
                                  LineStyle.thin;
                              range5.cellStyle.borders.bottom.lineStyle =
                                  LineStyle.double;

                              sheet
                                  .getRangeByName('D${17 + i + 2}')
                                  .setText("₹ " + netTotal.toString());

                              sheet
                                  .getRangeByName('A${17 + i + 6}')
                                  .setText("CERTIFIED To  BE TRUE & CORRECT ");
                              sheet.getRangeByName('A${17 + i + 7}').setText(
                                  "FOR: " +
                                      storage
                                          .read("firm_name")
                                          .toString()
                                          .toUpperCase());

                              sheet.getRangeByName('A${17 + i + 11}').setText(
                                  "(${storage.read("client_name").toString().toUpperCase()})");
                              sheet.getRangeByName('A${17 + i + 7}').setText(
                                  "FOR: " + storage.read("firm_name").toString());

                              sheet
                                  .getRangeByName('A${17 + i + 12}')
                                  .setText("Prop.");

                              storage.read("place_of_supply").toString() == null
                                  ? sheet
                                  .getRangeByName('A${17 + i + 13}')
                                  .setText("")
                                  : sheet
                                  .getRangeByName('A${17 + i + 13}')
                                  .setText("PLACE: " +
                                  storage
                                      .read("place_of_supply")
                                      .toString());

                              sheet.getRangeByName('A${17 + i + 14}').setText(
                                  "DATE: " +
                                      formatDate(DateTime.now(),
                                          [dd, "/", mm, "/", yyyy]));

                              final List<int> bytes = workbook.saveAsStream();
                              workbook.dispose();
                              workbook.dispose();

                              final String path =
                                  (await getApplicationSupportDirectory()).path;
                              final String fileName =
                                  "$path/ProfitAndLossAccount.xlsx";
                              final File file = File(fileName);
                              await file.writeAsBytes(bytes, flush: true);

                              OpenFile.open(fileName);

                              print(" Purchase: ------ " +
                                  controller.purchaseController.text);
                            },
                            child: Image.asset(
                              "images/excel.png",
                              width: 30,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
