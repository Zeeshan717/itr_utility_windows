import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:accountx/app/modules/financial_page/computation_page/newpdf_api.dart';
import 'package:accountx/app/modules/financial_page/computation_page/pdf_total_computation.dart';
import 'package:accountx/app/modules/financial_page/computation_page/total_computation_data.dart';

import 'package:accountx/app/modules/incomeMasterPage/incomeMasterController.dart';
import 'package:accountx/app/modules/taxCalPage/taxController.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:share_plus/share_plus.dart';

class TotalComputationView extends StatefulWidget {
  const TotalComputationView({Key? key}) : super(key: key);

  @override
  State<TotalComputationView> createState() => _TotalComputationViewState();
}

class _TotalComputationViewState extends State<TotalComputationView> {
  GetStorage storage = GetStorage();
  var imcontroller = Get.put(IncomeMasterController());
  var controller = Get.put(ComputationController());
  var taxcontroller = Get.put(TaxController());

  String dropdownValue = "ITR-1";
  String dropdownValue2 = "Individual";
  String? dropdownValue3;

  @override
  void initState() {
    imcontroller.incomeMasterDetailsCall();
    taxcontroller.incomeMasterDetailsCall();

    controller.getUserDetailsApiCall();
    // taxcontroller.getTotalDeduction();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    taxcontroller.taxtotaldeductcontroller.text = storage.read("deductionvalue");
    taxcontroller.taxtotaltaxpayablecontroller.text = storage.read("taxablevalue");
    //taxcontroller.taxtotalincomecontroller.text=storage.read("totalIncomegetx");

    double gross = double.parse(storage.read("gross"));

    print("hiiiii--------" + taxcontroller.taxtotalincomecontroller.text);
    taxcontroller.taxtaxablecontroller.text =
        (gross - double.parse(taxcontroller.taxtotaldeductcontroller.text.isEmpty ? "0.0" : taxcontroller.taxtotaldeductcontroller.text)).toString();

    print("tdeduc--------" + taxcontroller.taxtotaldeductcontroller.text);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Computation",
          style: TextStyle(fontSize: 16, color: Colors.black),
        ),
        backgroundColor: Colors.white,
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Name: " + storage.read("client_name").toString()),
                    Text("Address: " + storage.read("distributor_address").toString()),
                    // Text("Phone No: " + storage.read("phone_no").toString()),
                    // Text("Email: " + controller.tcEmail)
                  ],
                ),

                //Computation of Income
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Computation of Income",
                      style: TextStyle(fontSize: 20),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    /*   Text("("),
                    Container(
                      child:DropdownButton<String>(
                        value: dropdownValue,
                        icon: const Icon(Icons.arrow_downward),
                        elevation: 16,
                        style: const TextStyle(color: Colors.deepPurple),
                        underline: Container(
                          height: 2,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            dropdownValue = newValue!;
                          });
                        },
                        items: <String>["ITR-1","ITR-2","ITR-3","ITR-4","ITR-5","ITR-6"]
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Text(")")*/
                  ],
                ),
                SizedBox(
                  height: 10,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: controller.tcPhoneController,
                          decoration: InputDecoration(
                              labelText: "Phone No",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: controller.tcEmailController,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(10),
                              labelText: "Email",
                              isDense: true,
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),

                //1
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: controller.tcPanController,
                          decoration: InputDecoration(
                              labelText: "PAN",
                              contentPadding: EdgeInsets.all(10),
                              isDense: true,
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),

                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: Colors.grey, width: 1)),
                        height: 40,
                        child: DropdownButton<String>(
                          isExpanded: true,
                          value: controller.tcdropdownValue2,
                          icon: const Icon(Icons.arrow_drop_down),
                          elevation: 16,
                          style: const TextStyle(color: Colors.black),
                          underline: SizedBox(),
                          onChanged: (String? newValue) {
                            setState(() {
                              controller.dropdownValue2 = newValue!;
                            });
                            print("Status: ------- " + newValue.toString());
                          },
                          items: <String>['Individual', 'Firm', 'Private Ltd. Company', 'Public Ltd. Company', 'HUF']
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                //2

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: controller.tcFatherNameController,
                          decoration: InputDecoration(
                              labelText: "Father's Name",
                              contentPadding: EdgeInsets.all(10),
                              isDense: true,
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5), border: Border.all(color: Colors.grey, width: 1)),
                        height: 40,
                        child: DropdownButton<String>(
                          hint: Text("Select Gender"),
                          isExpanded: true,
                          value: controller.tcdropdownValue3,
                          icon: const Icon(Icons.arrow_drop_down),
                          elevation: 16,
                          style: const TextStyle(color: Colors.black),
                          underline: SizedBox(),
                          onChanged: (String? newValue) {
                            setState(() {
                              controller.tcdropdownValue3 = newValue!;
                            });
                            print("Status: ------- " + newValue.toString());
                          },
                          items: <String>['N/A', 'Male', 'Female', 'Other'].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                //3
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: controller.tcBankNumberController,
                          decoration: InputDecoration(
                            isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "Bank A/C Number",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: controller.tcIfscCodeController,
                          decoration: InputDecoration(
                            isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "IFSC Code",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 10,
                ),
                //4
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: controller.tcEFillingController,
                          decoration: InputDecoration(
                            isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "E-Filing Status",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: controller.tcAadharCardNumber,
                          decoration: InputDecoration(
                            isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "Aadhaar Card Number",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 30,
                ),
                //Tax Summary (Amount in Rs)
                Center(
                    child: Text(
                  "Tax Summary (Amount in Rs)",
                  style: TextStyle(fontSize: 20),
                  textAlign: TextAlign.center,
                )),
                SizedBox(
                  height: 10,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //1
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Flexible(
                          child: Container(
                            height: 40,
                            child: Text("Salary"),
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: imcontroller.salaryController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),

                    //2
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Income from House Property"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: imcontroller.hpController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    //3
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Business and Profession"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: imcontroller.bandpController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    //4
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Capital Gain"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: imcontroller.capitalController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    //5

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Other Income"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: imcontroller.otherController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 10,
                    ),
                    //4
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Agriculture"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: imcontroller.agricultureController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 10,
                    ),
                    //6
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text(
                            "Gross Total Income",
                            style: TextStyle(color: Colors.cyan, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: taxcontroller.taxtotalincomecontroller,

                              decoration: InputDecoration(
                                isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    //7
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Total Deduction"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: taxcontroller.taxtotaldeductcontroller,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    //8
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Total Income (Taxable)"),
                        ),
                        Flexible(
                          child: Text(
                            "Rounded off from ₹0 as per Section 288A",
                            style: TextStyle(fontSize: 10),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: taxcontroller.taxtaxablecontroller,

                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    //9
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Total Tax Payable"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: taxcontroller.taxtotaltaxpayablecontroller,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    //10
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Less : Taxes Paid"),
                        ),
                        Flexible(child: Text("TDS")),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              controller: controller.ittdsController,
                              onChanged: (val) {
                                controller.itrefundController.clear();
                                controller.itCreditController.clear();
                                controller.ithealthController.clear();
                                controller.ittotTaxController.clear();
                                controller.refund();
                                controller.credit();
                                controller.ithealth();
                                controller.ittotTax();
                              },
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    //10
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Refund"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: controller.itrefundController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    Text(
                      "Taxes are applicable as per normal provision Please refer Annexure for details",
                      style: TextStyle(color: Colors.red),
                    ),

                    //Income Tax

                    SizedBox(
                      height: 10,
                    ),

                    Center(
                        child: Text(
                      "Income Tax",
                      style: TextStyle(fontSize: 20),
                    )),

                    SizedBox(
                      height: 10,
                    ),

                    //1
                    SizedBox(
                      height: 10,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Total Income"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: taxcontroller.taxtotalincomecontroller,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Basic Exemption"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: taxcontroller.taxtotaldeductcontroller,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    //3
                    SizedBox(
                      height: 10,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Income Tax (Before Tax Credit U/S 87A)"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: taxcontroller.taxtaxablecontroller,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Tax Credit U/S 87A"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: controller.itCreditController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Income Tax"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: taxcontroller.taxtotaltaxpayablecontroller,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),


                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Health and Education Cess (@4% GIT)"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: controller.ithealthController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Total Tax"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: controller.ittotTaxController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Tax Paid"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: controller.ittdsController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),



                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("Refund"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              readOnly: true,
                              controller: controller.itrefundController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("GSTIN"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              controller: controller.itGstinController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text("GST Gross Receipts"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              controller: controller.itGstGrossController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          width: 100,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Color(0xff1739E4),
                            ),
                            onPressed: () async {
                              TotalComputaionData cd = TotalComputaionData();
                              List<Map> genDetails = cd.getPdfGd();
                              final pdfFile = await PdfTotalComputationApi.generate(genDetails);

                              NewPdfApi.openFile(pdfFile);
                            },
                            child: Text(
                              "Download",
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 100,
                          child:ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Color(0xff1739E4),
                              ),
                              onPressed: () async {
                                TotalComputaionData cd = TotalComputaionData();
                                List<Map> genDetails = cd.getPdfGd();
                                final pdfFile = await PdfTotalComputationApi.generate(genDetails);

                                Share.shareFiles([pdfFile.path.toString()]);
                              },
                              child: Text("Share")),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 70,
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
