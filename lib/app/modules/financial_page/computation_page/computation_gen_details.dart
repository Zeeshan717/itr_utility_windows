import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class GenDetails extends StatefulWidget {
  GenDetails({Key? key}) : super(key: key);

  @override
  State<GenDetails> createState() => _GenDetailsState();
}

class _GenDetailsState extends State<GenDetails> {
  GetStorage storage = GetStorage();
  var _computationController = Get.put(ComputationController());
  @override
  void initState() {
    _computationController.gdNameController.text = storage.read("client_name").toString();
    _computationController.gdAddressController.text = storage.read("distributor_address").toString();
    _computationController.gdPhoneNoController.text = storage.read("phone_no").toString();
    // _computationController.gdEmailController.text = storage.read("client_name").toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.start,
                //   children: [
                //     Text("Name: " + storage.read("client_name").toString()),
                //     Text("Address: " + storage.read("distributor_address").toString()),
                //   ],
                // ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.start,
                //   children: [Text("Phone No: " + storage.read("phone_no").toString()), Text("Email: ")],
                // ),
                //Computation of Income
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Computation of Income",
                      style: TextStyle(fontSize: 20),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    /*   Text("("),
                    Container(
                      child:DropdownButton<String>(
                        value: dropdownValue,
                        icon: const Icon(Icons.arrow_downward),
                        elevation: 16,
                        style: const TextStyle(color: Colors.deepPurple),
                        underline: Container(
                          height: 2,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            dropdownValue = newValue!;
                          });
                        },
                        items: <String>["ITR-1","ITR-2","ITR-3","ITR-4","ITR-5","ITR-6"]
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Text(")")*/
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          readOnly: true,
                          controller: _computationController.gdNameController,
                          decoration: InputDecoration(
                            isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "Name",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.gdAddressController,
                          decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "Address",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 10,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          readOnly: true,
                          controller: _computationController.gdPhoneNoController,
                          decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "Phone No",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.gdEmailController,
                          decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "Email",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 10,
                ),
                //1
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.gdPanController,
                          decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "PAN",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: Colors.grey, width: 1)),
                        height: 40,
                        child: DropdownButton<String>(
                          isExpanded: true,
                          value: _computationController.dropdownValue2,
                          icon: const Icon(Icons.arrow_drop_down),
                          elevation: 16,
                          style: const TextStyle(color: Colors.black),
                          underline: SizedBox(),
                          onChanged: (String? newValue) {
                            setState(() {
                              _computationController.dropdownValue2 = newValue!;
                            });
                            print("Status: ------- " + newValue.toString());
                          },
                          items: <String>['Individual', 'Firm', 'Private Ltd. Company', 'Public Ltd. Company', 'HUF']
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                //2
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.gdFatherNameController,
                          decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "Father's Name",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: Colors.grey, width: 1)),
                        height: 40,
                        child: DropdownButton<String>(
                          hint: Text("Select Gender"),
                          isExpanded: true,
                          value: _computationController.dropdownValue3,
                          icon: const Icon(Icons.arrow_drop_down),
                          elevation: 16,
                          style: const TextStyle(color: Colors.black),
                          underline: SizedBox(),
                          onChanged: (String? newValue) {
                            setState(() {
                              _computationController.dropdownValue3 = newValue!;
                            });
                            print("Status: ------- " + newValue.toString());
                          },
                          items: <String>['N/A', 'Male', 'Female', 'Other'].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                //3
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.gdBankAcNumberController,
                          decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "Bank A/C Number",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.gdIfscCodeController,
                          decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "IFSC Code",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 10,
                ),
                //4
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.gdEFillingStatusController,
                          decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "E-Filing Status",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.gdAadhaarCardController,
                          decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(10),
                              labelText: "Aadhar Card Number",
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 30,
                ),
                //Tax Summary (Amount in Rs)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
