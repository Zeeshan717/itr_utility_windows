import 'dart:io';

import 'package:accountx/app/model/computation_bank_model.dart';
import 'package:accountx/app/model/computation_tax_paid_model.dart';
import 'package:accountx/app/modules/financial_page/computation_page/newpdf_api.dart';
import 'package:accountx/app/utils/pdf_api.dart';

import 'package:pdf/widgets.dart';

class PdfComputationApi {
  static Future<File> generate(List<Map> genDetails) async {
    final pdf = Document();
    List<ComputationTaxPaidModel> tpList = genDetails[3]['computation_taxpaid_list'];
    final taxPaidData = tpList.map((e) => [e.tpName, e.tpTan, e.tpToi, e.tpDy, e.tpBrought, e.tpTds, e.tpIncomefw, e.tpBalance]).toList();
    final taxPaidHeaders = [
      'Name          ',
      'Tan            ',
      'Type of Income',
      'Deducted Year         ',
      'Brought Forward TDS',
      'TDS Amount Claimed',
      'Income For Which TDS Paid',
      'Balance (INR)                    '
    ];

    List<ComputationBankModel> bankList = genDetails[4]['computaion_bank_list'];
    var count = 0;
    final bankData = bankList.map((e) {
      return [++count, e.cbName, e.cbifsc, e.cbaccount];
    }).toList();

    final bankHeaders = [
      'S NO.',
      'Name of the Bank',
      'IFSC Code',
      'Account No',
    ];

    pdf.addPage(MultiPage(build: (Context) {
      return [
        Column(children: [
          //Icon(const IconData(0xf04e1)),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Firm Name: " + genDetails[0]['name'], style: TextStyle(fontWeight: FontWeight.bold)),
            Text("AY 2022-2023", style: TextStyle(fontWeight: FontWeight.bold))
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [Text("Address: ", style: TextStyle(fontWeight: FontWeight.bold)), Text(genDetails[0]['address'])]),
            Text("FY 2021-2022", style: TextStyle(fontWeight: FontWeight.bold)),
          ]),

          Row(children: [Text("Mobile: ", style: TextStyle(fontWeight: FontWeight.bold)), Text(genDetails[0]['phone'])]),
          Row(children: [Text("E-Mail: ", style: TextStyle(fontWeight: FontWeight.bold)), Text(genDetails[0]['email'])]),
          SizedBox(height: 20),
          Text("Computation of Income (ITR-3)", style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(height: 10),
          Divider(thickness: 2),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("PAN:", style: TextStyle(fontWeight: FontWeight.bold)),

              Text("Father's Name:", style: TextStyle(fontWeight: FontWeight.bold)),
              Text("Bank A/C no.:", style: TextStyle(fontWeight: FontWeight.bold)),
              Text("E-Filling Status:", style: TextStyle(fontWeight: FontWeight.bold)),
              // Text("Selected tax regime: ", style: TextStyle(fontWeight: FontWeight.bold))
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(genDetails[0]['pan']),

              Text(genDetails[0]['father_name']),
              Text(genDetails[0]['bank_ac_no']),
              Text(genDetails[0]['efiling_status']),
              // Text(genDetails[0]['Old'])
            ]),
            Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Status:", style: TextStyle(fontWeight: FontWeight.bold)),
              // Text("Residential Status:", style: TextStyle(fontWeight: FontWeight.bold)),
              Text("Gender:", style: TextStyle(fontWeight: FontWeight.bold)),
              Text("IFSC code:", style: TextStyle(fontWeight: FontWeight.bold)),
              Text("Aadhaar Card Number: ", style: TextStyle(fontWeight: FontWeight.bold)),
            ]),
            Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(genDetails[0]['companyType']),
              // Text("Resident"),
              Text(genDetails[0]['gender']),
              Text(genDetails[0]['ifsc_code']),
              Text(genDetails[0]['aadhar_card']),
            ])
          ]),
          SizedBox(height: 20),

          //Tax Summary Section

          Text("Tax Summary (Amount in INR)", style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(height: 20),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Salary (Rs)"), Text(genDetails[1]["salary"])]),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("Income from House Property (Rs)"), Text(genDetails[1]["income_hp"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Business and Profession (Rs)"), Text(genDetails[1]["bp"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Capital Gain (Rs)"), Text(genDetails[1]["cg"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Other Sources (Rs)"), Text(genDetails[1]["other_income"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Gross Total Income (Rs)", style: TextStyle(fontWeight: FontWeight.bold)),
            Text(genDetails[1]["gross_total_income"], style: TextStyle(fontWeight: FontWeight.bold))
          ]),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("Less: Total Deductions (Rs)"), Text(genDetails[1]["total_deduction"])]),

          Padding(
            padding: EdgeInsets.only(left: 50),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Text("Total Income (Taxable) (Rs)"),
              Text("       Rounded off as per\n       Section 288A"),
              Text(genDetails[1]["total_income_taxable"])
            ]),
          ),

          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Total Tax Payable (Rs)"), Text(genDetails[1]["total_taxpayable"])]),

          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Less: Taxes Paid (TDS) (Rs)"), Text(genDetails[1]["tds"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Refund (Rs)", style: TextStyle(fontWeight: FontWeight.bold)),
            Text(
                double.parse(genDetails[1]["refund"].toString().isEmpty ? "0.0" : genDetails[1]["refund"].toString()) < 0
                    ? "(" + genDetails[1]["refund"] + ")"
                    : genDetails[1]["refund"],
                style: TextStyle(fontWeight: FontWeight.bold))
          ]),
          Row(children: [Text("Taxes are applicable as per normal provision\nPlease refer Annexure(s) for details")]),
          SizedBox(height: 20),

          //Income Tax Section

          Text("Income Tax", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          SizedBox(height: 20),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Total Income (Rs)"), Text(genDetails[2]["total_income"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Basic Exemption (Rs)"), Text(genDetails[2]["basic_Exemption"])]),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("Income Tax (Before Tax Credit U/S 87A) (Rs)"), Text(genDetails[2]["it_before_taxCredit"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Tax Credit U/S 87A (Rs)"), Text(genDetails[2]["tax_credit"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Income Tax (Rs)"), Text(genDetails[2]["income_tax"])]),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("Health and education cess (4% GIT) (Rs)"), Text(genDetails[2]["health_cess"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Total Tax (Rs)"), Text(genDetails[2]["total_tax"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Tax Paid (Rs)"), Text(genDetails[2]["taxpaid"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Refund (Rs)"),
            Text(
              double.parse(genDetails[2]["refund"].toString().isEmpty ? "0.0" : genDetails[1]["refund"].toString()) < 0
                  ? "(" + genDetails[1]["refund"] + ")"
                  : genDetails[1]["refund"],
            )
          ]),
          SizedBox(height: 20),
          Text("GST Details", style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(height: 20),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("GSTIN"), Text(genDetails[2]["gst_in"])]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("Gross Receipts (Rs)"), Text(genDetails[2]["gst_gross"])]),
          SizedBox(height: 20),
          Text("Taxes Paid", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          SizedBox(height: 20),
          Text("(TDS On Non Salary Income)", style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
          SizedBox(height: 20),
          Text("TDS On Non Salary Income", style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(height: 20),
          Table.fromTextArray(
            headerStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 9),
            headers: taxPaidHeaders,
            data: taxPaidData,
          ),
          SizedBox(height: 20),
          Text("Bank Account Details", style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(height: 20),
          Table.fromTextArray(
            headerStyle: TextStyle(fontWeight: FontWeight.bold),
            headers: bankHeaders,
            data: bankData,
          ),
          SizedBox(height: 20)
        ]),
      ];
    }));

    return NewPdfApi.saveDocument(name: 'my_example.pdf', pdf: pdf);
  }
}

// class User {
//   final String name;
//   final int age;

//   const User({required this.name, required this.age});
// }

// final users = [
    //   User(name: 'James', age: 19),
    //   User(name: 'Sarah', age: 21),
    //   User(name: 'Emma', age: 28),
    // ];
    // final data = users.map((user) => [user.name, user.age]).toList();
  // final newlist = list.map((e){
   // {"e.name":"e.value"}
  //  })
    // final data = [
    //   ['james', 19, 20, 123],
    //   ['Sarah', 21, 20, 124],
    //   ['Emma', 28, 19, 123]
    // ];