import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class TaxPaid extends StatefulWidget {
  TaxPaid({Key? key}) : super(key: key);
  var count = 0;

  @override
  State<TaxPaid> createState() => _TaxPaidState();
}

class _TaxPaidState extends State<TaxPaid> {
  var _computationController = Get.put(ComputationController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    widget.count++;

    if (widget.count == 1) {
      _computationController.addtpkey(
        "Name",
      );
    }

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(14, 10, 14, 10),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Text('Taxes paid\n(TDS on Non Salary Income)', style: TextStyle(fontSize: 20), textAlign: TextAlign.center),
              InkWell(
                onTap: () {
                  _computationController.addtpkey(
                    "Name",
                  );
                  setState(() {});
                  print("element----------" + _computationController.computationTaxPaidList[0].tpToi);
                  print("element----------" + _computationController.computationTaxPaidList[1].tpTan);
                  print("element----------" + _computationController.computationTaxPaidList[1].tpTds);
                  print("element----------" + _computationController.computationTaxPaidList[0].tpDy);
                },
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: const Color(0xff0B28BE),
                            border: Border.all(
                              color: const Color(0xff0B28BE),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(4))),
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                          size: 18,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text('Add more data', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Container(
                      height: 40,
                      child: TextField(
                        onChanged: (val) {
                          _computationController.computationTaxPaidList[0].tpName = val;
                        },
                        controller: _computationController.tpnameController,
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(10),
                            labelText: "Name",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Flexible(
                    child: Container(
                      height: 40,
                      child: TextField(
                        onChanged: (val) {
                          _computationController.computationTaxPaidList[0].tpTan = val;
                        },
                        controller: _computationController.tptanController,
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(10),
                            labelText: "Tan",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Container(
                      height: 40,
                      child: TextField(
                        onChanged: (val) {
                          _computationController.computationTaxPaidList[0].tpToi = val;
                        },
                        controller: _computationController.tptoiController,
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(10),
                            labelText: "Type Of Income",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Flexible(
                    child: Container(
                      height: 40,
                      child: TextField(
                        onChanged: (val) {
                          _computationController.computationTaxPaidList[0].tpDy = val;
                        },
                        keyboardType: TextInputType.number,
                        controller: _computationController.tpdyController,
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(10),
                            labelText: "Deducted Year",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Container(
                      height: 40,
                      child: TextField(
                        onChanged: (val) {
                          _computationController.computationTaxPaidList[0].tpBrought = val;
                        },
                        keyboardType: TextInputType.number,
                        controller: _computationController.tpbroughtController,
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(10),
                            labelText: "Brought Forward TDS",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Flexible(
                    child: Container(
                      height: 40,
                      child: TextField(
                        onChanged: (val) {
                          _computationController.computationTaxPaidList[0].tpTds = val;
                        },
                        keyboardType: TextInputType.number,
                        controller: _computationController.tptdsController,
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(10),
                            labelText: "TDS Amount Claimed",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Container(
                      height: 40,
                      child: TextField(
                        onChanged: (val) {
                          _computationController.computationTaxPaidList[0].tpIncomefw = val;
                        },
                        keyboardType: TextInputType.number,
                        controller: _computationController.tpincomefwController,
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(10),
                            labelText: "Income For Which TDS Paid",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Flexible(
                    child: Container(
                      height: 40,
                      child: TextField(
                        onChanged: (val) {
                          _computationController.computationTaxPaidList[0].tpBalance = val;
                        },
                        keyboardType: TextInputType.number,
                        controller: _computationController.tpbalanceController,
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(10),
                            labelText: "Balance",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                child: Column(
                  children: List.generate(_computationController.computationTaxPaidList.length - 1, (index) {
                    List<TextEditingController> taxespaidnameController =
                        List.generate(_computationController.computationTaxPaidList.length, (index) => TextEditingController());
                    taxespaidnameController[index + 1].text = _computationController.computationTaxPaidList[index + 1].tpName;

                    List<TextEditingController> taxespaidtanController =
                        List.generate(_computationController.computationTaxPaidList.length, (index) => TextEditingController());
                    taxespaidtanController[index + 1].text = _computationController.computationTaxPaidList[index + 1].tpTan;

                    List<TextEditingController> taxespaidtoiController =
                        List.generate(_computationController.computationTaxPaidList.length, (index) => TextEditingController());
                    taxespaidtoiController[index + 1].text = _computationController.computationTaxPaidList[index + 1].tpToi;

                    List<TextEditingController> taxespaiddyController =
                        List.generate(_computationController.computationTaxPaidList.length, (index) => TextEditingController());
                    taxespaiddyController[index + 1].text = _computationController.computationTaxPaidList[index + 1].tpDy;

                    List<TextEditingController> taxespaidbroughtController =
                        List.generate(_computationController.computationTaxPaidList.length, (index) => TextEditingController());
                    taxespaidbroughtController[index + 1].text = _computationController.computationTaxPaidList[index + 1].tpBrought;

                    List<TextEditingController> taxespaidtdsController =
                        List.generate(_computationController.computationTaxPaidList.length, (index) => TextEditingController());
                    taxespaidtdsController[index + 1].text = _computationController.computationTaxPaidList[index + 1].tpTds;

                    List<TextEditingController> taxespaidincomefwController =
                        List.generate(_computationController.computationTaxPaidList.length, (index) => TextEditingController());
                    taxespaidincomefwController[index + 1].text = _computationController.computationTaxPaidList[index + 1].tpIncomefw;

                    List<TextEditingController> taxespaidbalanceController =
                        List.generate(_computationController.computationTaxPaidList.length, (index) => TextEditingController());
                    taxespaidbalanceController[index + 1].text = _computationController.computationTaxPaidList[index + 1].tpBalance;

                    return InkWell(
                      onTap: () {
                        _computationController.computationTaxPaidList.removeAt(index + 1);

                        setState(() {});
                      },
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
                          child: Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 244, 2, 2),
                                    border: Border.all(
                                      color: Color.fromARGB(255, 244, 2, 2),
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(4))),
                                // color: Color.fromARGB(255, 244, 2, 2),
                                child: Icon(
                                  Icons.delete,
                                  color: Colors.white,
                                  size: 18,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text('DELETE', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Container(
                                height: 40,
                                child: TextField(
                                  onChanged: (val) {
                                    _computationController.computationTaxPaidList[index + 1].tpName = val;
                                  },

                                  // print("taxpaid ----------------"+_computationController.computationTaxPaidList[index].tpName);
                                  controller: taxespaidnameController[index + 1],
                                  decoration: InputDecoration(
                                      labelText: "Name",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Container(
                                height: 40,
                                child: TextField(
                                  onChanged: (val) {
                                    _computationController.computationTaxPaidList[index + 1].tpTan = val;
                                  },
                                  controller: taxespaidtanController[index + 1],
                                  decoration: InputDecoration(
                                      labelText: "Tan",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Container(
                                height: 40,
                                child: TextField(
                                  onChanged: (val) {
                                    _computationController.computationTaxPaidList[index + 1].tpToi = val;
                                  },
                                  controller: taxespaidtoiController[index + 1],
                                  decoration: InputDecoration(
                                      labelText: "Type Of Income",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Container(
                                height: 40,
                                child: TextField(
                                  onChanged: (val) {
                                    _computationController.computationTaxPaidList[index + 1].tpDy = val;
                                  },
                                  controller: taxespaiddyController[index + 1],
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                      labelText: "Deducted Year",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Container(
                                height: 40,
                                child: TextField(
                                  onChanged: (val) {
                                    _computationController.computationTaxPaidList[index + 1].tpBrought = val;
                                  },
                                  controller: taxespaidbroughtController[index + 1],
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                      labelText: "Brought Forward TDS",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Container(
                                height: 40,
                                child: TextField(
                                  onChanged: (val) {
                                    _computationController.computationTaxPaidList[index + 1].tpTds = val;
                                  },
                                  keyboardType: TextInputType.number,
                                  controller: taxespaidtdsController[index + 1],
                                  decoration: InputDecoration(
                                      labelText: "TDS Amount Claimed",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Container(
                                height: 40,
                                child: TextField(
                                  onChanged: (val) {
                                    _computationController.computationTaxPaidList[index + 1].tpIncomefw = val;
                                  },
                                  controller: taxespaidincomefwController[index + 1],
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                      labelText: "Income For Which TDS Paid",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Container(
                                height: 40,
                                child: TextField(
                                  onChanged: (val) {
                                    _computationController.computationTaxPaidList[index + 1].tpBalance = val;
                                  },
                                  controller: taxespaidbalanceController[index + 1],
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                      labelText: "Balance",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ]),
                    );
                  }),
                ),
              ),
              /*Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text('Total', textAlign: TextAlign.center, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                    ],
                  ),
                  Flexible(
                    child: Container(
                      height: 40,
                      child: SizedBox(
                        width: 185,
                        child: TextField(
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ),
                ],
              ),*/
              SizedBox(height: 70,)
            ],
          ),
        ),
      ),
    );
  }
}
