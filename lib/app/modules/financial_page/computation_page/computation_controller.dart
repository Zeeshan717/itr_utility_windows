import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:accountx/app/model/computation_data_to_json_model.dart';
import 'package:accountx/app/model/computation_head_model.dart';
import 'package:accountx/app/model/user_client_model.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_data.dart';
import 'package:accountx/app/modules/financial_page/computation_page/newpdf_api.dart';
import 'package:accountx/app/modules/financial_page/computation_page/pdf_computation_api.dart';
import 'package:accountx/app/modules/taxCalPage/taxController.dart';
import 'package:accountx/app/services/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get_storage/get_storage.dart';
import 'package:share_plus/share_plus.dart';

import '../../../model/computation_bank_model.dart';
import '../../../model/computation_tax_paid_model.dart';

// ignore: deprecated_member_use
class ComputationController extends GetxController with SingleGetTickerProviderMixin {
  GetStorage box = GetStorage();
  var radioValue = "Normal Disability".obs;
  var politicalradio = "50 %".obs;
  var ageValue = "0 to 60".obs;
  var controller = Get.put(TaxController());
  String dropdownValue = "ITR-1";
  String dropdownValue2 = "Individual";
  String dropdownValue3 = "N/A";

  String tcdropdownValue2 = "Individual";
  String tcdropdownValue3 = "N/A";

  void setRadioValue(var value) {
    radioValue.value = value;
  }

  void setpolitical(var value) {
    politicalradio.value = value;
  }

  void selectAge(var value) {
    ageValue.value = value;
  }

  TabController? tabController;

  List<ComputationTaxPaidModel> computationTaxPaidList = [];
  List<ComputationBankModel> computationBankList = [];

  List<ComputationHeadModel> computationHeadListSalary = [];
  List<ComputationHeadModel> computationHeadListHp = [];
  List<ComputationHeadModel> computationHeadListBp = [];
  List<ComputationHeadModel> computationHeadListCg = [];
  List<ComputationHeadModel> computationHeadListOtherIncome = [];
  List<ComputationHeadModel> computationHeadListTotalDeduction = [];

  // General Details Controller **Start**

  TextEditingController gdNameController = TextEditingController();
  TextEditingController gdAddressController = TextEditingController();
  TextEditingController gdPhoneNoController = TextEditingController();
  TextEditingController gdEmailController = TextEditingController();

  TextEditingController gdPanController = TextEditingController();
  TextEditingController gdFatherNameController = TextEditingController();
  TextEditingController gdBankAcNumberController = TextEditingController();
  TextEditingController gdIfscCodeController = TextEditingController();
  TextEditingController gdEFillingStatusController = TextEditingController();
  TextEditingController gdAadhaarCardController = TextEditingController();

  /// General Details controlle **End** ///

  TextEditingController salaryController = TextEditingController();
  TextEditingController hpController = TextEditingController();
  TextEditingController bpController = TextEditingController();
  TextEditingController cgController = TextEditingController();
  TextEditingController otherIncomeController = TextEditingController();
  TextEditingController grossTotalIncomeController = TextEditingController();

  //income summary
  TextEditingController totalDeductionController = TextEditingController();
  TextEditingController totalIncomeController = TextEditingController();
  TextEditingController totalTaxPayableController = TextEditingController();
  TextEditingController tdsController = TextEditingController();
  TextEditingController refundController = TextEditingController();
  TextEditingController newAmountController = TextEditingController();
  TextEditingController newRemarkController = TextEditingController();

  TextEditingController gstInController = TextEditingController();
  TextEditingController gstGrossController = TextEditingController();

// Alert Dialog
  TextEditingController dialogNameController = TextEditingController();
  TextEditingController dialogAmountController = TextEditingController();
  TextEditingController amount80GController = TextEditingController();
  TextEditingController remark80GController = TextEditingController();
  //Taxespaid controllers
  TextEditingController tpnameController = TextEditingController();
  TextEditingController tptanController = TextEditingController();
  TextEditingController tptoiController = TextEditingController();
  TextEditingController tpdyController = TextEditingController();
  TextEditingController tpbroughtController = TextEditingController();
  TextEditingController tptdsController = TextEditingController();
  TextEditingController tpincomefwController = TextEditingController();
  TextEditingController tpbalanceController = TextEditingController();

  //computation bank controllers
  TextEditingController cbNameController = TextEditingController();
  TextEditingController cbifscController = TextEditingController();
  TextEditingController cbaccountController = TextEditingController();

  TextEditingController taxCreditController = TextEditingController();
  TextEditingController healthController = TextEditingController();
  TextEditingController totTaxController = TextEditingController();

  //total computation controllers from api
  TextEditingController ittdsController = TextEditingController();
  TextEditingController itrefundController = TextEditingController();
  TextEditingController itCreditController = TextEditingController();
  TextEditingController ithealthController = TextEditingController();
  TextEditingController ittotTaxController = TextEditingController();
  TextEditingController itGstinController = TextEditingController();
  TextEditingController itGstGrossController = TextEditingController();

  //total controller for gen Detatils

  TextEditingController tcPanController = TextEditingController();
  TextEditingController tcFatherNameController = TextEditingController();

  TextEditingController tcBankNumberController = TextEditingController();
  TextEditingController tcIfscCodeController = TextEditingController();
  TextEditingController tcEFillingController = TextEditingController();
  TextEditingController tcAadharCardNumber = TextEditingController();
  TextEditingController tcPhoneController = TextEditingController();
  TextEditingController tcEmailController = TextEditingController();

  @override
  void onInit() {
    tabController = TabController(length: 5, vsync: this);
    super.onInit();
  }

  void getTotalSalary() {
    var tsalary = 0.0;
    computationHeadListSalary.forEach((element) {
      tsalary += double.parse(element.value.isEmpty ? "0.0" : element.value);
    });
    salaryController.text = tsalary.toString();
  }

  void getTotalHp() {
    var tHp = 0.0;
    computationHeadListHp.forEach((element) {
      tHp += double.parse(element.value.isEmpty ? "0.0" : element.value);
    });
    hpController.text = tHp.toString();
  }

  void getTotalBp() {
    var tBp = 0.0;
    computationHeadListBp.forEach((element) {
      tBp += double.parse(element.value.isEmpty ? "0.0" : element.value);
    });
    bpController.text = tBp.toString();
  }

  void getTotalCg() {
    var tCg = 0.0;
    computationHeadListCg.forEach((element) {
      tCg += double.parse(element.value.isEmpty ? "0.0" : element.value);
    });
    cgController.text = tCg.toString();
  }

  void getTotalOtherIncome() {
    var tOtherIncome = 0.0;
    computationHeadListOtherIncome.forEach((element) {
      tOtherIncome += double.parse(element.value.isEmpty ? "0.0" : element.value);
    });
    otherIncomeController.text = tOtherIncome.toString();
  }

  void getTotalGrossIncome() {
    var salary = double.parse(salaryController.text.isEmpty ? "0.0" : salaryController.text);
    var hp = double.parse(hpController.text.isEmpty ? "0.0" : hpController.text);
    var bp = double.parse(bpController.text.isEmpty ? "0.0" : bpController.text);
    var cg = double.parse(cgController.text.isEmpty ? "0.0" : cgController.text);
    var otherIncome = double.parse(otherIncomeController.text.isEmpty ? "0.0" : otherIncomeController.text);
    var totalGrossIncome = salary + hp + bp + cg + otherIncome;
    grossTotalIncomeController.text = totalGrossIncome.toString();
    getTotalIncome();
  }

  /*void getTotalDeduction() {
    for(int i=0;i<computationHeadListTotalDeduction.length;i++){
      if(computationHeadListTotalDeduction[i].value=="80C" ||computationHeadListTotalDeduction[i].value=="80CCC"
      ||computationHeadListTotalDeduction[i].value=="80CCD(1)" || computationHeadListTotalDeduction[i].value=="80CCD(1B)"){
      }else{
        var tDeduction = 0.0;
          tDeduction += double.parse(computationHeadListTotalDeduction[i].value.isEmpty ? "0.0" : computationHeadListTotalDeduction[i].value);

        totalDeductionController.text = tDeduction.toString();
      }
    }


  }*/

  void getTotalIncome() {
    print("working");
    var grossTotalIncome = double.parse(grossTotalIncomeController.text.isEmpty ? "0.0" : grossTotalIncomeController.text);
    var totalDeduction = double.parse(totalDeductionController.text.isEmpty ? "0.0" : totalDeductionController.text);
    var totalIncome = grossTotalIncome - totalDeduction;
    totalIncomeController.text = totalIncome.toStringAsFixed(0);

    /* totalTaxOld(double.parse(grossTotalIncomeController.text.isEmpty ? "0.0" : grossTotalIncomeController.text),
        double.parse(totalDeductionController.text.isEmpty ? "0.0" : totalDeductionController.text), 1);

    GetTotalRefund();*/
  }

  void addHead(String headName) {
    switch (headName) {
      case "Salary":
        {
          computationHeadListSalary.add(ComputationHeadModel(headName, "", "", ""));
        }
        break;

      case "Hp":
        {
          computationHeadListHp.add(ComputationHeadModel(headName, "", "", ""));
        }
        break;
      case "Bp":
        {
          computationHeadListBp.add(ComputationHeadModel(headName, "", "", ""));
        }
        break;
      case "Cg":
        {
          computationHeadListCg.add(ComputationHeadModel(headName, "", "", ""));
        }
        break;
      case "OtherIncome":
        {
          computationHeadListOtherIncome.add(ComputationHeadModel(headName, "", "", ""));
        }
        break;

      case "TotalDeduction":
        {
          computationHeadListTotalDeduction.add(ComputationHeadModel(headName, "", "", ""));
        }
        break;

      default:
        {
//statements;
        }
        break;
    }
  }

  void addtpkey(String tpkey) {
    computationTaxPaidList.add(ComputationTaxPaidModel(tpkey, "", "", "", "", "", "", "", ""));
  }

  void GetTotalRefund() {
    var totalTax = double.parse(totalTaxPayableController.text.isEmpty ? "0.0" : totalTaxPayableController.text);
    var taxesPaid = double.parse(tdsController.text.isEmpty ? "0.0" : tdsController.text);

    refundController.text = (taxesPaid - totalTax).toStringAsFixed(2);
  }

  // ------------Total Tax Payable Function---------

  double totalTaxOld(double totalInc, double totalDeduction, int age) {
    var tax = 0.0;
    // age > 0 && age <= 60
    if (age == 1) {
      double taxableIncome = totalInc - totalDeduction;

      print(taxableIncome);
      if (taxableIncome <= 250000) {
        tax = 0.0;
      }
      if (taxableIncome > 250000 && taxableIncome <= 500000) {
        var a1 = taxableIncome - 250000;
        tax = a1 * 5 / 100;
        tax = tax + tax * 4 / 100;
      }
      if (taxableIncome > 500000 && taxableIncome <= 1000000) {
        var a1 = taxableIncome - 500000;
        tax = 250000 * 5 / 100 + a1 * 20 / 100;
        tax = tax + tax * 4 / 100;
      }
      if (taxableIncome > 1000000) {
        var a1 = taxableIncome - 1000000;
        tax = 250000 * 5 / 100 + 500000 * 20 / 100 + a1 * 30 / 100;
        tax = tax + tax * 4 / 100;
      }
    } else if (age == 2) //age > 60 && age <= 80
    {
      double taxableIncome = totalInc - totalDeduction;
      if (taxableIncome <= 300000) {
        tax = 0.0;
      }
      if (taxableIncome > 300000 && taxableIncome <= 500000) {
        var a1 = taxableIncome - 300000;
        tax = a1 * 5 / 100;
        tax = tax + tax * 4 / 100;
      }
      if (taxableIncome > 500000 && taxableIncome <= 1000000) {
        var a1 = taxableIncome - 500000;
        tax = 200000 * 5 / 100 + a1 * 20 / 100;
        tax = tax + tax * 4 / 100;
      }
      if (taxableIncome > 1000000) {
        var a1 = taxableIncome - 1000000;
        tax = 200000 * 5 / 100 + 500000 * 20 / 100 + a1 * 30 / 100;
        tax = tax + tax * 4 / 100;
      }
    } else if (age == 3) {
      double taxableIncome = totalInc - totalDeduction;
      if (taxableIncome <= 500000) {
        tax = 0.0;
      }
      if (taxableIncome > 500000 && taxableIncome <= 1000000) {
        var a1 = taxableIncome - 500000;
        tax = a1 * 20 / 100;
        tax = tax + tax * 4 / 100;
      }
      if (taxableIncome > 1000000) {
        var a1 = taxableIncome - 1000000;
        tax = 500000 * 20 / 100 + a1 * 30 / 100;
        tax = tax + tax * 4 / 100;
      }
    }
    double taxableIncome = totalInc - totalDeduction;
    totalTaxPayableController.text = (taxableIncome <= 500000 ? 0.0 : tax).toString();
    return taxableIncome <= 500000 ? 0.0 : tax;
  }

  void addbankname(String cbname) {
    computationBankList.add(ComputationBankModel(cbname, "", "", ""));
  }

  void taxcredit() {
    if (double.parse(totalIncomeController.text) >= 500000) {
      taxCreditController.text = "12500";
    } else if (double.parse(totalIncomeController.text) > 250000 && double.parse(totalIncomeController.text) < 500000) {
      taxCreditController.text = (double.parse(totalIncomeController.text) * 5 / 100).toString();
    }
  }

  void health() {
    healthController.text = (double.parse(totalTaxPayableController.text) * 4 / 100).toString();

    //print("healthhhhhhh");
  }

  void totTax() {
    totTaxController.text = (double.parse(totalTaxPayableController.text) + double.parse(healthController.text)).toString();
  }

  //total computation
  void credit() {
    if (double.parse(controller.taxtaxablecontroller.text) >= 500000) {
      itCreditController.text = "12500";
    } else if (double.parse(controller.taxtaxablecontroller.text) > 250000 && double.parse(controller.taxtaxablecontroller.text) < 500000) {
      itCreditController.text = (double.parse(controller.taxtaxablecontroller.text) * 5 / 100).toString();
    }
    else{
      itCreditController.text="0.0";
    }
  }

  void refund() {
    itrefundController.text = (double.parse(ittdsController.text) - double.parse(controller.taxtotaltaxpayablecontroller.text)).toString();
  }

  void ithealth() {
    ithealthController.text = (double.parse(controller.taxtotaltaxpayablecontroller.text) * 4 / 100).toString();
  }

  void ittotTax() {
    ittotTaxController.text = (double.parse(controller.taxtotaltaxpayablecontroller.text) + double.parse(ithealthController.text)).toString();
  }

// total computation page Apis

  void getUserDetailsApiCall() async {
    var loginValue = box.read('loginValue');
    var clientId1 = box.read("client_id");
    EasyLoading.show(status: "");
    await Api.UserClientListApi(
            loginValue['admin_id'], loginValue['type'], loginValue['imei_no'], loginValue['user_role'], loginValue['manager_detail'])
        .then((value) {
      UserClientList newValue = value;
      if (newValue.success == 1) {
        newValue.clientArray.forEach((element) {
          if (element.clientId.toString() == clientId1.toString()) {
            EasyLoading.dismiss();
            tcPanController.text = element.panNo;
            tcFatherNameController.text = element.proprietorFathersName;
            tcBankNumberController.text = element.accountNo;
            tcIfscCodeController.text = element.ifscNo;
            tcAadharCardNumber.text = element.accountNo;
            itGstinController.text = element.gstinNo;

            tcPhoneController.text = element.phoneNo;
            tcEmailController.text = element.clientEmail;
          }
          EasyLoading.dismiss();
        });
      } else {
        EasyLoading.dismiss();
      }
    });
  }

  void computationjsonApiCall(BuildContext context) async {
    var clientId1 = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    String computationJsonData = getJsonComputationObject();
    EasyLoading.show(status: "");
    await Api.jsonComputationApi(clientId1, financialYear, adminId, computationJsonData).then((value) {
      if (value["response_status"].toString() == "1") {
        EasyLoading.dismiss();
// print("workinggg --=============");
        showDialog(context: context, builder:(context){
          return AlertDialog(
            backgroundColor: Colors.transparent,
            insetPadding: EdgeInsets.symmetric(horizontal: 10,vertical: 100),
            content: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20)
              ),
              width: MediaQuery.of(context).size.width,
              height: 240,

              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(onPressed: () {
                        Get.back();
                      },icon: Icon(Icons.clear,color: Colors.red,),),
                    ],
                  ),
                  SizedBox(height: 5,),
                  Icon(
                    Icons.check_circle_outline_sharp,
                    color: Colors.green,
                    size: 50,
                  ),
                  SizedBox(height: 5,),
                  Text(
                    value["message"],
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: 100,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Color(0xff1739E4)
                              ),
                              onPressed: () async {
                                ComputaionData cd = ComputaionData();
                                List<Map> genDetails = cd.getPdfGd();
                                final pdfFile = await PdfComputationApi.generate(genDetails);

                                NewPdfApi.openFile(pdfFile);
                                Get.back();
                              },
                              child: Text("Download"))),
                      SizedBox(width: 20,),
                      Platform.isWindows?Container():SizedBox(
                          width: 100,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Color(0xff1739E4)
                              ),
                              onPressed: () async {

                                ComputaionData cd = ComputaionData();
                                List<Map> genDetails = cd.getPdfGd();
                                final pdfFile = await PdfComputationApi.generate(genDetails);

                                Share.shareFiles([pdfFile.path.toString()]);
                                Get.back();
                              },
                              child: Text("Share"))),
                    ],
                  )
                ],
              ),
            ),
          );
        });
        // Get.defaultDialog(
        //     title: " ",
        //     barrierDismissible: false,
        //     content: SizedBox(
        //       width: 400,
        //       child: Column(
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         children: [
        //           Text(
        //             "Data saved \nSuccessfully",
        //             style: TextStyle(fontSize: 20),
        //           ),
        //           SizedBox(
        //             height: 20,
        //           ),
        //           Icon(
        //             Icons.check_circle_outline_sharp,
        //             color: Colors.green,
        //             size: 50,
        //           ),
        //         ],
        //       ),
        //     ),
        //     actions: [
        //       Row(
        //         mainAxisAlignment: MainAxisAlignment.center,
        //         children: [
        //           SizedBox(
        //               width: 100,
        //               child: ElevatedButton(
        //                   style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
        //                   onPressed: () async{
        //
        //                     ComputaionData cd = ComputaionData();
        //                     List<Map> genDetails = cd.getPdfGd();
        //                     final pdfFile = await PdfComputationApi.generate(genDetails);
        //
        //                     NewPdfApi.openFile(pdfFile);
        //
        //                     Get.back();
        //                     Get.back();
        //                   },
        //                   child: Text("Download"))),
        //           SizedBox(
        //             width: 10,
        //           ),
        //           SizedBox(width: 100, child: ElevatedButton(
        //               style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
        //               onPressed: () async{
        //                 ComputaionData cd = ComputaionData();
        //                 List<Map> genDetails = cd.getPdfGd();
        //                 final pdfFile = await PdfComputationApi.generate(genDetails);
        //
        //                 Share.shareFiles([pdfFile.path.toString()]);
        //               }, child: Text("Share"))),
        //         ],
        //       ),
        //       SizedBox(
        //         height: 1,
        //       )
        //     ]);
      } else {

        EasyLoading.dismiss();
        print("not  workinggg --=============");


        showDialog(context: context, builder:(context){
          return AlertDialog(
            backgroundColor: Colors.transparent,
            insetPadding: EdgeInsets.symmetric(horizontal: 10,vertical: 100),
            content: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20)
              ),
              width: MediaQuery.of(context).size.width,
              height: 240,

              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(onPressed: () {
                        Get.back();
                      },icon: Icon(Icons.clear,color: Colors.red,),),
                    ],
                  ),
                  SizedBox(height: 5,),
                  Icon(
                    Icons.check_circle_outline_sharp,
                    color: Colors.green,
                    size: 50,
                  ),
                  SizedBox(height: 5,),
                  Text(
                    value["message"],
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: 100,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Color(0xff1739E4)
                              ),
                              onPressed: () async {
                                ComputaionData cd = ComputaionData();
                                List<Map> genDetails = cd.getPdfGd();
                                final pdfFile = await PdfComputationApi.generate(genDetails);

                                NewPdfApi.openFile(pdfFile);
                                Get.back();
                              },
                              child: Text("Download"))),
                      SizedBox(width: 20,),
                      Platform.isWindows?Container():SizedBox(
                          width: 100,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Color(0xff1739E4)
                              ),
                              onPressed: () async {

                                ComputaionData cd = ComputaionData();
                                List<Map> genDetails = cd.getPdfGd();
                                final pdfFile = await PdfComputationApi.generate(genDetails);

                                Share.shareFiles([pdfFile.path.toString()]);
                                Get.back();
                              },
                              child: Text("Share"))),
                    ],
                  )
                ],
              ),
            ),
          );
        });

       /* Get.defaultDialog(

            title: " ",
            barrierDismissible: true,
            content: SizedBox(
              width: 400,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    value["message"],
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Icon(
                    Icons.check_circle_outline_sharp,
                    color: Colors.green,
                    size: 50,
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                      width: 100,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
                          onPressed: () async {
                            ComputaionData cd = ComputaionData();
                            List<Map> genDetails = cd.getPdfGd();
                            final pdfFile = await PdfComputationApi.generate(genDetails);

                            NewPdfApi.openFile(pdfFile);

                            Get.back();
                          },
                          child: Text("Download"))),
                  SizedBox(
                    width: 10,
                  ),
                  SizedBox(
                      width: 100,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
                          onPressed: () async {
                            ComputaionData cd = ComputaionData();
                            List<Map> genDetails = cd.getPdfGd();
                            final pdfFile = await PdfComputationApi.generate(genDetails);

                            Share.shareFiles([pdfFile.path.toString()]);
      },
                          child: Text("Share"))),
                ],
              ),
              SizedBox(
                height: 1,
              )
            ]);*/
      }
    });
  }

  String getJsonComputationObject() {
    List<List<String>> jsonSalaryList = [];

    computationHeadListSalary.forEach((element) {
      List<String> jsonSalaryInsideList = [];
      jsonSalaryInsideList.add(element.name);
      jsonSalaryInsideList.add(element.value);
      jsonSalaryList.add(jsonSalaryInsideList);
    });

    List<List<String>> jsonHpList = [];

    computationHeadListHp.forEach((element) {
      List<String> jsonHpInsideList = [];
      jsonHpInsideList.add(element.name);
      jsonHpInsideList.add(element.value);
      jsonHpList.add(jsonHpInsideList);
    });

    List<List<String>> jsonBpList = [];

    computationHeadListBp.forEach((element) {
      List<String> jsonBpInsideList = [];
      jsonBpInsideList.add(element.name);
      jsonBpInsideList.add(element.value);
      jsonBpList.add(jsonBpInsideList);
    });

    List<List<String>> jsonCgList = [];

    computationHeadListCg.forEach((element) {
      List<String> jsonCgInsideList = [];
      jsonCgInsideList.add(element.name);
      jsonCgInsideList.add(element.value);
      jsonCgList.add(jsonCgInsideList);
    });

    List<List<String>> jsonOiList = [];

    computationHeadListOtherIncome.forEach((element) {
      List<String> jsonOiInsideList = [];
      jsonOiInsideList.add(element.name);
      jsonOiInsideList.add(element.value);
      jsonOiList.add(jsonOiInsideList);
    });

    List<List<String>> jsonTdList = [];

    computationHeadListTotalDeduction.forEach((element) {
      List<String> jsonTdInsideList = [];
      jsonTdInsideList.add(element.name);
      jsonTdInsideList.add(element.value);
      jsonTdList.add(jsonTdInsideList);
    });

    List<String> jsonIncomeTaxList = [
      grossTotalIncomeController.text,
      totalDeductionController.text,
      totalIncomeController.text,
      taxCreditController.text,
      totalTaxPayableController.text,
      healthController.text,
      totTaxController.text,
      tdsController.text,
      refundController.text
    ];

    List<List<String>> jsonTaxPaidList = [];

    computationTaxPaidList.forEach((element) {
      List<String> jsonTaxPaidInsideList = [];
      jsonTaxPaidInsideList.add(element.tpName);
      jsonTaxPaidInsideList.add(element.tpTan);
      jsonTaxPaidInsideList.add(element.tpToi);
      jsonTaxPaidInsideList.add(element.tpDy);
      jsonTaxPaidInsideList.add(element.tpBrought);
      jsonTaxPaidInsideList.add(element.tpTds);
      jsonTaxPaidInsideList.add(element.tpIncomefw);
      jsonTaxPaidInsideList.add(element.tpBalance);

      jsonTaxPaidList.add(jsonTaxPaidInsideList);
    });

    List<List<String>> jsonBankList = [];

    computationBankList.forEach((element) {
      List<String> jsonBankInsideList = [];
      jsonBankInsideList.add(element.cbName);
      jsonBankInsideList.add(element.cbifsc);
      jsonBankInsideList.add(element.cbaccount);

      jsonBankList.add(jsonBankInsideList);
    });

    ComputationDataToJson cdj = ComputationDataToJson(
        type: "N/A",
        address: gdAddressController.text,
        contact: gdPhoneNoController.text,
        email: gdEmailController.text,
        pan: gdPanController.text,
        father: gdFatherNameController.text,
        account: gdBankAcNumberController.text,
        fileStatus: gdEFillingStatusController.text,
        cstatus: dropdownValue2,
        gender: dropdownValue3,
        ifsc: gdIfscCodeController.text,
        aadhar: gdAadhaarCardController.text,
        gtotal: "N/A",
        rBefore: "N/A",
        rAfter: "N/A",
        salary: IncomeType(total: salaryController.text, list: jsonSalaryList),
        businessProfession: IncomeType(total: bpController.text, list: jsonBpList),
        houseIncome: IncomeType(total: hpController.text, list: jsonHpList),
        capitalGain: IncomeType(total: cgController.text, list: jsonCgList),
        otherIncome: IncomeType(total: otherIncomeController.text, list: jsonOiList),
        totalDeduction: IncomeType(total: totalDeductionController.text, list: jsonTdList),
        incomeTax: jsonIncomeTaxList,
        tds: jsonTaxPaidList,
        tdsTotal: "N/A",
        bank: jsonBankList);

    var map = cdj.toJson();
    String encodedJson = jsonEncode(map);
    return encodedJson;
  }
}
