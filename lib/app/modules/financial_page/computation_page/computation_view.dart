import 'package:accountx/app/modules/calculators_page/calculators_controller.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_bank_ac.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_gen_details.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_income_tax.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_tax_summary.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_taxes_paid.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ComputationView extends GetResponsiveView<ComputationController> {
  ComputationView({Key? key})
      : super(key: key, settings: const ResponsiveScreenSettings(desktopChangePoint: 800, tabletChangePoint: 700, watchChangePoint: 300));
  var _computationController = Get.put(ComputationController());
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: const Color(0xff0B28BE),
            bottom: TabBar(
              isScrollable: true,
              indicatorColor: Colors.white,
              controller: _computationController.tabController,
              tabs: const [
                Tab(
                  text: "General Details",
                ),
                Tab(
                  text: "Income Summary ",
                ),
                Tab(
                  text: "Income Tax",
                ),
                Tab(
                  text: "Taxes paid",
                ),
                Tab(
                  text: "Bank Account Details",
                ),
              ],
            ),
            centerTitle: true,
            title: const Text(
              "Computation",
              style: TextStyle(fontSize: 16),
            ),
          ),
          body: TabBarView(
            controller: _computationController.tabController,
            children: [GenDetails(), const TaxSummary(), ComputationIncomeTax(), TaxPaid(), BankDetails()],
          ),
        ));
  }
}















// import 'package:flutter/material.dart';
// import 'package:get_storage/get_storage.dart';

// class ComputationView extends StatefulWidget {
//   const ComputationView({Key? key}) : super(key: key);

//   @override
//   State<ComputationView> createState() => _ComputationViewState();
// }

// class _ComputationViewState extends State<ComputationView> {
//   GetStorage storage = GetStorage();

//   String dropdownValue = "ITR-1";
//   String dropdownValue2 = "Individual";
//   String? dropdownValue3;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: Text(
//           "Computation",
//           style: TextStyle(fontSize: 16, color: Colors.white),
//         ),
//         backgroundColor: Color(0xff1739E4),
//       ),
//       body: 
//       Container(
//         width: MediaQuery.of(context).size.width,
//         height: MediaQuery.of(context).size.height,
//         color: Colors.white,
//         child: SingleChildScrollView(
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 20),
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.start,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Text("Name: " + storage.read("client_name").toString()),
//                     Text("Address: " + storage.read("distributor_address").toString()),
//                   ],
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [Text("Phone No: " + storage.read("phone_no").toString()), Text("Email: ")],
//                 ),
//                 //Computation of Income
//                 SizedBox(
//                   height: 20,
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   children: [
//                     Text(
//                       "Computation of Income",
//                       style: TextStyle(fontSize: 20),
//                     ),
//                     SizedBox(
//                       width: 10,
//                     ),
//                     /*   Text("("),
//                     Container(
//                       child:DropdownButton<String>(
//                         value: dropdownValue,
//                         icon: const Icon(Icons.arrow_downward),
//                         elevation: 16,
//                         style: const TextStyle(color: Colors.deepPurple),
//                         underline: Container(
//                           height: 2,
//                           color: Colors.deepPurpleAccent,
//                         ),
//                         onChanged: (String? newValue) {
//                           setState(() {
//                             dropdownValue = newValue!;
//                           });
//                         },
//                         items: <String>["ITR-1","ITR-2","ITR-3","ITR-4","ITR-5","ITR-6"]
//                             .map<DropdownMenuItem<String>>((String value) {
//                           return DropdownMenuItem<String>(
//                             value: value,
//                             child: Text(value),
//                           );
//                         }).toList(),
//                       ),
//                     ),
//                     Text(")")*/
//                   ],
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 //1
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Flexible(
//                       child: Container(
//                         height: 40,
//                         child: TextField(
//                           decoration: InputDecoration(
//                               labelText: "PAN",
//                               border:
//                                   OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                         ),
//                       ),
//                     ),
//                     SizedBox(
//                       width: 5,
//                     ),
//                     Flexible(
//                       child: Container(
//                         padding: EdgeInsets.symmetric(horizontal: 10),
//                         decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: Colors.grey, width: 1)),
//                         height: 40,
//                         child: DropdownButton<String>(
//                           isExpanded: true,
//                           value: dropdownValue2,
//                           icon: const Icon(Icons.arrow_drop_down),
//                           elevation: 16,
//                           style: const TextStyle(color: Colors.black),
//                           underline: SizedBox(),
//                           onChanged: (String? newValue) {
//                             setState(() {
//                               dropdownValue2 = newValue!;
//                             });
//                             print("Status: ------- " + newValue.toString());
//                           },
//                           items: <String>['Individual', 'Firm', 'Private Ltd. Company', 'Public Ltd. Company', 'HUF']
//                               .map<DropdownMenuItem<String>>((String value) {
//                             return DropdownMenuItem<String>(
//                               value: value,
//                               child: Text(value),
//                             );
//                           }).toList(),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 //2
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Flexible(
//                       child: Container(
//                         height: 40,
//                         child: TextField(
//                           decoration: InputDecoration(
//                               labelText: "Father's Name",
//                               border:
//                                   OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                         ),
//                       ),
//                     ),
//                     SizedBox(
//                       width: 5,
//                     ),
//                     Flexible(
//                       child: Container(
//                         padding: EdgeInsets.symmetric(horizontal: 10),
//                         decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: Colors.grey, width: 1)),
//                         height: 40,
//                         child: DropdownButton<String>(
//                           hint: Text("Select Gender"),
//                           isExpanded: true,
//                           value: dropdownValue3,
//                           icon: const Icon(Icons.arrow_drop_down),
//                           elevation: 16,
//                           style: const TextStyle(color: Colors.black),
//                           underline: SizedBox(),
//                           onChanged: (String? newValue) {
//                             setState(() {
//                               dropdownValue3 = newValue!;
//                             });
//                             print("Status: ------- " + newValue.toString());
//                           },
//                           items: <String>['Male', 'Female', 'Other'].map<DropdownMenuItem<String>>((String value) {
//                             return DropdownMenuItem<String>(
//                               value: value,
//                               child: Text(value),
//                             );
//                           }).toList(),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 //3
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Flexible(
//                       child: Container(
//                         height: 40,
//                         child: TextField(
//                           decoration: InputDecoration(
//                               labelText: "Bank A/C Number",
//                               border:
//                                   OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                         ),
//                       ),
//                     ),
//                     SizedBox(
//                       width: 5,
//                     ),
//                     Flexible(
//                       child: Container(
//                         height: 40,
//                         child: TextField(
//                           decoration: InputDecoration(
//                               labelText: "IFSC Code",
//                               border:
//                                   OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),

//                 SizedBox(
//                   height: 10,
//                 ),
//                 //4
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Flexible(
//                       child: Container(
//                         height: 40,
//                         child: TextField(
//                           decoration: InputDecoration(
//                               labelText: "E-Filing Status",
//                               border:
//                                   OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                         ),
//                       ),
//                     ),
//                     SizedBox(
//                       width: 5,
//                     ),
//                     Flexible(
//                       child: Container(
//                         height: 40,
//                         child: TextField(
//                           decoration: InputDecoration(
//                               labelText: "Aadhaar Card Number",
//                               border:
//                                   OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),

//                 SizedBox(
//                   height: 30,
//                 ),
//                 //Tax Summary (Amount in Rs)
//                 Center(
//                     child: Text(
//                   "Tax Summary (Amount in Rs)",
//                   style: TextStyle(fontSize: 20),
//                   textAlign: TextAlign.center,
//                 )),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     //1
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: Text("Salary"),
//                           ),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),

//                     //2
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Income from House Property"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //3
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Business and Profession"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //4
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Capital Gain"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //5

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Other Income"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //6
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text(
//                             "Gross Total Income",
//                             style: TextStyle(color: Colors.cyan, fontWeight: FontWeight.bold),
//                           ),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               enabled: false,
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //7
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Total Deduction"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //8
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Total Income (Taxable)"),
//                         ),
//                         Flexible(
//                           child: Text(
//                             "Rounded off from ₹0 as per Section 288A",
//                             style: TextStyle(fontSize: 10),
//                             textAlign: TextAlign.center,
//                           ),
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               enabled: false,
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //9
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Total Tax Payable"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //10
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Less : Taxes Paid"),
//                         ),
//                         Flexible(child: Text("TDS")),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //10
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Refund"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     Text(
//                       "Taxes are applicable as per normal provision Please refer Annexure for details",
//                       style: TextStyle(color: Colors.red),
//                     ),

//                     //Income Tax

//                     SizedBox(
//                       height: 10,
//                     ),

//                     Center(
//                         child: Text(
//                       "Income Tax",
//                       style: TextStyle(fontSize: 20),
//                     )),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //1
//                     SizedBox(
//                       height: 10,
//                     ),

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Total Income"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               enabled: false,
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //2
//                     SizedBox(
//                       height: 10,
//                     ),

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Basic Exemption"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //3
//                     SizedBox(
//                       height: 10,
//                     ),

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Income Tax (Before Tax Credit U/S 87A)"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //4
//                     SizedBox(
//                       height: 10,
//                     ),

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Tax Credit U/S 87A"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //5
//                     SizedBox(
//                       height: 10,
//                     ),

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Income Tax"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //6
//                     SizedBox(
//                       height: 10,
//                     ),

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Health and Education Cess (@4% GIT)"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //7
//                     SizedBox(
//                       height: 10,
//                     ),

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Total Tax"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //8
//                     SizedBox(
//                       height: 10,
//                     ),

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Total Tax"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               enabled: false,
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //9
//                     SizedBox(
//                       height: 10,
//                     ),

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Tax Paid"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               enabled: false,
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),

//                     //10
//                     SizedBox(
//                       height: 10,
//                     ),

//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Flexible(
//                           child: Text("Refund"),
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Flexible(
//                           child: Container(
//                             height: 40,
//                             child: TextField(
//                               enabled: false,
//                               decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),

//                     SizedBox(
//                       height: 10,
//                     ),
//                   ],
//                 )
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
