import 'dart:ffi';

import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_data.dart';
import 'package:accountx/app/modules/financial_page/computation_page/newpdf_api.dart';
import 'package:accountx/app/modules/financial_page/computation_page/pdf_computation_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class ComputaionPreviewPage extends StatefulWidget {
  const ComputaionPreviewPage({Key? key}) : super(key: key);

  @override
  State<ComputaionPreviewPage> createState() => _ComputaionPreviewPageState();
}

class _ComputaionPreviewPageState extends State<ComputaionPreviewPage> {
  var _previewController = Get.put(ComputationController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Preview Page"),
        backgroundColor: Color(0xff0B28BE),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(children: [
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text("General Details"),
                    SizedBox(
                      height: 15,
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Name: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gdNameController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Address: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gdAddressController.text),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Phone No: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gdPhoneNoController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Email: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gdEmailController.text),
                              ],
                            ),
                          ),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "PAN : ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gdPanController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Individual: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.dropdownValue2),
                              ],
                            ),
                          ),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Father's Name: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gdFatherNameController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Gender: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.dropdownValue3.toString()),
                              ],
                            ),
                          ),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Bank A/C No.: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gdBankAcNumberController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "IFSC Code: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gdIfscCodeController.text),
                              ],
                            ),
                          ),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "E-Filing Status: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gdEFillingStatusController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Aadhar Card: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gdAadhaarCardController.text),
                              ],
                            ),
                          ),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                ),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text("Income Summary"),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: [
                        Text(
                          "Salary: ",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        Text(_previewController.salaryController.text),
                      ],
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                            children: List.generate(
                                _previewController.computationHeadListSalary.length,
                                (index) => Column(
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Text(_previewController.computationHeadListSalary[index].name + ": "),
                                            Text(_previewController.computationHeadListSalary[index].value),
                                          ],
                                        ),
                                      ],
                                    ))),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text(
                          "Income From House Property: ",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        Text(_previewController.hpController.text),
                      ],
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                            children: List.generate(
                                _previewController.computationHeadListHp.length,
                                (index) => Column(
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Text(_previewController.computationHeadListHp[index].name + ": "),
                                            Text(_previewController.computationHeadListHp[index].value),
                                          ],
                                        ),
                                      ],
                                    ))),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text(
                          "Business & Profession: ",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        Text(_previewController.bpController.text),
                      ],
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                            children: List.generate(_previewController.computationHeadListBp.length, (index) {
                          print("------Name--------" + _previewController.computationHeadListBp[index].name);
                          return Column(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Text(_previewController.computationHeadListBp[index].name + ": "),
                                  Text(_previewController.computationHeadListBp[index].value),
                                ],
                              ),
                            ],
                          );
                        })),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text(
                          "Capital Gain: ",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        Text(_previewController.cgController.text),
                      ],
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                            children: List.generate(
                                _previewController.computationHeadListCg.length,
                                (index) => Column(
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Text(_previewController.computationHeadListCg[index].name + ": "),
                                            Text(_previewController.computationHeadListCg[index].value + ": "),
                                          ],
                                        ),
                                      ],
                                    ))),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text(
                          "Other Income: ",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        Text(_previewController.otherIncomeController.text),
                      ],
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                            children: List.generate(
                                _previewController.computationHeadListOtherIncome.length,
                                (index) => Column(
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Text(_previewController.computationHeadListOtherIncome[index].name + ": "),
                                            Text(_previewController.computationHeadListOtherIncome[index].value + ": "),
                                          ],
                                        ),
                                      ],
                                    ))),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text(
                          "Gross Total Income: ",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        Text(_previewController.grossTotalIncomeController.text),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text(
                          "Total Deduction: ",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        Text(_previewController.totalDeductionController.text),
                      ],
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                            children: List.generate(
                                _previewController.computationHeadListTotalDeduction.length,
                                (index) => Column(
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Text(_previewController.computationHeadListTotalDeduction[index].name + ": "),
                                            Text(_previewController.computationHeadListTotalDeduction[index].value + ": "),
                                          ],
                                        ),
                                      ],
                                    ))),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Total Income (Taxable): ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.totalIncomeController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Total Tax Payable: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.totalTaxPayableController.text),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "TDS: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.tdsController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Refund: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.refundController.text),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Divider(
                  color: Colors.grey[900],
                ),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text("Income Tax"),
                    SizedBox(
                      height: 15,
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Total Income: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.grossTotalIncomeController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Basic Exemption: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.totalDeductionController.text),
                              ],
                            ),
                          ),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Income Tax(Before Tax Credit U/S 87A): ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.totalIncomeController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Tax Credit U/S 87A: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.taxCreditController.text),
                              ],
                            ),
                          ),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Income Tax : ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.totalTaxPayableController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Health & Education Cess (@ 4% GIT): ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.healthController.text),
                              ],
                            ),
                          ),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Total Tax: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.totTaxController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Tax Paid: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.tdsController.text),
                              ],
                            ),
                          ),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Refund ",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(_previewController.refundController.text),
                          ],
                        ),
                      ],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                    IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "GSTIN: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gstInController.text),
                              ],
                            ),
                          ),
                          VerticalDivider(
                            color: Colors.grey[900],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "GST Gross Receipts: ",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(_previewController.gstGrossController.text),
                              ],
                            ),
                          ),
                          SizedBox(width: 10)
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[900],
                    ),
                  ],
                ),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Taxes Paid \n(TDS on Non-Salary Income)",
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                            children: List.generate(
                          _previewController.computationTaxPaidList.length,
                          (index) => Card(
                            margin: EdgeInsets.symmetric(vertical: 10),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                  IntrinsicHeight(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Name: ",
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationTaxPaidList[index].tpName),
                                            ],
                                          ),
                                        ),
                                        VerticalDivider(
                                          color: Colors.grey[900],
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Tan: ",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationTaxPaidList[index].tpTan),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10)
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                  IntrinsicHeight(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Type Of Income: ",
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationTaxPaidList[index].tpToi),
                                            ],
                                          ),
                                        ),
                                        VerticalDivider(
                                          color: Colors.grey[900],
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Deducted Year: ",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationTaxPaidList[index].tpDy),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10)
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                  IntrinsicHeight(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Brougth Forward: ",
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationTaxPaidList[index].tpBrought),
                                            ],
                                          ),
                                        ),
                                        VerticalDivider(
                                          color: Colors.grey[900],
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "TDS Amount",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationTaxPaidList[index].tpTds),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10)
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                  IntrinsicHeight(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Income for which TDS Paid: ",
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationTaxPaidList[index].tpIncomefw),
                                            ],
                                          ),
                                        ),
                                        VerticalDivider(
                                          color: Colors.grey[900],
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Balance: ",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationTaxPaidList[index].tpBalance),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ))),
                  ],
                ),
                Divider(),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Bank Account Details",
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                        padding: EdgeInsets.only(bottom: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                            children: List.generate(
                          _previewController.computationBankList.length,
                          (index) => Card(
                            margin: EdgeInsets.symmetric(vertical: 10),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Column(
                                children: [
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                  IntrinsicHeight(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Name of the Bank: ",
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationBankList[index].cbName),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10)
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                  IntrinsicHeight(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "IFSC Code: ",
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationBankList[index].cbifsc),
                                            ],
                                          ),
                                        ),
                                        VerticalDivider(
                                          color: Colors.grey[900],
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Account No.: ",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Text(_previewController.computationBankList[index].cbaccount),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10)
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey[900],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ))),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                        height: 40,
                        width: 140,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
                            onPressed: () {
                              Get.back();
                            },
                            child: Text("Edit"))),
                    SizedBox(
                        height: 40,
                        width: 140,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
                            onPressed: () async {
                              // ComputaionData cd = ComputaionData();
                              // List<Map> genDetails = cd.getPdfGd();
                              // final pdfFile = await PdfComputationApi.generate(genDetails);
                              //
                              // NewPdfApi.openFile(pdfFile);
                              _previewController.computationjsonApiCall(context);
                            },
                            child: Text("Submit")))
                  ],
                )
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
