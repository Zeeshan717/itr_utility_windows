import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:accountx/app/modules/incomeMasterPage/incomeMasterController.dart';
import 'package:accountx/app/modules/taxCalPage/taxController.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class TotalComputaionData {
  // var Controller = Get.put(ComputationController());
  var imcontroller = Get.put(IncomeMasterController());
  var taxcontroller = Get.put(TaxController());
  var controller = Get.put(ComputationController());
  GetStorage storage = GetStorage();

  List<Map> getPdfGd() {
    List<Map> generalDetailsData = [
      {
        'name': storage.read("client_name").toString(),
        'address': storage.read("distributor_address").toString(),
        'phone': storage.read("phone_no").toString(),
        'email': "N/A",
        'pan': controller.tcPanController.text.isEmpty ? "N/A" : controller.tcPanController.text,
        'companyType': controller.tcdropdownValue2,
        'father_name': controller.tcFatherNameController.text.isEmpty ? "N/A" : controller.tcFatherNameController.text,
        'gender': controller.tcdropdownValue3,
        'bank_ac_no': controller.tcBankNumberController.text.isEmpty ? "N/A" : controller.tcBankNumberController.text,
        'ifsc_code': controller.tcIfscCodeController.text.isEmpty ? "N/A" : controller.tcIfscCodeController.text,
        'efiling_status': "N/A",
        'aadhar_card': controller.tcAadharCardNumber.text.isEmpty ? "N/A" : controller.tcAadharCardNumber.text,
      },
      {
        'salary': double.parse(imcontroller.salaryController.text.isEmpty ? "0.00" :imcontroller.salaryController.text).toStringAsFixed(2),
        'income_hp': double.parse(imcontroller.hpController.text.isEmpty ? "0.00" :imcontroller.hpController.text).toStringAsFixed(2),
        'bp': double.parse(imcontroller.bandpController.text.isEmpty ? "0.00" :imcontroller.bandpController.text).toStringAsFixed(2),
        'cg': double.parse(imcontroller.capitalController.text.isEmpty ? "0.00" :imcontroller.capitalController.text).toStringAsFixed(2),
        'other_income': double.parse(imcontroller.otherController.text.isEmpty ? "0.00" :imcontroller.otherController.text).toStringAsFixed(2),
        'agriculture': double.parse(imcontroller.agricultureController.text.isEmpty ? "0.00" :imcontroller.agricultureController.text).toStringAsFixed(2),
        'gross_total_income': double.parse(taxcontroller.taxtotalincomecontroller.text.isEmpty ? "0.00" :taxcontroller.taxtotalincomecontroller.text).toStringAsFixed(2),
        'total_deduction': double.parse(taxcontroller.taxtotaldeductcontroller.text.isEmpty ? "0.00" :taxcontroller.taxtotaldeductcontroller.text).toStringAsFixed(2),
        'total_income_taxable': double.parse(taxcontroller.taxtaxablecontroller.text.isEmpty ? "0.00" :taxcontroller.taxtaxablecontroller.text).toStringAsFixed(2),
        'total_taxpayable': double.parse(taxcontroller.taxtotaltaxpayablecontroller.text.isEmpty ? "0.00" :taxcontroller.taxtotaltaxpayablecontroller.text).toStringAsFixed(2),
        'tds': double.parse(controller.ittdsController.text.isEmpty ? "0.00" :controller.ittdsController.text).toStringAsFixed(2),
        'refund': double.parse(controller.itrefundController.text.isEmpty ? "0.00" :controller.itrefundController.text).toStringAsFixed(2),
      },
      {
        'total_income': double.parse(taxcontroller.taxtotalincomecontroller.text.isEmpty ? "0.00" :taxcontroller.taxtotalincomecontroller.text).toStringAsFixed(2),
        'basic_Exemption': double.parse(taxcontroller.taxtotaldeductcontroller.text.isEmpty ? "0.00" :taxcontroller.taxtotaldeductcontroller.text).toStringAsFixed(2),
        'it_before_taxCredit': double.parse(taxcontroller.taxtaxablecontroller.text.isEmpty ? "0.00" :taxcontroller.taxtaxablecontroller.text).toStringAsFixed(2),
        'tax_credit': double.parse(controller.itCreditController.text.isEmpty ? "0.00" :controller.itCreditController.text).toStringAsFixed(2),
        'income_tax':double.parse( taxcontroller.taxtotaltaxpayablecontroller.text.isEmpty ? "0.00" :taxcontroller.taxtotaltaxpayablecontroller.text).toStringAsFixed(2),
        'health_cess': double.parse(controller.ithealthController.text.isEmpty ? "0.00" :controller.ithealthController.text).toStringAsFixed(2),
        'total_tax':double.parse( controller.ittotTaxController.text.isEmpty ? "0.00" :controller.ittotTaxController.text).toStringAsFixed(2),
        'taxpaid':double.parse( controller.ittdsController.text.isEmpty ? "0.00" :controller.ittdsController.text).toStringAsFixed(2),
        'refund': double.parse(controller.itrefundController.text.isEmpty ? "0.00" :controller.itrefundController.text).toStringAsFixed(2),
        'gst_in': controller.itGstinController.text,
        'gst_gross': double.parse(controller.itGstGrossController.text.isEmpty ? "0.00" :controller.itGstGrossController.text).toStringAsFixed(2),
      },
      // {'computation_taxpaid_list': Controller.computationTaxPaidList},
      // {'computaion_bank_list': Controller.computationBankList}
    ];

    return generalDetailsData;
  }
}
