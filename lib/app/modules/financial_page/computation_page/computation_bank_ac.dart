import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_preview_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class BankDetails extends StatefulWidget {
  BankDetails({Key? key}) : super(key: key);
  var count = 0;

  @override
  State<BankDetails> createState() => _BankDetailsState();
}

class _BankDetailsState extends State<BankDetails> {
  var _computationController = Get.put(ComputationController());
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    widget.count++;

    if (widget.count == 1) {
      _computationController.addbankname(
        "Name",
      );
    }
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Text('Bank Account Details', style: TextStyle(fontSize: 20)),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  children: [
                    InkWell(
                      onTap: () {
                        _computationController.addbankname(
                          "Name",
                        );
                        setState(() {});
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: const Color(0xff0B28BE),
                            border: Border.all(
                              color: const Color(0xff0B28BE),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(4))),
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                          size: 18,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text('Add more A/C', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                height: 40,
                child: TextField(
                  onChanged: (val) {
                    _computationController.computationBankList[0].cbName = val;
                  },
                  controller: _computationController.cbNameController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      labelText: "Name of the Bank",
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Container(
                      height: 40,
                      child: TextField(
                        onChanged: (val) {
                          _computationController.computationBankList[0].cbifsc = val;
                        },
                        controller: _computationController.cbifscController,
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(10),
                            labelText: "IFSC Code",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Flexible(
                    child: Container(
                      height: 40,
                      child: TextField(
                        keyboardType: TextInputType.number,
                        onChanged: (val) {
                          _computationController.computationBankList[0].cbaccount = val;
                        },
                        controller: _computationController.cbaccountController,
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.all(10),
                            labelText: "Account No.",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Container(
                  child: Column(
                      children: List.generate(_computationController.computationBankList.length - 1, (index) {
                List<TextEditingController> bankNameController =
                    List.generate(_computationController.computationBankList.length, (index) => TextEditingController());
                List<TextEditingController> bankIfscController =
                    List.generate(_computationController.computationBankList.length, (index) => TextEditingController());

                List<TextEditingController> bankAccountController =
                    List.generate(_computationController.computationBankList.length, (index) => TextEditingController());

                bankNameController[index + 1].text = _computationController.computationBankList[index + 1].cbName;
                bankIfscController[index + 1].text = _computationController.computationBankList[index + 1].cbifsc;
                bankAccountController[index + 1].text = _computationController.computationBankList[index + 1].cbaccount;
                // print('----' + _computationController.computationBankList[index].toString());

                return InkWell(
                  onTap: () {
                    _computationController.computationBankList.removeAt(index + 1);
                    setState(() {});
                  },
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
                      child: Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Color.fromARGB(255, 244, 2, 2),
                                border: Border.all(
                                  color: Color.fromARGB(255, 244, 2, 2),
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(4))),
                            // color: Color.fromARGB(255, 244, 2, 2),
                            child: Icon(
                              Icons.delete,
                              color: Colors.white,
                              size: 18,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('DELETE', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 40,
                      child: TextField(
                        onChanged: (val) {
                          _computationController.computationBankList[index + 1].cbName = val;
                        },
                        controller: bankNameController[index + 1],
                        decoration: InputDecoration(
                            labelText: "Name of the Bank",
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              onChanged: (val) {
                                _computationController.computationBankList[index + 1].cbifsc = val;
                              },
                              controller: bankIfscController[index + 1],
                              decoration: InputDecoration(
                                  labelText: "IFSC Code",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.number,
                              onChanged: (val) {
                                _computationController.computationBankList[index + 1].cbaccount = val;
                              },
                              controller: bankAccountController[index],
                              decoration: InputDecoration(
                                  labelText: "Account No.",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ]),
                );
              }))),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Color(0xff0B28BE)
                ),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (Context) => ComputaionPreviewPage()));
                  },
                  child: Text("Preview")),
              SizedBox(height: 70,)
            ],
          ),
        ),
      ),
    );
  }
}
