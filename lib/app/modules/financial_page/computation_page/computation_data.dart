import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:get/get.dart';

class ComputaionData {
  var Controller = Get.put(ComputationController());

  List<Map> getPdfGd() {
    List<Map> generalDetailsData = [
      {
        'name': Controller.gdNameController.text,
        'address': Controller.gdAddressController.text,
        'phone': Controller.gdPhoneNoController.text,
        'email': Controller.gdEmailController.text,
        'pan': Controller.gdPanController.text,
        'companyType': Controller.dropdownValue2,
        'father_name': Controller.gdFatherNameController.text,
        'gender': Controller.dropdownValue3,
        'bank_ac_no': Controller.gdBankAcNumberController.text,
        'ifsc_code': Controller.gdIfscCodeController.text,
        'efiling_status': Controller.gdEFillingStatusController.text,
        'aadhar_card': Controller.gdAadhaarCardController.text
      },
      {
        'salary': double.parse(Controller.salaryController.text.isEmpty ? "0.00" : Controller.salaryController.text).toStringAsFixed(2),
        'income_hp': double.parse(Controller.hpController.text.isEmpty ? "0.00" : Controller.hpController.text).toStringAsFixed(2),
        'bp': double.parse(Controller.bpController.text.isEmpty ? "0.00" : Controller.bpController.text).toStringAsFixed(2),
        'cg': double.parse(Controller.cgController.text.isEmpty ? "0.00" : Controller.cgController.text).toStringAsFixed(2),
        'other_income':
            double.parse(Controller.otherIncomeController.text.isEmpty ? "0.00" : Controller.otherIncomeController.text).toStringAsFixed(2),
        'gross_total_income':
            double.parse(Controller.grossTotalIncomeController.text.isEmpty ? "0.00" : Controller.grossTotalIncomeController.text).toStringAsFixed(2),
        'total_deduction':
            double.parse(Controller.totalDeductionController.text.isEmpty ? "0.00" : Controller.totalDeductionController.text).toStringAsFixed(2),
        'total_income_taxable':
            double.parse(Controller.totalIncomeController.text.isEmpty ? "0.00" : Controller.totalIncomeController.text).toStringAsFixed(2),
        'total_taxpayable':
            double.parse(Controller.totalTaxPayableController.text.isEmpty ? "0.00" : Controller.totalTaxPayableController.text).toStringAsFixed(2),
        'tds': double.parse(Controller.tdsController.text.isEmpty ? "0.00" : Controller.tdsController.text).toStringAsFixed(2),
        'refund': double.parse(Controller.refundController.text.isEmpty ? "0.00" : Controller.refundController.text).toStringAsFixed(2),
      },
      {
        'total_income':
            double.parse(Controller.grossTotalIncomeController.text.isEmpty ? "0.00" : Controller.grossTotalIncomeController.text).toStringAsFixed(2),
        'basic_Exemption':
            double.parse(Controller.totalDeductionController.text.isEmpty ? "0.00" : Controller.totalDeductionController.text).toStringAsFixed(2),
        'it_before_taxCredit':
            double.parse(Controller.totalIncomeController.text.isEmpty ? "0.00" : Controller.totalIncomeController.text).toStringAsFixed(2),
        'tax_credit': double.parse(Controller.taxCreditController.text.isEmpty ? "0.00" : Controller.taxCreditController.text).toStringAsFixed(2),
        'income_tax':
            double.parse(Controller.totalTaxPayableController.text.isEmpty ? "0.00" : Controller.totalTaxPayableController.text).toStringAsFixed(2),
        'health_cess': double.parse(Controller.healthController.text.isEmpty ? "0.00" : Controller.healthController.text).toStringAsFixed(2),
        'total_tax': double.parse(Controller.totTaxController.text.isEmpty ? "0.00" : Controller.totTaxController.text).toStringAsFixed(2),
        'taxpaid': double.parse(Controller.tdsController.text.isEmpty ? "0.00" : Controller.tdsController.text).toStringAsFixed(2),
        'refund': double.parse(Controller.refundController.text.isEmpty ? "0.00" : Controller.refundController.text).toStringAsFixed(2),
        'gst_in': Controller.gstInController.text,
        'gst_gross': double.parse(Controller.gstGrossController.text.isEmpty ? "0.00" : Controller.gstGrossController.text).toStringAsFixed(2),
      },
      {'computation_taxpaid_list': Controller.computationTaxPaidList},
      {'computaion_bank_list': Controller.computationBankList}
    ];

    return generalDetailsData;
  }
}
