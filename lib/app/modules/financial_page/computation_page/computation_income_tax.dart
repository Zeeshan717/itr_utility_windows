import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class ComputationIncomeTax extends StatefulWidget {
  const ComputationIncomeTax({Key? key}) : super(key: key);

  @override
  State<ComputationIncomeTax> createState() => _ComputationIncomeTaxState();
}

class _ComputationIncomeTaxState extends State<ComputationIncomeTax> {
  var _computationController = Get.put(ComputationController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),

                Center(
                    child: Text(
                  "Income Tax",
                  style: TextStyle(fontSize: 20),
                )),

                SizedBox(
                  height: 20,
                ),


                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("Total Income"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          keyboardType: TextInputType.number,
                          controller: _computationController.grossTotalIncomeController,

                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),



                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("Basic Exemption"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.totalDeductionController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),


                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("Income Tax (Before Tax Credit U/S 87A)"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.totalIncomeController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),


                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("Tax Credit U/S 87A"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.taxCreditController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("Income Tax"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.totalTaxPayableController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),


                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("Health and Education Cess (@4% GIT)"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.healthController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),


                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("Total Tax"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.totTaxController,
                          keyboardType: TextInputType.number,
                          readOnly: true,
                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),


                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("Tax Paid"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.tdsController,
                          keyboardType: TextInputType.number,
                          readOnly: true,
                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),


                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("Refund"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.refundController,
                          keyboardType: TextInputType.number,
                          readOnly: true,
                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("GSTIN"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.gstInController,
                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text("GST Gross Receipts"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _computationController.gstGrossController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              isDense:true,
                              contentPadding: EdgeInsets.all(10),
                              border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 70,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
