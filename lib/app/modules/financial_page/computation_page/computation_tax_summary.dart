import 'dart:io';

import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class TaxSummary extends StatefulWidget {
  const TaxSummary({Key? key}) : super(key: key);

  @override
  State<TaxSummary> createState() => _TaxSummaryState();
}

class _TaxSummaryState extends State<TaxSummary> {
  double count = 0;
  double totalEGT = 0;
  var newage = 0;
  File? file;
  List<String> countlist = [];

  // String dropDownDeduction = "80C";
  var dropDownDeduction = "80C";
  GetStorage storage = GetStorage();
  var currentIndex = -1;

  var controller;

  final _computationController = Get.put(ComputationController());

  //TextEditingController updateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                const Center(
                    child: Text(
                  "Income Summary (Amount in Rs)",
                  style: TextStyle(fontSize: 20),
                  textAlign: TextAlign.center,
                )),
                const SizedBox(
                  height: 20,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //1
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            _computationController.addHead(
                              "Salary",
                            );
                            setState(() {});
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: const Color(0xff0B28BE),
                                border: Border.all(
                                  color: const Color(0xff0B28BE),
                                ),
                                borderRadius: const BorderRadius.all(Radius.circular(4))),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 18,
                            ),
                          ),
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: const Text("Salary"),
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              onChanged: (newValue) {
                                _computationController.getTotalGrossIncome();
                                setState(() {});
                              },
                              readOnly: _computationController.computationHeadListSalary.isEmpty ? false : true,
                              controller: _computationController.salaryController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 2),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: List.generate(_computationController.computationHeadListSalary.length, (index) {
                            return Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        _computationController.computationHeadListSalary.removeAt(index);
                                        _computationController.getTotalSalary();
                                        _computationController.getTotalGrossIncome();

                                        setState(() {});
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: const Color.fromARGB(255, 244, 2, 2),
                                            border: Border.all(
                                              color: const Color.fromARGB(255, 244, 2, 2),
                                            ),
                                            borderRadius: const BorderRadius.all(Radius.circular(4))),
                                        // color: Color.fromARGB(255, 244, 2, 2),
                                        child: const Icon(
                                          Icons.delete,
                                          color: Colors.white,
                                          size: 18,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    // Flexible(
                                    //   child: Container(
                                    //     height: 40,
                                    //     child: Text("Salary"),
                                    //   ),
                                    // ),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.name,
                                          onChanged: (value1) {
                                            _computationController.computationHeadListSalary[index].name = value1;

                                            // _computationController.GetTotalSalary();
                                            // setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),

                                    const SizedBox(width: 5),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                                          onChanged: (value1) {
                                            _computationController.computationHeadListSalary[index].value = value1;
                                            _computationController.getTotalSalary();
                                            _computationController.getTotalGrossIncome();
                                            setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            );
                          }),
                        ),
                      ),
                    ),
                    //2
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            _computationController.addHead(
                              "Hp",
                            );
                            setState(() {});
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: const Color(0xff0B28BE),
                                border: Border.all(
                                  color: const Color(0xff0B28BE),
                                ),
                                borderRadius: const BorderRadius.all(Radius.circular(4))),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 18,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Flexible(
                          child: Text("Income from House Property"),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              onChanged: (newValue) {
                                _computationController.getTotalGrossIncome();
                                setState(() {});
                              },
                              controller: _computationController.hpController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: List.generate(_computationController.computationHeadListHp.length, (index) {
                            return Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        _computationController.computationHeadListHp.removeAt(index);
                                        _computationController.getTotalHp();
                                        _computationController.getTotalGrossIncome();
                                        setState(() {});
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: const Color.fromARGB(255, 244, 2, 2),
                                            border: Border.all(
                                              color: const Color.fromARGB(255, 244, 2, 2),
                                            ),
                                            borderRadius: const BorderRadius.all(Radius.circular(4))),
                                        // color: Color.fromARGB(255, 244, 2, 2),
                                        child: const Icon(
                                          Icons.delete,
                                          color: Colors.white,
                                          size: 18,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    // Flexible(
                                    //   child: Container(
                                    //     height: 40,
                                    //     child: Text("Salary"),
                                    //   ),
                                    // ),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.name,
                                          onChanged: (newValue) {
                                            _computationController.computationHeadListHp[index].name = newValue;
                                            _computationController.getTotalHp();
                                            setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                                          onChanged: (newValue) {
                                            _computationController.computationHeadListHp[index].value = newValue;
                                            _computationController.getTotalHp();
                                            _computationController.getTotalGrossIncome();

                                            setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            );
                          }),
                        ),
                      ),
                    ),

                    //3
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            _computationController.addHead("Bp");
                            setState(() {});
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: const Color(0xff0B28BE),
                                border: Border.all(
                                  color: const Color(0xff0B28BE),
                                ),
                                borderRadius: const BorderRadius.all(Radius.circular(4))),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 18,
                            ),
                          ),
                        ),
                        const Flexible(
                          child: Text("Business and Profession"),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              onChanged: (newValue) {
                                _computationController.getTotalGrossIncome();
                                setState(() {});
                              },
                              controller: _computationController.bpController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: List.generate(_computationController.computationHeadListBp.length, (index) {
                            return Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        _computationController.computationHeadListBp.removeAt(index);
                                        _computationController.getTotalBp();
                                        _computationController.getTotalGrossIncome();
                                        setState(() {});
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: const Color.fromARGB(255, 244, 2, 2),
                                            border: Border.all(
                                              color: const Color.fromARGB(255, 244, 2, 2),
                                            ),
                                            borderRadius: const BorderRadius.all(Radius.circular(4))),
                                        // color: Color.fromARGB(255, 244, 2, 2),
                                        child: const Icon(
                                          Icons.delete,
                                          color: Colors.white,
                                          size: 18,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    // Flexible(
                                    //   child: Container(
                                    //     height: 40,
                                    //     child: Text("Salary"),
                                    //   ),
                                    // ),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.name,
                                          onChanged: (newValue) {
                                            _computationController.computationHeadListBp[index].name = newValue;
                                            _computationController.getTotalBp();
                                            setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),

                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                                          onChanged: (newValue) {
                                            _computationController.computationHeadListBp[index].value = newValue;
                                            _computationController.getTotalBp();
                                            _computationController.getTotalGrossIncome();
                                            setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            );
                          }),
                        ),
                      ),
                    ),

                    //4
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            _computationController.addHead("Cg");
                            setState(() {});
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: const Color(0xff0B28BE),
                                border: Border.all(
                                  color: const Color(0xff0B28BE),
                                ),
                                borderRadius: const BorderRadius.all(Radius.circular(4))),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 18,
                            ),
                          ),
                        ),
                        const Flexible(
                          child: Text("Capital Gain"),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              onChanged: (newValue) {
                                _computationController.getTotalGrossIncome();
                                setState(() {});
                              },
                              controller: _computationController.cgController,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: List.generate(_computationController.computationHeadListCg.length, (index) {
                            return Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        _computationController.computationHeadListCg.removeAt(index);
                                        _computationController.getTotalCg();
                                        _computationController.getTotalGrossIncome();
                                        setState(() {});
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: const Color.fromARGB(255, 244, 2, 2),
                                            border: Border.all(
                                              color: const Color.fromARGB(255, 244, 2, 2),
                                            ),
                                            borderRadius: const BorderRadius.all(Radius.circular(4))),
                                        // color: Color.fromARGB(255, 244, 2, 2),
                                        child: const Icon(
                                          Icons.delete,
                                          color: Colors.white,
                                          size: 18,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    // Flexible(
                                    //   child: Container(
                                    //     height: 40,
                                    //     child: Text("Salary"),
                                    //   ),
                                    // ),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.name,
                                          onChanged: (newValue) {
                                            _computationController.computationHeadListCg[index].name = newValue;
                                            _computationController.getTotalCg();
                                            setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                                          onChanged: (newValue) {
                                            _computationController.computationHeadListCg[index].value = newValue;
                                            _computationController.getTotalCg();
                                            _computationController.getTotalGrossIncome();
                                            setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            );
                          }),
                        ),
                      ),
                    ),

                    //5

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            _computationController.addHead("OtherIncome");
                            setState(() {});
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: const Color(0xff0B28BE),
                                border: Border.all(
                                  color: const Color(0xff0B28BE),
                                ),
                                borderRadius: const BorderRadius.all(Radius.circular(4))),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 18,
                            ),
                          ),
                        ),
                        const Flexible(
                          child: Text("Other Income"),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              controller: _computationController.otherIncomeController,
                              onChanged: (value) {
                                _computationController.getTotalGrossIncome();
                                setState(() {});
                              },
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: List.generate(_computationController.computationHeadListOtherIncome.length, (index) {
                            return Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        _computationController.computationHeadListOtherIncome.removeAt(index);
                                        _computationController.getTotalOtherIncome();
                                        _computationController.getTotalGrossIncome();
                                        setState(() {});
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: const Color.fromARGB(255, 244, 2, 2),
                                            border: Border.all(
                                              color: const Color.fromARGB(255, 244, 2, 2),
                                            ),
                                            borderRadius: const BorderRadius.all(Radius.circular(4))),
                                        // color: Color.fromARGB(255, 244, 2, 2),
                                        child: const Icon(
                                          Icons.delete,
                                          color: Colors.white,
                                          size: 18,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    // Flexible(
                                    //   child: Container(
                                    //     height: 40,
                                    //     child: Text("Salary"),
                                    //   ),
                                    // ),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.name,
                                          onChanged: (newValue) {
                                            _computationController.computationHeadListOtherIncome[index].name = newValue;
                                            _computationController.getTotalOtherIncome();
                                            setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                                          onChanged: (newValue) {
                                            _computationController.computationHeadListOtherIncome[index].value = newValue;
                                            _computationController.getTotalOtherIncome();
                                            _computationController.getTotalGrossIncome();
                                            setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            );
                          }),
                        ),
                      ),
                    ),

                    //6
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Flexible(
                          child: Text(
                            "Gross Total Income",
                            style: TextStyle(color: Colors.cyan, fontWeight: FontWeight.bold),
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              controller: _computationController.grossTotalIncomeController,
                              readOnly: true,
                              decoration: InputDecoration(
                                isDense:true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    const SizedBox(
                      height: 10,
                    ),

                    //7
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            _computationController.addHead("TotalDeduction");
                            setState(() {});
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: const Color(0xff0B28BE),
                                border: Border.all(
                                  color: const Color(0xff0B28BE),
                                ),
                                borderRadius: const BorderRadius.all(Radius.circular(4))),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 18,
                            ),
                          ),
                        ),
                        const Flexible(
                          child: Text("Total Deduction"),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              onChanged: (value) {
                                _computationController.getTotalIncome();
                                setState(() {});
                              },
                              controller: _computationController.totalDeductionController,
                              decoration: InputDecoration(
                                  isDense:true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: List.generate(_computationController.computationHeadListTotalDeduction.length, (index) {
                            List<TextEditingController> updateController =
                                List.generate(_computationController.computationHeadListTotalDeduction.length, (index) => TextEditingController());

                            controller = updateController[index].text;

                            if (_computationController.computationHeadListTotalDeduction.isNotEmpty) {
                              if (_computationController.computationHeadListTotalDeduction[index].name == "") {
                                dropDownDeduction = "80C";
                                updateController[index].text = _computationController.computationHeadListTotalDeduction[index].value;
                              } else {
                                var deductName = _computationController.computationHeadListTotalDeduction[index].name;
                                if (deductName == "80C" ||
                                    deductName == "80CCC" ||
                                    deductName == "80CCD(1)" ||
                                    deductName == "80CCD(1B)" ||
                                    deductName == "80D" ||
                                    deductName == "80EE" ||
                                    deductName == "80EEA" ||
                                    deductName == "80EEB" ||
                                    deductName == "80G" ||
                                    deductName == "80GG" ||
                                    deductName == "80GGB/80GGC" ||
                                    deductName == "80TTA" ||
                                    deductName == "80TTB" ||
                                    deductName == "80U") {
                                  dropDownDeduction = _computationController.computationHeadListTotalDeduction[index].name;
                                  updateController[index].text = _computationController.computationHeadListTotalDeduction[index].value;
                                } else {
                                  dropDownDeduction = "Other";

                                  updateController[index].text = _computationController.computationHeadListTotalDeduction[index].value;
                                }
                              }
                            }
                            return Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        _computationController.computationHeadListTotalDeduction.removeAt(index);
                                        _computationController.getTotalOtherIncome();
                                        // _computationController.getTotalDeduction();

                                        _computationController.getTotalIncome();

                                        setState(() {});
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: const Color.fromARGB(255, 244, 2, 2),
                                            border: Border.all(
                                              color: const Color.fromARGB(255, 244, 2, 2),
                                            ),
                                            borderRadius: const BorderRadius.all(Radius.circular(4))),
                                        // color: Color.fromARGB(255, 244, 2, 2),
                                        child: const Icon(
                                          Icons.delete,
                                          color: Colors.white,
                                          size: 18,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    // Flexible(
                                    //   child: Container(
                                    //     height: 40,
                                    //     child: Text("Salary"),
                                    //   ),
                                    // ),
                                    Flexible(
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(horizontal: 10),
                                        decoration:
                                            BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: Colors.grey, width: 1)),
                                        height: 40,
                                        // height: 40,
                                        child: DropdownButton<String>(
                                          isExpanded: true,
                                          value: dropDownDeduction,
                                          icon: const Icon(Icons.arrow_drop_down),
                                          elevation: 16,
                                          style: const TextStyle(color: Colors.black),
                                          underline: const SizedBox(),
                                          onChanged: (String? newValue) {
                                            setState(() {
/*<<<<<<< HEAD

                                              if(newValue == "80GGB/80GGC") {
                                                Get.defaultDialog(
                                                  barrierDismissible: false,
                                                  title: "Upload File",
                                                  content: Column(
                                                    children: [
                                                      Row(
                                                        children: [
                                                          IconButton(onPressed: () async {
                                                            FilePickerResult? result = await FilePicker.platform.pickFiles();

                                                            if(result != null) {
                                                              file = File(result.files.single.path.toString());
                                                            }

                                                            print("File Path : ===== "+result!.files.single.path.toString());

                                                          }, icon: Image.asset("images/upload.png",color: Color(0xff0B28BE),)),
                                                          SizedBox(width: 10,),
                                                          Flexible(
                                                            child: SizedBox(
                                                              height: 40,
                                                              child: TextField(
                                                                controller: _computationController.newAmountController,
                                                                decoration: InputDecoration(
                                                                  hintText: "Amount",
                                                                  border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(5),
                                                                    borderSide: BorderSide(width: 1,color: Colors.grey)
                                                                  )
                                                                ),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      SizedBox(height: 10,),
                                                      Row(
                                                        children: [
                                                          IconButton(onPressed: (){}, icon: Text("")),
                                                          SizedBox(width: 10,),
                                                          Flexible(
                                                            child: SizedBox(
                                                              height: 40,
                                                              child: TextField(
                                                                controller: _computationController.newRemarkController,
                                                                decoration: InputDecoration(
                                                                  hintText: "Remark",
                                                                  border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(5),
                                                                    borderSide: BorderSide(width: 1,color: Colors.grey)
                                                                  )
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      )
=======*/
                                              if (newValue == "80GGB/80GGC") {
                                                Get.defaultDialog(
                                                    barrierDismissible: false,
                                                    title: "Upload File",
                                                    content: Column(
                                                      children: [
                                                        Row(
                                                          children: [
                                                            IconButton(
                                                                onPressed: () async {
                                                                  FilePickerResult? result = await FilePicker.platform.pickFiles();

                                                                  if (result != null) {
                                                                    file = File(result.files.single.path.toString());
                                                                  }

                                                                  print("File Path : ===== " + result!.files.single.path.toString());
                                                                },
                                                                icon: Image.asset(
                                                                  "images/upload.png",
                                                                  color: Color(0xff0B28BE),
                                                                )),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Flexible(
                                                              child: SizedBox(
                                                                height: 40,
                                                                child: TextField(
                                                                  controller: _computationController.newAmountController,
                                                                  decoration: InputDecoration(
                                                                      hintText: "Amount",
                                                                      border: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(5),
                                                                          borderSide: BorderSide(width: 1, color: Colors.grey))),
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Row(
                                                          children: [
                                                            IconButton(onPressed: () {}, icon: Text("")),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Flexible(
                                                              child: SizedBox(
                                                                height: 40,
                                                                child: TextField(
                                                                  controller: _computationController.newRemarkController,
                                                                  decoration: InputDecoration(
                                                                      hintText: "Remark",
                                                                      border: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(5),
                                                                          borderSide: BorderSide(width: 1, color: Colors.grey))),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                    actions: [
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                        children: [
                                                          TextButton(
                                                              onPressed: () {
                                                                setState(() {
                                                                  _computationController.computationHeadListTotalDeduction[index].value =
                                                                      _computationController.newAmountController.text;
                                                                });

                                                                Get.back();
                                                                _computationController.newAmountController.clear();
                                                                _computationController.newRemarkController.clear();
                                                              },
                                                              child: Text("OK")),
                                                          TextButton(
                                                              onPressed: () {
                                                                Get.back();
                                                                _computationController.newAmountController.clear();
                                                                _computationController.newRemarkController.clear();
                                                              },
                                                              child: Text("Cancel")),
                                                        ],
                                                      )
                                                    ]);
                                              }

                                              if (newValue == "80U") {
                                                Get.defaultDialog(
                                                  barrierDismissible: false,
                                                  title: "80U",
                                                  content: Column(
                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Obx(() => Radio(
                                                              value: "Normal Disability",
                                                              groupValue: _computationController.radioValue.value,
                                                              onChanged: (value) {
                                                                _computationController.setRadioValue(value);
                                                              })),
                                                          SizedBox(
                                                            width: 5,
                                                          ),
                                                          Text("Normal Disability : Rs. 75,000")
                                                        ],
                                                      ),
                                                      Row(
                                                        children: [
                                                          Obx(() => Radio(
                                                              value: "Severe Disability",
                                                              groupValue: _computationController.radioValue.value,
                                                              onChanged: (value) {
                                                                _computationController.setRadioValue(value);
                                                                // print("_computationController.radioValue.value;"+_computationController.radioValue.value.toString());
                                                              })),
                                                          SizedBox(
                                                            width: 5,
                                                          ),
                                                          Text("Severe Disability : Rs. 1,25,000"),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                  actions: [
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                      children: [
                                                        TextButton(
                                                            onPressed: () {
                                                              if (_computationController.radioValue.value.toString() == "Normal Disability") {
                                                                setState(() {
                                                                  _computationController.computationHeadListTotalDeduction[index].value = "75000";
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  _computationController.computationHeadListTotalDeduction[index].value = "125000";
                                                                });
                                                              }

                                                              Get.back(); //   print("_computationController.radioValue.value    ---- "+_computationController.radioValue.value.toString());
                                                            },
                                                            child: Text("OK")),
                                                        TextButton(
                                                            onPressed: () {
                                                              Get.back(); //   print("_computationController.radioValue.value    ---- "+_computationController.radioValue.value.toString());
                                                            },
                                                            child: Text("Cancel")),
                                                      ],
                                                    )
                                                  ],
                                                );
                                              }

                                              if (newValue == "80G") {
                                                Get.defaultDialog(
                                                  barrierDismissible: false,
                                                  title: "Donation ",
                                                  content: Column(
                                                    children: [
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                        children: [
                                                          Row(
                                                            children: [
                                                              Obx(() => Radio(
                                                                  value: "50 %",
                                                                  groupValue: _computationController.politicalradio.value,
                                                                  onChanged: (value) {
                                                                    _computationController.setpolitical(value);
                                                                  })),
                                                              SizedBox(
                                                                width: 5,
                                                              ),
                                                              Text("50 %")
                                                            ],
                                                          ),
                                                          Row(
                                                            children: [
                                                              Obx(() => Radio(
                                                                  value: "100 %",
                                                                  groupValue: _computationController.politicalradio.value,
                                                                  onChanged: (value) {
                                                                    _computationController.setpolitical(value);
                                                                    // print("_computationController.radioValue.value;"+_computationController.radioValue.value.toString());
                                                                  })),
                                                              SizedBox(
                                                                width: 5,
                                                              ),
                                                              Text("100 %"),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      TextField(
                                                        controller: _computationController.amount80GController,
                                                        decoration: InputDecoration(
                                                            hintText: "Enter Amount",
                                                            contentPadding: EdgeInsets.all(5),
                                                            border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey))),
                                                      ),
                                                      SizedBox(height: 20),
                                                      TextField(
                                                        controller: _computationController.remark80GController,
                                                        decoration: InputDecoration(
                                                            hintText: "Remarks",
                                                            contentPadding: EdgeInsets.all(5),
                                                            border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey))),
                                                      ),
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      Text(
                                                        "* Allowed donation in cash upto Rs.2000/-",
                                                        style: TextStyle(color: Colors.red, fontSize: 12),
                                                      ),
                                                    ],
                                                  ),
                                                  actions: [
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                      children: [
                                                        TextButton(
                                                            onPressed: () {
                                                              setState(() {
                                                                _computationController.computationHeadListTotalDeduction[index].value =
                                                                    _computationController.amount80GController.text;
                                                              });
                                                              if (_computationController.radioValue.value.toString() == "50%") {
                                                                setState(() {
                                                                  print(_computationController.computationHeadListTotalDeduction[index].value);
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  print(_computationController.computationHeadListTotalDeduction[index].value);
                                                                });
                                                              }
                                                              print("passss------------- " + _computationController.remark80GController.text);

                                                              Get.back();
                                                              _computationController.amount80GController.clear();
                                                              _computationController.remark80GController.clear();
                                                            },
                                                            child: Text("OK")),
                                                        TextButton(
                                                            onPressed: () {
                                                              Get.back(); //   print("_computationController.radioValue.value    ---- "+_computationController.radioValue.value.toString());
                                                            },
                                                            child: Text("Cancel")),
                                                      ],
                                                    )
                                                  ],
                                                );
                                              }

                                              if (newValue == "Other") {
                                                // getDialog(_computationController, index);
                                                Get.defaultDialog(
                                                    barrierDismissible: false,
                                                    contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                                                    title: "Add Deduction",
                                                    titleStyle: const TextStyle(fontSize: 15),
                                                    content: Column(
                                                      children: [
                                                        SizedBox(
                                                          height: 30,
                                                          child: TextField(
                                                            keyboardType: TextInputType.name,
                                                            controller: _computationController.dialogNameController,
                                                            decoration: InputDecoration(
                                                                hintText: "Enter Deduction Name",
                                                                contentPadding: const EdgeInsets.all(5),
                                                                border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(5),
                                                                    borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        SizedBox(
                                                          height: 30,
                                                          child: TextField(
                                                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                                                            controller: _computationController.dialogAmountController,
                                                            decoration: InputDecoration(
                                                                hintText: "Enter Amount",
                                                                contentPadding: const EdgeInsets.all(5),
                                                                border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(5),
                                                                    borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        )
                                                      ],
                                                    ),
                                                    actions: [
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [
                                                          TextButton(
                                                              onPressed: () {
                                                                _computationController.dialogAmountController.clear();
                                                                _computationController.dialogNameController.clear();

                                                                FocusManager.instance.primaryFocus?.unfocus();
                                                                Get.back();
                                                              },
                                                              child: const Text("Cancel")),
                                                          TextButton(
                                                              onPressed: () {
                                                                _computationController.computationHeadListTotalDeduction[index].newname =
                                                                    _computationController.dialogNameController.text;
                                                                _computationController.computationHeadListTotalDeduction[index].value =
                                                                    _computationController.dialogAmountController.text;

                                                                //      _computationController.dialogNameController.clear();
                                                                // _computationController.dialogAmountController.clear();

                                                                Get.back();

                                                                // _computationController.getTotalDeduction();
                                                                _computationController.getTotalIncome();

                                                                setState(() {
                                                                  // print(_computationController.computationHeadListTotalDeduction[index].value);
                                                                });
                                                                _computationController.dialogAmountController.clear();
                                                                _computationController.dialogNameController.clear();
                                                              },
                                                              child: const Text("OK")),
                                                        ],
                                                      )
                                                    ]);
                                              }

                                              dropDownDeduction = newValue!;
                                              _computationController.computationHeadListTotalDeduction[index].name = newValue;
                                            });
                                          },
                                          items: <String>[
                                            '80C',
                                            '80CCC',
                                            '80CCD(1)',
                                            '80CCD(1B)',
                                            '80D',
                                            '80EE',
                                            '80EEA',
                                            '80EEB',
                                            '80G',
                                            '80GG',
                                            '80GGB/80GGC',
                                            '80TTA',
                                            '80TTB',
                                            '80U',
                                            'Other'
                                          ].map<DropdownMenuItem<String>>((String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                        ),

                                        // TextField(
                                        //   onChanged: (newValue) {
                                        //     _computationController.computationHeadListTotalDeduction[index].name = newValue;
                                        //     _computationController.getTotalOtherIncome();
                                        //     setState(() {});
                                        //   },
                                        //   decoration: InputDecoration(
                                        //       border: OutlineInputBorder(
                                        //           borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.grey, width: 1))),
                                        // ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Flexible(
                                      child: Container(
                                        height: 40,
                                        child: TextField(
                                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                                          controller: updateController[index],
                                          onChanged: (newValue) {
                                            _computationController.computationHeadListTotalDeduction[index].value = newValue;
                                            _computationController.totalDeductionController.clear();
                                            // _computationController.getTotalDeduction();
                                            _computationController.getTotalIncome();

                                            // setState(() {});
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1))),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            );
                          }),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
                            onPressed: () {
                              double total80cd = 0;
                              double total80c = 0;
                              totalEGT = 0;
                              _computationController.totalTaxPayableController.clear();
                              _computationController.tdsController.clear();
                              _computationController.refundController.clear();

                              for (int i = 0; i < _computationController.computationHeadListTotalDeduction.length; i++) {
                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80C") {
                                  total80c += double.parse(_computationController.computationHeadListTotalDeduction[i].value.toString());
                                }
                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80CCC") {
                                  total80c += double.parse(_computationController.computationHeadListTotalDeduction[i].value.toString());
                                }
                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80CCD(1)") {
                                  total80c += double.parse(_computationController.computationHeadListTotalDeduction[i].value.toString());
                                }

                                if (_computationController.computationHeadListTotalDeduction[i].name == "80CCD(1B)") {
                                  if (total80c > 150000) {
                                    if (double.parse(_computationController.computationHeadListTotalDeduction[i].value) > 50000) {
                                      _computationController.computationHeadListTotalDeduction[i].value = "50000";
                                      total80cd = 50000;
                                    } else {
                                      total80cd = double.parse(_computationController.computationHeadListTotalDeduction[i].value);
                                    }
                                  } else {
                                    total80cd = 200000 - total80c;
                                    if (double.parse(_computationController.computationHeadListTotalDeduction[i].value) > total80cd) {
                                      _computationController.computationHeadListTotalDeduction[i].value = total80cd.toString();
                                    } else {
                                      total80cd = double.parse(_computationController.computationHeadListTotalDeduction[i].value);
                                    }
                                  }
                                }
                              }
                              if (total80c > 150000) {
                                total80c = 150000;
                              }
                              print(" count----------==== " + total80c.toString());
                              print("80ccd(1b)======-----" + total80cd.toString());

                              for (int i = 0; i < _computationController.computationHeadListTotalDeduction.length; i++) {
                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80D") {
                                  if (double.parse(_computationController.computationHeadListTotalDeduction[i].value) > 100000) {
                                    _computationController.computationHeadListTotalDeduction[i].value = "100000";
                                  }
                                  print("80D-------= " + _computationController.computationHeadListTotalDeduction[i].value);
                                }

                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80EE") {
                                  if (double.parse(_computationController.computationHeadListTotalDeduction[i].value) > 50000) {
                                    _computationController.computationHeadListTotalDeduction[i].value = "50000";
                                  }
                                  print("80EE------= " + _computationController.computationHeadListTotalDeduction[i].value);
                                }

                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80EEA") {
                                  if (double.parse(_computationController.computationHeadListTotalDeduction[i].value) > 150000) {
                                    _computationController.computationHeadListTotalDeduction[i].value = "150000";
                                  }
                                  print("80EEA------= " + _computationController.computationHeadListTotalDeduction[i].value);
                                }
                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80EEB") {
                                  if (double.parse(_computationController.computationHeadListTotalDeduction[i].value) > 150000) {
                                    _computationController.computationHeadListTotalDeduction[i].value = "150000";
                                  }
                                  print("80EEB------= " + _computationController.computationHeadListTotalDeduction[i].value);
                                }
                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80GG") {
                                  if (double.parse(_computationController.computationHeadListTotalDeduction[i].value) > 60000) {
                                    _computationController.computationHeadListTotalDeduction[i].value = "60000";
                                  }
                                  print("80GG------= " + _computationController.computationHeadListTotalDeduction[i].value);
                                }

                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80TTA") {
                                  if (double.parse(_computationController.computationHeadListTotalDeduction[i].value) > 10000) {
                                    _computationController.computationHeadListTotalDeduction[i].value = "10000";
                                  }
                                  print("80TTA------= " + _computationController.computationHeadListTotalDeduction[i].value);
                                }
                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80TTB") {
                                  if (double.parse(_computationController.computationHeadListTotalDeduction[i].value) > 50000) {
                                    _computationController.computationHeadListTotalDeduction[i].value = "50000";
                                  }
                                  print("80TTB------= " + _computationController.computationHeadListTotalDeduction[i].value);
                                }
                              }
                              for (int i = 0; i < _computationController.computationHeadListTotalDeduction.length; i++) {
                                if (_computationController.computationHeadListTotalDeduction[i].name.toString() == "80D" ||
                                    _computationController.computationHeadListTotalDeduction[i].name.toString() == "Other" ||
                                    _computationController.computationHeadListTotalDeduction[i].name.toString() == "80EE" ||
                                    _computationController.computationHeadListTotalDeduction[i].name.toString() == "80EEA" ||
                                    _computationController.computationHeadListTotalDeduction[i].name.toString() == "80EEB" ||
                                    _computationController.computationHeadListTotalDeduction[i].name.toString() == "80G" ||
                                    _computationController.computationHeadListTotalDeduction[i].name.toString() == "80GG" ||
                                    _computationController.computationHeadListTotalDeduction[i].name.toString() == "80GGB/80GGC" ||
                                    _computationController.computationHeadListTotalDeduction[i].name.toString() == "80TTA" ||
                                    _computationController.computationHeadListTotalDeduction[i].name.toString() == "80TTB" ||
                                    _computationController.computationHeadListTotalDeduction[i].name.toString() == "80U") {
                                  totalEGT += double.parse(_computationController.computationHeadListTotalDeduction[i].value);
                                }
                              }
                              _computationController.totalDeductionController.text = (totalEGT + total80c + total80cd).toString();
                              _computationController.getTotalIncome();

                              Get.defaultDialog(
                                  barrierDismissible: false,
                                  title: "Age",
                                  content: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Text("Select Your Age"),
                                      Row(
                                        children: [
                                          Obx(() => Radio(
                                              value: "0 to 60",
                                              groupValue: _computationController.ageValue.value,
                                              onChanged: (value) {
                                                _computationController.selectAge(value);
                                              })),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text("0 to 60")
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Obx(() => Radio(
                                              value: "60 to 80",
                                              groupValue: _computationController.ageValue.value,
                                              onChanged: (value) {
                                                _computationController.selectAge(value);
                                              })),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text("60 to 80")
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Obx(() => Radio(
                                              value: "80 & above",
                                              groupValue: _computationController.ageValue.value,
                                              onChanged: (value) {
                                                _computationController.selectAge(value);
                                              })),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text("80 & above")
                                        ],
                                      ),
                                    ],
                                  ),
                                  actions: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        TextButton(
                                            onPressed: () {
                                              if (_computationController.ageValue.value.toString() == "0 to 60") {
                                                newage = 1;
                                              } else if (_computationController.ageValue.value.toString() == "60 to 80") {
                                                newage = 2;
                                              } else {
                                                newage = 3;
                                              }

                                              Get.back();

                                              print("age-------= " + newage.toString());
                                              print("total income------" + _computationController.totalIncomeController.text);
                                              print("total ded------" + _computationController.totalDeductionController.text);
                                              _computationController.totalTaxOld(double.parse(_computationController.totalIncomeController.text),
                                                  double.parse(_computationController.totalDeductionController.text), newage);

                                              print("taxpayable--------" + _computationController.totalTaxPayableController.text);
                                              if (_computationController.tdsController.text.isEmpty) {
                                                _computationController.tdsController.text = "0";
                                                _computationController.GetTotalRefund();
                                              } else {
                                                _computationController.GetTotalRefund();
                                              }

                                              _computationController.taxcredit();
                                              _computationController.health();
                                              _computationController.totTax();
                                            },
                                            child: Text("OK")),
                                        TextButton(
                                            onPressed: () {
                                              Get.back();
                                            },
                                            child: Text("Cancel")),
                                      ],
                                    )
                                  ]);
                            },
                            child: Text("Calculate"),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    //8
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Flexible(
                          child: Text("Total Income (Taxable)"),
                        ),
                        const Flexible(
                          child: Text(
                            "Rounded off from ₹0 as per Section 288A",
                            style: TextStyle(fontSize: 10),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              controller: _computationController.totalIncomeController,
                             readOnly: true,
                              decoration: InputDecoration(
                                  isDense:true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),

                    const SizedBox(
                      height: 10,
                    ),

                    //9
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Flexible(
                          child: Text("Total Tax Payable"),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.phone,
                              controller: _computationController.totalTaxPayableController,
                              decoration: InputDecoration(
                                  isDense:true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    const SizedBox(
                      height: 20,
                    ),

                    //10
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Flexible(
                          child: Text("Less : Taxes Paid"),
                        ),
                        const Flexible(child: Text("TDS")),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              onChanged: (value) {
                                _computationController.GetTotalRefund();
                                setState(() {});
                              },
                              controller: _computationController.tdsController,
                              decoration: InputDecoration(
                                  isDense:true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    const SizedBox(
                      height: 20,
                    ),


                    //10
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Flexible(
                          child: Text("Refund"),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(decimal: true),
                              controller: _computationController.refundController,
                              decoration: InputDecoration(
                                  isDense:true,
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                            ),
                          ),
                        ),
                      ],
                    ),

                    const SizedBox(
                      height: 10,
                    ),

                    const Text(
                      "Taxes are applicable as per normal provision Please refer Annexure for details",
                      style: TextStyle(color: Colors.red),
                    ),

                    //Income Tax
                    const SizedBox(
                      height: 20,
                    )
                  ],
                ),
                SizedBox(
                  height: 70,
                )
              ],
            ),
          ),
        ),
      ),
    ));
  }
}

void getDialog(ComputationController _computationController, int index) {}
