import 'package:accountx/app/model/Annexure_model.dart';

import 'package:accountx/app/modules/financial_page/annexure_page/annexure_controller.dart';
import 'package:accountx/app/modules/financial_page/financial_view.dart';
import 'package:accountx/app/modules/financial_page/profit_and_loss_page/profit_and_loss_controller.dart';
import 'package:accountx/app/modules/homePage/homeView.dart';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/utils.dart';
import 'package:get_storage/get_storage.dart';

class AnnexureView extends StatefulWidget {
  const AnnexureView({Key? key}) : super(key: key);

  @override
  State<AnnexureView> createState() => _AnnexureViewState();
}

class _AnnexureViewState extends State<AnnexureView> {
  var controller = Get.put(AnnexureController());
  GetStorage box = GetStorage();
  Future<double>? a;

  @override
  void initState() {

    controller.getProfitAndLossApiCall();
    controller.addFixedAssets(
      "key",
    );
    try{
      controller.getTotalDepValue().then((value) {

        if(controller.fixedAssetList.isNotEmpty){
          controller.fixedAssetList[0].amount = value.toString();
          controller.fixedAssetList[0].name = "Fixed Assets";
          controller.depFixedAssetTotal.text = value.toString();
        }

      });
    }catch(e){
     // controller.fixedAssetList = [];

      controller.depFixedAssetTotal.text = "";

    }
    controller.getAnnexureApiCall().then((value) => setState(() {}));
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    print("-----fixedAssetList.length-----"+controller.fixedAssetList.length.toString());
    return Scaffold(
        body: Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.only(top: 20,left: 20, right: 20,bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Annexure",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,letterSpacing: 0.6),
                ),
                InkWell(
                  onTap: (){
                    initState();
                  },
                  child: Container(
                      color: Color(0xff0B28BE).withOpacity(.8),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Text("Sync Data",style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16,color: Colors.white),),
                            Icon(Icons.sync, color:Colors.white ,),
                          ],
                        ),
                      ))),],
            ),
          ),
          Divider(),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "LIABILITIES",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            "Capital A/C",
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),
          //1
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.capitalopeningBalanceController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      labelText: "Opening Balance",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
              SizedBox(
                width: 5,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.capitalinterestController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      labelText: "Interest on SB A/C",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),
          //2
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  controller: controller.capitaltdsFYController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      labelText: "TDS F.Y. 2018-19",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
              SizedBox(
                width: 5,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.capitalNetProfitController,
                  readOnly: true,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      labelText: "NET PROFIT",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          //3
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  // print("------addcr---------"+controller.crAdd[1].amount);
                  // print("------draddd---------"+controller.drAdd[1].drname);

                  Get.defaultDialog(
                      barrierDismissible: false,
                      title: "Add New Head",
                      content: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            height: 40,
                            child: TextField(
                              controller: controller.capitalNewHeadController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  hintText: "Enter head",
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey),
                                  )),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: controller.capitalNewAmountController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  hintText: "Enter amount",
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey),
                                  )),
                            ),
                          )
                        ],
                      ),
                      actions: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TextButton(
                                onPressed: () {
                                  controller.crAdd.add(CRModel(
                                      head: controller
                                          .capitalNewHeadController.text,
                                      amount: controller
                                          .capitalNewAmountController.text));
                                  setState(() {});

                                  Get.back();

                                  controller.capitalNewHeadController.clear();
                                  controller.capitalNewAmountController.clear();
                                },
                                child: Text("OK")),
                            TextButton(
                                onPressed: () {
                                  Get.back();
                                  controller.capitalNewHeadController.clear();
                                  controller.capitalNewAmountController.clear();
                                },
                                child: Text("Cancel"))
                          ],
                        )
                      ]);

                  setState(() {});
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              Flexible(
                  child: Container(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: Text("Add (CR)"),
              )),
              SizedBox(
                width: 1,
              ),
            ],
          ),

          Container(
            child: Column(
              children: List.generate(controller.crAdd.length, (index) {
                List<TextEditingController> capitalCRNameController =
                    List.generate(controller.crAdd.length,
                        (index) => TextEditingController());

                List<TextEditingController> capitalCRAmountController =
                    List.generate(controller.crAdd.length,
                        (index) => TextEditingController());

                capitalCRNameController[index].text =
                    controller.crAdd[index].head;
                capitalCRAmountController[index].text =
                    controller.crAdd[index].amount;

                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.crAdd.removeAt(index);
                            setState(() {});
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            controller: capitalCRNameController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            controller: capitalCRAmountController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                );
              }),
            ),
          ),

          SizedBox(
            height: 10,
          ),

          Row(
            children: [
              CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 12,
                /*child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),*/
              ),
              SizedBox(
                width: 5,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: Container(
                  padding: EdgeInsets.only(left: 5),
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width,
                  height: 40,
                  child: Text("Total (CR)",style: TextStyle(
                    fontWeight: FontWeight.w600,
                    letterSpacing: 0.6,
                    fontSize: 16,

                  ),),
                ),
              )),
              SizedBox(
                width: 1,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.capitalTotalCrController,
                  readOnly: true,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          //4
          SizedBox(
            height: 20,
          ),

          Row(
            children: [
              CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 12,
                /*child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),*/
              ),
              SizedBox(
                width: 5,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: Container(
                  padding: EdgeInsets.only(left: 5),
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width,
                  height: 40,
                  child: Text("TDS"),
                ),
              )),
              SizedBox(
                width: 1,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.capitaltdsDrController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          //4
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              InkWell(
                onTap: () {
                  Get.defaultDialog(
                      barrierDismissible: false,
                      title: "Add New Head",
                      content: Column(
                        children: [
                          SizedBox(
                            height: 40,
                            child: TextField(
                              controller: controller.drawingNameController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  hintText: "Enter head",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: BorderSide(
                                          color: Colors.grey, width: 1))),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                            height: 40,
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: controller.drawingAmountController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  hintText: "Enter amount",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: BorderSide(
                                          color: Colors.grey, width: 1))),
                            ),
                          ),
                        ],
                      ),
                      actions: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TextButton(
                                onPressed: () {
                                  controller.drAdd.add(DRModel(
                                      drname:
                                          controller.drawingNameController.text,
                                      dramount: controller
                                          .drawingAmountController.text));
                                  setState(() {});
                                  Get.back();
                                  controller.drawingNameController.clear();
                                  controller.drawingAmountController.clear();
                                },
                                child: Text("OK")),
                            TextButton(
                                onPressed: () {
                                  setState(() {});

                                  Get.back();
                                  controller.drawingNameController.clear();
                                  controller.drawingAmountController.clear();
                                },
                                child: Text("Cancel")),
                          ],
                        )
                      ]);
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: Container(
                  padding: EdgeInsets.only(left: 5),
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width,
                  height: 40,
                  child: Text("Drawings"),
                ),
              )),
              /*SizedBox(
                    width: 1,
                  ),*/
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.capitalTotalDrawingController,
                  readOnly: true,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: List.generate(controller.drAdd.length, (index) {
                List<TextEditingController> capitalDRNameController =
                    List.generate(controller.drAdd.length,
                        (index) => TextEditingController());

                List<TextEditingController> capitalDRAmountController =
                    List.generate(controller.drAdd.length,
                        (index) => TextEditingController());
                capitalDRNameController[index].text =
                    controller.drAdd[index].drname;
                capitalDRAmountController[index].text =
                    controller.drAdd[index].dramount;
                return Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.drAdd.removeAt(index);
                            setState(() {});
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            flex: 4 ,
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            controller: capitalDRNameController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          flex: 5 ,
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            controller: capitalDRAmountController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    )
                  ],
                );
              }),
            ),
          ),

          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 12,
                /*child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),*/
              ),
              SizedBox(
                width: 5,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: Container(
                  padding: EdgeInsets.only(left: 5),
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width,
                  height: 40,
                  child: Text("Total (DR)",style: TextStyle(
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.6,
                      fontSize: 16
                  )),
                ),
              )),
              SizedBox(
                width: 5,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.capitalTotalDRController,
                  readOnly: true,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          SizedBox(
            height: 20,
          ),

          Row(
            children: [
              CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 12,
                /*child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),*/
              ),
              SizedBox(
                width: 5,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: Container(
                  padding: EdgeInsets.only(left: 5),
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width,
                  height: 40,
                  child: Text("Balance",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 0.5,

                  ),),
                ),
              )),
              SizedBox(
                width: 5,
              ),

              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.capitalTotalBalanceController,
                  readOnly: true,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          SizedBox(
            height: 10,
          ),
          Divider(),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  controller.addprovisions(
                    "key",
                  );

                  setState(() {});
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              Flexible(
                  child: Container(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: Text("Provisions"),
              )),
              SizedBox(
                width: 1,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.provisionController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          SizedBox(
            height: 10,
          ),
          Container(
            child: Column(
              children:
                  List.generate(controller.provisionsList.length, (index) {
                List<TextEditingController> liabilityNameController =
                    List.generate(controller.provisionsList.length,
                        (index) => TextEditingController());

                List<TextEditingController> liabilityAmountController =
                    List.generate(controller.provisionsList.length,
                        (index) => TextEditingController());

                liabilityNameController[index].text =
                    controller.provisionsList[index].name;
                liabilityAmountController[index].text =
                    controller.provisionsList[index].amount;

                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.provisionsList.removeAt(index);
                            setState(() {});
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            onChanged: (val) {
                              controller.provisionsList[index].name = val;
                            },
                            controller: liabilityNameController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              controller.provisionsList[index].amount = val;
                            },
                            controller: liabilityAmountController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                );
              }),
            ),
          ),

          SizedBox(
            height: 20,
          ),
          // Text(
          //   "Fixed Assets",
          //   style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          // ),
          // //1
          // SizedBox(
          //   height: 20,
          // ),

          // Row(
          //   children: [
          //     Flexible(
          //         child: SizedBox(
          //       height: 40,
          //       child: TextField(
          //         decoration: InputDecoration(
          //             isDense: true,
          //             contentPadding: EdgeInsets.all(10),
          //             labelText: "Particular",
          //             border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(width: 1, color: Colors.grey))),
          //       ),
          //     )),
          //     SizedBox(
          //       width: 5,
          //     ),
          //     Flexible(
          //         child: SizedBox(
          //       height: 40,
          //       child: TextField(
          //         decoration: InputDecoration(
          //             isDense: true,
          //             contentPadding: EdgeInsets.all(10),
          //             labelText: "Amount",
          //             border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: BorderSide(width: 1, color: Colors.grey))),
          //       ),
          //     )),
          //   ],
          // ),
          // //2
          // SizedBox(
          //   height: 20,
          // ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  controller.addDutiesAndTaxes(
                    "key",
                  );

                  setState(() {});
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              Flexible(
                  child: Container(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: Text("Duties And Taxes"),
              )),
              SizedBox(
                width: 1,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.dutiesController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          Container(
            child: Column(
              children:
                  List.generate(controller.dutiesAndTaxesList.length, (index) {
                List<TextEditingController> liabilityNameController =
                    List.generate(controller.dutiesAndTaxesList.length,
                        (index) => TextEditingController());

                List<TextEditingController> liabilityAmountController =
                    List.generate(controller.dutiesAndTaxesList.length,
                        (index) => TextEditingController());

                liabilityNameController[index].text =
                    controller.dutiesAndTaxesList[index].name;
                liabilityAmountController[index].text =
                    controller.dutiesAndTaxesList[index].amount;

                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.dutiesAndTaxesList.removeAt(index);
                            setState(() {});
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            onChanged: (val) {
                              controller.dutiesAndTaxesList[index].name = val;
                            },
                            controller: liabilityNameController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              controller.dutiesAndTaxesList[index].amount = val;
                            },
                            controller: liabilityAmountController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                );
              }),
            ),
          ),

          SizedBox(
            height: 20,
          ),

          Row(
            children: [
              InkWell(
                onTap: () {
                  Get.defaultDialog(
                      barrierDismissible: false,
                      title: "Add New Liabilities",
                      content: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            height: 40,
                            child: TextField(
                              controller: controller.dialogNameController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  hintText: "Enter head",
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey),
                                  )),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          // SizedBox(
                          //   height: 40,
                          //   child: TextField(
                          //     controller: controller.dialogAmountController,
                          //     decoration: InputDecoration(
                          //         isDense: true,
                          //         contentPadding: EdgeInsets.all(10),
                          //         hintText: "Enter amount",
                          //         border: OutlineInputBorder(
                          //           borderRadius: BorderRadius.circular(5),
                          //           borderSide: BorderSide(width: 1, color: Colors.grey),
                          //         )),
                          //   ),
                          // )
                        ],
                      ),
                      actions: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TextButton(
                                onPressed: () {
                                  // controller.addmoreAsset(controller.MoreAssetAddList[index].assetHead[i].key)
                                  controller.addmoreLiabilities(
                                      controller.dialogNameController.text);
                                  //controller.liabilityAddList.add(liabilityModel(lname: controller.liabilityNameController.text, lamount: controller.liabilityAmountController.text));
                                  setState(() {});

                                  Get.back();
                                  //
                                  // controller.liabilityNameController.clear();
                                  // controller.liabilityAmountController.clear();
                                },
                                child: Text("OK")),
                            TextButton(
                                onPressed: () {
                                  Get.back();
                                  // controller.liabilityNameController.clear();
                                  // controller.liabilityAmountController.clear();
                                },
                                child: Text("Cancel"))
                          ],
                        )
                      ]);

                  setState(() {});
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Text("Add more liabilities"),
              SizedBox(
                width: 1,
              ),
            ],
          ),

          SizedBox(height: 20),

          Container(
            child: Column(
              children: List.generate(controller.MoreLiabilitiesAddList.length,
                  (index) {
                //  List<TextEditingController> AssetNameController =
                //   List.generate(controller.AssetAddList.length, (index) => TextEditingController());

                List<TextEditingController> extraLiablitesTotalController =
                    List.generate(controller.MoreLiabilitiesAddList.length,
                        (index) => TextEditingController());

                var a = 0.0;
                controller.MoreLiabilitiesAddList[index].liabilityhead
                    .forEach((e) {
                  a += double.parse(e.lamount.isEmpty ? "0.0" : e.lamount);
                });

                extraLiablitesTotalController[index].text = a.toString();

                // AssetNameController[index].text = controller.AssetAddList[index].assetname;
                // extraLiablitesTotalController[index].text = controller.MoreLiabilitiesAddList[index].total;

                return Column(
                  children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(controller.MoreLiabilitiesAddList[index].key,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15)),
                          Container(
                            decoration: BoxDecoration(
                                color: Color(0xff1739E4),
                                border: Border.all(color: Color(0xff1739E4)),
                                borderRadius: BorderRadius.circular(10)),
                            child: SizedBox(
                              height: 40,
                              child: Row(
                                children: [
                                  TextButton(
                                      style: ButtonStyle(
                                          overlayColor:
                                              MaterialStateProperty.all(
                                                  Color.fromARGB(
                                                      255, 96, 121, 244))),
                                      onPressed: () {
                                        controller.addliabilities("key", index);
                                        setState(() {});
                                      },
                                      child: Icon(
                                        Icons.add,
                                        color: Colors.white,
                                      )),
                                  TextButton(
                                      style: ButtonStyle(
                                          overlayColor:
                                              MaterialStateProperty.all(
                                                  Color.fromARGB(
                                                      255, 96, 121, 244))),
                                      onPressed: () {
                                        controller.MoreLiabilitiesAddList
                                            .removeAt(index);
                                        setState(() {});
                                      },
                                      child: Icon(
                                        Icons.remove,
                                        color: Colors.white,
                                      ))
                                ],
                              ),
                            ),
                          )
                        ]),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: Column(
                        children:
                            //List.generate(controller.AssetAddList.length, (i)
                            List.generate(
                                controller.MoreLiabilitiesAddList[index]
                                    .liabilityhead.length, (i) {
                          List<TextEditingController> liabNameController =
                              List.generate(
                                  controller.MoreLiabilitiesAddList[index]
                                      .liabilityhead.length,
                                  (i) => TextEditingController());

                          List<TextEditingController> liabAmountController =
                              List.generate(
                                  controller.MoreLiabilitiesAddList[index]
                                      .liabilityhead.length,
                                  (i) => TextEditingController());

                          liabNameController[i].text = controller
                              .MoreLiabilitiesAddList[index]
                              .liabilityhead[i]
                              .lname;
                          liabAmountController[i].text = controller
                              .MoreLiabilitiesAddList[index]
                              .liabilityhead[i]
                              .lamount;

                          return Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  IconButton(
                                    onPressed: () {
                                      //print('-------------------$i');
                                      // print("---------------hello world------------" + controller.MoreAssetAddList[index].assetHead.toString());
                                      controller.MoreLiabilitiesAddList[index]
                                          .liabilityhead
                                          .removeAt(i);

                                      print("working");
                                      // controller.AssetAddList.removeAt(index);
                                      setState(() {});
                                    },
                                    icon: Icon(
                                      Icons.delete,
                                      color: Colors.red,
                                    ),
                                  ),
                                  Flexible(
                                      child: SizedBox(
                                    height: 40,
                                    child: TextField(
                                      onChanged: (val) {
                                        controller.MoreLiabilitiesAddList[index]
                                            .liabilityhead[i].lname = val;
                                        // controller.AssetAddList[index].assetname=val;
                                      },
                                      controller: liabNameController[i],
                                      decoration: InputDecoration(
                                          isDense: true,
                                          contentPadding: EdgeInsets.all(10),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey))),
                                    ),
                                  )),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Flexible(
                                      child: SizedBox(
                                    height: 40,
                                    child: TextField(
                                      keyboardType: TextInputType.number,
                                      onChanged: (val) {
                                        // controller.AssetAddList[index].assetamount=val;
                                        controller.MoreLiabilitiesAddList[index]
                                            .liabilityhead[i].lamount = val;
                                      },
                                      controller: liabAmountController[i],
                                      decoration: InputDecoration(
                                          isDense: true,
                                          contentPadding: EdgeInsets.all(10),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey))),
                                    ),
                                  )),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              )
                            ],
                          );
                        }),
                      ),
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: Container(
                            padding: EdgeInsets.only(left: 5),
                            alignment: Alignment.centerLeft,
                            width: MediaQuery.of(context).size.width,
                            height: 40,
                            child: Text("TOTAL"),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            controller: extraLiablitesTotalController[index],
                            readOnly: true,
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                );
              }),
            ),
          ),

          Text(
            "ASSETS",
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),

          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  controller.addFixedAssets(
                    "key",
                  );

                  setState(() {});
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              Flexible(
                  child: Container(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: Text("Fixed Assets (Total)"),
              )),
              SizedBox(
                width: 1,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.fixedAssetsController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          SizedBox(
            height: 20,
          ),

          Row(
            children: [
              CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 12,
                /*child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),*/
              ),
              SizedBox(
                width: 5,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: Container(
                  padding: EdgeInsets.only(left: 5),
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width,
                  height: 40,
                  child: Text("Fixed Assets (From Dep)"),
                ),
              )),
              SizedBox(
                width: 1,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  onChanged: (val) {
                    //  controller.fixedAssetList[0].amount = v;
                  },
                  controller: controller.depFixedAssetTotal,
                  readOnly: true,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          //4
          SizedBox(
            height: 20,
          ),
          Container(
            child: Column(
              children:
                  List.generate(controller.fixedAssetList.length==0?controller.fixedAssetList.length:controller.fixedAssetList.length -1, (index) {
                List<TextEditingController> liabilityNameController =
                    List.generate(controller.fixedAssetList.length,
                        (index) => TextEditingController());

                List<TextEditingController> liabilityAmountController =
                    List.generate(controller.fixedAssetList.length,
                        (index) => TextEditingController());

                liabilityNameController[index + 1].text =
                    controller.fixedAssetList[index + 1].name;
                liabilityAmountController[index + 1].text =
                    controller.fixedAssetList[index + 1].amount;

                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.fixedAssetList.removeAt(index + 1);
                            setState(() {});
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            onChanged: (val) {
                              controller.fixedAssetList[index + 1].name = val;
                            },
                            controller: liabilityNameController[index + 1],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              controller.fixedAssetList[index + 1].amount = val;
                            },
                            controller: liabilityAmountController[index + 1],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                );
              }),
            ),
          ),

          SizedBox(
            height: 20,
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  controller.addSundryDebtor(
                    "key",
                  );

                  setState(() {});
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              Flexible(
                  child: Container(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: Text("Sundry Debtors"),
              )),
              SizedBox(
                width: 1,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.debtorsController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          Container(
            child: Column(
              children:
                  List.generate(controller.sundryDebtorList.length, (index) {
                List<TextEditingController> liabilityNameController =
                    List.generate(controller.sundryDebtorList.length,
                        (index) => TextEditingController());

                List<TextEditingController> liabilityAmountController =
                    List.generate(controller.sundryDebtorList.length,
                        (index) => TextEditingController());

                liabilityNameController[index].text =
                    controller.sundryDebtorList[index].name;
                liabilityAmountController[index].text =
                    controller.sundryDebtorList[index].amount;

                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.sundryDebtorList.removeAt(index);
                            setState(() {});
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            onChanged: (val) {
                              controller.sundryDebtorList[index].name = val;
                            },
                            controller: liabilityNameController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              controller.sundryDebtorList[index].amount = val;
                            },
                            controller: liabilityAmountController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                );
              }),
            ),
          ),

          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  controller.addCashAndBankBalance(
                    "key",
                  );

                  setState(() {});
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              Flexible(
                  child: Container(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: Text("Cash And Bank Balance"),
              )),
              SizedBox(
                width: 1,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.cashController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          Container(
            child: Column(
              children: List.generate(controller.cashAndBankBalanceList.length,
                  (index) {
                List<TextEditingController> liabilityNameController =
                    List.generate(controller.cashAndBankBalanceList.length,
                        (index) => TextEditingController());

                List<TextEditingController> liabilityAmountController =
                    List.generate(controller.cashAndBankBalanceList.length,
                        (index) => TextEditingController());

                liabilityNameController[index].text =
                    controller.cashAndBankBalanceList[index].name;
                liabilityAmountController[index].text =
                    controller.cashAndBankBalanceList[index].amount;

                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.cashAndBankBalanceList.removeAt(index);
                            setState(() {});
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            onChanged: (val) {
                              controller.cashAndBankBalanceList[index].name =
                                  val;
                            },
                            controller: liabilityNameController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              controller.cashAndBankBalanceList[index].amount =
                                  val;
                            },
                            controller: liabilityAmountController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                );
              }),
            ),
          ),

          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  controller.addLoansAndAdvances(
                    "key",
                  );

                  setState(() {});
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              Flexible(
                  child: Container(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: Text("Loans And Advances "),
              )),
              SizedBox(
                width: 1,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.loansController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          Container(
            child: Column(
              children: List.generate(controller.loansAndAdvancesList.length,
                  (index) {
                List<TextEditingController> liabilityNameController =
                    List.generate(controller.loansAndAdvancesList.length,
                        (index) => TextEditingController());

                List<TextEditingController> liabilityAmountController =
                    List.generate(controller.loansAndAdvancesList.length,
                        (index) => TextEditingController());

                liabilityNameController[index].text =
                    controller.loansAndAdvancesList[index].name;
                liabilityAmountController[index].text =
                    controller.loansAndAdvancesList[index].amount;

                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.loansAndAdvancesList.removeAt(index);
                            setState(() {});
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            onChanged: (val) {
                              controller.loansAndAdvancesList[index].name = val;
                            },
                            controller: liabilityNameController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              controller.loansAndAdvancesList[index].amount =
                                  val;
                            },
                            controller: liabilityAmountController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                );
              }),
            ),
          ),

          SizedBox(
            height: 20,
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  controller.addClosingStock(
                    "key",
                  );

                  setState(() {});
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              Flexible(
                  child: Container(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: Text("Closing Stock"),
              )),
              SizedBox(
                width: 1,
              ),
              Flexible(
                  child: SizedBox(
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: controller.closingStockController,
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide:
                              BorderSide(width: 1, color: Colors.grey))),
                ),
              )),
            ],
          ),

          Container(
            child: Column(
              children:
                  List.generate(controller.closingStockList.length, (index) {
                List<TextEditingController> liabilityNameController =
                    List.generate(controller.closingStockList.length,
                        (index) => TextEditingController());

                List<TextEditingController> liabilityAmountController =
                    List.generate(controller.closingStockList.length,
                        (index) => TextEditingController());

                liabilityNameController[index].text =
                    controller.closingStockList[index].name;
                liabilityAmountController[index].text =
                    controller.closingStockList[index].amount;

                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.closingStockList.removeAt(index);
                            setState(() {});
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            onChanged: (val) {
                              controller.closingStockList[index].name = val;
                            },
                            controller: liabilityNameController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              controller.closingStockList[index].amount = val;
                            },
                            controller: liabilityAmountController[index],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                );
              }),
            ),
          ),

          SizedBox(
            height: 20,
          ),

          Row(
            children: [
              InkWell(
                onTap: () {
                  Get.defaultDialog(
                      barrierDismissible: false,
                      title: "Add New Assets",
                      content: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            height: 40,
                            child: TextField(
                              controller: controller.dialogNameController,
                              decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(10),
                                  hintText: "Enter head",
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey),
                                  )),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                      actions: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TextButton(
                                onPressed: () {
                                  controller.addmoreAsset(
                                      controller.dialogNameController.text);
                                  //controller.liabilityAddList.add(liabilityModel(lname: controller.liabilityNameController.text, lamount: controller.liabilityAmountController.text));
                                  setState(() {});

                                  Get.back();
                                  //
                                  // controller.liabilityNameController.clear();
                                  // controller.liabilityAmountController.clear();
                                },
                                child: Text("OK")),
                            TextButton(
                                onPressed: () {
                                  Get.back();
                                  // controller.liabilityNameController.clear();
                                  // controller.liabilityAmountController.clear();
                                },
                                child: Text("Cancel"))
                          ],
                        )
                      ]);

                  setState(() {});
                },
                child: CircleAvatar(
                  radius: 12,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Text("Add more assets"),
              SizedBox(
                width: 1,
              ),
            ],
          ),

          SizedBox(height: 10),

          Container(
            child: Column(
              children:
                  List.generate(controller.MoreAssetAddList.length, (index) {
                List<TextEditingController> extraAssetsTotalController =
                    List.generate(controller.MoreAssetAddList.length,
                        (index) => TextEditingController());

                var a = 0.0;
                controller.MoreAssetAddList[index].assetHead.forEach((e) {
                  a += double.parse(
                      e.assetamount.isEmpty ? "0.0" : e.assetamount);
                });

                extraAssetsTotalController[index].text = a.toString();

                return Column(
                  children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(controller.MoreAssetAddList[index].key,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15)),
                          Container(
                            decoration: BoxDecoration(
                                color: Color(0xff1739E4),
                                border: Border.all(color: Color(0xff1739E4)),
                                borderRadius: BorderRadius.circular(10)),
                            child: SizedBox(
                              height: 40,
                              child: Row(
                                children: [
                                  TextButton(
                                      style: ButtonStyle(
                                          overlayColor:
                                              MaterialStateProperty.all(
                                                  Color.fromARGB(
                                                      255, 96, 121, 244))),
                                      onPressed: () {
                                        controller.addAsset("key", index);
                                        setState(() {});
                                      },
                                      child: Icon(
                                        Icons.add,
                                        color: Colors.white,
                                      )),
                                  TextButton(
                                      style: ButtonStyle(
                                          overlayColor:
                                              MaterialStateProperty.all(
                                                  Color.fromARGB(
                                                      255, 96, 121, 244))),
                                      onPressed: () {
                                        controller.MoreAssetAddList.removeAt(
                                            index);
                                        setState(() {});
                                      },
                                      child: Icon(
                                        Icons.remove,
                                        color: Colors.white,
                                      ))
                                ],
                              ),
                            ),
                          )
                        ]),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: Column(
                        children:
                            //List.generate(controller.AssetAddList.length, (i)
                            List.generate(
                                controller.MoreAssetAddList[index].assetHead
                                    .length, (i) {
                          List<TextEditingController> AssetNameController =
                              List.generate(
                                  controller
                                      .MoreAssetAddList[index].assetHead.length,
                                  (i) => TextEditingController());

                          List<TextEditingController> AssetAmountController =
                              List.generate(
                                  controller
                                      .MoreAssetAddList[index].assetHead.length,
                                  (i) => TextEditingController());

                          AssetNameController[i].text = controller
                              .MoreAssetAddList[index].assetHead[i].assetname;
                          AssetAmountController[i].text = controller
                              .MoreAssetAddList[index].assetHead[i].assetamount;

                          return Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  IconButton(
                                    onPressed: () {
                                      controller
                                          .MoreAssetAddList[index].assetHead
                                          .removeAt(i);

                                      setState(() {});
                                    },
                                    icon: Icon(
                                      Icons.delete,
                                      color: Colors.red,
                                    ),
                                  ),
                                  Flexible(
                                      child: SizedBox(
                                    height: 40,
                                    child: TextField(
                                      onChanged: (val) {
                                        controller.MoreAssetAddList[index]
                                            .assetHead[i].assetname = val;
                                        // controller.AssetAddList[index].assetname=val;
                                      },
                                      controller: AssetNameController[i],
                                      decoration: InputDecoration(
                                          isDense: true,
                                          contentPadding: EdgeInsets.all(10),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey))),
                                    ),
                                  )),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Flexible(
                                      child: SizedBox(
                                    height: 40,
                                    child: TextField(
                                      keyboardType: TextInputType.number,
                                      onChanged: (val) {
                                        // controller.AssetAddList[index].assetamount=val;
                                        controller.MoreAssetAddList[index]
                                            .assetHead[i].assetamount = val;
                                      },
                                      controller: AssetAmountController[i],
                                      decoration: InputDecoration(
                                          isDense: true,
                                          contentPadding: EdgeInsets.all(10),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey))),
                                    ),
                                  )),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              )
                            ],
                          );
                        }),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: Container(
                            padding: EdgeInsets.only(left: 5),
                            alignment: Alignment.centerLeft,
                            width: MediaQuery.of(context).size.width,
                            height: 40,
                            child: Text("TOTAL"),
                          ),
                        )),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                            child: SizedBox(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            controller: extraAssetsTotalController[index],
                            readOnly: true,
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey))),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                );
              }),
            ),
          ),

          SizedBox(
            height: 20,
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                width: 150,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Color(0xff1739E4)),
                    onPressed: () {
//                        if(controller.capitalopeningBalanceController.text.isNotEmpty ||
// controller.capitalinterestController.text.isNotEmpty ||
// controller.capitaltdsFYController.text.isNotEmpty ||
// controller.capitalNetProfitController.text.isNotEmpty ||
// controller.capitaltdsDrController.text.isNotEmpty ){

                      if (controller
                          .capitalNetProfitController.text.isNotEmpty) {
                        if (controller
                            .capitalinterestController.text.isNotEmpty) {
                          if (controller
                              .capitaltdsFYController.text.isNotEmpty) {
                            if (controller.capitalopeningBalanceController.text
                                .isNotEmpty) {
                              if (controller
                                  .capitaltdsDrController.text.isNotEmpty) {
                                controller.TotalCr();
                                controller.TotalDrawings();
                                controller.totalBalance();
                                controller.annexureProvisionTotalBalance();
                                controller.annexureDutiesTotalBalance();
                                controller.annexureSundryDebtorsTotalBalance();
                                controller.annexureCashAndBankBalance();
                                controller.annexureLoanAndAdvanceTotalBalance();
                                controller.fixedAssetList[0].amount =
                                    controller.depFixedAssetTotal.text;
                                controller.annexureFixedAssetsTotalBalance();
                                controller.annexureClosingStockTotalBalance();

                                setState(() {});
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content:
                                            Text("TDS field is required !!")));
                              }
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                  content: Text(
                                      "Opening Balance field is required !!")));
                            }
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content:
                                    Text("TDS F.Y. field is required !!")));
                          }
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Interest field is required !!")));
                        }
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(
                                "Please go to the profit and loss page !!")));
                      }

// }else{
//   Fluttertoast.showToast(
//         msg: "This is a Toast message",  // message
//         toastLength: Toast.LENGTH_SHORT, // length
//         gravity: ToastGravity.CENTER,    // location
//                   // duration
//     );
// }

                      // controller.generateExcel();

                      // controller.getJsonAnnexureObject();
                    },
                    child: Text("Calculate")),
              ),
              SizedBox(
                width: 150,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Color(0xff1739E4)),
                    onPressed: () {
                      // controller.getJsonAnnexureObject();

                      if (controller.capitalTotalBalanceController.text
                          .toString()
                          .isEmpty) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Please click on Calculate Button")));
                      } else {
                        controller.annexurejsonApiCall(context);
                      }
                    },
                    child: Text("Submit")),
              ),
            ],
          ),

          SizedBox(
            height: 70,
          ),
        ]),
      ),
    ));
  }
}
