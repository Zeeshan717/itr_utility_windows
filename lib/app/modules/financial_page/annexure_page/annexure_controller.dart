import 'dart:convert';
import 'dart:io';

import 'package:accountx/app/model/annexure_json_model.dart';
import 'package:accountx/app/modules/financial_page/computation_page/computation_data.dart';
import 'package:accountx/app/modules/financial_page/profit_and_loss_page/profit_and_loss_controller.dart';
import 'package:accountx/app/services/api.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:get_storage/get_storage.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart' hide Row;
import 'package:flutter/material.dart' as m;

import '../../../model/Annexure_model.dart';

class AnnexureController extends GetxController {
  GetStorage box = GetStorage();
  TextEditingController drawingNameController = TextEditingController();
  TextEditingController drawingAmountController = TextEditingController();

  List<CRModel> crAdd = [];
  List<DRModel> drAdd = [];
  List<LiabilityModel> liabilityAddList = [];

  List<MoreAssetsModel> MoreAssetAddList = [];
  List<MoreLiabilitiesModel> MoreLiabilitiesAddList = [];

  List<FixedAssetModel> fixedAssetList = [];
  List<SundryDebtorModel> sundryDebtorList = [];
  List<CashAndBankBalanceModel> cashAndBankBalanceList = [];
  List<LoansAndAdvancesModel> loansAndAdvancesList = [];
  List<ProvisionsModel> provisionsList = [];
  List<DutiesAndTaxesModel> dutiesAndTaxesList = [];
  List<ClosingStockModel> closingStockList = [];
//total cr
  TextEditingController capitalNewHeadController = TextEditingController();
  TextEditingController capitalNewAmountController = TextEditingController();
  TextEditingController capitalopeningBalanceController =
      TextEditingController();
  TextEditingController capitalinterestController = TextEditingController();
  TextEditingController capitaltdsFYController = TextEditingController();
  TextEditingController capitalNetProfitController = TextEditingController();
  TextEditingController capitalTotalCrController = TextEditingController();
  TextEditingController capitaltdsDrController = TextEditingController();
  TextEditingController capitalTotalDrawingController = TextEditingController();
  TextEditingController capitalTotalDRController = TextEditingController();
  TextEditingController capitalTotalBalanceController = TextEditingController();
  TextEditingController closingStockController = TextEditingController();
  TextEditingController depFixedAssetTotal = TextEditingController();

  TextEditingController dialogNameController = TextEditingController();
  TextEditingController dialogAmountController = TextEditingController();

  // Liabilities Controllers

  TextEditingController provisionController = TextEditingController();
  TextEditingController dutiesController = TextEditingController();
  TextEditingController fixedAssetsController = TextEditingController();

  TextEditingController debtorsController = TextEditingController();
  TextEditingController cashController = TextEditingController();
  TextEditingController loansController = TextEditingController();

  //total dr

  void TotalCr() {
    double extraCr = 0.0;
    for (int i = 0; i < crAdd.length; i++) {
      extraCr += double.parse(crAdd[i].amount);
    }
    capitalTotalCrController.text = (extraCr +
            double.parse(capitalopeningBalanceController.text.isEmpty
                ? "0.0"
                : capitalopeningBalanceController.text) +
            double.parse(capitalinterestController.text.isEmpty
                ? "0.0"
                : capitalinterestController.text) +
            double.parse(capitaltdsFYController.text.isEmpty
                ? "0.0"
                : capitaltdsFYController.text) +
            double.parse(capitalNetProfitController.text.isEmpty
                ? "0.0"
                : capitalNetProfitController.text))
        .toString();
  }

  void TotalDrawings() {
    double extraDr = 0.0;
    for (int i = 0; i < drAdd.length; i++) {
      extraDr += double.parse(drAdd[i].dramount);
    }
    capitalTotalDrawingController.text = extraDr.toString();
    capitalTotalDRController.text = (extraDr +
            double.parse(capitaltdsDrController.text.isEmpty
                ? "0.0"
                : capitaltdsDrController.text))
        .toString();
  }

  void totalBalance() {
    capitalTotalBalanceController.text =
        (double.parse(capitalTotalCrController.text) -
                double.parse(capitalTotalDRController.text))
            .toStringAsFixed(2);
  }

  void addliability(String lkey) {
    liabilityAddList.add(LiabilityModel(lkey, "", ""));
  }

  void addAsset(String key, int index) {
    // AssetAddList.add(AssetModel(key,"",""));
    // MoreAssetAddList.add(MoreAssetsModel(key, [AssetModel(key, " ", ""), AssetModel()], ""));
    MoreAssetAddList[index].assetHead.add(AssetModel(key, " ", ""));
  }

  void addmoreAsset(String key) {
    MoreAssetAddList.add(MoreAssetsModel(key, [], ""));
  }

  void addliabilities(String key, int index) {
    // AssetAddList.add(AssetModel(key,"",""));
    // MoreAssetAddList.add(MoreAssetsModel(key, [AssetModel(key, " ", ""), AssetModel()], ""));
    MoreLiabilitiesAddList[index]
        .liabilityhead
        .add(LiabilityModel(key, " ", ""));
  }

  void addmoreLiabilities(String key) {
    MoreLiabilitiesAddList.add(MoreLiabilitiesModel(key, [], ""));
  }

  void addprovisions(String key) {
    provisionsList.add(ProvisionsModel(key, "", ""));
  }

  void addDutiesAndTaxes(String key) {
    dutiesAndTaxesList.add(DutiesAndTaxesModel(key, "", ""));
  }

  void addLoansAndAdvances(String key) {
    loansAndAdvancesList.add(LoansAndAdvancesModel(key, "", ""));
  }

  void addCashAndBankBalance(String key) {
    cashAndBankBalanceList.add(CashAndBankBalanceModel(key, "", ""));
  }

  void addSundryDebtor(String key) {
    sundryDebtorList.add(SundryDebtorModel(key, "", ""));
  }

  void addFixedAssets(String key) {
    fixedAssetList.add(FixedAssetModel(key, "", ""));
  }

  void addClosingStock(String key) {
    closingStockList.add(ClosingStockModel(key, "", ""));
  }

  void annexureFixedAssetsTotalBalance() {
    if(fixedAssetList.length != 0){
      var a = 0.0;
      for (int i = 0; i < fixedAssetList.length; i++) {
        print(fixedAssetList[i].amount);
        a += double.parse(fixedAssetList[i].amount);
      }
      fixedAssetsController.text = a.toString();
    }

  }

  void annexureProvisionTotalBalance() {
 if(provisionsList.length != 0){
   var a = 0.0;
   for (int i = 0; i < provisionsList.length; i++) {
     a += double.parse(provisionsList[i].amount);
   }
   provisionController.text = a.toString();
 }
  }

  void annexureDutiesTotalBalance() {
    if(dutiesAndTaxesList.length != 0){
      var a = 0.0;
      for (int i = 0; i < dutiesAndTaxesList.length; i++) {
        a += double.parse(dutiesAndTaxesList[i].amount);
      }
      dutiesController.text = a.toString();
    }

  }

  void annexureSundryDebtorsTotalBalance() {
  if(sundryDebtorList.length != 0){
    var a = 0.0;
    for (int i = 0; i < sundryDebtorList.length; i++) {
      a += double.parse(sundryDebtorList[i].amount);
    }
    debtorsController.text = a.toString();
  }
  }

  void annexureCashAndBankBalance() {
    if(cashAndBankBalanceList.length != 0){
      var a = 0.0;
      for (int i = 0; i < cashAndBankBalanceList.length; i++) {
        a += double.parse(cashAndBankBalanceList[i].amount);
      }
      cashController.text = a.toString();
    }
  }

  void annexureLoanAndAdvanceTotalBalance() {
    if(loansAndAdvancesList.length != 0){
      var a = 0.0;
      for (int i = 0; i < loansAndAdvancesList.length; i++) {
        a += double.parse(loansAndAdvancesList[i].amount);
      }
      loansController.text = a.toString();
    }

  }

  void annexureClosingStockTotalBalance() {
    if(closingStockList.length != 0){
      var a = 0.0;
      for (int i = 0; i < closingStockList.length; i++) {
        a += double.parse(closingStockList[i].amount);
      }
      closingStockController.text = a.toString();
    }
  }

  void getProfitAndLossApiCall() async {
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var finacialYear = box.read("finacial_year");

    await Api.getProfitAndLossApi(clientId, finacialYear, adminId)
        .then((value) {
      try {
        if (value["response_p_and_l"].toString().isNotEmpty) {
          print("-----------closing Stock ann--------" +
              value["response_p_and_l"][0]["closing_stock"]);
          closingStockController.text =
              value["response_p_and_l"][0]["closing_stock"];
          capitalNetProfitController.text =
              value["response_p_and_l"][0]["net_profit"];
        }
      } catch (e) {
        print("Error on Profit and Loss Account" + e.toString());
        closingStockController.text = "0" ;
            capitalNetProfitController.text = "0" ;
      }
    });
  }

  Future<List<dynamic>> getDepreciationApiCall() async {
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    var value1;
    var depList = await Api.getDepreciationApi(
            clientId.toString(), financialYear.toString(), adminId.toString())
        .then((value) {
      value1 = value["response_depreciation_list"];
    });

    // List<dynamic> newList = depList;
    // var a = 0.0;
    // for (int i = 0; i < newList.length; i++) {
    //   a += double.parse(newList[i]["closing_balance"]);
    // }

    // print(a.toString());
    // print(value1.toString());
    return value1 == null ? [] : value1;
  }

  Future<double> getTotalDepValue() async {
    var list = await getDepreciationApiCall();
    var a = 0.0;
    for (int i = 0; i < list.length; i++) {
      a += double.parse(list[i]["closing_balance"]);
    }
    return a;
  }

  void annexurejsonApiCall(BuildContext context) async {
    var clientId1 = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    String annexureJsonData = getJsonAnnexureObject();
    EasyLoading.show(status: "");
    await Api.jsonAnnexureApi(
            clientId1, financialYear, adminId, annexureJsonData)
        .then((value) {
      if (value["response_status"].toString() == "1") {
        EasyLoading.dismiss();
        print("if");

        m.showDialog(
            context: context,
            builder: (context) {
              return m.AlertDialog(
                backgroundColor: m.Colors.transparent,
                insetPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 100),
                content: Container(
                  decoration: BoxDecoration(
                      color: m.Colors.white,
                      borderRadius: BorderRadius.circular(20)),
                  width: MediaQuery.of(context).size.width,
                  height: 240,
                  child: m.Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          m.IconButton(
                            onPressed: () {
                              Get.back();
                            },
                            icon: Icon(
                              m.Icons.clear,
                              color: m.Colors.red,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Icon(
                        m.Icons.check_circle_outline_sharp,
                        color: m.Colors.green,
                        size: 50,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        value["message"],
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                              width: 100,
                              child: m.ElevatedButton(
                                  style: m.ElevatedButton.styleFrom(
                                      primary: Color(0xff1739E4)),
                                  onPressed: () async {
                                    generateExcel().then((value) {
                                      OpenFile.open(value);
                                    });
                                    Get.back();
                                  },
                                  child: Text("Download"))),
                          SizedBox(
                            width: 20,
                          ),
                          Platform.isWindows
                              ? Container()
                              : SizedBox(
                                  width: 100,
                                  child: m.ElevatedButton(
                                      style: m.ElevatedButton.styleFrom(
                                          primary: Color(0xff1739E4)),
                                      onPressed: () async {
                                        // ComputaionData cd = ComputaionData();
                                        // List<Map> genDetails = cd.getPdfGd();
                                        // final pdfFile = await PdfComputationApi.generate(genDetails);

                                        generateExcel().then((value) {
                                          Share.shareFiles([value]);
                                        });
                                        Get.back();
                                      },
                                      child: Text("Share"))),
                        ],
                      )
                    ],
                  ),
                ),
              );
            });

        // Get.defaultDialog(
        //     title: " ",
        //     barrierDismissible: false,
        //     content: SizedBox(
        //       width: 400,
        //       child: m.Column(
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         children: [
        //           Row(
        //             children: [
        //               Flexible(
        //                 child: Text(
        //                   "Data saved \nSuccessfully",
        //                   style: TextStyle(fontSize: 20),
        //                 ),
        //               ),
        //               Flexible(
        //                 child: m.IconButton(
        //                     onPressed: () {
        //                       Get.back();
        //                     },
        //                     icon: Icon(
        //                       m.Icons.clear,
        //                       color: m.Colors.red,
        //                     )),
        //               )
        //             ],
        //           ),
        //           SizedBox(
        //             height: 20,
        //           ),
        //           Icon(
        //             m.Icons.check_circle_outline_sharp,
        //             color: m.Colors.green,
        //             size: 50,
        //           ),
        //         ],
        //       ),
        //     ),
        //     actions: [
        //       m.Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //         children: [
        //           SizedBox(
        //               width: 100,
        //               child: m.ElevatedButton(
        //                   style: m.ElevatedButton.styleFrom(primary: Color(0xff1739E4)),
        //                   onPressed: () {
        //                     generateExcel().then((value) {
        //                       OpenFile.open(value);
        //                     });
        //                     Get.back();
        //                   },
        //                   child: Text("Download"))),
        //           SizedBox(
        //             width: 20,
        //           ),
        //           SizedBox(
        //               width: 100,
        //               child: m.ElevatedButton(
        //                   style: m.ElevatedButton.styleFrom(primary: Color(0xff1739E4)),
        //                   onPressed: () {
        //                     generateExcel().then((value) {
        //                       Share.shareFiles([value]);
        //                     });
        //                     Get.back();
        //                   },
        //                   child: Text("Share"))),
        //         ],
        //       ),
        //       SizedBox(
        //         height: 1,
        //       )
        //     ]);
      } else {
        print("else");
        EasyLoading.dismiss();
        m.showDialog(
            context: context,
            builder: (context) {
              return m.AlertDialog(
                backgroundColor: m.Colors.transparent,
                insetPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 100),
                content: Container(
                  decoration: BoxDecoration(
                      color: m.Colors.white,
                      borderRadius: BorderRadius.circular(20)),
                  width: MediaQuery.of(context).size.width,
                  height: 240,
                  child: m.Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          m.IconButton(
                            onPressed: () {
                              Get.back();
                            },
                            icon: Icon(
                              m.Icons.clear,
                              color: m.Colors.red,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Icon(
                        m.Icons.check_circle_outline_sharp,
                        color: m.Colors.green,
                        size: 50,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        value["message"],
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                              width: 100,
                              child: m.ElevatedButton(
                                  style: m.ElevatedButton.styleFrom(
                                      primary: Color(0xff1739E4)),
                                  onPressed: () async {
                                    generateExcel().then((value) {
                                      OpenFile.open(value);
                                    });
                                    Get.back();
                                  },
                                  child: Text("Download"))),
                          SizedBox(
                            width: 20,
                          ),
                          Platform.isWindows
                              ? Container()
                              : SizedBox(
                                  width: 100,
                                  child: m.ElevatedButton(
                                      style: m.ElevatedButton.styleFrom(
                                          primary: Color(0xff1739E4)),
                                      onPressed: () async {
                                        // ComputaionData cd = ComputaionData();
                                        // List<Map> genDetails = cd.getPdfGd();
                                        // final pdfFile = await PdfComputationApi.generate(genDetails);

                                        generateExcel().then((value) {
                                          Share.shareFiles([value]);
                                        });
                                        Get.back();
                                      },
                                      child: Text("Share"))),
                        ],
                      )
                    ],
                  ),
                ),
              );
            });

        /* Get.defaultDialog(
            title: " ",
            barrierDismissible: false,
            content: SizedBox(
              width: 300,
              child: m.Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                ],
              ),
            ),
            actions: [
              m.Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [

                ],
              ),
              SizedBox(
                height: 1,
              )
            ]);*/
      }
    });
  }

  getJsonAnnexureObject() {
    List<List<String>> jsonAddCrList = [];

    crAdd.forEach((element) {
      List<String> jsonList = [];
      jsonList.add(element.head);
      jsonList.add(element.amount);
      jsonAddCrList.add(jsonList);
    });

    List<List<String>> jsonAddDrList = [];

    drAdd.forEach((element) {
      List<String> jsonList = [];
      jsonList.add(element.drname);
      jsonList.add(element.dramount);
      jsonAddDrList.add(jsonList);
    });

    List<List<String>> jsonProvisionAddList = [];

    provisionsList.forEach((element) {
      List<String> jsonList = [];
      jsonList.add(element.name);
      jsonList.add(element.amount);
      jsonProvisionAddList.add(jsonList);
    });

    List<List<String>> jsonDutiesAndTaxesListAddList = [];

    dutiesAndTaxesList.forEach((element) {
      List<String> jsonList = [];
      jsonList.add(element.name);
      jsonList.add(element.amount);
      jsonDutiesAndTaxesListAddList.add(jsonList);
    });

    List<List<String>> jsonFixedAssetAddList = [];

    fixedAssetList.forEach((element) {
      List<String> jsonList = [];
      jsonList.add(element.name);
      jsonList.add(element.amount);
      jsonFixedAssetAddList.add(jsonList);
    });

    List<List<String>> jsonSundryDebtorAddList = [];

    sundryDebtorList.forEach((element) {
      List<String> jsonList = [];
      jsonList.add(element.name);
      jsonList.add(element.amount);
      jsonSundryDebtorAddList.add(jsonList);
    });

    List<List<String>> jsonClosingStockAddList = [];

    closingStockList.forEach((element) {
      List<String> jsonList = [];
      jsonList.add(element.name);
      jsonList.add(element.amount);
      jsonClosingStockAddList.add(jsonList);
    });

    List<List<String>> jsonCashAndBankBalanceAddList = [];

    cashAndBankBalanceList.forEach((element) {
      List<String> jsonList = [];
      jsonList.add(element.name);
      jsonList.add(element.amount);
      jsonCashAndBankBalanceAddList.add(jsonList);
    });

    List<List<String>> jsonLoansAndAdvancesAddList = [];

    loansAndAdvancesList.forEach((element) {
      List<String> jsonList = [];
      jsonList.add(element.name);
      jsonList.add(element.amount);
      jsonLoansAndAdvancesAddList.add(jsonList);
    });

    List<More> jsonMoreLiabilities = [];
    int count = 1;
    MoreLiabilitiesAddList.forEach((element) {
      var a = 0.0;
      for (int j = 0; j < element.liabilityhead.length; j++) {
        a += double.parse(element.liabilityhead[j].lamount);
      }

      List<List<String>> jsonOuterList = [];

      element.liabilityhead.forEach((element) {
        List<String> jsonList = [];
        jsonList.add(element.lname);
        jsonList.add(element.lamount);
        jsonOuterList.add(jsonList);
      });

      jsonMoreLiabilities.add(More(
          headName: element.key,
          total: a.toString(),
          sNo: "${String.fromCharCode("C".codeUnitAt(0) + count)}",
          list: jsonOuterList));
      count++;
    });

    int count1 = 1;
    List<More> jsonMoreAssets = [];
    MoreAssetAddList.forEach((element) {
      var a = 0.0;
      for (int j = 0; j < element.assetHead.length; j++) {
        a += double.parse(element.assetHead[j].assetamount);
      }

      List<List<String>> jsonOuterList = [];

      element.assetHead.forEach((element) {
        List<String> jsonList = [];
        jsonList.add(element.assetname);
        jsonList.add(element.assetamount);
        jsonOuterList.add(jsonList);
      });

      jsonMoreAssets.add(More(
          headName: element.key,
          total: a.toString(),
          sNo: "${String.fromCharCode("G".codeUnitAt(0) + count1 + count)}",
          list: jsonOuterList));
      count1++;
    });

    AnnexureJson aj = AnnexureJson(
        liabilities: Liabilities(
            capitalAC: CapitalAC(
                sNo: "A",
                openingBalance: capitalopeningBalanceController.text,
                interest: capitalinterestController.text,
                tdsFy: capitaltdsFYController.text,
                netProfit: capitalNetProfitController.text,
                moreCrList: jsonAddCrList,
                totalCr: capitalTotalCrController.text,
                tds: capitaltdsDrController.text,
                moreDrList: jsonAddDrList,
                closingBal: capitalTotalBalanceController.text),
            provision: Provision(
                total: provisionController.text,
                sNo: "B",
                provList: jsonProvisionAddList),
            dutiesTaxes: DutiesTaxes(
                total: dutiesController.text,
                sNo: "C",
                dutyList: jsonDutiesAndTaxesListAddList),
            moreLiablites: jsonMoreLiabilities),
        assets: Assets(
            fixedAssets: FixedAssets(
                total: fixedAssetsController.text,
                sNo: "(${String.fromCharCode("C".codeUnitAt(0) + count)})",
                fixedAssetList: jsonFixedAssetAddList),
            sundryDebtors: SundryDebtors(
                total: debtorsController.text,
                sNo: "(${String.fromCharCode("D".codeUnitAt(0) + count)})",
                debtorsList: jsonSundryDebtorAddList),
            closingStock: ClosingStock(
                total: closingStockController.text,
                sNo: "(${String.fromCharCode("E".codeUnitAt(0) + count)})",
                closingStockList: jsonClosingStockAddList),
            cashAndBank: CashAndBank(
                total: cashController.text,
                sNo: "(${String.fromCharCode("F".codeUnitAt(0) + count)})",
                cashList: jsonCashAndBankBalanceAddList),
            loansAndAndvaces: LoansAndAndvaces(
                total: loansController.text,
                sNo: "(${String.fromCharCode("G".codeUnitAt(0) + count)})",
                loansList: jsonLoansAndAdvancesAddList),
            moreAssets: jsonMoreAssets));

    var map = aj.toJson();
    String endcodedJson = jsonEncode(map);

    print(endcodedJson);
    return endcodedJson;
  }

  Future<String> generateExcel() async {
    final Workbook workbook = Workbook();
    final Worksheet sheet = workbook.worksheets[0];

    Style globalStyle = workbook.styles.add('style');

    globalStyle.fontName = 'Times New Roman';

    globalStyle.fontColor = '#000000';
    globalStyle.fontSize = 14;

    // int i = 0;
    // int j = 0;
    // int k = 0;

    // for (int i = 0, k = 0; k < MoreLiabilitiesAddList.length; k++) {
    //   sheet.getRangeByName("A${1 + i}").setText("(B)");
    //   sheet.getRangeByName('A${1 + i}').cellStyle.bold = true;
    //   sheet.getRangeByName("B${1 + i}").setText(MoreLiabilitiesAddList[k].key);
    //   // sheet.getRangeByName('B${}').cellStyle.bold = true;
    //   // sheet.getRangeByName('B${}').cellStyle.hAlign = HAlignType.center;
    //   // sheet.getRangeByName('B${}').cellStyle.bold = true;

    //   sheet.getRangeByName("A${2 + i}").setText("S.NO.");
    //   sheet.getRangeByName('A${2 + i}').cellStyle.bold = true;
    //   sheet.getRangeByName("B${2 + i}").setText("PARTICULARS");
    //   sheet.getRangeByName('B${2 + i}').columnWidth = 35;
    //   sheet.getRangeByName('B${2 + i}').cellStyle.bold = true;
    //   sheet.getRangeByName("C${2 + i}").setText("AMOUNT");
    //   sheet.getRangeByName('C${2 + i}').cellStyle.bold = true;

    //   for (j = 0; j < MoreLiabilitiesAddList[k].liabilityhead.length; j++) {
    //     sheet.getRangeByName("A${3 + i + j}").setText((j + 1).toString());
    //     sheet.getRangeByName("B${3 + i + j}").setText(MoreLiabilitiesAddList[k].liabilityhead[j].lname.toString());
    //     sheet.getRangeByName("C${3 + i + j}").setText(MoreLiabilitiesAddList[k].liabilityhead[j].lamount.toString());
    //   }
    //   sheet.getRangeByName("B${4 + i + j}").setText("TOTAL");
    //   sheet.getRangeByName('B${4 + i + j}').cellStyle.bold = true;
    //   sheet.getRangeByName("C${4 + i + j}").setText("65656");
    //   sheet.getRangeByName('C${4 + i + j}').cellStyle.bold = true;
    //   i = i + j + 5;
    // }

    Range range = sheet.getRangeByName('A7:F7');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

    // final Range range2 = sheet.getRangeByName('A20:F20');
    // range2.cellStyle.borders.all.color = '#000000';
    // range2.cellStyle.borders.top.lineStyle = LineStyle.thin;
    // range2.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

    // final Range range3 = sheet.getRangeByName('A27:F27');
    // range3.cellStyle.borders.all.color = '#000000';
    // range3.cellStyle.borders.top.lineStyle = LineStyle.thin;
    // range3.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

    // final Range range3 = sheet.getRangeByName('A27:F27');
    // range3.cellStyle.borders.all.color = '#000000';
    // range3.cellStyle.borders.top.lineStyle = LineStyle.thin;
    // range3.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

    sheet.name = "Annexure";
    sheet
        .getRangeByName("B2")
        .setText("${box.read("client_name")} (Prop. ${box.read("prop_name")})");
    sheet.getRangeByName('B2').cellStyle.bold = true;

    sheet.getRangeByName("B3").setText("${box.read("distributor_address")}");
    sheet.getRangeByName('B3').cellStyle.bold = true;

    sheet.getRangeByName("A5").setText(
        "ANNEXURE : : ANNEXED TO AND FORMING PART OF THE BALANCE SHEET AS ON 31st MARCH, 2020					");

    sheet.getRangeByName("A6").setText("(A)");
    sheet.getRangeByName('A6').cellStyle.bold = true;

    sheet.getRangeByName("B6").setText("CAPITAL A/C");
    sheet.getRangeByName('B6').cellStyle.bold = true;
    sheet.getRangeByName('B6').cellStyle.hAlign = HAlignType.center;

    sheet.getRangeByName('A7').cellStyle.bold = true;
    sheet.getRangeByName("A7").setText("DR/CR");

    sheet.getRangeByName('B7').cellStyle.bold = true;
    sheet.getRangeByName("B7").setText("PARTICULARS");
    sheet.getRangeByName('B7').columnWidth = 35;

    sheet.getRangeByName('C7').cellStyle.bold = true;
    sheet.getRangeByName("C7").setText("AMOUNT");

    sheet.getRangeByName('A8').cellStyle.bold = true;
    sheet.getRangeByName("A8").setText("CR");
    sheet.getRangeByName("B8").setText("OPENING BALANCE");
    sheet.getRangeByName("C8").setText(capitalopeningBalanceController.text);
    sheet.getRangeByName("B9").setText("INTREST ON SB A/C");
    sheet.getRangeByName("C9").setText(capitalinterestController.text);
    sheet.getRangeByName("B10").setText("TDS F.Y.2021-22");
    sheet.getRangeByName("C10").setText(capitaltdsFYController.text);
    sheet.getRangeByName("B11").setText("NET PROFIT");
    sheet.getRangeByName("C11").setText(capitalNetProfitController.text);
    int i = 0;
    for (i = 0; i < crAdd.length; i++) {
      sheet.getRangeByName('B${12 + i}').setText(crAdd[i].head.toString());
      sheet.getRangeByName('C${12 + i}').setText(crAdd[i].amount.toString());
    }

    sheet.getRangeByName('B${13 + i}').setText("TOTAL(CR)");
    sheet.getRangeByName('B${13 + i}').cellStyle.bold = true;
    sheet.getRangeByName('C${13 + i}').setText(capitalTotalCrController.text);
    sheet.getRangeByName('C${13 + i}').cellStyle.bold = true;

    sheet.getRangeByName('B${14 + i}').setText("TDS");
    sheet.getRangeByName('C${14 + i}').setText(capitaltdsDrController.text);
    sheet.getRangeByName('C${14 + i}').cellStyle.borders.all.color = '#000000';
    sheet.getRangeByName('C${14 + i}').cellStyle.borders.top.lineStyle =
        LineStyle.thin;
    //  sheet.getRangeByName('C${14 + i}').cellStyle.borders.bottom.lineStyle = LineStyle.thin;
    int j = 0;
    for (j = 0; j < drAdd.length; j++) {
      sheet
          .getRangeByName('B${15 + i + j}')
          .setText(drAdd[j].drname.toString());
      sheet
          .getRangeByName('C${15 + i + j}')
          .setText(drAdd[j].dramount.toString());
    }

    sheet.getRangeByName('B${16 + i + j}').setText("TOTAL(DR)");
    sheet.getRangeByName('B${16 + i + j}').cellStyle.bold = true;
    sheet
        .getRangeByName('C${16 + i + j}')
        .setText(capitalTotalDRController.text);
    sheet.getRangeByName('C${16 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName('B${17 + i + j}').setText("Closing Balance(CR)");
    sheet.getRangeByName('B${17 + i + j}').cellStyle.bold = true;
    sheet
        .getRangeByName('C${17 + i + j}')
        .setText(capitalTotalBalanceController.text);
    sheet.getRangeByName('C${17 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName('C${17 + i + j}').cellStyle.borders.all.color =
        '#000000';
    sheet.getRangeByName('C${17 + i + j}').cellStyle.borders.top.lineStyle =
        LineStyle.thin;
    sheet.getRangeByName('C${17 + i + j}').cellStyle.borders.bottom.lineStyle =
        LineStyle.thin;

    // PROVISIONS

    sheet.getRangeByName("A${20 + i + j}").setText("(B)");
    sheet.getRangeByName('A${20 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName("B${20 + i + j}").setText("PROVISIONS");

    sheet.getRangeByName('B${20 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName('B${20 + i + j}').cellStyle.hAlign = HAlignType.center;
    sheet.getRangeByName('B${20 + i + j}').cellStyle.bold = true;

    sheet.getRangeByName("A${21 + i + j}").setText("S.NO.");
    sheet.getRangeByName('A${21 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName("B${21 + i + j}").setText("PARTICULARS");
    sheet.getRangeByName('B${21 + i + j}').columnWidth = 35;
    sheet.getRangeByName('B${21 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName("C${21 + i + j}").setText("AMOUNT");
    sheet.getRangeByName('C${21 + i + j}').cellStyle.bold = true;

    range = sheet.getRangeByName('A${21 + i + j}:F${21 + i + j}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

    int k = 0;
    for (k = 0; k < provisionsList.length; k++) {
      sheet.getRangeByName("A${22 + k + i + j}").setText((k + 1).toString());
      sheet
          .getRangeByName("B${22 + k + i + j}")
          .setText(provisionsList[k].name.toString());
      sheet
          .getRangeByName("C${22 + k + i + j}")
          .setText(provisionsList[k].amount.toString());
    }
    sheet.getRangeByName("B${25 + i + j}").setText("TOTAL");
    sheet.getRangeByName('B${25 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName("C${25 + i + j}").setText(provisionController.text);
    sheet.getRangeByName('C${25 + i + j}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${25 + i + j}:F${25 + i + j}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

    // DUTIES AND TAXES

    sheet.getRangeByName("A${27 + i + j}").setText("(C)");
    sheet.getRangeByName('A${27 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName("B${27 + i + j}").setText("DUTIES AND TAXES");
    sheet.getRangeByName('B${27 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName('B${27 + i + j}').cellStyle.hAlign = HAlignType.center;
    sheet.getRangeByName('B${27 + i + j}').cellStyle.bold = true;

    sheet.getRangeByName("A${28 + i + j}").setText("S.NO.");
    sheet.getRangeByName('A${28 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName("B${28 + i + j}").setText("PARTICULARS");
    sheet.getRangeByName('B${28 + i + j}').columnWidth = 35;
    sheet.getRangeByName('B${28 + i + j}').cellStyle.bold = true;
    sheet.getRangeByName("C${28 + i + j}").setText("AMOUNT");
    sheet.getRangeByName('C${28 + i + j}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${28 + i + j}:F${28 + i + j}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

    int m = k + i - 1 + j;
    int n = 0;
    for (n = 0; n < dutiesAndTaxesList.length; n++) {
      sheet.getRangeByName("A${28 + m + n}").setText((n + 1).toString());
      sheet
          .getRangeByName("B${28 + m + n}")
          .setText(dutiesAndTaxesList[n].name.toString());
      sheet
          .getRangeByName("C${28 + m + n}")
          .setText(dutiesAndTaxesList[n].amount.toString());
    }
    sheet.getRangeByName("B${29 + m + n}").setText("TOTAL");
    sheet.getRangeByName('B${29 + m + n}').cellStyle.bold = true;
    sheet.getRangeByName("C${29 + m + n}").setText(dutiesController.text);
    sheet.getRangeByName('C${29 + m + n}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${29 + m + n}:F${29 + m + n}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

    // // MORE LAIBILITIS
    int nv = n + m + 32;

    int ml = 0;
    int mli = 0;
    int mlo = 0;
    String globalSn = "";
    for (mlo = 0; mlo < MoreLiabilitiesAddList.length; mlo++) {
      sheet
          .getRangeByName("A${nv + ml}")
          .setText("(${String.fromCharCode("D".codeUnitAt(0) + mlo)})");

      sheet.getRangeByName('A${nv + ml}').cellStyle.bold = true;
      sheet
          .getRangeByName("B${nv + ml}")
          .setText(MoreLiabilitiesAddList[mlo].key.toUpperCase());
      sheet.getRangeByName('B${nv + ml}').cellStyle.bold = true;
      sheet.getRangeByName('B${nv + ml}').cellStyle.hAlign = HAlignType.center;
      // sheet.getRangeByName('B${nv + ml}').cellStyle.bold = true;

      sheet.getRangeByName("A${nv + 1 + ml}").setText("S.NO.");
      sheet.getRangeByName('A${nv + 1 + ml}').cellStyle.bold = true;
      sheet.getRangeByName("B${nv + 1 + ml}").setText("PARTICULARS");
      sheet.getRangeByName('B${nv + 1 + ml}').columnWidth = 35;
      sheet.getRangeByName('B${nv + 1 + ml}').cellStyle.bold = true;
      sheet.getRangeByName("C${nv + 1 + ml}").setText("AMOUNT");
      sheet.getRangeByName('C${nv + 1 + ml}').cellStyle.bold = true;
      range = sheet.getRangeByName('A${nv + 1 + ml}:F${nv + 1 + ml}');
      range.cellStyle.borders.all.color = '#000000';
      range.cellStyle.borders.top.lineStyle = LineStyle.thin;
      range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
      var a = 0.0;
      for (mli = 0;
          mli < MoreLiabilitiesAddList[mlo].liabilityhead.length;
          mli++) {
        sheet
            .getRangeByName("A${nv + 2 + ml + mli}")
            .setText((mli + 1).toString());
        sheet.getRangeByName("B${nv + 2 + ml + mli}").setText(
            MoreLiabilitiesAddList[mlo].liabilityhead[mli].lname.toString());
        sheet.getRangeByName("C${nv + 2 + ml + mli}").setText(
            MoreLiabilitiesAddList[mlo].liabilityhead[mli].lamount.toString());
        a += double.parse(
            MoreLiabilitiesAddList[mlo].liabilityhead[mli].lamount);
      }
      sheet.getRangeByName("B${nv + 3 + ml + mli}").setText("TOTAL");
      sheet.getRangeByName('B${nv + 3 + ml + mli}').cellStyle.bold = true;
      sheet.getRangeByName("C${nv + 3 + ml + mli}").setText(a.toString());
      sheet.getRangeByName('C${nv + 3 + ml + mli}').cellStyle.bold = true;
      range =
          sheet.getRangeByName('A${nv + 3 + ml + mli}:F${nv + 3 + ml + mli}');
      range.cellStyle.borders.all.color = '#000000';
      range.cellStyle.borders.top.lineStyle = LineStyle.thin;
      range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
      ml = ml + mli + 4;
      print("ml: $ml \n mli:$mli \n mlo:$mlo \n nv:$nv");
    }
    // print("ml: $ml \n mli:$mli \n mlo:$mlo \n nv:$nv");
    globalSn = String.fromCharCode("D".codeUnitAt(0) + mlo);
    nv = ml + nv + 1;
    // FIXED ASSETS

    sheet
        .getRangeByName("A${nv}")
        .setText("(${String.fromCharCode(globalSn.codeUnitAt(0))})");
    sheet.getRangeByName('A${nv}').cellStyle.bold = true;
    sheet.getRangeByName("B${nv}").setText("FIXED ASSETS");
    sheet.getRangeByName('B${nv}').cellStyle.bold = true;
    sheet.getRangeByName('B${nv}').cellStyle.hAlign = HAlignType.center;
    sheet.getRangeByName('B${nv}').cellStyle.bold = true;

    sheet.getRangeByName("A${nv + 1}").setText("S.NO.");
    sheet.getRangeByName('A${nv + 1}').cellStyle.bold = true;
    sheet.getRangeByName("B${nv + 1}").setText("PARTICULARS");
    sheet.getRangeByName('B${nv + 1}').columnWidth = 35;
    sheet.getRangeByName('B${nv + 1}').cellStyle.bold = true;
    sheet.getRangeByName("C${nv + 1}").setText("AMOUNT");
    sheet.getRangeByName('C${nv + 1}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${nv + 1}:F${nv + 1}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
    k = 0;
    for (k = 0; k < fixedAssetList.length; k++) {
      sheet.getRangeByName("A${nv + k + 1}").setText((k + 1).toString());
      sheet
          .getRangeByName("B${nv + k + 1}")
          .setText(fixedAssetList[k].name.toString());
      sheet
          .getRangeByName("C${nv + k + 1}")
          .setText(fixedAssetList[k].amount.toString());
    }
    sheet.getRangeByName("B${nv + 2 + k}").setText("TOTAL");
    sheet.getRangeByName('B${nv + 2 + k}').cellStyle.bold = true;
    sheet.getRangeByName("C${nv + 2 + k}").setText(fixedAssetsController.text);
    sheet.getRangeByName('C${nv + 2 + k}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${nv + 2 + k}:F${nv + 2 + k}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

    nv = nv + k + 5;

// SUNDRY DEBTORS
    sheet
        .getRangeByName("A${nv}")
        .setText("(${String.fromCharCode(globalSn.codeUnitAt(0) + 1)})");
    sheet.getRangeByName('A${nv}').cellStyle.bold = true;
    sheet.getRangeByName("B${nv}").setText("SUNDRY DEBTORS");
    sheet.getRangeByName('B${nv}').cellStyle.bold = true;
    sheet.getRangeByName('B${nv}').cellStyle.hAlign = HAlignType.center;
    sheet.getRangeByName('B${nv}').cellStyle.bold = true;

    sheet.getRangeByName("A${nv + 1}").setText("S.NO.");
    sheet.getRangeByName('A${nv + 1}').cellStyle.bold = true;
    sheet.getRangeByName("B${nv + 1}").setText("PARTICULARS");
    sheet.getRangeByName('B${nv + 1}').columnWidth = 35;
    sheet.getRangeByName('B${nv + 1}').cellStyle.bold = true;
    sheet.getRangeByName("C${nv + 1}").setText("AMOUNT");
    sheet.getRangeByName('C${nv + 1}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${nv + 1}:F${nv + 1}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
    k = 0;
    for (k = 0; k < sundryDebtorList.length; k++) {
      sheet.getRangeByName("A${nv + k + 1}").setText((k + 1).toString());
      sheet
          .getRangeByName("B${nv + k + 1}")
          .setText(sundryDebtorList[k].name.toString());
      sheet
          .getRangeByName("C${nv + k + 1}")
          .setText(sundryDebtorList[k].amount.toString());
    }
    sheet.getRangeByName("B${nv + k + 2}").setText("TOTAL");
    sheet.getRangeByName('B${nv + k + 2}').cellStyle.bold = true;
    sheet.getRangeByName("C${nv + k + 2}").setText(debtorsController.text);
    sheet.getRangeByName('C${nv + k + 2}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${nv + k + 2}:F${nv + k + 2}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

// CLOSING STOCK
    nv = nv + k + 5;
    sheet
        .getRangeByName("A${nv}")
        .setText("(${String.fromCharCode(globalSn.codeUnitAt(0) + 2)})");
    sheet.getRangeByName('A${nv}').cellStyle.bold = true;
    sheet.getRangeByName("B${nv}").setText("CLOSING STOCK");
    sheet.getRangeByName('B${nv}').cellStyle.bold = true;
    sheet.getRangeByName('B${nv}').cellStyle.hAlign = HAlignType.center;
    sheet.getRangeByName('B${nv}').cellStyle.bold = true;

    sheet.getRangeByName("A${nv + 1}").setText("S.NO.");
    sheet.getRangeByName('A${nv + 1}').cellStyle.bold = true;
    sheet.getRangeByName("B${nv + 1}").setText("PARTICULARS");
    sheet.getRangeByName('B${nv + 1}').columnWidth = 35;
    sheet.getRangeByName('B${nv + 1}').cellStyle.bold = true;
    sheet.getRangeByName("C${nv + 1}").setText("AMOUNT");
    sheet.getRangeByName('C${nv + 1}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${nv + 1}:F${nv + 1}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
    k = 0;
    for (k = 0; k < closingStockList.length; k++) {
      sheet.getRangeByName("A${nv + k + 1}").setText((k + 1).toString());
      sheet
          .getRangeByName("B${nv + k + 1}")
          .setText(closingStockList[k].name.toString());
      sheet
          .getRangeByName("C${nv + k + 1}")
          .setText(closingStockList[k].amount.toString());
    }
    sheet.getRangeByName("B${nv + k + 2}").setText("TOTAL");
    sheet.getRangeByName('B${nv + k + 2}').cellStyle.bold = true;
    sheet.getRangeByName("C${nv + k + 2}").setText(closingStockController.text);
    sheet.getRangeByName('C${nv + k + 2}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${nv + k + 2}:F${nv + k + 2}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
//     //CASH AND BANK BALANCE
    nv = nv + k + 5;
    sheet
        .getRangeByName("A${nv}")
        .setText("(${String.fromCharCode(globalSn.codeUnitAt(0) + 3)})");
    sheet.getRangeByName('A${nv}').cellStyle.bold = true;
    sheet.getRangeByName("B${nv}").setText("CASH AND BANK BALANCE");
    sheet.getRangeByName('B${nv}').cellStyle.bold = true;
    sheet.getRangeByName('B${nv}').cellStyle.hAlign = HAlignType.center;
    sheet.getRangeByName('B${nv}').cellStyle.bold = true;

    sheet.getRangeByName("A${nv + 1}").setText("S.NO.");
    sheet.getRangeByName('A${nv + 1}').cellStyle.bold = true;
    sheet.getRangeByName("B${nv + 1}").setText("PARTICULARS");
    sheet.getRangeByName('B${nv + 1}').columnWidth = 35;
    sheet.getRangeByName('B${nv + 1}').cellStyle.bold = true;
    sheet.getRangeByName("C${nv + 1}").setText("AMOUNT");
    sheet.getRangeByName('C${nv + 1}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${nv + 1}:F${nv + 1}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
    k = 0;
    for (k = 0; k < cashAndBankBalanceList.length; k++) {
      sheet.getRangeByName("A${nv + k + 1}").setText((k + 1).toString());
      sheet
          .getRangeByName("B${nv + k + 1}")
          .setText(cashAndBankBalanceList[k].name.toString());
      sheet
          .getRangeByName("C${nv + k + 1}")
          .setText(cashAndBankBalanceList[k].amount.toString());
    }
    sheet.getRangeByName("B${nv + k + 2}").setText("TOTAL");
    sheet.getRangeByName('B${nv + k + 2}').cellStyle.bold = true;
    sheet.getRangeByName("C${nv + k + 2}").setText(cashController.text);
    sheet.getRangeByName('C${nv + k + 2}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${nv + k + 2}:F${nv + k + 2}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

//     // LOANS AND ADVANCE
    nv = nv + k + 5;
    sheet
        .getRangeByName("A${nv}")
        .setText("(${String.fromCharCode(globalSn.codeUnitAt(0) + 4)})");
    sheet.getRangeByName('A${nv}').cellStyle.bold = true;
    sheet.getRangeByName("B${nv}").setText("LOANS AND ADVANCES");
    sheet.getRangeByName('B${nv}').cellStyle.bold = true;
    sheet.getRangeByName('B${nv}').cellStyle.hAlign = HAlignType.center;
    sheet.getRangeByName('B${nv}').cellStyle.bold = true;

    sheet.getRangeByName("A${nv + 1}").setText("S.NO.");
    sheet.getRangeByName('A${nv + 1}').cellStyle.bold = true;
    sheet.getRangeByName("B${nv + 1}").setText("PARTICULARS");
    sheet.getRangeByName('B${nv + 1}').columnWidth = 35;
    sheet.getRangeByName('B${nv + 1}').cellStyle.bold = true;
    sheet.getRangeByName("C${nv + 1}").setText("AMOUNT");
    sheet.getRangeByName('C${nv + 1}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${nv + 1}:F${nv + 1}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
    k = 0;
    for (k = 0; k < loansAndAdvancesList.length; k++) {
      sheet.getRangeByName("A${nv + k + 1}").setText((k + 1).toString());
      sheet
          .getRangeByName("B${nv + k + 1}")
          .setText(loansAndAdvancesList[k].name.toString());
      sheet
          .getRangeByName("C${nv + k + 1}")
          .setText(loansAndAdvancesList[k].amount.toString());
    }
    sheet.getRangeByName("B${nv + k + 2}").setText("TOTAL");
    sheet.getRangeByName('B${nv + k + 2}').cellStyle.bold = true;
    sheet.getRangeByName("C${nv + k + 2}").setText(loansController.text);
    sheet.getRangeByName('C${nv + k + 2}').cellStyle.bold = true;
    range = sheet.getRangeByName('A${nv + k + 2}:F${nv + k + 2}');
    range.cellStyle.borders.all.color = '#000000';
    range.cellStyle.borders.top.lineStyle = LineStyle.thin;
    range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;

// MORE ASSETS

    nv = nv + k + 5;
    ml = 0;
    mli = 0;
    mlo = 0;
    globalSn = String.fromCharCode(globalSn.codeUnitAt(0) + 5);
    for (mlo = 0; mlo < MoreAssetAddList.length; mlo++) {
      sheet
          .getRangeByName("A${nv + ml}")
          .setText("(${String.fromCharCode(globalSn.codeUnitAt(0) + mlo)})");

      sheet.getRangeByName('A${nv + ml}').cellStyle.bold = true;
      sheet
          .getRangeByName("B${nv + ml}")
          .setText(MoreAssetAddList[mlo].key.toUpperCase());
      sheet.getRangeByName('B${nv + ml}').cellStyle.bold = true;
      sheet.getRangeByName('B${nv + ml}').cellStyle.hAlign = HAlignType.center;
      // sheet.getRangeByName('B${nv + ml}').cellStyle.bold = true;

      sheet.getRangeByName("A${nv + 1 + ml}").setText("S.NO.");
      sheet.getRangeByName('A${nv + 1 + ml}').cellStyle.bold = true;
      sheet.getRangeByName("B${nv + 1 + ml}").setText("PARTICULARS");
      sheet.getRangeByName('B${nv + 1 + ml}').columnWidth = 35;
      sheet.getRangeByName('B${nv + 1 + ml}').cellStyle.bold = true;
      sheet.getRangeByName("C${nv + 1 + ml}").setText("AMOUNT");
      sheet.getRangeByName('C${nv + 1 + ml}').cellStyle.bold = true;

      range = sheet.getRangeByName('A${nv + 1 + ml}:F${nv + 1 + ml}');
      range.cellStyle.borders.all.color = '#000000';
      range.cellStyle.borders.top.lineStyle = LineStyle.thin;
      range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
      var a = 0.0;
      for (mli = 0; mli < MoreAssetAddList[mlo].assetHead.length; mli++) {
        sheet
            .getRangeByName("A${nv + 2 + ml + mli}")
            .setText((mli + 1).toString());
        sheet
            .getRangeByName("B${nv + 2 + ml + mli}")
            .setText(MoreAssetAddList[mlo].assetHead[mli].assetname.toString());
        sheet.getRangeByName("C${nv + 2 + ml + mli}").setText(
            MoreAssetAddList[mlo].assetHead[mli].assetamount.toString());
        a += double.parse(MoreAssetAddList[mlo].assetHead[mli].assetamount);
      }
      sheet.getRangeByName("B${nv + 3 + ml + mli}").setText("TOTAL");
      sheet.getRangeByName('B${nv + 3 + ml + mli}').cellStyle.bold = true;
      sheet.getRangeByName("C${nv + 3 + ml + mli}").setText(a.toString());
      sheet.getRangeByName('C${nv + 3 + ml + mli}').cellStyle.bold = true;
      range =
          sheet.getRangeByName('A${nv + 3 + ml + mli}:F${nv + 3 + ml + mli}');
      range.cellStyle.borders.all.color = '#000000';
      range.cellStyle.borders.top.lineStyle = LineStyle.thin;
      range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
      ml = ml + mli + 4;
      print("ml: $ml \n mli:$mli \n mlo:$mlo \n nv:$nv");
    }
    nv = ml + nv + 1;

    sheet
        .getRangeByName('A${nv + 4}')
        .setText("CERTIFIED To  BE TRUE & CORRECT ");
    sheet
        .getRangeByName('A${nv + 5}')
        .setText("FOR: " + box.read("client_name").toString().toUpperCase());

    sheet
        .getRangeByName('A${nv + 9}')
        .setText("(${box.read("prop_name").toString().toUpperCase()})");
    // sheet.getRangeByName('A${nv + 5}').setText("FOR: " + box.read("firm_name").toString());

    sheet.getRangeByName('A${nv + 10}').setText("Prop.");

    box.read("place_of_supply").toString() == null
        ? sheet.getRangeByName('A${nv + 11}').setText("")
        : sheet
            .getRangeByName('A${nv + 11}')
            .setText("PLACE: " + box.read("ca_city").toString());

    sheet.getRangeByName('A${nv + 12}').setText(
        "DATE: " + formatDate(DateTime.now(), [dd, "/", mm, "/", yyyy]));

    final List<int> bytes = workbook.saveAsStream();
    workbook.dispose();
    workbook.dispose();
    final String path = (await getApplicationSupportDirectory()).path;
    final String fileName = "$path/Annexure.xlsx";
    final File file = File(fileName);
    await file.writeAsBytes(bytes, flush: true);
    return fileName;
  }

  Future getAnnexureApiCall() async {
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var finacialYear = box.read("finacial_year");

    try {
      await Api.getAnnexurApi(clientId, finacialYear, adminId).then((value) {
        if(value.annexureJson == null){
          print("------bloc 1--------getAnnexurApi--------");
          capitalopeningBalanceController.text =
              value.annexureJson.liabilities.capitalAC.openingBalance;
          capitaltdsFYController.text =
              value.annexureJson.liabilities.capitalAC.tdsFy;
          capitalinterestController.text =
              value.annexureJson.liabilities.capitalAC.interest;
          capitaltdsDrController.text =
              value.annexureJson.liabilities.capitalAC.tds;
          crAdd = value.annexureJson.liabilities.capitalAC.moreCrList
              .map((e) => CRModel(head: e[0], amount: e[1]))
              .toList();
          drAdd = value.annexureJson.liabilities.capitalAC.moreDrList
              .map((e) => DRModel(drname: e[0], dramount: e[1]))
              .toList();
          provisionsList = value.annexureJson.liabilities.provision.provList
              .map((e) => ProvisionsModel("", e[0], e[1]))
              .toList();
          dutiesAndTaxesList = value.annexureJson.liabilities.dutiesTaxes.dutyList
              .map((e) => DutiesAndTaxesModel("", e[0], e[1]))
              .toList();
          fixedAssetList = value.annexureJson.assets.fixedAssets.fixedAssetList
              .map((e) => FixedAssetModel("", e[0], e[1]))
              .toList();

          sundryDebtorList = value.annexureJson.assets.sundryDebtors.debtorsList
              .map((e) => SundryDebtorModel("", e[0], e[1]))
              .toList();
          loansAndAdvancesList = value
              .annexureJson.assets.loansAndAndvaces.loansList
              .map((e) => LoansAndAdvancesModel("", e[0], e[1]))
              .toList();
          cashAndBankBalanceList = value.annexureJson.assets.cashAndBank.cashList
              .map((e) => CashAndBankBalanceModel("", e[0], e[1]))
              .toList();
          closingStockList = value
              .annexureJson.assets.closingStock.closingStockList
              .map((e) => ClosingStockModel("", e[0], e[1]))
              .toList();
          MoreLiabilitiesAddList = value.annexureJson.liabilities.moreLiablites
              .map((e) => MoreLiabilitiesModel(
              e.headName,
              e.list.map((e) => LiabilityModel("", e[0], e[1])).toList(),
              e.total))
              .toList();
          MoreAssetAddList = value.annexureJson.assets.moreAssets
              .map((e) => MoreAssetsModel(
              e.headName,
              e.list.map((e) => AssetModel("", e[0], e[1])).toList(),
              e.total))
              .toList();
        }else{
          print("------bloc 2------getAnnexurApi----------");
          print("------bloc 2------getAnnexurApi-----value-----"+value.annexureJson.liabilities.capitalAC.openingBalance.toString());

          capitalopeningBalanceController.text =
              value.annexureJson.liabilities.capitalAC.openingBalance;
          capitaltdsFYController.text =
              value.annexureJson.liabilities.capitalAC.tdsFy;
          capitalinterestController.text =
              value.annexureJson.liabilities.capitalAC.interest;
          capitaltdsDrController.text =
              value.annexureJson.liabilities.capitalAC.tds;
          capitalTotalCrController.text = value.annexureJson.liabilities.capitalAC.totalCr;
          crAdd = value.annexureJson.liabilities.capitalAC.moreCrList
              .map((e) => CRModel(head: e[0], amount: e[1]))
              .toList();
          drAdd = value.annexureJson.liabilities.capitalAC.moreDrList
              .map((e) => DRModel(drname: e[0], dramount: e[1]))
              .toList();
          provisionsList = value.annexureJson.liabilities.provision.provList
              .map((e) => ProvisionsModel("", e[0], e[1]))
              .toList();
          provisionController.text = value.annexureJson.liabilities.provision.total ;
          dutiesAndTaxesList = value.annexureJson.liabilities.dutiesTaxes.dutyList
              .map((e) => DutiesAndTaxesModel("", e[0], e[1]))
              .toList();
          dutiesController.text = value.annexureJson.liabilities.dutiesTaxes.total;

          fixedAssetsController.text = value.annexureJson.assets.fixedAssets.total;
          fixedAssetList = value.annexureJson.assets.fixedAssets.fixedAssetList
              .map((e) => FixedAssetModel("", e[0], e[1]))
              .toList();
          sundryDebtorList = value.annexureJson.assets.sundryDebtors.debtorsList
              .map((e) => SundryDebtorModel("", e[0], e[1]))
              .toList();
         debtorsController.text = value.annexureJson.assets.sundryDebtors.total ;
          loansAndAdvancesList = value
              .annexureJson.assets.loansAndAndvaces.loansList
              .map((e) => LoansAndAdvancesModel("", e[0], e[1]))
              .toList();
         loansController.text = value.annexureJson.assets.loansAndAndvaces.total;
          cashAndBankBalanceList = value.annexureJson.assets.cashAndBank.cashList
              .map((e) => CashAndBankBalanceModel("", e[0], e[1]))
              .toList();
          cashController.text = value.annexureJson.assets.cashAndBank.total;
          closingStockList = value
              .annexureJson.assets.closingStock.closingStockList
              .map((e) => ClosingStockModel("", e[0], e[1]))
              .toList();
          MoreLiabilitiesAddList = value.annexureJson.liabilities.moreLiablites
              .map((e) => MoreLiabilitiesModel(
              e.headName,
              e.list.map((e) => LiabilityModel("", e[0], e[1])).toList(),
              e.total))
              .toList();
          MoreAssetAddList = value.annexureJson.assets.moreAssets
              .map((e) => MoreAssetsModel(
              e.headName,
              e.list.map((e) => AssetModel("", e[0], e[1])).toList(),
              e.total))
              .toList();
        }

      });
    } catch (e) {

      capitalopeningBalanceController.text =
          "0";
      capitaltdsFYController.text =
          "";
      capitalinterestController.text =
          "0";
      capitaltdsDrController.text =
          "0";
      capitalTotalCrController.text = "0";
      capitalTotalDrawingController.text = "0";
      capitalTotalDRController.text = "0";
      capitalTotalBalanceController.text = "0";
      fixedAssetsController.text = "0";
      debtorsController.text = "0";
      cashController.text = "0";
      loansController.text = "0";
      closingStockController.text = "0";
      provisionController.text = "0";
      dutiesController.text = "0";
      debtorsController.text = "0";

      crAdd = [];
      drAdd = [];
      provisionsList = [];
      dutiesAndTaxesList = [];
      fixedAssetList = [];
      sundryDebtorList = [];
      loansAndAdvancesList = [];
      cashAndBankBalanceList = [];
      closingStockList = [];
      MoreLiabilitiesAddList = [];
      MoreAssetAddList = [];

      print("Error on getAnnexurApi in Annexure: "+e.toString());
      /* showDialog(context: context, builder:(context){
        return AlertDialog(
          backgroundColor: Colors.transparent,
          insetPadding: EdgeInsets.symmetric(horizontal: 10,vertical: 100),
          content: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20)
            ),
            width: MediaQuery.of(context).size.width,
            height: 240,

            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("No data available",style: TextStyle(fontWeight: FontWeight.bold),),
                SizedBox(height: 20,),
                Text("Please go to the annexure page!!"),
                SizedBox(height: 30,),


                ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
                    onPressed: () async {
                      Get.back();
                    },
                    child: Text("OK"))
                */ /*   IconButton(onPressed: () {
                     Get.back();
                   },icon: Icon(Icons.clear,color: Colors.red,),),*/ /*





              ],
            ),
          ),
        );
      });*/
    }
  }
}
