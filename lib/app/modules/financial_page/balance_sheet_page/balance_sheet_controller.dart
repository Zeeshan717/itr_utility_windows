import 'package:accountx/app/model/annexure_json_model.dart';
import 'package:accountx/app/model/balance_sheet_module.dart';
import 'package:accountx/app/modules/financial_page/annexure_page/annexure_view.dart';
import 'package:accountx/app/services/api.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:get_storage/get_storage.dart';

class BalanceSheetController extends GetxController {
  GetStorage box = GetStorage();

  TextEditingController capitalController = TextEditingController();
  TextEditingController fixedAssetsController = TextEditingController();
  TextEditingController securedLoansController = TextEditingController();
  TextEditingController unsecuredLoansController = TextEditingController();
  TextEditingController provisionsController = TextEditingController();
  TextEditingController dutiesAndTaxesController = TextEditingController();
  TextEditingController sundryDebtorsController = TextEditingController();
  TextEditingController closingStockController = TextEditingController();
  TextEditingController cashAndBankBalController = TextEditingController();
  TextEditingController loansAndAndvancesController = TextEditingController();
  TextEditingController sundryCreditorsController = TextEditingController();

  TextEditingController newHeadName = TextEditingController();
  TextEditingController newAmount = TextEditingController();

  List<BalanceSheetList> balanceSheetList = [];

  List<More> moreLiabilites = [];
  List<More> moreAssets = [];

  Future getAnnexureApiCall(BuildContext context) async {
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var finacialYear = box.read("finacial_year");

   try {
     await Api.getAnnexurApi(clientId, finacialYear, adminId).then((value) {
       capitalController.text = value.annexureJson.liabilities.capitalAC.closingBal;
       fixedAssetsController.text = value.annexureJson.assets.fixedAssets.total;
       loansAndAndvancesController.text = value.annexureJson.assets.loansAndAndvaces.total;
       sundryDebtorsController.text = value.annexureJson.assets.sundryDebtors.total;
       cashAndBankBalController.text = value.annexureJson.assets.cashAndBank.total;
       closingStockController.text = value.annexureJson.assets.closingStock.total;
       provisionsController.text = value.annexureJson.liabilities.provision.total;
       dutiesAndTaxesController.text = value.annexureJson.liabilities.dutiesTaxes.total;
       moreLiabilites = value.annexureJson.liabilities.moreLiablites;
       moreAssets = value.annexureJson.assets.moreAssets;
     });
   }catch(e){

     capitalController.text = "0";
     fixedAssetsController.text = "0";
     loansAndAndvancesController.text = "0";
     sundryDebtorsController.text = "0";
     cashAndBankBalController.text = "0";
     closingStockController.text = "0";
     provisionsController.text = "0";
     dutiesAndTaxesController.text = "0";
     moreLiabilites = [];
     moreAssets = [];

     showDialog(context: context, builder:(context){
       return AlertDialog(
         backgroundColor: Colors.transparent,
         insetPadding: EdgeInsets.symmetric(horizontal: 10,vertical: 100),
         content: Container(
           decoration: BoxDecoration(
               color: Colors.white,
               borderRadius: BorderRadius.circular(20)
           ),
           width: MediaQuery.of(context).size.width,
           height: 240,

           child: Column(
             mainAxisAlignment: MainAxisAlignment.center,
             children: [
               Text("No data available",style: TextStyle(fontWeight: FontWeight.bold),),
               SizedBox(height: 20,),
               Text("Please go to the annexure page!!"),
                   SizedBox(height: 30,),


                   ElevatedButton(
                       style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
                       onPressed: () async {
                        Get.back();
                       },
                       child: Text("OK"))
                /*   IconButton(onPressed: () {
                     Get.back();
                   },icon: Icon(Icons.clear,color: Colors.red,),),*/





             ],
           ),
         ),
       );
     });
   }
  }

  double totalLiabilities() {
    var c = 0.0;
    var b = 0.0;
    for (int i = 0; i < moreLiabilites.length; i++) {
      b += double.parse(moreLiabilites[i].total);
    }
    c = b + double.parse(capitalController.text.isEmpty?"0.0":capitalController.text) + double.parse(provisionsController.text.isEmpty?"0.0":provisionsController.text) + double.parse(dutiesAndTaxesController.text.isEmpty?"0.0":dutiesAndTaxesController.text);
    return c;
  }
}
