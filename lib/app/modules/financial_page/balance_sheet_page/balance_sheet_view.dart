import 'dart:io';

import 'package:accountx/app/model/balance_sheet_module.dart';
import 'package:accountx/app/modules/financial_page/balance_sheet_page/balance_sheet_controller.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart' hide Column, Row;

class BalanceSheetView extends StatefulWidget {
  const BalanceSheetView({Key? key}) : super(key: key);

  @override
  State<BalanceSheetView> createState() => _BalanceSheetViewState();
}

class _BalanceSheetViewState extends State<BalanceSheetView> {
  GetStorage storage = GetStorage();

  var controller = Get.put(BalanceSheetController());

  @override
  void initState() {
    controller.getAnnexureApiCall(context).then((value) {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20,left: 20, right: 20,bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Annexure",
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,letterSpacing: 0.6),
                    ),
                    InkWell(
                        onTap: (){
                          initState();
                        },
                        child: Container(
                            color: Color(0xff0B28BE).withOpacity(.8),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Text("Sync Data",style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16,color: Colors.white),),
                                  Icon(Icons.sync, color:Colors.white ,),
                                ],
                              ),
                            ))),],
                ),
              ),
              Divider(),
              SizedBox(
                height: 10,
              ),

              Row(
                children: [
                  Flexible(
                      child: Container(
                    height: 40,
                    child: TextField(
                      readOnly: true,
                      keyboardType: TextInputType.number,
                      controller: controller.capitalController,
                      decoration: InputDecoration(
                          labelText: "Capital",
                          border:
                              OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                    ),
                  )),
                  const SizedBox(
                    width: 5,
                  ),
                  Flexible(
                      child: Container(
                    height: 40,
                    child: TextField(
                      readOnly: true,
                      keyboardType: TextInputType.number,
                      controller: controller.fixedAssetsController,
                      decoration: InputDecoration(
                          labelText: "Fixed Assets",
                          border:
                              OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                    ),
                  )),
                ],
              ),
              //2
              const SizedBox(
                height: 10,
              ),
              // Row(
              //   children: [
              //     Flexible(
              //         child: Container(
              //       height: 40,
              //       child: TextField(
              //         keyboardType: TextInputType.number,
              //         controller: controller.securedLoansController,
              //         decoration: InputDecoration(
              //             labelText: "Secured Loans",
              //             border:
              //                 OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
              //       ),
              //     )),
              //     const SizedBox(
              //       width: 5,
              //     ),
              //     Flexible(
              //         child: Container(
              //       height: 40,
              //       child: TextField(
              //         keyboardType: TextInputType.number,
              //         controller: controller.unsecuredLoansController,
              //         decoration: InputDecoration(
              //             labelText: "Unsecured Loans",
              //             border:
              //                 OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
              //       ),
              //     )),
              //   ],
              // ),
              // //3
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Flexible(
                      child: Container(
                    height: 40,
                    child: TextField(
                      readOnly: true,
                      keyboardType: TextInputType.number,
                      controller: controller.provisionsController,
                      decoration: InputDecoration(
                          labelText: "Provisions",
                          border:
                              OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                    ),
                  )),
                  const SizedBox(
                    width: 5,
                  ),
                  Flexible(
                      child: Container(
                    height: 40,
                    child: TextField(
                      readOnly: true,
                      keyboardType: TextInputType.number,
                      controller: controller.dutiesAndTaxesController,
                      decoration: InputDecoration(
                          labelText: "Duties & Taxes",
                          border:
                              OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                    ),
                  )),
                ],
              ),
              //4
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Flexible(
                      child: Container(
                    height: 40,
                    child: TextField(
                      readOnly: true,
                      keyboardType: TextInputType.number,
                      controller: controller.sundryDebtorsController,
                      decoration: InputDecoration(
                          labelText: "Sundry Debtors",
                          border:
                              OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                    ),
                  )),
                  const SizedBox(
                    width: 5,
                  ),
                  Flexible(
                      child: Container(
                    height: 40,
                    child: TextField(
                      readOnly: true,
                      keyboardType: TextInputType.number,
                      controller: controller.closingStockController,
                      decoration: InputDecoration(
                          labelText: "Closing Stock",
                          border:
                              OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                    ),
                  )),
                ],
              ),
              //5
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Flexible(
                      child: Container(
                    height: 40,
                    child: TextField(
                      readOnly: true,
                      keyboardType: TextInputType.number,
                      controller: controller.cashAndBankBalController,
                      decoration: InputDecoration(
                          labelText: "Cash & Bank Bal.",
                          border:
                              OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                    ),
                  )),
                  const SizedBox(
                    width: 5,
                  ),
                  Flexible(
                      child: Container(
                    height: 40,
                    child: TextField(
                      readOnly: true,
                      keyboardType: TextInputType.number,
                      controller: controller.loansAndAndvancesController,
                      decoration: InputDecoration(
                          labelText: "Loans And Advances",
                          border:
                              OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                    ),
                  )),
                ],
              ),
              //6
              const SizedBox(
                height: 10,
              ),
              // Row(
              //   children: [
              //     Flexible(
              //         child: Container(
              //       height: 40,
              //       child: TextField(
              //         keyboardType: TextInputType.number,
              //         controller: controller.sundryCreditorsController,
              //         decoration: InputDecoration(
              //             labelText: "Sundry Creditors",
              //             border:
              //                 OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
              //       ),
              //     )),
              //     const SizedBox(
              //       width: 5,
              //     ),
              //     Flexible(child: Container())
              //     /*Flexible(child: Container(
              //       height: 40,
              //       child: TextField(
              //         decoration: InputDecoration(
              //             labelText: "Notes on Accounts & Accounting Policies",
              //             border: OutlineInputBorder(
              //                 borderRadius: BorderRadius.circular(5),
              //                 borderSide: BorderSide(width: 1,color: Colors.grey)
              //             )
              //         ),
              //       ),
              //     )),*/
              //   ],
              // ),

              const SizedBox(
                height: 10,
              ),

              Text(
                "More Assets",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: List.generate(controller.moreAssets.length, (index) {
                    TextEditingController newHeadName = TextEditingController();
                    TextEditingController newAmount = TextEditingController();

                    newHeadName.text = controller.moreAssets[index].headName.toString();
                    newAmount.text = controller.moreAssets[index].total.toString();

                    return Column(
                      children: [
                        Row(
                          children: [
                            Flexible(
                                child: Container(
                              height: 40,
                              child: TextField(
                                keyboardType: TextInputType.number,
                                controller: newAmount,
                                decoration: InputDecoration(
                                    labelText: newHeadName.text,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                              ),
                            )),
                            const SizedBox(
                              width: 5,
                            ),
                            // Flexible(
                            //     child: Container(
                            //   height: 40,
                            //   child: TextField(
                            //     keyboardType: TextInputType.number,
                            //     controller: controller.loansAndAndvancesController,
                            //     decoration: InputDecoration(
                            //         labelText: "Loans And Advances",
                            //         border: OutlineInputBorder(
                            //             borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                            //   ),
                            // )),
                          ],
                        ),

                        const SizedBox(
                          height: 10,
                        ),
                        // Row(
                        //   children: [
                        //     Flexible(
                        //         child: Container(
                        //       height: 40,
                        //       child: TextField(
                        //         controller: newHeadName,
                        //         keyboardType: TextInputType.name,
                        //         decoration: InputDecoration(
                        //             border: OutlineInputBorder(
                        //                 borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                        //       ),
                        //     )),
                        //     const SizedBox(
                        //       width: 5,
                        //     ),
                        //     Flexible(
                        //         child: Container(
                        //       height: 40,
                        //       child: TextField(
                        //         controller: newAmount,
                        //         keyboardType: TextInputType.number,
                        //         decoration: InputDecoration(
                        //             border: OutlineInputBorder(
                        //                 borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                        //       ),
                        //     )),
                        //     const SizedBox(
                        //       width: 5,
                        //     ),
                        //     IconButton(
                        //         onPressed: () {
                        //           controller.balanceSheetList.removeAt(index);
                        //           setState(() {});
                        //         },
                        //         icon: const Icon(
                        //           Icons.delete,
                        //           color: Colors.red,
                        //         ))
                        //   ],
                        // )
                      ],
                    );
                  }),
                ),
              ),

              Text(
                "More Liabilities",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: List.generate(controller.moreLiabilites.length, (index) {
                    TextEditingController newHeadName = TextEditingController();
                    TextEditingController newAmount = TextEditingController();

                    newHeadName.text = controller.moreLiabilites[index].headName.toString();
                    newAmount.text = controller.moreLiabilites[index].total.toString();
                    setState(() {});

                    return Column(
                      children: [
                        Row(
                          children: [
                            Flexible(
                                child: Container(
                              height: 40,
                              child: TextField(
                                keyboardType: TextInputType.number,
                                controller: newAmount,
                                decoration: InputDecoration(
                                    labelText: newHeadName.text,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                              ),
                            )),
                            const SizedBox(
                              width: 5,
                            ),
                            // Flexible(
                            //     child: Container(
                            //   height: 40,
                            //   child: TextField(
                            //     keyboardType: TextInputType.number,
                            //     controller: controller.loansAndAndvancesController,
                            //     decoration: InputDecoration(
                            //         labelText: "Loans And Advances",
                            //         border: OutlineInputBorder(
                            //             borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.grey))),
                            //   ),
                            // )),
                          ],
                        ),

                        const SizedBox(
                          height: 10,
                        ),
                        // Row(
                        //   children: [
                        //     Flexible(
                        //         child: Container(
                        //       height: 40,
                        //       child: TextField(
                        //         controller: newHeadName,
                        //         keyboardType: TextInputType.name,
                        //         decoration: InputDecoration(
                        //             border: OutlineInputBorder(
                        //                 borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                        //       ),
                        //     )),
                        //     const SizedBox(
                        //       width: 5,
                        //     ),
                        //     Flexible(
                        //         child: Container(
                        //       height: 40,
                        //       child: TextField(
                        //         controller: newAmount,
                        //         keyboardType: TextInputType.number,
                        //         decoration: InputDecoration(
                        //             border: OutlineInputBorder(
                        //                 borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1))),
                        //       ),
                        //     )),
                        //     const SizedBox(
                        //       width: 5,
                        //     ),
                        //     IconButton(
                        //         onPressed: () {
                        //           controller.balanceSheetList.removeAt(index);
                        //           setState(() {});
                        //         },
                        //         icon: const Icon(
                        //           Icons.delete,
                        //           color: Colors.red,
                        //         ))
                        //   ],
                        // )
                      ],
                    );
                  }),
                ),
              ),
              const SizedBox(
                height: 20,
              ),

              SizedBox(
                  height: 40,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: const Color(0xff1739E4)),
                      onPressed: () async {


                        if (controller.capitalController.text.isEmpty &&
                            controller.fixedAssetsController.text.isEmpty &&

                            controller.provisionsController.text.isEmpty &&
                            controller.dutiesAndTaxesController.text.isEmpty &&
                            controller.sundryDebtorsController.text.isEmpty &&
                            controller.closingStockController.text.isEmpty &&
                            controller.cashAndBankBalController.text.isEmpty &&
                            controller.loansAndAndvancesController.text.isEmpty ) {
                          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Fields are empty")));
                        } else {

                          double c = controller.totalLiabilities();
                          final Workbook workbook = Workbook();
                          final Worksheet sheet = workbook.worksheets[0];

                          Style globalStyle = workbook.styles.add('style');

                          globalStyle.fontName = 'Times New Roman';
                          globalStyle.bold = true;

                          globalStyle.fontColor = '#000000';
                          globalStyle.fontSize = 14;

                          final Range range = sheet.getRangeByName('A6:F6');
                          range.cellStyle.borders.all.color = '#000000';
                          range.cellStyle.borders.top.lineStyle = LineStyle.double;
                          range.cellStyle.borders.bottom.lineStyle = LineStyle.thin;
                          final Range range2 = sheet.getRangeByName('A6:F6');
                          range2.cellStyle.borders.all.color = '#000000';
                          range2.cellStyle.borders.top.lineStyle = LineStyle.double;

                          range2.cellStyle.borders.bottom.lineStyle = LineStyle.double;

                          // sheet.getRangeByName('A16').setText("NOTES ON ACCOUNTS & ");
                          // sheet.getRangeByName('A16').cellStyle.bold = true;
                          // sheet.getRangeByName('A16').cellStyle.fontSize = 11;
                          // sheet.getRangeByName('A16').cellStyle.underline = true;

                          // sheet.getRangeByName('B16').setText('"I"');

                          // sheet.getRangeByName('A17').setText("ACCOUNTING POLICIES");
                          // sheet.getRangeByName('A17').cellStyle.bold = true;
                          // sheet.getRangeByName('A17').cellStyle.fontSize = 11;
                          // sheet.getRangeByName('A17').cellStyle.underline = true;

                          // final Range range3 = sheet.getRangeByName('C18');
                          // range3.cellStyle.borders.all.color = '#000000';
                          // range3.cellStyle.borders.top.lineStyle = LineStyle.double;
                          // range3.cellStyle.borders.top.lineStyle = LineStyle.thin;
                          // range3.cellStyle.borders.bottom.lineStyle = LineStyle.double;

                          // final Range range4 = sheet.getRangeByName('F18');
                          // range4.cellStyle.borders.all.color = '#000000';
                          // range4.cellStyle.borders.top.lineStyle = LineStyle.double;
                          // range4.cellStyle.borders.top.lineStyle = LineStyle.thin;
                          // range4.cellStyle.borders.bottom.lineStyle = LineStyle.double;

                          /*  final Range range3 = sheet.getRangeByName('D14');
                        range3.cellStyle.borders.all.color = '#000000';
                        range3.cellStyle.borders.top.lineStyle = LineStyle.thin;
                        range3.cellStyle.borders.bottom.lineStyle =
                            LineStyle.double;*/

                          sheet.name = "Balance Sheet";

                          storage.read("client_name").toString().isEmpty
                              ? sheet.getRangeByName('B1').setText("")
                              : sheet
                              .getRangeByName('B1')
                              .setText(storage.read("client_name").toString() + " (Prop.  ${storage.read("prop_name").toString()})");
                          sheet.getRangeByName("B1").cellStyle.fontSize = 50;
                          sheet.getRangeByName('B1').cellStyle.bold = true;
                          sheet.getRangeByName('B1').cellStyle = globalStyle;
                          sheet.getRangeByName('B1').cellStyle = globalStyle;

                          storage.read("distributor_address").toString().isEmpty
                              ? sheet.getRangeByName('B2').setText("")
                              : sheet.getRangeByName('B2').setText(storage.read("distributor_address").toString());

                          sheet.getRangeByName('B2').cellStyle = globalStyle;

                          sheet.getRangeByName('B4').setText("BALANCE SHEET AS ON 31/03/2022");
                          sheet.getRangeByName('B4').cellStyle.bold = true;
                          sheet.getRangeByName('B4').cellStyle.fontSize = 11;

                          sheet.getRangeByName('A6').setText("LIABILITIES      ");
                          sheet.getRangeByName('A6').cellStyle.fontSize = 12;
                          sheet.getRangeByName('A6').cellStyle.bold = true;
                          sheet.getRangeByName('A6').columnWidth = 35;

                          sheet.getRangeByName('B6').setText("ANN.");
                          sheet.getRangeByName('B6').cellStyle.fontSize = 12;
                          sheet.getRangeByName('B6').cellStyle.bold = true;
                          sheet.getRangeByName('B6').columnWidth = 12.5;

                          sheet.getRangeByName('C6').setText("AMOUNT");
                          sheet.getRangeByName('C6').cellStyle.fontSize = 12;
                          sheet.getRangeByName('C6').cellStyle.bold = true;
                          sheet.getRangeByName('C6').columnWidth = 12.5;

                          sheet.getRangeByName('D6').setText("ASSETS");
                          sheet.getRangeByName('D6').cellStyle.fontSize = 12;
                          sheet.getRangeByName('D6').cellStyle.bold = true;
                          sheet.getRangeByName('D6').columnWidth = 12.5;

                          sheet.getRangeByName('E6').setText("ANN.");
                          sheet.getRangeByName('E6').cellStyle.fontSize = 12;
                          sheet.getRangeByName('E6').cellStyle.bold = true;
                          sheet.getRangeByName('E6').columnWidth = 12.5;

                          sheet.getRangeByName('F6').setText("AMOUNT");
                          sheet.getRangeByName('F6').cellStyle.fontSize = 12;
                          sheet.getRangeByName('F6').cellStyle.bold = true;
                          sheet.getRangeByName('F6').columnWidth = 12.5;

                          sheet.getRangeByName('A7').setText("CAPITAL");
                          sheet.getRangeByName('A7').cellStyle.bold = true;
                          sheet.getRangeByName('A7').cellStyle.underline = true;
                          sheet.getRangeByName('A7').cellStyle.fontSize = 11;
                          sheet.getRangeByName('B7').setText('"A"');

                          sheet.getRangeByName('C7').setText(double.parse(controller.capitalController.text.isEmpty?"0.0":controller.capitalController.text.toString()).toStringAsFixed(2));

                          sheet.getRangeByName('D7').setText("FIXED ASSETS");
                          sheet.getRangeByName('D7').cellStyle.bold = true;
                          sheet.getRangeByName('D7').cellStyle.underline = true;
                          sheet.getRangeByName('D7').cellStyle.fontSize = 11;

                          sheet.getRangeByName('E7').setText('"D"');
                          sheet.getRangeByName('F7').setText(double.parse(controller.fixedAssetsController.text.isEmpty?"0.0":controller.fixedAssetsController.text.toString()).toStringAsFixed(2));

                          // sheet.getRangeByName('A9').setText("LOANS");
                          // sheet.getRangeByName('A9').cellStyle.bold = true;
                          // sheet.getRangeByName('A9').cellStyle.underline = true;
                          // sheet.getRangeByName('A9').cellStyle.fontSize = 11;

                          // sheet.getRangeByName('A10').setText("SECURED LOANS");

                          // sheet.getRangeByName('C10').setText(double.parse(controller.securedLoansController.text.toString()).toStringAsFixed(2));

                          sheet.getRangeByName('D10').setText("CURRENT ASSETS");
                          sheet.getRangeByName('D10').columnWidth = 25;
                          sheet.getRangeByName('D10').cellStyle.bold = true;
                          sheet.getRangeByName('D10').cellStyle.underline = true;
                          sheet.getRangeByName('D10').cellStyle.fontSize = 11;

                          sheet.getRangeByName('D11').setText("SUNDRY DEBTORS");
                          sheet.getRangeByName('E11').setText('"E"');
                          sheet.getRangeByName('F11').setText(double.parse(controller.sundryDebtorsController.text.isEmpty?"0.0":controller.sundryDebtorsController.text.toString()).toStringAsFixed(2));

                          sheet.getRangeByName('A12').setText("CURRENT LIAB. & PROV.");
                          sheet.getRangeByName('A12').cellStyle.bold = true;
                          sheet.getRangeByName('A12').cellStyle.underline = true;
                          sheet.getRangeByName('A12').cellStyle.fontSize = 11;

                          sheet.getRangeByName('D12').setText("CLOSING STOCK");
                          sheet.getRangeByName('E12').setText('"F"');
                          sheet.getRangeByName('F12').setText(double.parse(controller.closingStockController.text.isEmpty?"0.0":controller.closingStockController.text.toString()).toStringAsFixed(2));

                          sheet.getRangeByName('A13').setText("PROVISIONS");
                          sheet.getRangeByName('B13').setText('"B"');
                          sheet.getRangeByName('C13').setText(double.parse(controller.provisionsController.text.isEmpty?"0.0":controller.provisionsController.text.toString()).toStringAsFixed(2));

                          sheet.getRangeByName('D13').setText("CASH & BANK BAL.");
                          sheet.getRangeByName('E13').setText('"G"');
                          sheet.getRangeByName('F13').setText(double.parse(controller.cashAndBankBalController.text.isEmpty?"0.0":controller.cashAndBankBalController.text.toString()).toStringAsFixed(2));

                          sheet.getRangeByName('A14').setText("DUTIES & TAXES");
                          sheet.getRangeByName('B14').setText('"C"');
                          sheet.getRangeByName('C14').setText(double.parse(controller.dutiesAndTaxesController.text.isEmpty?"0.0":controller.dutiesAndTaxesController.text.toString()).toStringAsFixed(2));

                          int i;
                          for (i = 0; i < controller.moreLiabilites.length; i++) {
                            sheet.getRangeByName('A${16 + i}').setText(controller.moreLiabilites[i].headName);
                            sheet.getRangeByName('B${16 + i}').setText(controller.moreLiabilites[i].sNo);
                            sheet
                                .getRangeByName('C${16 + i}')
                                .setText('"${double.parse(controller.moreLiabilites[i].total.toString()).toStringAsFixed(2)}"');
                          }
                          var j = 16 + i;

                          for (i = 0; i < controller.moreAssets.length; i++) {
                            sheet.getRangeByName('D${16 + i}').setText(controller.moreAssets[i].headName);
                            sheet.getRangeByName('E${16 + i}').setText(controller.moreAssets[i].sNo);
                            sheet
                                .getRangeByName('F${16 + i}')
                                .setText('"${double.parse(controller.moreAssets[i].total.toString()).toStringAsFixed(2)}"');
                          }
                          var k = 16 + i;
                          if (j > k) {
                            sheet.getRangeByName('C${j}').setText(c.toString());
                            sheet.getRangeByName('F${j}').setText(c.toString());
                            sheet.getRangeByName('F${j}').cellStyle.borders.all.color = '#000000';
                            sheet.getRangeByName('F${j}').cellStyle.borders.top.lineStyle = LineStyle.thin;
                            sheet.getRangeByName('F${j}').cellStyle.borders.bottom.lineStyle = LineStyle.double;
                            sheet.getRangeByName('C${j}').cellStyle.borders.all.color = '#000000';
                            sheet.getRangeByName('C${j}').cellStyle.borders.top.lineStyle = LineStyle.thin;
                            sheet.getRangeByName('C${j}').cellStyle.borders.bottom.lineStyle = LineStyle.double;
                          } else {
                            sheet.getRangeByName('C${k}').setText(c.toString());
                            sheet.getRangeByName('F${k}').setText(c.toString());
                            sheet.getRangeByName('F${k}').cellStyle.borders.all.color = '#000000';
                            sheet.getRangeByName('F${k}').cellStyle.borders.top.lineStyle = LineStyle.thin;
                            sheet.getRangeByName('F${k}').cellStyle.borders.bottom.lineStyle = LineStyle.double;
                            sheet.getRangeByName('C${k}').cellStyle.borders.all.color = '#000000';
                            sheet.getRangeByName('C${k}').cellStyle.borders.top.lineStyle = LineStyle.thin;
                            sheet.getRangeByName('C${k}').cellStyle.borders.bottom.lineStyle = LineStyle.double;
                          }

                          sheet.getRangeByName('D14').setText("LOANS AND ADVANCES");
                          sheet.getRangeByName('E14').setText('"H"');
                          sheet
                              .getRangeByName('F14')
                              .setText('"${double.parse(controller.loansAndAndvancesController.text.isEmpty?"0.0":controller.loansAndAndvancesController.text.toString()).toStringAsFixed(2)}"');

                          // sheet.getRangeByName("C18").setText("2804304.81");
                          // sheet.getRangeByName("F18").setText("2804304.81");

                          sheet.getRangeByName('A22').setText("CERTIFIED To  BE TRUE & CORRECT ");
                          sheet.getRangeByName('A23').setText("FOR: ${storage.read("client_name").toString().toUpperCase()}");

                          sheet.getRangeByName('A27').setText("(${storage.read("prop_name").toString().toUpperCase()})");
                          sheet.getRangeByName('A28').setText("Prop.");

                          storage.read("ca_city").toString().isEmpty
                              ? sheet.getRangeByName('A30').setText("")
                              : sheet.getRangeByName('A30').setText("PLACE: " + storage.read("ca_city").toString());

                          sheet.getRangeByName('A31').setText("DATE: " + formatDate(DateTime.now(), [dd, "/", mm, "/", yyyy]));
                          final List<int> bytes = workbook.saveAsStream();
                          workbook.dispose();
                          workbook.dispose();

                          final String path = (await getApplicationSupportDirectory()).path;
                          final String fileName = "$path/balance_sheet.xlsx";
                          final File file = File(fileName);
                          await file.writeAsBytes(bytes, flush: true);

                          OpenFile.open(fileName);

                        }
                      },
                      child: const Text("Download")))
            ],
          ),
        ),
      ),
    );
  }
}
