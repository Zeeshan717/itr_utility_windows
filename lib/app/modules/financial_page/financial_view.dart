import 'package:accountx/app/modules/financial_page/annexure_page/annexure_view.dart';

import 'package:accountx/app/modules/financial_page/computation_page/computation_controller.dart';

import 'package:accountx/app/modules/financial_page/computation_page/computation_view.dart';

import 'package:accountx/app/modules/financial_page/computation_page/total_computation_view.dart';
import 'package:accountx/app/modules/financial_page/profit_and_loss_page/profit_and_loss_view.dart';

import 'package:accountx/app/modules/taxCalPage/taxController.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../taxCalPage/taxController.dart';

class FinancialView extends StatefulWidget {


   FinancialView({Key? key}) : super(key: key);

  @override
  State<FinancialView> createState() => _FinancialViewState();
}

class _FinancialViewState extends State<FinancialView> {
  var controller = Get.put(TaxController());
  var computaionController = Get.put(ComputationController());
  GetStorage storage = GetStorage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     /* appBar: AppBar(
        backgroundColor: Color(0xff1739E4),
        centerTitle: true,
        title: Text(
          "Financial Statement",
          style: TextStyle(fontSize: 16),
        ),
      ),*/
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      Get.to(ProfitAndLossView());
                    },
                    child: Card(
                      elevation: 5,
                      child: Container(
                        alignment: Alignment.center,
                        width: 100,
                        height: 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "images/profitandloss.svg",
                              width: 30,
                            ),
                            Text(
                              "Profit And Loss Account",
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      var deductvalue = storage.read("deductionvalue");
                      // print("  TTTTTTTTTTT-------------- $deductvalue");

                      if (deductvalue.toString().isNotEmpty) {
                        Get.to(TotalComputationView());
                      } else {
                        Get.defaultDialog(
                            title: "Alert",
                            titleStyle: TextStyle(fontSize: 16),
                            content: Flexible(
                                child: Text(
                              "Please go to the calculator page",
                              style: TextStyle(fontSize: 13),
                            )),
                            actions: [
                              SizedBox(
                                height: 30,
                                child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
                                    onPressed: () {
                                      Get.back();
                                    },
                                    child: Text("OK")),
                              )
                            ]);
                      }
                    },
                    child: Card(
                      elevation: 5,
                      child: Container(
                        alignment: Alignment.center,
                        width: 100,
                        height: 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "images/computation.svg",
                              width: 30,
                            ),
                            Text(
                              "Total Computation",
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      computaionController.computationTaxPaidList.clear();
                      computaionController.computationBankList.clear();
                      Get.to(ComputationView());
                    },
                    child: Card(
                      elevation: 5,
                      child: Container(
                        alignment: Alignment.center,
                        width: 100,
                        height: 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "images/totalcomputation.svg",
                              width: 30,
                            ),
                            Text(
                              "Computation",
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {

                      /*var deductvalue = storage.read("deductionvalue");
                      // print("  TTTTTTTTTTT-------------- $deductvalue");

                      if (deductvalue.toString().isNotEmpty) {
                        Get.to(TotalComputationView());
                      } else {
                        Get.defaultDialog(
                          barrierDismissible: false,
                          title: "Alert",
                            titleStyle: TextStyle(fontSize: 16),
                            content: Flexible(child: Text("Please go to the calculator page",style: TextStyle(fontSize: 13),)), actions: [
                          SizedBox(
                            height:30,
                            child: ElevatedButton(

                                style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE)),
                                onPressed: () {
                                  Get.back();
                                },
                                child: Text("OK")),
                          )
                        ]);
                      }*/

                      Get.to(AnnexureView());

                    },
                    child: Card(
                      elevation: 5,
                      child: Container(
                        alignment: Alignment.center,
                        width: 100,
                        height: 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "images/annexure.svg",
                              width: 30,
                            ),
                            Text(
                              "Annexure",
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {

                      // Get.toNamed("/balancesheet");
                    },
                    child: Card(
                      elevation: 5,
                      child: Container(
                        alignment: Alignment.center,
                        width: 100,
                        height: 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "images/balancesheet.svg",
                              width: 30,
                            ),
                            Text(
                              "Balance Sheet",
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: null,
                    child: Card(
                      elevation: 0,
                      child: Container(
                        alignment: Alignment.center,
                        width: 100,
                        height: 100,
                        /*child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset("images/balance.svg",width: 30,),
                            Text("Balance Sheet",textAlign: TextAlign.center,)
                          ],
                        ),*/
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
