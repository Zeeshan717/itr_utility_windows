// ignore_for_file: file_names

import 'package:accountx/app/modules/loginPage/loginController.dart';
import 'package:get/get.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<LoginController>(LoginController());
    // Get.lazyPut(() => LoginController());
  }
}
