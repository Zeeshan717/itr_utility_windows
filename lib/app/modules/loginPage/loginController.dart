// ignore_for_file: file_names, non_constant_identifier_names

import 'package:accountx/app/modules/homePage/homeView.dart';
import 'package:accountx/app/services/api.dart';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:window_manager/window_manager.dart';

class LoginController extends GetxController with WindowListener {

  GetStorage box3 = GetStorage();
  var isVisible = true.obs;
  final rememberMe = true.obs;
  TextEditingController userNameCtx = TextEditingController();
  TextEditingController passwordCtx = TextEditingController();


/*  @override
  void onInit() {
   WindowManager.instance.addListener(this);
    super.onInit();
  }*/

  void updateRememberMe() {
    rememberMe.value = !rememberMe.value;
  }

  void userLogin(BuildContext context) async {
    var email_id = userNameCtx.text;
    var password = passwordCtx.text;
    await Api.loginApi(email_id, password, '1', '', '', '', '', '', '', '').then((value) {
      if (value['success'] == 1) {
        EasyLoading.dismiss();
        Get.off(HomeView());
        box3.write('loginValue', value);
        if (rememberMe.value) {
          box3.write('isLogined', true);
        }
      } else {
        EasyLoading.dismiss();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(value['response_msg'].toString())),
        );
      }
    });
  }

 /* @override
  void onWindowFocus() {
    update();
  }

  @override
  void onWindowMaximize() {
    WindowManager.instance.unmaximize();
  }*/

}
