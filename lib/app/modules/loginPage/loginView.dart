// ignore_for_file: sized_box_for_whitespace, file_names

import 'package:accountx/app/modules/loginPage/loginController.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class LoginView extends GetView<LoginController> {
  LoginView({Key? key}) : super(key: key);
  final LoginController _loginController = Get.find();
  final _formKey = GlobalKey<FormState>();
  // DateTime? currentBackPressTime;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 30,
              ),
              SvgPicture.asset(
                "images/login.svg",
                height: 200,
                fit: BoxFit.contain,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "Login",
                      style: TextStyle(fontSize: 20, color: Color(0xff233862), fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 350,
                  child: Form(
                      key: _formKey,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    boxShadow: [BoxShadow(blurRadius: 4, color: Colors.grey, spreadRadius: 0, blurStyle: BlurStyle.outer)],
                                  ),
                                  child: const CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Icon(
                                      Icons.person,
                                      color: Color(0xff112DC3),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Flexible(
                                  child: SizedBox(
                                    height: 70,
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Username Required';
                                        }
                                        return null;
                                      },
                                      controller: _loginController.userNameCtx,
                                      decoration: InputDecoration(
                                        enabledBorder: UnderlineInputBorder(
                                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1)),
                                        focusedBorder: UnderlineInputBorder(
                                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xff0e3048), width: 1)),
                                        labelText: "Username",
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Obx(
                              () => Row(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                      boxShadow: [BoxShadow(blurRadius: 6, color: Colors.grey, offset: Offset.fromDirection(0.0, 0.0))],
                                    ),
                                    child: const CircleAvatar(
                                      backgroundColor: Colors.white,
                                      child: Icon(
                                        Icons.lock,
                                        color: Color(0xff112DC3),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Flexible(
                                    child: SizedBox(
                                      height: 70,
                                      child: TextFormField(
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Password Required';
                                          }
                                          return null;
                                        },
                                        controller: _loginController.passwordCtx,
                                        obscureText: _loginController.isVisible.value,
                                        decoration: InputDecoration(
                                          suffixIcon: IconButton(
                                            onPressed: () {
                                              _loginController.isVisible.value = !_loginController.isVisible.value;
                                            },
                                            icon: _loginController.isVisible.value
                                                ? const Icon(
                                                    Icons.visibility,
                                                    color: Color(0xff112DC3),
                                                  )
                                                : const Icon(
                                                    Icons.visibility_off,
                                                    color: Color(0xff112DC3),
                                                  ),
                                          ),
                                          enabledBorder: UnderlineInputBorder(
                                              borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.grey, width: 1)),
                                          focusedBorder: UnderlineInputBorder(
                                              borderRadius: BorderRadius.circular(5),
                                              borderSide: const BorderSide(color: Color(0xff0E3048), width: 1)),
                                          labelText: "Password",
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            // SizedBox(height: 5,),
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.end,
                            //   crossAxisAlignment: CrossAxisAlignment.end,
                            //   children: [
                            //     TextButton(onPressed: () {}, child: Text("Forgot Password?",style: TextStyle(
                            //         color: Color(0xff0E3048)
                            //     ),)),
                            //   ],
                            // ),
                            const SizedBox(
                              height: 40,
                            ),
                            SizedBox(
                              width: 260,
                              height: 40,
                              child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      primary: const Color(0xff0B28BE), shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7))),
                                  onPressed: () {
                                    if(_loginController.userNameCtx.text.isEmpty) {
                                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Username is required !!")));
                                    }
                                    else if(_loginController.passwordCtx.text.isEmpty) {
                                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Password is required !!")));
                                    } else {
                                      EasyLoading.show(status: 'loading...');
                                      _loginController.userLogin(context);
                                    }
                                    /*if (_formKey.currentState!.validate()) {

                                    } else {
                                      // ScaffoldMessenger.of(context).showSnackBar(
                                      //   const SnackBar(content: Text('Processing Data')),
                                      // );
                                    }*/
                                  },
                                  child: Text("Login".toUpperCase())),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Obx(
                                  () => Checkbox(
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                                      activeColor: const Color(0xff0E3048),
                                      value: _loginController.rememberMe.value,
                                      onChanged: (value) {
                                        _loginController.updateRememberMe();
                                      }),
                                ),
                                const SizedBox(
                                  width: 2,
                                ),
                                const Text(
                                  "Remember Me",
                                  style: TextStyle(color: Color(0xff0E3048)),
                                )
                              ],
                            )
                          ],
                        ),
                      )),
                ),
              ),
              // SizedBox(height: 10,),
              // Row(
              //   crossAxisAlignment: CrossAxisAlignment.center,
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     Text("Don't have an account yet?"),
              //     SizedBox(width: 2,),
              //     TextButton(onPressed: () {}, child: Text("Sign Up"),)
              //   ],
              // )
            ],
          ),
        ),
      ),
    );
  }

  // Future<bool> onWillPop(context) {
  //   DateTime now = DateTime.now();
  //   if (currentBackPressTime == null || now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
  //     currentBackPressTime = now;
  //     // ScaffoldMessenger.of(context).showSnackBar(snackBar)
  //     Fluttertoast.showToast(msg: "double click to exit");

  //     return Future.value(false);
  //   }
  //   return Future.value(true);
  // }

}
