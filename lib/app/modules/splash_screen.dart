import 'dart:async';

import 'package:accountx/app/modules/homePage/homeView.dart';
import 'package:accountx/app/modules/loginPage/loginController.dart';
import 'package:accountx/app/modules/loginPage/loginView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);
  static const String id = 'splashscreen';
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // GetStorage box1 = GetStorage();
    // bool isLogined = false;
    // isLogined = box1.read('isLogined') ?? false;
    Get.put(LoginController());
    var isLogined = false;
    Timer(Duration(seconds: 5), () {
      isLogined == false || isLogined == null
          // ? Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginView()))
          // : Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeView()));
          ? Get.off(LoginView())
          : Get.off(HomeView());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(),
            Spacer(),
            Image.asset(
              "images/itrlogo.png",
              width: 120,
              height: 120,
            ),
            Spacer(),
            Text(
              "Zucol Group",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Text(
              "V 1.0",
              style: TextStyle(
                fontSize: 12,
              ),
            ),
            SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }
}
