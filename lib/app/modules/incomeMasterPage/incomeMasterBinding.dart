// ignore_for_file: file_names

import 'package:accountx/app/modules/incomeMasterPage/incomeMasterController.dart';
import 'package:get/get.dart';

class IncomeMasterBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<IncomeMasterController>(IncomeMasterController());
  }
}