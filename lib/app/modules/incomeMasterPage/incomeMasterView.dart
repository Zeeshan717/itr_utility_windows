// ignore_for_file: file_names, sized_box_for_whitespace, avoid_print

import 'dart:io';

import 'package:accountx/app/modules/incomeMasterPage/incomeMasterController.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:accountx/app/services/api.dart';

class IncomeMaster extends StatefulWidget {
  const IncomeMaster({Key? key}) : super(key: key);
  @override
  State<IncomeMaster> createState() => _IncomeMasterState();
  void callBackouter(){
    _IncomeMasterState().callBack();
  }

}

class _IncomeMasterState extends State<IncomeMaster> {
  File? image;
  var controller = Get.put(IncomeMasterController());

  @override
  void initState() {
    controller.incomeMasterDetailsCall();
    super.initState();
  }

  void callBack(){
    controller.incomeMasterDetailsCall();
  }

  @override
  Widget build(BuildContext context) {
    controller.incomeMasterControl();
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            "INCOME MASTER",
            style: TextStyle(fontSize: 16),
          ),
          backgroundColor: const Color(0xff0B28BE),
        ),
        body: LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.white,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    //Salary Start
                    const SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const [
                          Text("SALARY"),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                              color: const Color(0xffC1CDFF), width: 1)),
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      child: Row(
                        children: [
                          Flexible(
                            child: TextFormField(
                              style: const TextStyle(fontSize: 13),
                              controller: controller.salaryController,
                              enabled: false,
                              decoration: InputDecoration(
                                disabledBorder:OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Colors.transparent)) ,
                                contentPadding: const EdgeInsets.all(15),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xffC1CDFF))),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xff0B28BE))),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              controller.amountController.clear();
                              controller.remarkController.clear();
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                        contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        backgroundColor: Colors.transparent,
                                        content: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Scrollbar(
                                            thumbVisibility: true,
                                            child: ListView(
                                              shrinkWrap: true,
                                              children: [
                                                Column(
                                                  children: [
                                                    Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 430,
                                                      decoration: BoxDecoration(
                                                          color: Colors.white,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10)),
                                                      child: Column(
                                                        children: [
                                                          Container(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            alignment: Alignment
                                                                .center,
                                                            decoration:
                                                                const BoxDecoration(
                                                                    color: Color(
                                                                        0xff1739E4),
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .only(
                                                                      topLeft: Radius
                                                                          .circular(
                                                                              10),
                                                                      topRight:
                                                                          Radius.circular(
                                                                              10),
                                                                    )),
                                                            width:
                                                                MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                            height: 40,
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                const Text(""),
                                                                const Text(
                                                                  "SALARY",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          18,
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                                InkWell(
                                                                    onTap: () {
                                                                      Get.back();
                                                                    },
                                                                    child:
                                                                        const Icon(
                                                                      Icons
                                                                          .clear,
                                                                      color: Colors
                                                                          .white,
                                                                    )),
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                const Text(
                                                                  "FILE UPLOAD",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          13),
                                                                ),
                                                                InkWell(
                                                                  onTap:
                                                                      () async {
                                                                    if (Platform.isWindows) {
                                                                      FilePickerResult?
                                                                          filePickerResult =
                                                                          await FilePicker
                                                                              .platform
                                                                              .pickFiles(type: FileType.image);
                                                                      if (filePickerResult !=
                                                                          null) {
                                                                        PlatformFile
                                                                            file =
                                                                            filePickerResult.files.single;
                                                                        controller.image = File(file
                                                                            .path
                                                                            .toString());
                                                                        print("File Path:" +
                                                                            controller.image.toString());
                                                                      }
                                                                    } else {
                                                                      showDialog(
                                                                          barrierDismissible:
                                                                              false,
                                                                          context:
                                                                              context,
                                                                          builder:
                                                                              (context) {
                                                                            return AlertDialog(
                                                                              insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                              contentPadding: EdgeInsets.zero,
                                                                              content: Container(
                                                                                height: 130,
                                                                                child: Column(
                                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                  children: [
                                                                                    Container(
                                                                                      padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                      width: MediaQuery.of(context).size.width,
                                                                                      height: 50,
                                                                                      color: Colors.indigo,
                                                                                      child: Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text(""),
                                                                                          const Text(
                                                                                            "Choose Source",
                                                                                            style: TextStyle(color: Colors.white),
                                                                                          ),
                                                                                          InkWell(
                                                                                              onTap: () {
                                                                                                Get.back();
                                                                                              },
                                                                                              child: const Icon(Icons.clear, color: Colors.white))
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                    const SizedBox(height: 10),
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                      children: [
                                                                                        Column(
                                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                          children: [
                                                                                            GestureDetector(
                                                                                                onTap: () {
                                                                                                  controller.uploadFromGallery().whenComplete(() {
                                                                                                    Get.back();
                                                                                                  });
                                                                                                },
                                                                                                child: Image.asset(
                                                                                                  "images/gallery.png",
                                                                                                  width: 30,
                                                                                                )),
                                                                                            const Text("Gallery")
                                                                                          ],
                                                                                        ),
                                                                                        Column(
                                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                          children: [
                                                                                            GestureDetector(
                                                                                                onTap: () {
                                                                                                  controller.uploadFromCamera().whenComplete(() {
                                                                                                    Get.back();
                                                                                                  });
                                                                                                },
                                                                                                child: Image.asset(
                                                                                                  "images/camera.png",
                                                                                                  width: 30,
                                                                                                )),
                                                                                            const Text("Camera")
                                                                                          ],
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                    const SizedBox(
                                                                                      height: 20,
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                            );
                                                                          });
                                                                    }
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    padding: const EdgeInsets
                                                                            .symmetric(
                                                                        horizontal:
                                                                            10),
                                                                    decoration: BoxDecoration(
                                                                        color: const Color(
                                                                            0xff1739E4),
                                                                        borderRadius:
                                                                            BorderRadius.circular(5)),
                                                                    height: 25,
                                                                    child: Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceEvenly,
                                                                      children: const [
                                                                        Text(
                                                                          "Upload",
                                                                          style:
                                                                              TextStyle(color: Colors.white),
                                                                        ),
                                                                        SizedBox(
                                                                          width:
                                                                              10,
                                                                        ),
                                                                        Icon(
                                                                          Icons
                                                                              .cloud_upload,
                                                                          color:
                                                                              Colors.white,
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Text(
                                                                  "Amount"
                                                                      .toUpperCase(),
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          13),
                                                                ),
                                                                const SizedBox()
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: SizedBox(
                                                              height: 40,
                                                              child:
                                                                  TextFormField(
                                                                controller:
                                                                    controller
                                                                        .amountController,
                                                                keyboardType:
                                                                    TextInputType
                                                                        .number,
                                                                decoration: InputDecoration(
                                                                    border: OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        borderSide: const BorderSide(
                                                                            color:
                                                                                Color(0xffC1CDFF),
                                                                            width: 1))),
                                                              ),
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Text(
                                                                  "Remark"
                                                                      .toUpperCase(),
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          13),
                                                                ),
                                                                const SizedBox(
                                                                  height: 10,
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Container(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child:
                                                                TextFormField(
                                                              controller: controller
                                                                  .remarkController,
                                                              keyboardType:
                                                                  TextInputType
                                                                      .text,
                                                              maxLines: 4,
                                                              decoration: InputDecoration(
                                                                  border: OutlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      borderSide:
                                                                          const BorderSide(
                                                                              color: Color(0xffC1CDFF)))),
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 50,
                                                          ),
                                                          Container(
                                                            margin:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            width:
                                                                MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                            height: 50,
                                                            child:
                                                                ElevatedButton(
                                                                    style: ElevatedButton.styleFrom(
                                                                        primary:
                                                                            const Color(
                                                                                0xff0B28BE)),
                                                                    onPressed:
                                                                        () {
                                                                      if(controller.amountController.text.isEmpty) {
                                                                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Amount is required !!")));
                                                                      } else if(controller.remarkController.text.isEmpty) {
                                                                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Remark is required !!")));
                                                                      } else {
                                                                        EasyLoading.show(
                                                                            status:
                                                                            "Loading");
                                                                        controller
                                                                            .incomeMasterCall(
                                                                            context,
                                                                            "Salary",
                                                                            controller.amountController.text,
                                                                            controller.remarkController.text)
                                                                            .whenComplete(() {
                                                                          controller
                                                                              .incomeMasterDetailsCall();
                                                                          Get.back();
                                                                      }
                                                                    );
                                                                      }
                                                                    },
                                                                    child: Text(
                                                                        "Submit"
                                                                            .toUpperCase())),
                                                          ),
                                                          const SizedBox(
                                                            height: 20,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                        ));
                                  });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          InkWell(
                            onTap: () {
                              controller
                                  .incomeMasterDetailsCall()
                                  .whenComplete(() {
                                // print("---------incomeMasterResponse---------------" + controller.incomeMasterResponse.value.toString());
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const Text("Salary Details"),
                                            InkWell(
                                                onTap: () {
                                                  Get.back();
                                                },
                                                child: const Icon(
                                                  Icons.clear,
                                                  color: Colors.red,
                                                ))
                                          ],
                                        ),
                                        content: controller
                                            .transcationList.isNotEmpty ? Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 300,
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 20),
                                                child: Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment
                                                      .center,
                                                  children: [
                                                    Expanded(child: Text("S. No",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Head",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold))),
                                                    Expanded(child: Text("Amount",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold))),
                                                    Expanded(child: Text("Reamrk",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold))),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),
                                                  ],
                                                ),
                                              ),
                                              Expanded(
                                                child: ListView.builder(
                                                    itemCount: controller
                                                        .transcationList.length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return Padding(
                                                        padding: const EdgeInsets.symmetric(horizontal: 20),
                                                        child: Column(
                                                          children: [
                                                            SizedBox(height: 10,),
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Expanded(
                                                                    child: Text(
                                                                  "${index + 1}",
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller.transcationList[
                                                                          index]
                                                                      ["income_head"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList[
                                                                      index]["amount"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList[
                                                                      index]["remark"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap: () {
                                                                          showDialog(
                                                                              barrierDismissible:
                                                                                  false,
                                                                              context:
                                                                                  context,
                                                                              builder:
                                                                                  (context) {
                                                                                return AlertDialog(
                                                                                  backgroundColor:
                                                                                      Colors.white,
                                                                                  contentPadding:
                                                                                      EdgeInsets.zero,
                                                                                  insetPadding:
                                                                                      EdgeInsets.symmetric(horizontal: 50,vertical: 150),
                                                                                  content:
                                                                                      Container(
                                                                                        alignment: Alignment.topRight,
                                                                                    width: MediaQuery.of(context).size.width,
                                                                                    height: 400,
                                                                                    decoration: BoxDecoration(
                                                                                        color: Colors.black,
                                                                                        image: DecorationImage(
                                                                                          fit: BoxFit.fill,
                                                                                          image: NetworkImage(
                                                                                            "https://appexperts.net/btconnect/" + controller.transcationList[index]["document_path"].toString(),
                                                                                          ),
                                                                                        )),
                                                                                        child: IconButton(onPressed: (){
                                                                                          Get.back();
                                                                                        },
                                                                                        icon: Image.asset("images/close.png",width: 30,),
                                                                                        ),
                                                                                  ),
                                                                                );
                                                                              });
                                                                        },
                                                                        child:
                                                                            SvgPicture
                                                                                .asset(
                                                                          "images/visibility.svg",
                                                                          width: 20,
                                                                        ))),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap: () {
                                                                          print(
                                                                              'Edit salary working');
                                                                          controller
                                                                              .amountController
                                                                              .text = controller
                                                                                  .transcationList[index]
                                                                              [
                                                                              "amount"];
                                                                          controller
                                                                              .remarkController
                                                                              .text = controller
                                                                                  .transcationList[index]
                                                                              [
                                                                              "remark"];
                                                                          Get.back();

                                                                          showDialog(
                                                                              barrierDismissible:
                                                                                  false,
                                                                              context:
                                                                                  context,
                                                                              builder:
                                                                                  (context) {
                                                                                return AlertDialog(
                                                                                    insetPadding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                    backgroundColor: Colors.transparent,
                                                                                    content: Container(
                                                                                      width: MediaQuery.of(context).size.width,
                                                                                      child: Scrollbar(
                                                                                        thumbVisibility: true,
                                                                                        child: ListView(
                                                                                          shrinkWrap: true,
                                                                                          children: [
                                                                                            Column(
                                                                                              children: [
                                                                                                Container(
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 430,
                                                                                                  decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                                                                                  child: Column(
                                                                                                    children: [
                                                                                                      Container(
                                                                                                        padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                        alignment: Alignment.center,
                                                                                                        decoration: const BoxDecoration(
                                                                                                            color: Color(0xff1739E4),
                                                                                                            borderRadius: BorderRadius.only(
                                                                                                              topLeft: Radius.circular(10),
                                                                                                              topRight: Radius.circular(10),
                                                                                                            )),
                                                                                                        width: MediaQuery.of(context).size.width,
                                                                                                        height: 40,
                                                                                                        child: Row(
                                                                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                          children: [
                                                                                                            const Text(""),
                                                                                                            const Text(
                                                                                                              "SALARY",
                                                                                                              style: TextStyle(fontSize: 18, color: Colors.white),
                                                                                                            ),
                                                                                                            InkWell(
                                                                                                                onTap: () {
                                                                                                                  Get.back();
                                                                                                                },
                                                                                                                child: const Icon(
                                                                                                                  Icons.clear,
                                                                                                                  color: Colors.white,
                                                                                                                )),
                                                                                                          ],
                                                                                                        ),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 10,
                                                                                                      ),
                                                                                                      Padding(
                                                                                                        padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                        child: Row(
                                                                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                          children: [
                                                                                                            const Text(
                                                                                                              "FILE UPLOAD",
                                                                                                              style: TextStyle(fontSize: 13),
                                                                                                            ),
                                                                                                            InkWell(
                                                                                                              onTap: () async {
                                                                                                                if (Platform.isWindows) {
                                                                                                                  FilePickerResult? filePickerResult = await FilePicker.platform.pickFiles(type: FileType.image);
                                                                                                                  if (filePickerResult != null) {
                                                                                                                    PlatformFile file = filePickerResult.files.single;
                                                                                                                    controller.image = File(file.path.toString());
                                                                                                                    print("File Path:" + controller.image.toString());
                                                                                                                  }
                                                                                                                } else {
                                                                                                                  showDialog(
                                                                                                                      barrierDismissible: false,
                                                                                                                      context: context,
                                                                                                                      builder: (context) {
                                                                                                                        return AlertDialog(
                                                                                                                          contentPadding: EdgeInsets.zero,
                                                                                                                          insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                                                                          content: Container(
                                                                                                                            height: 130,
                                                                                                                            child: Column(
                                                                                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                              children: [
                                                                                                                                Container(
                                                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                                                  height: 50,
                                                                                                                                  color: Colors.indigo,
                                                                                                                                  child: Row(
                                                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                    children: [
                                                                                                                                      const Text(""),
                                                                                                                                      const Text(
                                                                                                                                        "Choose Source",
                                                                                                                                        style: TextStyle(color: Colors.white),
                                                                                                                                      ),
                                                                                                                                      InkWell(
                                                                                                                                          onTap: () {
                                                                                                                                            Get.back();
                                                                                                                                          },
                                                                                                                                          child: const Icon(Icons.clear, color: Colors.white))
                                                                                                                                    ],
                                                                                                                                  ),
                                                                                                                                ),
                                                                                                                                const SizedBox(height: 10),
                                                                                                                                Row(
                                                                                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                  children: [
                                                                                                                                    Column(
                                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                      children: [
                                                                                                                                        GestureDetector(
                                                                                                                                            onTap: () {
                                                                                                                                              controller.uploadFromGallery().whenComplete(() {
                                                                                                                                                Get.back();
                                                                                                                                              });
                                                                                                                                            },
                                                                                                                                            child: Image.asset(
                                                                                                                                              "images/gallery.png",
                                                                                                                                              width: 30,
                                                                                                                                            )),
                                                                                                                                        const Text("Gallery")
                                                                                                                                      ],
                                                                                                                                    ),
                                                                                                                                    Column(
                                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                      children: [
                                                                                                                                        GestureDetector(
                                                                                                                                            onTap: () {
                                                                                                                                              controller.uploadFromCamera().whenComplete(() {
                                                                                                                                                Get.back();
                                                                                                                                              });
                                                                                                                                            },
                                                                                                                                            child: Image.asset(
                                                                                                                                              "images/camera.png",
                                                                                                                                              width: 30,
                                                                                                                                            )),
                                                                                                                                        const Text("Camera")
                                                                                                                                      ],
                                                                                                                                    ),
                                                                                                                                  ],
                                                                                                                                ),
                                                                                                                                const SizedBox(
                                                                                                                                  height: 20,
                                                                                                                                )
                                                                                                                              ],
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                        );
                                                                                                                      });
                                                                                                                }
                                                                                                              },
                                                                                                              child: Container(
                                                                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                                decoration: BoxDecoration(color: const Color(0xff1739E4), borderRadius: BorderRadius.circular(5)),
                                                                                                                height: 25,
                                                                                                                child: Row(
                                                                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                  children: const [
                                                                                                                    Text(
                                                                                                                      "Upload",
                                                                                                                      style: TextStyle(color: Colors.white),
                                                                                                                    ),
                                                                                                                    SizedBox(
                                                                                                                      width: 10,
                                                                                                                    ),
                                                                                                                    Icon(
                                                                                                                      Icons.cloud_upload,
                                                                                                                      color: Colors.white,
                                                                                                                    )
                                                                                                                  ],
                                                                                                                ),
                                                                                                              ),
                                                                                                            )
                                                                                                          ],
                                                                                                        ),
                                                                                                      ),
                                                                                                      Padding(
                                                                                                        padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                        child: Row(
                                                                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                          children: [
                                                                                                            Text(
                                                                                                              "Amount".toUpperCase(),
                                                                                                              style: const TextStyle(fontSize: 13),
                                                                                                            ),
                                                                                                            const SizedBox()
                                                                                                          ],
                                                                                                        ),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 10,
                                                                                                      ),
                                                                                                      Padding(
                                                                                                        padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                        child: SizedBox(
                                                                                                          height: 40,
                                                                                                          child: TextFormField(
                                                                                                            controller: controller.amountController,
                                                                                                            keyboardType: TextInputType.number,
                                                                                                            decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1))),
                                                                                                          ),
                                                                                                        ),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 10,
                                                                                                      ),
                                                                                                      Padding(
                                                                                                        padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                        child: Row(
                                                                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                          children: [
                                                                                                            Text(
                                                                                                              "Remark".toUpperCase(),
                                                                                                              style: const TextStyle(fontSize: 13),
                                                                                                            ),
                                                                                                            const SizedBox(
                                                                                                              height: 10,
                                                                                                            )
                                                                                                          ],
                                                                                                        ),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 10,
                                                                                                      ),
                                                                                                      Container(
                                                                                                        padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                        child: TextFormField(
                                                                                                          controller: controller.remarkController,
                                                                                                          keyboardType: TextInputType.text,
                                                                                                          maxLines: 4,
                                                                                                          decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                                                        ),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 50,
                                                                                                      ),
                                                                                                      Container(
                                                                                                        margin: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                        width: MediaQuery.of(context).size.width,
                                                                                                        height: 50,
                                                                                                        child: ElevatedButton(
                                                                                                            style: ElevatedButton.styleFrom(primary: const Color(0xff0B28BE)),
                                                                                                            onPressed: () {
                                                                                                              print("-----------working");
                                                                                                              if (controller.amountController.text.isNotEmpty) {
                                                                                                                EasyLoading.show();
                                                                                                                controller.updateIncomeMasterCall(controller.transcationList[index]["detail_id"], controller.amountController.text, controller.remarkController.text, context).whenComplete(() {
                                                                                                                  controller.incomeMasterDetailsCall();
                                                                                                                  //Get.back();
                                                                                                                });
                                                                                                              } else {
                                                                                                                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Fields are empty !!")));
                                                                                                              }
                                                                                                            },
                                                                                                            child: Text("Submit".toUpperCase())),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 20,
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                              ],
                                                                                            )
                                                                                          ],
                                                                                        ),
                                                                                      ),
                                                                                    ));
                                                                              });
                                                                        },
                                                                        child:
                                                                            SvgPicture
                                                                                .asset(
                                                                          "images/pencil.svg",
                                                                          width: 20,
                                                                        ))),
                                                                Expanded(
                                                                    child: InkWell(
                                                                      onTap:()async{
                                                                        showDialog(context: context, builder: (context)=>Center(
                                                                          child: Container(
                                                                            width: 50,
                                                                              height:50,
                                                                              child: CircularProgressIndicator()),
                                                                        ));
                                                                        await Api.deleteDepreciationEntryApi("incomeMasterSalary",controller.transcationList[index]["detail_id"]).then((value){
                                                                          print("---delete Response----"+value.toString());
                                                                          Get.back();
                                                                          if(value["response_status"].toString() == "1"){
                                                                            Get.back();
                                                                          }
                                                                          controller.incomeMasterDetailsCall().then((value){
                                                                            setState((){});
                                                                          });
                                                                        });
                                                          },
                                                                        child:Icon(Icons.delete)
                                                                        )),

                                                              ],
                                                            ),
                                                            SizedBox(height: 10,)
                                                          ],
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            ],
                                          ),
                                        ) :
                                        dataNotFound(context)
                                      );
                                    });
                              });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.visibility,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                    //Salary End

                    //Start HP
                    const SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const [
                          Text("HP"),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                              color: const Color(0xffC1CDFF), width: 1)),
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      child: Row(
                        children: [
                          Flexible(
                            child: TextField(
                              style: const TextStyle(fontSize: 13),
                              controller: controller.hpController,
                              enabled: false,
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(15),
                                disabledBorder:OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Colors.transparent)) ,
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xffC1CDFF))),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xff0B28BE))),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        backgroundColor: Colors.transparent,
                                        content: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Scrollbar(
                                            thumbVisibility: true,
                                            child: ListView(
                                              shrinkWrap: true,
                                              children: [
                                                Column(
                                                  children: [
                                                    Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 430,
                                                      decoration: BoxDecoration(
                                                          color: Colors.white,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10)),
                                                      child: Column(
                                                        children: [
                                                          Container(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            alignment: Alignment
                                                                .center,
                                                            decoration:
                                                                const BoxDecoration(
                                                                    color: Color(
                                                                        0xff1739E4),
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .only(
                                                                      topLeft: Radius
                                                                          .circular(
                                                                              10),
                                                                      topRight:
                                                                          Radius.circular(
                                                                              10),
                                                                    )),
                                                            width:
                                                                MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                            height: 40,
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                const Text(""),
                                                                const Text(
                                                                  "HP",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          18,
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                                InkWell(
                                                                    onTap: () {
                                                                      Get.back();
                                                                    },
                                                                    child:
                                                                        const Icon(
                                                                      Icons
                                                                          .clear,
                                                                      color: Colors
                                                                          .white,
                                                                    )),
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                const Text(
                                                                  "FILE UPLOAD",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          13),
                                                                ),
                                                                InkWell(
                                                                  onTap:
                                                                      () async {
                                                                    if (Platform
                                                                        .isWindows) {
                                                                      FilePickerResult? filePickerResult = await FilePicker
                                                                          .platform
                                                                          .pickFiles(
                                                                              type: FileType.image)
                                                                          .whenComplete(() {
                                                                        Get.back();
                                                                      });
                                                                      if (filePickerResult !=
                                                                          null) {
                                                                        PlatformFile
                                                                            file =
                                                                            filePickerResult.files.single;
                                                                        controller.image = File(file
                                                                            .path
                                                                            .toString());
                                                                        print("File Path:" +
                                                                            controller.image.toString());
                                                                      }
                                                                    } else {
                                                                      showDialog(
                                                                          barrierDismissible:
                                                                              false,
                                                                          context:
                                                                              context,
                                                                          builder:
                                                                              (context) {
                                                                            return AlertDialog(
                                                                              contentPadding: EdgeInsets.zero,
                                                                              insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                              content: Container(
                                                                                height: 130,
                                                                                child: Column(
                                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                  children: [
                                                                                    Container(
                                                                                      padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                      width: MediaQuery.of(context).size.width,
                                                                                      height: 50,
                                                                                      color: Colors.indigo,
                                                                                      child: Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text(""),
                                                                                          const Text(
                                                                                            "Choose Source",
                                                                                            style: TextStyle(color: Colors.white),
                                                                                          ),
                                                                                          InkWell(
                                                                                              onTap: () {
                                                                                                Get.back();
                                                                                              },
                                                                                              child: const Icon(Icons.clear, color: Colors.white))
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                    const SizedBox(height: 10),
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                      children: [
                                                                                        Column(
                                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                          children: [
                                                                                            GestureDetector(
                                                                                                onTap: () {
                                                                                                  controller.uploadFromGallery().whenComplete(() {
                                                                                                    Get.back();
                                                                                                  });
                                                                                                },
                                                                                                child: Image.asset(
                                                                                                  "images/gallery.png",
                                                                                                  width: 30,
                                                                                                )),
                                                                                            const Text("Gallery")
                                                                                          ],
                                                                                        ),
                                                                                        Column(
                                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                          children: [
                                                                                            GestureDetector(
                                                                                                onTap: () {
                                                                                                  controller.uploadFromCamera().whenComplete(() {
                                                                                                    Get.back();
                                                                                                  });
                                                                                                },
                                                                                                child: Image.asset(
                                                                                                  "images/camera.png",
                                                                                                  width: 30,
                                                                                                )),
                                                                                            const Text("Camera")
                                                                                          ],
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                    const SizedBox(
                                                                                      height: 20,
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                            );
                                                                          });
                                                                    }
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    padding: const EdgeInsets
                                                                            .symmetric(
                                                                        horizontal:
                                                                            10),
                                                                    decoration: BoxDecoration(
                                                                        color: const Color(
                                                                            0xff1739E4),
                                                                        borderRadius:
                                                                            BorderRadius.circular(5)),
                                                                    height: 25,
                                                                    child: Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceEvenly,
                                                                      children: const [
                                                                        Text(
                                                                          "Upload",
                                                                          style:
                                                                              TextStyle(color: Colors.white),
                                                                        ),
                                                                        SizedBox(
                                                                          width:
                                                                              10,
                                                                        ),
                                                                        Icon(
                                                                          Icons
                                                                              .cloud_upload,
                                                                          color:
                                                                              Colors.white,
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Text(
                                                                  "Amount"
                                                                      .toUpperCase(),
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          13),
                                                                ),
                                                                const SizedBox()
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: SizedBox(
                                                              height: 40,
                                                              child:
                                                                  TextFormField(
                                                                keyboardType:
                                                                    TextInputType
                                                                        .number,
                                                                controller:
                                                                    controller
                                                                        .amountController2,
                                                                decoration: InputDecoration(
                                                                    border: OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        borderSide: const BorderSide(
                                                                            color:
                                                                                Color(0xffC1CDFF),
                                                                            width: 1))),
                                                              ),
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Text(
                                                                  "Remark"
                                                                      .toUpperCase(),
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          13),
                                                                ),
                                                                const SizedBox(
                                                                  height: 10,
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Container(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child:
                                                                TextFormField(
                                                              keyboardType:
                                                                  TextInputType
                                                                      .text,
                                                              controller: controller
                                                                  .remarkController2,
                                                              maxLines: 4,
                                                              decoration: InputDecoration(
                                                                  border: OutlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      borderSide:
                                                                          const BorderSide(
                                                                              color: Color(0xffC1CDFF)))),
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 50,
                                                          ),
                                                          Container(
                                                            margin:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            width:
                                                                MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                            height: 50,
                                                            child:
                                                                ElevatedButton(
                                                                    style: ElevatedButton.styleFrom(
                                                                        primary:
                                                                            const Color(
                                                                                0xff0B28BE)),
                                                                    onPressed:
                                                                        () async {
                                                                      if(controller.amountController2.text.isEmpty) {
                                                                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Amount is required")));
                                                                      } else  if(controller.remarkController2.text.isEmpty) {
                                                                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Reamrk is required")));
                                                                      } else {
                                                                        EasyLoading.show(
                                                                            status:
                                                                            "Loading");
                                                                        controller
                                                                            .incomeMasterCall(
                                                                            context,
                                                                            "HP",
                                                                            controller.amountController2.text,
                                                                            controller.remarkController2.text)
                                                                            .whenComplete(() {
                                                                          controller
                                                                              .incomeMasterDetailsCall();
                                                                          Get.back();
                                                                        });
                                                                      }
                                                                    },
                                                                    child: Text(
                                                                        "Submit"
                                                                            .toUpperCase())),
                                                          ),
                                                          const SizedBox(
                                                            height: 20,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                        ));
                                  });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          InkWell(
                            onTap: () {
                              controller
                                  .incomeMasterDetailsCall()
                                  .whenComplete(() {
                                // print("---------incomeMasterResponse---------------" + controller.incomeMasterResponse.value.toString());
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const Text("HP Details"),
                                            InkWell(
                                                onTap: () {
                                                  Get.back();
                                                },
                                                child: const Icon(
                                                  Icons.clear,
                                                  color: Colors.red,
                                                ))
                                          ],
                                        ),
                                        content: controller
                                            .transcationList2.isNotEmpty ? Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 300,
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 20),
                                                child: Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment
                                                      .center,
                                                  children: [
                                                    Expanded(child: Text("S. No",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Head",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Amount",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Reamrk",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),
                                                    Expanded(child:Text(""))
                                                  ],
                                                ),
                                              ),
                                              Expanded(
                                                child: ListView.builder(
                                                    itemCount: controller
                                                        .transcationList2
                                                        .length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return Padding(
                                                        padding: const EdgeInsets.symmetric(horizontal: 20),
                                                        child: Column(
                                                          children: [
                                                            SizedBox(height: 10,),
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Expanded(
                                                                    child: Text(
                                                                  "${index + 1}",
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller.transcationList2[
                                                                          index]
                                                                      ["income_head"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList2[
                                                                      index]["amount"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList2[
                                                                      index]["remark"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap: () {
                                                                          showDialog(
                                                                              barrierDismissible:
                                                                                  false,
                                                                              context:
                                                                                  context,
                                                                              builder:
                                                                                  (context) {
                                                                                return AlertDialog(
                                                                                  backgroundColor:
                                                                                      Colors.transparent,
                                                                                  contentPadding:
                                                                                      EdgeInsets.zero,
                                                                                  insetPadding:
                                                                                  EdgeInsets.symmetric(horizontal: 50,vertical: 150),
                                                                                  content:
                                                                                      Container(
                                                                                        alignment: Alignment.topRight,
                                                                                    width: MediaQuery.of(context).size.width,
                                                                                    height: 400,
                                                                                    decoration: BoxDecoration(
                                                                                        color: Colors.black,
                                                                                        image: DecorationImage(
                                                                                          fit: BoxFit.fill,
                                                                                          image: NetworkImage(
                                                                                            "https://appexperts.net/btconnect/" + controller.transcationList2[index]["document_path"].toString(),
                                                                                          ),
                                                                                        )),
                                                                                        child: IconButton(onPressed: (){
                                                                                          Get.back();
                                                                                        },
                                                                                          icon: Image.asset("images/close.png",width: 30,),
                                                                                        ),
                                                                                  ),
                                                                                );
                                                                              });
                                                                        },
                                                                        child:
                                                                            SvgPicture
                                                                                .asset(
                                                                          "images/visibility.svg",
                                                                          width: 24,
                                                                        ))),
                                                                Expanded(
                                                                    child: InkWell(
                                                                  onTap: () {
                                                                    print(
                                                                        'Edit Hp working');
                                                                    controller
                                                                        .amountController
                                                                        .text = controller
                                                                            .transcationList2[
                                                                        index]["amount"];
                                                                    controller
                                                                        .remarkController
                                                                        .text = controller
                                                                            .transcationList2[
                                                                        index]["remark"];
                                                                    Get.back();

                                                                    showDialog(
                                                                        barrierDismissible:
                                                                            false,
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (context) {
                                                                          return AlertDialog(
                                                                              insetPadding: const EdgeInsets.symmetric(
                                                                                  horizontal:
                                                                                      10),
                                                                              backgroundColor:
                                                                                  Colors
                                                                                      .transparent,
                                                                              content:
                                                                                  Container(
                                                                                width: MediaQuery.of(context)
                                                                                    .size
                                                                                    .width,
                                                                                child:
                                                                                    Scrollbar(
                                                                                  thumbVisibility:
                                                                                      true,
                                                                                  child:
                                                                                      ListView(
                                                                                    shrinkWrap: true,
                                                                                    children: [
                                                                                      Column(
                                                                                        children: [
                                                                                          Container(
                                                                                            width: MediaQuery.of(context).size.width,
                                                                                            height: 420,
                                                                                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                                                                            child: Column(
                                                                                              children: [
                                                                                                Container(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  alignment: Alignment.center,
                                                                                                  decoration: const BoxDecoration(
                                                                                                      color: Color(0xff1739E4),
                                                                                                      borderRadius: BorderRadius.only(
                                                                                                        topLeft: Radius.circular(10),
                                                                                                        topRight: Radius.circular(10),
                                                                                                      )),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 40,
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    children: [
                                                                                                      const Text(""),
                                                                                                      const Text(
                                                                                                        "HP",
                                                                                                        style: TextStyle(fontSize: 18, color: Colors.white),
                                                                                                      ),
                                                                                                      InkWell(
                                                                                                          onTap: () {
                                                                                                            Get.back();
                                                                                                          },
                                                                                                          child: const Icon(
                                                                                                            Icons.clear,
                                                                                                            color: Colors.white,
                                                                                                          )),
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      const Text(
                                                                                                        "FILE UPLOAD",
                                                                                                        style: TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      InkWell(
                                                                                                        onTap: () async {
                                                                                                          if (Platform.isWindows) {
                                                                                                            FilePickerResult? filePickerResult = await FilePicker.platform.pickFiles(type: FileType.image).whenComplete(() {
                                                                                                              Get.back();
                                                                                                            });
                                                                                                            if (filePickerResult != null) {
                                                                                                              PlatformFile file = filePickerResult.files.single;
                                                                                                              controller.image = File(file.path.toString());
                                                                                                              print("File Path:" + controller.image.toString());
                                                                                                            }
                                                                                                          } else {
                                                                                                            showDialog(
                                                                                                                barrierDismissible: false,
                                                                                                                context: context,
                                                                                                                builder: (context) {
                                                                                                                  return AlertDialog(
                                                                                                                    contentPadding: EdgeInsets.zero,
                                                                                                                    insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                                                                    content: Container(
                                                                                                                      height: 130,
                                                                                                                      child: Column(
                                                                                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                        children: [
                                                                                                                          Container(
                                                                                                                            padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                                            width: MediaQuery.of(context).size.width,
                                                                                                                            height: 50,
                                                                                                                            color: Colors.indigo,
                                                                                                                            child: Row(
                                                                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                              children: [
                                                                                                                                const Text(""),
                                                                                                                                const Text(
                                                                                                                                  "Choose Source",
                                                                                                                                  style: TextStyle(color: Colors.white),
                                                                                                                                ),
                                                                                                                                InkWell(
                                                                                                                                    onTap: () {
                                                                                                                                      Get.back();
                                                                                                                                    },
                                                                                                                                    child: const Icon(Icons.clear, color: Colors.white))
                                                                                                                              ],
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                          const SizedBox(height: 10),
                                                                                                                          Row(
                                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                            children: [
                                                                                                                              Column(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  GestureDetector(
                                                                                                                                      onTap: () {
                                                                                                                                        controller.uploadFromGallery().whenComplete(() {
                                                                                                                                          Get.back();
                                                                                                                                        });
                                                                                                                                      },
                                                                                                                                      child: Image.asset(
                                                                                                                                        "images/gallery.png",
                                                                                                                                        width: 30,
                                                                                                                                      )),
                                                                                                                                  const Text("Gallery")
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                              Column(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  GestureDetector(
                                                                                                                                      onTap: () {
                                                                                                                                        controller.uploadFromCamera().whenComplete(() {
                                                                                                                                          Get.back();
                                                                                                                                        });
                                                                                                                                      },
                                                                                                                                      child: Image.asset(
                                                                                                                                        "images/camera.png",
                                                                                                                                        width: 30,
                                                                                                                                      )),
                                                                                                                                  const Text("Camera")
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                            ],
                                                                                                                          ),
                                                                                                                          const SizedBox(
                                                                                                                            height: 20,
                                                                                                                          )
                                                                                                                        ],
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  );
                                                                                                                });
                                                                                                          }
                                                                                                        },
                                                                                                        child: Container(
                                                                                                          padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                          decoration: BoxDecoration(color: const Color(0xff1739E4), borderRadius: BorderRadius.circular(5)),
                                                                                                          height: 25,
                                                                                                          child: Row(
                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                            children: const [
                                                                                                              Text(
                                                                                                                "Upload",
                                                                                                                style: TextStyle(color: Colors.white),
                                                                                                              ),
                                                                                                              SizedBox(
                                                                                                                width: 10,
                                                                                                              ),
                                                                                                              Icon(
                                                                                                                Icons.cloud_upload,
                                                                                                                color: Colors.white,
                                                                                                              )
                                                                                                            ],
                                                                                                          ),
                                                                                                        ),
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        "Amount".toUpperCase(),
                                                                                                        style: const TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      const SizedBox()
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: SizedBox(
                                                                                                    height: 40,
                                                                                                    child: TextFormField(
                                                                                                      controller: controller.amountController,
                                                                                                      keyboardType: TextInputType.number,
                                                                                                      decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1))),
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        "Remark".toUpperCase(),
                                                                                                        style: const TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 10,
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Container(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: TextFormField(
                                                                                                    controller: controller.remarkController,
                                                                                                    keyboardType: TextInputType.text,
                                                                                                    maxLines: 4,
                                                                                                    decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 50,
                                                                                                ),
                                                                                                Container(
                                                                                                  margin: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 50,
                                                                                                  child: ElevatedButton(
                                                                                                      style: ElevatedButton.styleFrom(primary: const Color(0xff0B28BE)),
                                                                                                      onPressed: () {
                                                                                                        if (controller.amountController.text.isNotEmpty) {
                                                                                                          EasyLoading.show(status: "Loading");
                                                                                                          controller.updateIncomeMasterCall(controller.transcationList2[index]["detail_id"], controller.amountController.text, controller.remarkController.text, context).whenComplete(() {
                                                                                                            controller.incomeMasterDetailsCall();
                                                                                                            // Get.back();
                                                                                                          });
                                                                                                        } else {
                                                                                                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Fiels are empty")));
                                                                                                        }
                                                                                                      },
                                                                                                      child: Text("Submit".toUpperCase())),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 20,
                                                                                                )
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                        ],
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ));
                                                                        });
                                                                  },
                                                                  child: SvgPicture
                                                                      .asset(
                                                                    "images/pencil.svg",
                                                                    width: 24,
                                                                  ),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap:()async{
                                                                          showDialog(context: context, builder: (context)=>Center(
                                                                            child: Container(
                                                                                width: 50,
                                                                                height:50,
                                                                                child: CircularProgressIndicator()),
                                                                          ));
                                                                          await Api.deleteDepreciationEntryApi("incomeMasterHp",controller.transcationList2[index]["detail_id"]).then((value){
                                                                            print("---delete Response----"+value.toString());
                                                                            Get.back();
                                                                            if(value["response_status"].toString() == "1"){
                                                                              Get.back();
                                                                            }
                                                                            controller.incomeMasterDetailsCall().then((value){
                                                                              setState((){});
                                                                            });
                                                                          });
                                                                        },
                                                                        child:Icon(Icons.delete)
                                                                    )),
                                                              ],
                                                            ),
                                                            SizedBox(height: 10,),
                                                          ],
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            ],
                                          ),
                                        ) :  dataNotFound(context),
                                      );
                                    });
                              });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.visibility,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                    //End HP

                    //Start CAPITAL
                    const SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const [
                          Text("CAPITAL"),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                              color: const Color(0xffC1CDFF), width: 1)),
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      child: Row(
                        children: [
                          Flexible(
                            child: TextField(
                              style: const TextStyle(fontSize: 13),
                              controller: controller.capitalController,
                              enabled: false,
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(15),
                                disabledBorder:OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Colors.transparent)) ,
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xffC1CDFF))),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xff0B28BE))),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        backgroundColor: Colors.transparent,
                                        content: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Scrollbar(
                                            thumbVisibility: true,
                                            child: ListView(
                                              shrinkWrap: true,
                                              children: [
                                                Column(
                                                  children: [
                                                    Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 430,
                                                      decoration: BoxDecoration(
                                                          color: Colors.white,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10)),
                                                      child: Column(
                                                        children: [
                                                          Container(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            alignment: Alignment
                                                                .center,
                                                            decoration:
                                                                const BoxDecoration(
                                                                    color: Color(
                                                                        0xff1739E4),
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .only(
                                                                      topLeft: Radius
                                                                          .circular(
                                                                              10),
                                                                      topRight:
                                                                          Radius.circular(
                                                                              10),
                                                                    )),
                                                            width:
                                                                MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                            height: 40,
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                const Text(""),
                                                                const Text(
                                                                  "CAPITAL",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          18,
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                                InkWell(
                                                                    onTap: () {
                                                                      Get.back();
                                                                    },
                                                                    child:
                                                                        const Icon(
                                                                      Icons
                                                                          .clear,
                                                                      color: Colors
                                                                          .white,
                                                                    )),
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                const Text(
                                                                  "FILE UPLOAD",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          13),
                                                                ),
                                                                InkWell(
                                                                  onTap:
                                                                      () async {
                                                                    if (Platform
                                                                        .isWindows) {
                                                                      FilePickerResult?
                                                                          filePickerResult =
                                                                          await FilePicker
                                                                              .platform
                                                                              .pickFiles(type: FileType.image);
                                                                      if (filePickerResult !=
                                                                          null) {
                                                                        PlatformFile
                                                                            file =
                                                                            filePickerResult.files.single;
                                                                        controller.image = File(file
                                                                            .path
                                                                            .toString());
                                                                        print("File Path:" +
                                                                            controller.image.toString());
                                                                      }
                                                                    } else {
                                                                      showDialog(
                                                                          barrierDismissible:
                                                                              false,
                                                                          context:
                                                                              context,
                                                                          builder:
                                                                              (context) {
                                                                            return AlertDialog(
                                                                              contentPadding: EdgeInsets.zero,
                                                                              insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                              content: Container(
                                                                                height: 130,
                                                                                child: Column(
                                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                  children: [
                                                                                    Container(
                                                                                      padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                      width: MediaQuery.of(context).size.width,
                                                                                      height: 50,
                                                                                      color: Colors.indigo,
                                                                                      child: Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          const Text(""),
                                                                                          const Text(
                                                                                            "Choose Source",
                                                                                            style: TextStyle(color: Colors.white),
                                                                                          ),
                                                                                          InkWell(
                                                                                              onTap: () {
                                                                                                Get.back();
                                                                                              },
                                                                                              child: const Icon(Icons.clear, color: Colors.white))
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                    const SizedBox(height: 10),
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                      children: [
                                                                                        Column(
                                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                          children: [
                                                                                            GestureDetector(
                                                                                                onTap: () {
                                                                                                  controller.uploadFromGallery().whenComplete(() {
                                                                                                    Get.back();
                                                                                                  });
                                                                                                },
                                                                                                child: Image.asset(
                                                                                                  "images/gallery.png",
                                                                                                  width: 30,
                                                                                                )),
                                                                                            const Text("Gallery")
                                                                                          ],
                                                                                        ),
                                                                                        Column(
                                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                          children: [
                                                                                            GestureDetector(
                                                                                                onTap: () {
                                                                                                  controller.uploadFromCamera().whenComplete(() {
                                                                                                    Get.back();
                                                                                                  });
                                                                                                },
                                                                                                child: Image.asset(
                                                                                                  "images/camera.png",
                                                                                                  width: 30,
                                                                                                )),
                                                                                            const Text("Camera")
                                                                                          ],
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                    const SizedBox(
                                                                                      height: 20,
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                            );
                                                                          });
                                                                    }
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    padding: const EdgeInsets
                                                                            .symmetric(
                                                                        horizontal:
                                                                            10),
                                                                    decoration: BoxDecoration(
                                                                        color: const Color(
                                                                            0xff1739E4),
                                                                        borderRadius:
                                                                            BorderRadius.circular(5)),
                                                                    height: 25,
                                                                    child: Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceEvenly,
                                                                      children: const [
                                                                        Text(
                                                                          "Upload",
                                                                          style:
                                                                              TextStyle(color: Colors.white),
                                                                        ),
                                                                        SizedBox(
                                                                          width:
                                                                              10,
                                                                        ),
                                                                        Icon(
                                                                          Icons
                                                                              .cloud_upload,
                                                                          color:
                                                                              Colors.white,
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Text(
                                                                  "Amount"
                                                                      .toUpperCase(),
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          13),
                                                                ),
                                                                const SizedBox()
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: SizedBox(
                                                              height: 40,
                                                              child:
                                                                  TextFormField(
                                                                keyboardType:
                                                                    TextInputType
                                                                        .number,
                                                                controller:
                                                                    controller
                                                                        .amountController3,
                                                                decoration: InputDecoration(
                                                                    border: OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        borderSide: const BorderSide(
                                                                            color:
                                                                                Color(0xffC1CDFF),
                                                                            width: 1))),
                                                              ),
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Text(
                                                                  "Remark"
                                                                      .toUpperCase(),
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          13),
                                                                ),
                                                                const SizedBox(
                                                                  height: 10,
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          Container(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            child:
                                                                TextFormField(
                                                              keyboardType:
                                                                  TextInputType
                                                                      .text,
                                                              controller: controller
                                                                  .remarkController3,
                                                              maxLines: 4,
                                                              decoration: InputDecoration(
                                                                  border: OutlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      borderSide:
                                                                          const BorderSide(
                                                                              color: Color(0xffC1CDFF)))),
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            height: 50,
                                                          ),
                                                          Container(
                                                            margin:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                            width:
                                                                MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                            height: 50,
                                                            child:
                                                                ElevatedButton(
                                                                    style: ElevatedButton.styleFrom(
                                                                        primary:
                                                                            const Color(
                                                                                0xff0B28BE)),
                                                                    onPressed:
                                                                        () {
                                                                      if(controller.amountController3.text.isEmpty) {
                                                                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Amount is Required !!")));
                                                                      }
                                                                      else if(controller.remarkController3.text.isEmpty) {
                                                                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Amount is Required !!")));
                                                                      } else{
                                                                        EasyLoading.show(
                                                                            status:
                                                                            "Loading");
                                                                        controller
                                                                            .incomeMasterCall(
                                                                            context,
                                                                            "Capital",
                                                                            controller.amountController3.text,
                                                                            controller.remarkController3.text)
                                                                            .whenComplete(() {
                                                                          controller
                                                                              .incomeMasterDetailsCall();
                                                                          Get.back();
                                                                        });
                                                                      }

                                                                    },
                                                                    child: Text(
                                                                        "Submit"
                                                                            .toUpperCase())),
                                                          ),
                                                          const SizedBox(
                                                            height: 20,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                        ));
                                  });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          InkWell(
                            onTap: () {
                              controller
                                  .incomeMasterDetailsCall()
                                  .whenComplete(() {
                                // print("---------incomeMasterResponse---------------" + controller.incomeMasterResponse.value.toString());
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const Text("Capital Details"),
                                            InkWell(
                                                onTap: () {
                                                  Get.back();
                                                },
                                                child: const Icon(
                                                  Icons.clear,
                                                  color: Colors.red,
                                                ))
                                          ],
                                        ),
                                        content: controller
                                            .transcationList3.isNotEmpty?  Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 300,
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 20),
                                                child: Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment
                                                      .center,
                                                  children: [
                                                    Expanded(child: Text("S. No",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Head",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold))),
                                                    Expanded(child: Text("Amount",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold))),
                                                    Expanded(child: Text("Reamrk",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold))),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),
                                                  ],
                                                ),
                                              ),
                                              Expanded(
                                                child: ListView.builder(
                                                    itemCount: controller
                                                        .transcationList3
                                                        .length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return Padding(
                                                        padding: const EdgeInsets.symmetric(horizontal: 20),
                                                        child: Column(
                                                          children: [
                                                            SizedBox(height: 10,),
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Expanded(
                                                                    child: Text(
                                                                  "${index + 1}",
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller.transcationList3[
                                                                          index]
                                                                      ["income_head"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList3[
                                                                      index]["amount"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList3[
                                                                      index]["remark"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap: () {
                                                                          showDialog(
                                                                              barrierDismissible:
                                                                                  false,
                                                                              context:
                                                                                  context,
                                                                              builder:
                                                                                  (context) {
                                                                                return AlertDialog(
                                                                                  backgroundColor:
                                                                                      Colors.transparent,
                                                                                  contentPadding:
                                                                                      EdgeInsets.zero,
                                                                                  insetPadding:
                                                                                  EdgeInsets.symmetric(horizontal: 50,vertical: 150),
                                                                                  content:
                                                                                      Container(
                                                                                        alignment: Alignment.topRight,
                                                                                    width: MediaQuery.of(context).size.width,
                                                                                    height: 400,
                                                                                    decoration: BoxDecoration(
                                                                                        color: Colors.black,
                                                                                        image: DecorationImage(
                                                                                          fit: BoxFit.fill,
                                                                                          image: NetworkImage(
                                                                                            "https://appexperts.net/btconnect/" + controller.transcationList3[index]["document_path"].toString(),
                                                                                          ),
                                                                                        )),
                                                                                        child: IconButton(onPressed: (){
                                                                                          Get.back();
                                                                                        },
                                                                                          icon: Image.asset("images/close.png",width: 30,),
                                                                                        ),
                                                                                  ),
                                                                                );
                                                                              });
                                                                        },
                                                                        child:
                                                                            SvgPicture
                                                                                .asset(
                                                                          "images/visibility.svg",
                                                                          width: 24,
                                                                        ))),
                                                                Expanded(
                                                                    child: InkWell(
                                                                  onTap: () {
                                                                    print(
                                                                        'Edit Hp working');
                                                                    controller
                                                                        .amountController
                                                                        .text = controller
                                                                            .transcationList2[
                                                                        index]["amount"];
                                                                    controller
                                                                        .remarkController
                                                                        .text = controller
                                                                            .transcationList2[
                                                                        index]["remark"];
                                                                    Get.back();

                                                                    showDialog(
                                                                        barrierDismissible:
                                                                            false,
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (context) {
                                                                          return AlertDialog(
                                                                              insetPadding: const EdgeInsets.symmetric(
                                                                                  horizontal:
                                                                                      10),
                                                                              backgroundColor:
                                                                                  Colors
                                                                                      .transparent,
                                                                              content:
                                                                                  Container(
                                                                                width: MediaQuery.of(context)
                                                                                    .size
                                                                                    .width,
                                                                                child:
                                                                                    Scrollbar(
                                                                                  thumbVisibility:
                                                                                      true,
                                                                                  child:
                                                                                      ListView(
                                                                                    shrinkWrap: true,
                                                                                    children: [
                                                                                      Column(
                                                                                        children: [
                                                                                          Container(
                                                                                            width: MediaQuery.of(context).size.width,
                                                                                            height: 420,
                                                                                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                                                                            child: Column(
                                                                                              children: [
                                                                                                Container(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  alignment: Alignment.center,
                                                                                                  decoration: const BoxDecoration(
                                                                                                      color: Color(0xff1739E4),
                                                                                                      borderRadius: BorderRadius.only(
                                                                                                        topLeft: Radius.circular(10),
                                                                                                        topRight: Radius.circular(10),
                                                                                                      )),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 40,
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    children: [
                                                                                                      const Text(""),
                                                                                                      const Text(
                                                                                                        "SALARY",
                                                                                                        style: TextStyle(fontSize: 18, color: Colors.white),
                                                                                                      ),
                                                                                                      InkWell(
                                                                                                          onTap: () {
                                                                                                            Get.back();
                                                                                                          },
                                                                                                          child: const Icon(
                                                                                                            Icons.clear,
                                                                                                            color: Colors.white,
                                                                                                          )),
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      const Text(
                                                                                                        "FILE UPLOAD",
                                                                                                        style: TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      InkWell(
                                                                                                        onTap: () async {
                                                                                                          if (Platform.isWindows) {
                                                                                                            FilePickerResult? filePickerResult = await FilePicker.platform.pickFiles(type: FileType.image);
                                                                                                            if (filePickerResult != null) {
                                                                                                              PlatformFile file = filePickerResult.files.single;
                                                                                                              controller.image = File(file.path.toString());
                                                                                                              print("File Path:" + controller.image.toString());
                                                                                                            }
                                                                                                          } else {
                                                                                                            showDialog(
                                                                                                                barrierDismissible: false,
                                                                                                                context: context,
                                                                                                                builder: (context) {
                                                                                                                  return AlertDialog(
                                                                                                                    contentPadding: EdgeInsets.zero,
                                                                                                                    insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                                                                    content: Container(
                                                                                                                      height: 130,
                                                                                                                      child: Column(
                                                                                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                        children: [
                                                                                                                          Container(
                                                                                                                            padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                                            width: MediaQuery.of(context).size.width,
                                                                                                                            height: 50,
                                                                                                                            color: Colors.indigo,
                                                                                                                            child: Row(
                                                                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                              children: [
                                                                                                                                const Text(""),
                                                                                                                                const Text(
                                                                                                                                  "Choose Source",
                                                                                                                                  style: TextStyle(color: Colors.white),
                                                                                                                                ),
                                                                                                                                InkWell(
                                                                                                                                    onTap: () {
                                                                                                                                      Get.back();
                                                                                                                                    },
                                                                                                                                    child: const Icon(Icons.clear, color: Colors.white))
                                                                                                                              ],
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                          const SizedBox(height: 10),
                                                                                                                          Row(
                                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                            children: [
                                                                                                                              Column(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  GestureDetector(
                                                                                                                                      onTap: () {
                                                                                                                                        controller.uploadFromGallery().whenComplete(() {
                                                                                                                                          Get.back();
                                                                                                                                        });
                                                                                                                                      },
                                                                                                                                      child: Image.asset(
                                                                                                                                        "images/gallery.png",
                                                                                                                                        width: 30,
                                                                                                                                      )),
                                                                                                                                  const Text("Gallery")
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                              Column(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  GestureDetector(
                                                                                                                                      onTap: () {
                                                                                                                                        controller.uploadFromCamera().whenComplete(() {
                                                                                                                                          Get.back();
                                                                                                                                        });
                                                                                                                                      },
                                                                                                                                      child: Image.asset(
                                                                                                                                        "images/camera.png",
                                                                                                                                        width: 30,
                                                                                                                                      )),
                                                                                                                                  const Text("Camera")
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                            ],
                                                                                                                          ),
                                                                                                                          const SizedBox(
                                                                                                                            height: 20,
                                                                                                                          )
                                                                                                                        ],
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  );
                                                                                                                });
                                                                                                          }
                                                                                                        },
                                                                                                        child: Container(
                                                                                                          padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                          decoration: BoxDecoration(color: const Color(0xff1739E4), borderRadius: BorderRadius.circular(5)),
                                                                                                          height: 25,
                                                                                                          child: Row(
                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                            children: const [
                                                                                                              Text(
                                                                                                                "Upload",
                                                                                                                style: TextStyle(color: Colors.white),
                                                                                                              ),
                                                                                                              SizedBox(
                                                                                                                width: 10,
                                                                                                              ),
                                                                                                              Icon(
                                                                                                                Icons.cloud_upload,
                                                                                                                color: Colors.white,
                                                                                                              )
                                                                                                            ],
                                                                                                          ),
                                                                                                        ),
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        "Amount".toUpperCase(),
                                                                                                        style: const TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      const SizedBox()
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: SizedBox(
                                                                                                    height: 40,
                                                                                                    child: TextFormField(
                                                                                                      controller: controller.amountController,
                                                                                                      keyboardType: TextInputType.number,
                                                                                                      decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1))),
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        "Remark".toUpperCase(),
                                                                                                        style: const TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 10,
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Container(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: TextFormField(
                                                                                                    controller: controller.remarkController,
                                                                                                    keyboardType: TextInputType.text,
                                                                                                    maxLines: 4,
                                                                                                    decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 50,
                                                                                                ),
                                                                                                Container(
                                                                                                  margin: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 50,
                                                                                                  child: ElevatedButton(
                                                                                                      style: ElevatedButton.styleFrom(primary: const Color(0xff0B28BE)),
                                                                                                      onPressed: () {
                                                                                                        if (controller.amountController.text.isNotEmpty) {
                                                                                                          EasyLoading.show(status: "Loading");

                                                                                                          controller.updateIncomeMasterCall(controller.transcationList2[index]["detail_id"], controller.amountController.text, controller.remarkController.text, context).whenComplete(() {
                                                                                                            controller.incomeMasterDetailsCall();
                                                                                                            //Get.back();
                                                                                                          });
                                                                                                        } else {
                                                                                                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Fields are empty !!")));
                                                                                                        }
                                                                                                      },
                                                                                                      child: Text("Submit".toUpperCase())),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 20,
                                                                                                )
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                        ],
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ));
                                                                        });
                                                                  },
                                                                  child: InkWell(
                                                                    onTap: () {
                                                                      print(
                                                                          'Edit capital working');
                                                                      controller
                                                                          .amountController
                                                                          .text = controller
                                                                                  .transcationList3[
                                                                              index]
                                                                          ["amount"];
                                                                      controller
                                                                          .remarkController
                                                                          .text = controller
                                                                                  .transcationList3[
                                                                              index]
                                                                          ["remark"];
                                                                      Get.back();

                                                                      showDialog(
                                                                          barrierDismissible:
                                                                              false,
                                                                          context:
                                                                              context,
                                                                          builder:
                                                                              (context) {
                                                                            return AlertDialog(
                                                                                insetPadding: const EdgeInsets.symmetric(
                                                                                    horizontal:
                                                                                        10),
                                                                                backgroundColor: Colors
                                                                                    .transparent,
                                                                                content:
                                                                                    Container(
                                                                                  width:
                                                                                      MediaQuery.of(context).size.width,
                                                                                  child:
                                                                                      Scrollbar(
                                                                                    thumbVisibility: true,
                                                                                    child: ListView(
                                                                                      shrinkWrap: true,
                                                                                      children: [
                                                                                        Column(
                                                                                          children: [
                                                                                            Container(
                                                                                              width: MediaQuery.of(context).size.width,
                                                                                              height: 420,
                                                                                              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                                                                              child: Column(
                                                                                                children: [
                                                                                                  Container(
                                                                                                    padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                    alignment: Alignment.center,
                                                                                                    decoration: const BoxDecoration(
                                                                                                        color: Color(0xff1739E4),
                                                                                                        borderRadius: BorderRadius.only(
                                                                                                          topLeft: Radius.circular(10),
                                                                                                          topRight: Radius.circular(10),
                                                                                                        )),
                                                                                                    width: MediaQuery.of(context).size.width,
                                                                                                    height: 40,
                                                                                                    child: Row(
                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                      children: [
                                                                                                        const Text(""),
                                                                                                        const Text(
                                                                                                          "Capital",
                                                                                                          style: TextStyle(fontSize: 18, color: Colors.white),
                                                                                                        ),
                                                                                                        InkWell(
                                                                                                            onTap: () {
                                                                                                              Get.back();
                                                                                                            },
                                                                                                            child: const Icon(
                                                                                                              Icons.clear,
                                                                                                              color: Colors.white,
                                                                                                            )),
                                                                                                      ],
                                                                                                    ),
                                                                                                  ),
                                                                                                  const SizedBox(
                                                                                                    height: 10,
                                                                                                  ),
                                                                                                  Padding(
                                                                                                    padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                    child: Row(
                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                      children: [
                                                                                                        const Text(
                                                                                                          "FILE UPLOAD",
                                                                                                          style: TextStyle(fontSize: 13),
                                                                                                        ),
                                                                                                        InkWell(
                                                                                                          onTap: () async {
                                                                                                            if (Platform.isWindows) {
                                                                                                              FilePickerResult? filePickerResult = await FilePicker.platform.pickFiles(type: FileType.image);
                                                                                                              if (filePickerResult != null) {
                                                                                                                PlatformFile file = filePickerResult.files.single;
                                                                                                                controller.image = File(file.path.toString());
                                                                                                                print("File Path:" + controller.image.toString());
                                                                                                              }
                                                                                                            } else {
                                                                                                              showDialog(
                                                                                                                  barrierDismissible: false,
                                                                                                                  context: context,
                                                                                                                  builder: (context) {
                                                                                                                    return AlertDialog(
                                                                                                                      contentPadding: EdgeInsets.zero,
                                                                                                                      insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                                                                      content: Container(
                                                                                                                        height: 130,
                                                                                                                        child: Column(
                                                                                                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                          children: [
                                                                                                                            Container(
                                                                                                                              padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                                              width: MediaQuery.of(context).size.width,
                                                                                                                              height: 50,
                                                                                                                              color: Colors.indigo,
                                                                                                                              child: Row(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  const Text(""),
                                                                                                                                  const Text(
                                                                                                                                    "Choose Source",
                                                                                                                                    style: TextStyle(color: Colors.white),
                                                                                                                                  ),
                                                                                                                                  InkWell(
                                                                                                                                      onTap: () {
                                                                                                                                        Get.back();
                                                                                                                                      },
                                                                                                                                      child: const Icon(Icons.clear, color: Colors.white))
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                            ),
                                                                                                                            const SizedBox(height: 10),
                                                                                                                            Row(
                                                                                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                              children: [
                                                                                                                                Column(
                                                                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                  children: [
                                                                                                                                    GestureDetector(
                                                                                                                                        onTap: () {
                                                                                                                                          controller.uploadFromGallery().whenComplete(() {
                                                                                                                                            Get.back();
                                                                                                                                          });
                                                                                                                                        },
                                                                                                                                        child: Image.asset(
                                                                                                                                          "images/gallery.png",
                                                                                                                                          width: 30,
                                                                                                                                        )),
                                                                                                                                    const Text("Gallery")
                                                                                                                                  ],
                                                                                                                                ),
                                                                                                                                Column(
                                                                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                  children: [
                                                                                                                                    GestureDetector(
                                                                                                                                        onTap: () {
                                                                                                                                          controller.uploadFromCamera().whenComplete(() {
                                                                                                                                            Get.back();
                                                                                                                                          });
                                                                                                                                        },
                                                                                                                                        child: Image.asset(
                                                                                                                                          "images/camera.png",
                                                                                                                                          width: 30,
                                                                                                                                        )),
                                                                                                                                    const Text("Camera")
                                                                                                                                  ],
                                                                                                                                ),
                                                                                                                              ],
                                                                                                                            ),
                                                                                                                            const SizedBox(
                                                                                                                              height: 20,
                                                                                                                            )
                                                                                                                          ],
                                                                                                                        ),
                                                                                                                      ),
                                                                                                                    );
                                                                                                                  });
                                                                                                            }
                                                                                                          },
                                                                                                          child: Container(
                                                                                                            padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                            decoration: BoxDecoration(color: const Color(0xff1739E4), borderRadius: BorderRadius.circular(5)),
                                                                                                            height: 25,
                                                                                                            child: Row(
                                                                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                              children: const [
                                                                                                                Text(
                                                                                                                  "Upload",
                                                                                                                  style: TextStyle(color: Colors.white),
                                                                                                                ),
                                                                                                                SizedBox(
                                                                                                                  width: 10,
                                                                                                                ),
                                                                                                                Icon(
                                                                                                                  Icons.cloud_upload,
                                                                                                                  color: Colors.white,
                                                                                                                )
                                                                                                              ],
                                                                                                            ),
                                                                                                          ),
                                                                                                        )
                                                                                                      ],
                                                                                                    ),
                                                                                                  ),
                                                                                                  Padding(
                                                                                                    padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                    child: Row(
                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                      children: [
                                                                                                        Text(
                                                                                                          "Amount".toUpperCase(),
                                                                                                          style: const TextStyle(fontSize: 13),
                                                                                                        ),
                                                                                                        const SizedBox()
                                                                                                      ],
                                                                                                    ),
                                                                                                  ),
                                                                                                  const SizedBox(
                                                                                                    height: 10,
                                                                                                  ),
                                                                                                  Padding(
                                                                                                    padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                    child: SizedBox(
                                                                                                      height: 40,
                                                                                                      child: TextFormField(
                                                                                                        controller: controller.amountController,
                                                                                                        keyboardType: TextInputType.number,
                                                                                                        decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1))),
                                                                                                      ),
                                                                                                    ),
                                                                                                  ),
                                                                                                  const SizedBox(
                                                                                                    height: 10,
                                                                                                  ),
                                                                                                  Padding(
                                                                                                    padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                    child: Row(
                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                      children: [
                                                                                                        Text(
                                                                                                          "Remark".toUpperCase(),
                                                                                                          style: const TextStyle(fontSize: 13),
                                                                                                        ),
                                                                                                        const SizedBox(
                                                                                                          height: 10,
                                                                                                        )
                                                                                                      ],
                                                                                                    ),
                                                                                                  ),
                                                                                                  const SizedBox(
                                                                                                    height: 10,
                                                                                                  ),
                                                                                                  Container(
                                                                                                    padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                    child: TextFormField(
                                                                                                      controller: controller.remarkController,
                                                                                                      keyboardType: TextInputType.text,
                                                                                                      maxLines: 4,
                                                                                                      decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                                                    ),
                                                                                                  ),
                                                                                                  const SizedBox(
                                                                                                    height: 50,
                                                                                                  ),
                                                                                                  Container(
                                                                                                    margin: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                    width: MediaQuery.of(context).size.width,
                                                                                                    height: 50,
                                                                                                    child: ElevatedButton(
                                                                                                        style: ElevatedButton.styleFrom(primary: const Color(0xff0B28BE)),
                                                                                                        onPressed: () {
                                                                                                          if (controller.amountController.text.isNotEmpty) {
                                                                                                            EasyLoading.show(status: "Loading");

                                                                                                            controller.updateIncomeMasterCall(controller.transcationList3[index]["detail_id"], controller.amountController.text, controller.remarkController.text, context).whenComplete(() {
                                                                                                              controller.incomeMasterDetailsCall();
                                                                                                              //Get.back();
                                                                                                            });
                                                                                                          } else {
                                                                                                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Fields are empty !!")));
                                                                                                          }
                                                                                                        },
                                                                                                        child: Text("Submit".toUpperCase())),
                                                                                                  ),
                                                                                                  const SizedBox(
                                                                                                    height: 20,
                                                                                                  )
                                                                                                ],
                                                                                              ),
                                                                                            ),
                                                                                          ],
                                                                                        )
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                ));
                                                                          });
                                                                    },
                                                                    child: SvgPicture
                                                                        .asset(
                                                                      "images/pencil.svg",
                                                                      width: 24,
                                                                    ),
                                                                  ),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap:()async{
                                                                          showDialog(context: context, builder: (context)=>Center(
                                                                            child: Container(
                                                                                width: 50,
                                                                                height:50,
                                                                                child: CircularProgressIndicator()),
                                                                          ));
                                                                          await Api.deleteDepreciationEntryApi("incomeMasterCapital",controller.transcationList3[index]["detail_id"]).then((value){
                                                                            print("---delete Response----"+value.toString());
                                                                            Get.back();
                                                                            if(value["response_status"].toString() == "1"){
                                                                              Get.back();
                                                                            }
                                                                            controller.incomeMasterDetailsCall().then((value){
                                                                              setState((){});
                                                                            });
                                                                          });
                                                                        },
                                                                        child:Icon(Icons.delete)
                                                                    )),
                                                              ],
                                                            ),
                                                            SizedBox(height: 10,),
                                                          ],
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            ],
                                          ),
                                        ) : dataNotFound(context),
                                      );
                                    });
                              });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.visibility,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                    //End Capital

                    //Start B&P
                    const SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const [
                          Text("B&P"),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                              color: const Color(0xffC1CDFF), width: 1)),
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      child: Row(
                        children: [
                          Flexible(
                            child: TextField(
                              style: const TextStyle(fontSize: 13),
                              controller: controller.bandpController,
                              enabled: false,
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(15),
                                disabledBorder:OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Colors.transparent)) ,
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xffC1CDFF))),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xff0B28BE))),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                      insetPadding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      backgroundColor: Colors.transparent,
                                      content: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Scrollbar(
                                          thumbVisibility: true,
                                          child: ListView(
                                            shrinkWrap: true,
                                            children: [
                                              Column(
                                                children: [
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    height: 430,
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10)),
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          alignment:
                                                              Alignment.center,
                                                          decoration:
                                                              const BoxDecoration(
                                                                  color: Color(
                                                                      0xff1739E4),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            10),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            10),
                                                                  )),
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          height: 40,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              const Text(""),
                                                              const Text(
                                                                "B&P",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        18,
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                              InkWell(
                                                                  onTap: () {
                                                                    Get.back();
                                                                  },
                                                                  child:
                                                                      const Icon(
                                                                    Icons.clear,
                                                                    color: Colors
                                                                        .white,
                                                                  )),
                                                            ],
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              const Text(
                                                                "FILE UPLOAD",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        13),
                                                              ),
                                                              InkWell(
                                                                onTap: () async {
                                                                  if(Platform.isWindows) {
                                                                    FilePickerResult? filePickerResult = await FilePicker
                                                                        .platform.pickFiles(type: FileType.image);
                                                                    if(filePickerResult != null) {
                                                                      PlatformFile file = filePickerResult.files.single;
                                                                      controller.image = File(file.path.toString());
                                                                      print("File Path:"+controller.image.toString());
                                                                    }
                                                                  } else{
                                                                    showDialog(
                                                                        barrierDismissible:
                                                                        false,
                                                                        context:
                                                                        context,
                                                                        builder:
                                                                            (context) {
                                                                          return AlertDialog(
                                                                            contentPadding:
                                                                            EdgeInsets.zero,
                                                                            insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                            content:
                                                                            Container(
                                                                              height:
                                                                              130,
                                                                              child:
                                                                              Column(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                children: [
                                                                                  Container(
                                                                                    padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                    width: MediaQuery.of(context).size.width,
                                                                                    height: 50,
                                                                                    color: Colors.indigo,
                                                                                    child: Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                      children: [
                                                                                        const Text(""),
                                                                                        const Text(
                                                                                          "Choose Source",
                                                                                          style: TextStyle(color: Colors.white),
                                                                                        ),
                                                                                        InkWell(
                                                                                            onTap: () {
                                                                                              Get.back();
                                                                                            },
                                                                                            child: const Icon(Icons.clear, color: Colors.white))
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                  const SizedBox(height: 10),
                                                                                  Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                    children: [
                                                                                      Column(
                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          GestureDetector(
                                                                                              onTap: () {
                                                                                                controller.uploadFromGallery().whenComplete(() {
                                                                                                  Get.back();
                                                                                                });
                                                                                              },
                                                                                              child: Image.asset(
                                                                                                "images/gallery.png",
                                                                                                width: 30,
                                                                                              )),
                                                                                          const Text("Gallery")
                                                                                        ],
                                                                                      ),
                                                                                      Column(
                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          GestureDetector(
                                                                                              onTap: () {
                                                                                                controller.uploadFromCamera().whenComplete(() {
                                                                                                  Get.back();
                                                                                                });
                                                                                              },
                                                                                              child: Image.asset(
                                                                                                "images/camera.png",
                                                                                                width: 30,
                                                                                              )),
                                                                                          const Text("Camera")
                                                                                        ],
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                  const SizedBox(
                                                                                    height: 20,
                                                                                  )
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          );
                                                                        });
                                                                  }

                                                                },
                                                                child:
                                                                    Container(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      horizontal:
                                                                          10),
                                                                  decoration: BoxDecoration(
                                                                      color: const Color(
                                                                          0xff1739E4),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5)),
                                                                  height: 25,
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceEvenly,
                                                                    children: const [
                                                                      Text(
                                                                        "Upload",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.white),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            10,
                                                                      ),
                                                                      Icon(
                                                                        Icons
                                                                            .cloud_upload,
                                                                        color: Colors
                                                                            .white,
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                "Amount"
                                                                    .toUpperCase(),
                                                                style:
                                                                    const TextStyle(
                                                                        fontSize:
                                                                            13),
                                                              ),
                                                              const SizedBox()
                                                            ],
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: SizedBox(
                                                            height: 40,
                                                            child:
                                                                TextFormField(
                                                              keyboardType:
                                                                  TextInputType
                                                                      .number,
                                                              controller: controller
                                                                  .amountController4,
                                                              decoration: InputDecoration(
                                                                  border: OutlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      borderSide: const BorderSide(
                                                                          color: Color(
                                                                              0xffC1CDFF),
                                                                          width:
                                                                              1))),
                                                            ),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                "Remark"
                                                                    .toUpperCase(),
                                                                style:
                                                                    const TextStyle(
                                                                        fontSize:
                                                                            13),
                                                              ),
                                                              const SizedBox(
                                                                height: 10,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Container(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: TextFormField(
                                                            keyboardType:
                                                                TextInputType
                                                                    .text,
                                                            controller: controller
                                                                .remarkController4,
                                                            maxLines: 4,
                                                            decoration: InputDecoration(
                                                                border: OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                5),
                                                                    borderSide:
                                                                        const BorderSide(
                                                                            color:
                                                                                Color(0xffC1CDFF)))),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 50,
                                                        ),
                                                        Container(
                                                          margin:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          height: 50,
                                                          child: ElevatedButton(
                                                              style: ElevatedButton
                                                                  .styleFrom(
                                                                      primary:
                                                                          const Color(
                                                                              0xff0B28BE)),
                                                              onPressed: () {
                                                                if(controller.amountController4.text.isEmpty) {
                                                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Amount is required !!")));
                                                                } else  if(controller.remarkController4.text.isEmpty){
                                                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Remark is required !!")));
                                                                } else {
                                                                  EasyLoading
                                                                      .show();
                                                                  controller
                                                                      .incomeMasterCall(
                                                                      context,
                                                                      "B and P",
                                                                      controller
                                                                          .amountController4
                                                                          .text,
                                                                      controller
                                                                          .remarkController4
                                                                          .text)
                                                                      .whenComplete(
                                                                          () {
                                                                        controller
                                                                            .incomeMasterDetailsCall();
                                                                        Get.back();
                                                                      });
                                                                }

                                                              },
                                                              child: Text("Submit"
                                                                  .toUpperCase())),
                                                        ),
                                                        const SizedBox(
                                                          height: 20,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          InkWell(
                            onTap: () {
                              controller
                                  .incomeMasterDetailsCall()
                                  .whenComplete(() {
                                // print("---------incomeMasterResponse---------------" + controller.incomeMasterResponse.value.toString());
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const Text("B&P Details"),
                                            InkWell(
                                                onTap: () {
                                                  Get.back();
                                                },
                                                child: const Icon(
                                                  Icons.clear,
                                                  color: Colors.red,
                                                ))
                                          ],
                                        ),
                                        content: controller
                                            .transcationList4.isNotEmpty ? Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 300,
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 20),
                                                child: Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment
                                                      .center,
                                                  children: [
                                                    Expanded(child: Text("S. No",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Head",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Amount",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Reamrk",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),
                                                  ],
                                                ),
                                              ),
                                              Expanded(
                                                child: ListView.builder(
                                                    itemCount: controller
                                                        .transcationList4
                                                        .length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return Padding(
                                                        padding: const EdgeInsets.symmetric(horizontal: 20),
                                                        child: Column(
                                                          children: [
                                                            SizedBox(height: 10,),
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Expanded(
                                                                    child: Text(
                                                                  "${index + 1}",
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller.transcationList4[
                                                                          index]
                                                                      ["income_head"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList4[
                                                                      index]["amount"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList4[
                                                                      index]["remark"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap: () {

                                                                          showDialog(
                                                                              barrierDismissible:
                                                                                  false,
                                                                              context:
                                                                                  context,
                                                                              builder:
                                                                                  (context) {
                                                                                return AlertDialog(
                                                                                  backgroundColor:
                                                                                      Colors.transparent,
                                                                                  contentPadding:
                                                                                      EdgeInsets.zero,
                                                                                  insetPadding:
                                                                                  EdgeInsets.symmetric(horizontal: 50,vertical: 150),
                                                                                  content:
                                                                                      Container(
                                                                                        alignment: Alignment.topRight,
                                                                                    width: MediaQuery.of(context).size.width,
                                                                                    height: 400,
                                                                                    decoration: BoxDecoration(
                                                                                        color: Colors.black,
                                                                                        image: DecorationImage(
                                                                                          fit: BoxFit.fill,
                                                                                          image: NetworkImage(
                                                                                            "https://appexperts.net/btconnect/" + controller.transcationList4[index]["document_path"].toString(),
                                                                                          ),
                                                                                        )),
                                                                                        child: IconButton(onPressed: (){
                                                                                          Get.back();
                                                                                        },
                                                                                          icon: Image.asset("images/close.png",width: 30,),
                                                                                        ),
                                                                                  ),
                                                                                );
                                                                              });
                                                                        },
                                                                        child:
                                                                            SvgPicture
                                                                                .asset(
                                                                          "images/visibility.svg",
                                                                          width: 24,
                                                                        ))),
                                                                Expanded(
                                                                    child: InkWell(
                                                                  onTap: () {
                                                                    print(
                                                                        'Edit B&P working');
                                                                    controller
                                                                        .amountController
                                                                        .text = controller
                                                                            .transcationList4[
                                                                        index]["amount"];
                                                                    controller
                                                                        .remarkController
                                                                        .text = controller
                                                                            .transcationList4[
                                                                        index]["remark"];
                                                                    Get.back();

                                                                    showDialog(
                                                                        barrierDismissible:
                                                                            false,
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (context) {
                                                                          return AlertDialog(
                                                                              insetPadding: const EdgeInsets.symmetric(
                                                                                  horizontal:
                                                                                      10),
                                                                              backgroundColor:
                                                                                  Colors
                                                                                      .transparent,
                                                                              content:
                                                                                  Container(
                                                                                width: MediaQuery.of(context)
                                                                                    .size
                                                                                    .width,
                                                                                child:
                                                                                    Scrollbar(
                                                                                  thumbVisibility:
                                                                                      true,
                                                                                  child:
                                                                                      ListView(
                                                                                    shrinkWrap: true,
                                                                                    children: [
                                                                                      Column(
                                                                                        children: [
                                                                                          Container(
                                                                                            width: MediaQuery.of(context).size.width,
                                                                                            height: 420,
                                                                                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                                                                            child: Column(
                                                                                              children: [
                                                                                                Container(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  alignment: Alignment.center,
                                                                                                  decoration: const BoxDecoration(
                                                                                                      color: Color(0xff1739E4),
                                                                                                      borderRadius: BorderRadius.only(
                                                                                                        topLeft: Radius.circular(10),
                                                                                                        topRight: Radius.circular(10),
                                                                                                      )),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 40,
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    children: [
                                                                                                      const Text(""),
                                                                                                      const Text(
                                                                                                        "B And P",
                                                                                                        style: TextStyle(fontSize: 18, color: Colors.white),
                                                                                                      ),
                                                                                                      InkWell(
                                                                                                          onTap: () {
                                                                                                            Get.back();
                                                                                                          },
                                                                                                          child: const Icon(
                                                                                                            Icons.clear,
                                                                                                            color: Colors.white,
                                                                                                          )),
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      const Text(
                                                                                                        "FILE UPLOAD",
                                                                                                        style: TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      InkWell(
                                                                                                        onTap: () async {
                                                                                                          if(Platform.isWindows) {
                                                                                                            FilePickerResult? filePickerResult = await FilePicker
                                                                                                                .platform.pickFiles(type: FileType.image);
                                                                                                            if(filePickerResult != null) {
                                                                                                              PlatformFile file = filePickerResult.files.single;
                                                                                                              controller.image = File(file.path.toString());
                                                                                                              print("File Path:"+controller.image.toString());
                                                                                                            }
                                                                                                          }else {
                                                                                                            showDialog(
                                                                                                                barrierDismissible: false,
                                                                                                                context: context,
                                                                                                                builder: (context) {
                                                                                                                  return AlertDialog(
                                                                                                                    contentPadding: EdgeInsets.zero,
                                                                                                                    content: Container(
                                                                                                                      height: 130,
                                                                                                                      child: Column(
                                                                                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                        children: [
                                                                                                                          Container(
                                                                                                                            padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                                            width: MediaQuery.of(context).size.width,
                                                                                                                            height: 50,
                                                                                                                            color: Colors.indigo,
                                                                                                                            child: Row(
                                                                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                              children: [
                                                                                                                                const Text(""),
                                                                                                                                const Text(
                                                                                                                                  "Choose Source",
                                                                                                                                  style: TextStyle(color: Colors.white),
                                                                                                                                ),
                                                                                                                                InkWell(
                                                                                                                                    onTap: () {
                                                                                                                                      Get.back();
                                                                                                                                    },
                                                                                                                                    child: const Icon(Icons.clear, color: Colors.white))
                                                                                                                              ],
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                          const SizedBox(height: 10),
                                                                                                                          Row(
                                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                            children: [
                                                                                                                              Column(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  GestureDetector(
                                                                                                                                      onTap: () {
                                                                                                                                        controller.uploadFromGallery().whenComplete(() {
                                                                                                                                          Get.back();
                                                                                                                                        });
                                                                                                                                      },
                                                                                                                                      child: Image.asset(
                                                                                                                                        "images/gallery.png",
                                                                                                                                        width: 30,
                                                                                                                                      )),
                                                                                                                                  const Text("Gallery")
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                              Column(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  GestureDetector(
                                                                                                                                      onTap: () {
                                                                                                                                        controller.uploadFromCamera().whenComplete(() {
                                                                                                                                          Get.back();
                                                                                                                                        });
                                                                                                                                      },
                                                                                                                                      child: Image.asset(
                                                                                                                                        "images/camera.png",
                                                                                                                                        width: 30,
                                                                                                                                      )),
                                                                                                                                  const Text("Camera")
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                            ],
                                                                                                                          ),
                                                                                                                          const SizedBox(
                                                                                                                            height: 20,
                                                                                                                          )
                                                                                                                        ],
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  );
                                                                                                                });
                                                                                                          };
                                                                                                        },
                                                                                                        child: Container(
                                                                                                          padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                          decoration: BoxDecoration(color: const Color(0xff1739E4), borderRadius: BorderRadius.circular(5)),
                                                                                                          height: 25,
                                                                                                          child: Row(
                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                            children: const [
                                                                                                              Text(
                                                                                                                "Upload",
                                                                                                                style: TextStyle(color: Colors.white),
                                                                                                              ),
                                                                                                              SizedBox(
                                                                                                                width: 10,
                                                                                                              ),
                                                                                                              Icon(
                                                                                                                Icons.cloud_upload,
                                                                                                                color: Colors.white,
                                                                                                              )
                                                                                                            ],
                                                                                                          ),
                                                                                                        ),
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        "Amount".toUpperCase(),
                                                                                                        style: const TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      const SizedBox()
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: SizedBox(
                                                                                                    height: 40,
                                                                                                    child: TextFormField(
                                                                                                      controller: controller.amountController,
                                                                                                      keyboardType: TextInputType.number,
                                                                                                      decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1))),
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        "Remark".toUpperCase(),
                                                                                                        style: const TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 10,
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Container(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: TextFormField(
                                                                                                    controller: controller.remarkController,
                                                                                                    keyboardType: TextInputType.text,
                                                                                                    maxLines: 4,
                                                                                                    decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 50,
                                                                                                ),
                                                                                                Container(
                                                                                                  margin: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 50,
                                                                                                  child: ElevatedButton(
                                                                                                      style: ElevatedButton.styleFrom(primary: const Color(0xff0B28BE)),
                                                                                                      onPressed: () {
                                                                                                        if (controller.amountController.text.isNotEmpty) {
                                                                                                          EasyLoading.show(status: "Loading");
                                                                                                          controller.updateIncomeMasterCall(controller.transcationList4[index]["detail_id"], controller.amountController.text, controller.remarkController.text, context).whenComplete(() {
                                                                                                            controller.incomeMasterDetailsCall();
                                                                                                            //Get.back();
                                                                                                          });
                                                                                                        } else {
                                                                                                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Fields are empty !!")));
                                                                                                        }
                                                                                                      },
                                                                                                      child: Text("Submit".toUpperCase())),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 20,
                                                                                                )
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                        ],
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ));
                                                                        });
                                                                  },
                                                                  child: SvgPicture
                                                                      .asset(
                                                                    "images/pencil.svg",
                                                                    width: 24,
                                                                  ),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap:()async{
                                                                          showDialog(context: context, builder: (context)=>Center(
                                                                            child: Container(
                                                                                width: 50,
                                                                                height:50,
                                                                                child: CircularProgressIndicator()),
                                                                          ));
                                                                          await Api.deleteDepreciationEntryApi("incomeMasterBp",controller.transcationList4[index]["detail_id"]).then((value){
                                                                            print("---delete Response----"+value.toString());
                                                                            Get.back();
                                                                            if(value["response_status"].toString() == "1"){
                                                                              Get.back();
                                                                            }
                                                                            controller.incomeMasterDetailsCall().then((value){
                                                                              setState((){});
                                                                            });
                                                                          });
                                                                        },
                                                                        child:Icon(Icons.delete)
                                                                    )),

                                                              ],
                                                            ),
                                                            SizedBox(height: 10,),
                                                          ],
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            ],
                                          ),
                                        ) : dataNotFound(context),
                                      );
                                    });
                              });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.visibility,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),

                    //End B&P

                    //Start Other
                    const SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const [
                          Text("OTHER"),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                              color: const Color(0xffC1CDFF), width: 1)),
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      child: Row(
                        children: [
                          Flexible(
                            child: TextField(
                              style: const TextStyle(fontSize: 13),
                              controller: controller.otherController,
                              enabled: false,
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(15),
                                disabledBorder:OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Colors.transparent)) ,
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xffC1CDFF))),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xff0B28BE))),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                      insetPadding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      backgroundColor: Colors.transparent,
                                      content: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Scrollbar(
                                          thumbVisibility: true,
                                          child: ListView(
                                            shrinkWrap: true,
                                            children: [
                                              Column(
                                                children: [
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    height: 430,
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10)),
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          alignment:
                                                              Alignment.center,
                                                          decoration:
                                                              const BoxDecoration(
                                                                  color: Color(
                                                                      0xff1739E4),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            10),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            10),
                                                                  )),
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          height: 40,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              const Text(""),
                                                              const Text(
                                                                "OTHER",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        18,
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                              InkWell(
                                                                  onTap: () {
                                                                    Get.back();
                                                                  },
                                                                  child:
                                                                      const Icon(
                                                                    Icons.clear,
                                                                    color: Colors
                                                                        .white,
                                                                  )),
                                                            ],
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              const Text(
                                                                "FILE UPLOAD",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        13),
                                                              ),
                                                              InkWell(
                                                                onTap: () async {
                                                                  if(Platform.isWindows) {
                                                                    FilePickerResult? filePickerResult = await FilePicker
                                                                        .platform.pickFiles(type: FileType.image);
                                                                    if(filePickerResult != null) {
                                                                      PlatformFile file = filePickerResult.files.single;
                                                                      controller.image = File(file.path.toString());
                                                                      print("File Path:"+controller.image.toString());
                                                                    }
                                                                  } else {
                                                                    showDialog(
                                                                        barrierDismissible:
                                                                        false,
                                                                        context:
                                                                        context,
                                                                        builder:
                                                                            (context) {
                                                                          return AlertDialog(
                                                                            contentPadding:
                                                                            EdgeInsets.zero,
                                                                            insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                            content:
                                                                            Container(
                                                                              height:
                                                                              130,
                                                                              child:
                                                                              Column(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                children: [
                                                                                  Container(
                                                                                    padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                    width: MediaQuery.of(context).size.width,
                                                                                    height: 50,
                                                                                    color: Colors.indigo,
                                                                                    child: Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                      children: [
                                                                                        const Text(""),
                                                                                        const Text(
                                                                                          "Choose Source",
                                                                                          style: TextStyle(color: Colors.white),
                                                                                        ),
                                                                                        InkWell(
                                                                                            onTap: () {
                                                                                              Get.back();
                                                                                            },
                                                                                            child: const Icon(Icons.clear, color: Colors.white))
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                  const SizedBox(height: 10),
                                                                                  Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                    children: [
                                                                                      Column(
                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          GestureDetector(
                                                                                              onTap: () {
                                                                                                controller.uploadFromGallery().whenComplete(() {
                                                                                                  Get.back();
                                                                                                });
                                                                                              },
                                                                                              child: Image.asset(
                                                                                                "images/gallery.png",
                                                                                                width: 30,
                                                                                              )),
                                                                                          const Text("Gallery")
                                                                                        ],
                                                                                      ),
                                                                                      Column(
                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          GestureDetector(
                                                                                              onTap: () {
                                                                                                controller.uploadFromCamera().whenComplete(() {
                                                                                                  Get.back();
                                                                                                });
                                                                                              },
                                                                                              child: Image.asset(
                                                                                                "images/camera.png",
                                                                                                width: 30,
                                                                                              )),
                                                                                          const Text("Camera")
                                                                                        ],
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                  const SizedBox(
                                                                                    height: 20,
                                                                                  )
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          );
                                                                        });
                                                                  }

                                                                },
                                                                child:
                                                                    Container(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      horizontal:
                                                                          10),
                                                                  decoration: BoxDecoration(
                                                                      color: const Color(
                                                                          0xff1739E4),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5)),
                                                                  height: 25,
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceEvenly,
                                                                    children: const [
                                                                      Text(
                                                                        "Upload",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.white),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            10,
                                                                      ),
                                                                      Icon(
                                                                        Icons
                                                                            .cloud_upload,
                                                                        color: Colors
                                                                            .white,
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                "Amount"
                                                                    .toUpperCase(),
                                                                style:
                                                                    const TextStyle(
                                                                        fontSize:
                                                                            13),
                                                              ),
                                                              const SizedBox()
                                                            ],
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: SizedBox(
                                                            height: 40,
                                                            child:
                                                                TextFormField(
                                                              keyboardType:
                                                                  TextInputType
                                                                      .number,
                                                              controller: controller
                                                                  .amountController5,
                                                              decoration: InputDecoration(
                                                                  border: OutlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      borderSide: const BorderSide(
                                                                          color: Color(
                                                                              0xffC1CDFF),
                                                                          width:
                                                                              1))),
                                                            ),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                "Remark"
                                                                    .toUpperCase(),
                                                                style:
                                                                    const TextStyle(
                                                                        fontSize:
                                                                            13),
                                                              ),
                                                              const SizedBox(
                                                                height: 10,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Container(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: TextFormField(
                                                            keyboardType:
                                                                TextInputType
                                                                    .text,
                                                            controller: controller
                                                                .remarkController5,
                                                            maxLines: 4,
                                                            decoration: InputDecoration(
                                                                border: OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                5),
                                                                    borderSide:
                                                                        const BorderSide(
                                                                            color:
                                                                                Color(0xffC1CDFF)))),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 50,
                                                        ),
                                                        Container(
                                                          margin:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          height: 50,
                                                          child: ElevatedButton(
                                                              style: ElevatedButton
                                                                  .styleFrom(
                                                                      primary:
                                                                          const Color(
                                                                              0xff0B28BE)),
                                                              onPressed: () {
                                                                if(controller.amountController5.text.isEmpty) {
                                                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Amount is required !!")));
                                                                } else  if(controller.remarkController5.text.isEmpty) {
                                                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Remark is required !!")));
                                                                } else {
                                                                  EasyLoading
                                                                      .show();
                                                                  controller
                                                                      .incomeMasterCall(
                                                                      context,
                                                                      "Other",
                                                                      controller
                                                                          .amountController5
                                                                          .text,
                                                                      controller
                                                                          .remarkController5
                                                                          .text)
                                                                      .whenComplete(
                                                                          () {
                                                                        controller
                                                                            .incomeMasterDetailsCall();
                                                                        Get.back();
                                                                      });
                                                                }
                                                              },
                                                              child: Text("Submit"
                                                                  .toUpperCase())),
                                                        ),
                                                        const SizedBox(
                                                          height: 20,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          InkWell(
                            onTap: () {
                              controller
                                  .incomeMasterDetailsCall()
                                  .whenComplete(() {
                                // print("---------incomeMasterResponse---------------" + controller.incomeMasterResponse.value.toString());
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const Text("Other Details"),
                                            InkWell(
                                                onTap: () {
                                                  Get.back();
                                                },
                                                child: const Icon(
                                                  Icons.clear,
                                                  color: Colors.red,
                                                ))
                                          ],
                                        ),
                                        content: controller
                                            .transcationList5.isNotEmpty ? Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 300,
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 20),
                                                child: Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment
                                                      .center,
                                                  children: [
                                                    Expanded(child: Text("S. No",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Head",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Amount",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Reamrk",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),
                                                  ],
                                                ),
                                              ),
                                              Expanded(
                                                child: ListView.builder(
                                                    itemCount: controller
                                                        .transcationList5
                                                        .length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return Padding(
                                                        padding: const EdgeInsets.symmetric(horizontal: 20),
                                                        child: Column(
                                                          children: [
                                                            SizedBox(height: 10,),
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Expanded(
                                                                    child: Text(
                                                                  "${index + 1}",
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller.transcationList5[
                                                                          index]
                                                                      ["income_head"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList5[
                                                                      index]["amount"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList5[
                                                                      index]["remark"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap: () {
                                                                          showDialog(
                                                                              barrierDismissible:
                                                                                  false,
                                                                              context:
                                                                                  context,
                                                                              builder:
                                                                                  (context) {
                                                                                return AlertDialog(
                                                                                  backgroundColor:
                                                                                      Colors.transparent,
                                                                                  contentPadding:
                                                                                      EdgeInsets.zero,
                                                                                  insetPadding:
                                                                                  EdgeInsets.symmetric(horizontal: 50,vertical: 150),
                                                                                  content:
                                                                                      Container(
                                                                                        alignment: Alignment.topRight,
                                                                                    width: MediaQuery.of(context).size.width,
                                                                                    height: 400,
                                                                                    decoration: BoxDecoration(
                                                                                        color: Colors.black,
                                                                                        image: DecorationImage(
                                                                                          fit: BoxFit.fill,
                                                                                          image: NetworkImage(
                                                                                            "https://appexperts.net/btconnect/" + controller.transcationList5[index]["document_path"].toString(),
                                                                                          ),
                                                                                        )),
                                                                                        child: IconButton(onPressed: (){
                                                                                          Get.back();
                                                                                        },
                                                                                          icon: Image.asset("images/close.png",width: 30,),
                                                                                        ),
                                                                                  ),
                                                                                );
                                                                              });
                                                                        },
                                                                        child:
                                                                            SvgPicture
                                                                                .asset(
                                                                          "images/visibility.svg",
                                                                          width: 24,
                                                                        ))),
                                                                Expanded(
                                                                    child: InkWell(
                                                                  onTap: () {
                                                                    print(
                                                                        'Edit other working');
                                                                    controller
                                                                        .amountController
                                                                        .text = controller
                                                                            .transcationList5[
                                                                        index]["amount"];
                                                                    controller
                                                                        .remarkController
                                                                        .text = controller
                                                                            .transcationList5[
                                                                        index]["remark"];
                                                                    Get.back();

                                                                    showDialog(
                                                                        barrierDismissible:
                                                                            false,
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (context) {
                                                                          return AlertDialog(
                                                                              insetPadding: const EdgeInsets.symmetric(
                                                                                  horizontal:
                                                                                      10),
                                                                              backgroundColor:
                                                                                  Colors
                                                                                      .transparent,
                                                                              content:
                                                                                  Container(
                                                                                width: MediaQuery.of(context)
                                                                                    .size
                                                                                    .width,
                                                                                child:
                                                                                    Scrollbar(
                                                                                  thumbVisibility:
                                                                                      true,
                                                                                  child:
                                                                                      ListView(
                                                                                    shrinkWrap: true,
                                                                                    children: [
                                                                                      Column(
                                                                                        children: [
                                                                                          Container(
                                                                                            width: MediaQuery.of(context).size.width,
                                                                                            height: 420,
                                                                                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                                                                            child: Column(
                                                                                              children: [
                                                                                                Container(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  alignment: Alignment.center,
                                                                                                  decoration: const BoxDecoration(
                                                                                                      color: Color(0xff1739E4),
                                                                                                      borderRadius: BorderRadius.only(
                                                                                                        topLeft: Radius.circular(10),
                                                                                                        topRight: Radius.circular(10),
                                                                                                      )),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 40,
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    children: [
                                                                                                      const Text(""),
                                                                                                      const Text(
                                                                                                        "Other",
                                                                                                        style: TextStyle(fontSize: 18, color: Colors.white),
                                                                                                      ),
                                                                                                      InkWell(
                                                                                                          onTap: () {
                                                                                                            Get.back();
                                                                                                          },
                                                                                                          child: const Icon(
                                                                                                            Icons.clear,
                                                                                                            color: Colors.white,
                                                                                                          )),
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      const Text(
                                                                                                        "FILE UPLOAD",
                                                                                                        style: TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      InkWell(
                                                                                                        onTap: () async {
                                                                                                          if(Platform.isWindows) {
                                                                                                            FilePickerResult? filePickerResult = await FilePicker
                                                                                                                .platform.pickFiles(type: FileType.image);
                                                                                                            if(filePickerResult != null) {
                                                                                                              PlatformFile file = filePickerResult.files.single;
                                                                                                              controller.image = File(file.path.toString());
                                                                                                              print("File Path:"+controller.image.toString());
                                                                                                            }
                                                                                                          } else {
                                                                                                            showDialog(
                                                                                                                barrierDismissible: false,
                                                                                                                context: context,
                                                                                                                builder: (context) {
                                                                                                                  return AlertDialog(
                                                                                                                    contentPadding: EdgeInsets.zero,
                                                                                                                    insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                                                                    content: Container(
                                                                                                                      height: 130,
                                                                                                                      child: Column(
                                                                                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                        children: [
                                                                                                                          Container(
                                                                                                                            padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                                            width: MediaQuery.of(context).size.width,
                                                                                                                            height: 50,
                                                                                                                            color: Colors.indigo,
                                                                                                                            child: Row(
                                                                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                              children: [
                                                                                                                                const Text(""),
                                                                                                                                const Text(
                                                                                                                                  "Choose Source",
                                                                                                                                  style: TextStyle(color: Colors.white),
                                                                                                                                ),
                                                                                                                                InkWell(
                                                                                                                                    onTap: () {
                                                                                                                                      Get.back();
                                                                                                                                    },
                                                                                                                                    child: const Icon(Icons.clear, color: Colors.white))
                                                                                                                              ],
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                          const SizedBox(height: 10),
                                                                                                                          Row(
                                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                            children: [
                                                                                                                              Column(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  GestureDetector(
                                                                                                                                      onTap: () {
                                                                                                                                        controller.uploadFromGallery().whenComplete(() {
                                                                                                                                          Get.back();
                                                                                                                                        });
                                                                                                                                      },
                                                                                                                                      child: Image.asset(
                                                                                                                                        "images/gallery.png",
                                                                                                                                        width: 30,
                                                                                                                                      )),
                                                                                                                                  const Text("Gallery")
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                              Column(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  GestureDetector(
                                                                                                                                      onTap: () {
                                                                                                                                        controller.uploadFromCamera().whenComplete(() {
                                                                                                                                          Get.back();
                                                                                                                                        });
                                                                                                                                      },
                                                                                                                                      child: Image.asset(
                                                                                                                                        "images/camera.png",
                                                                                                                                        width: 30,
                                                                                                                                      )),
                                                                                                                                  const Text("Camera")
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                            ],
                                                                                                                          ),
                                                                                                                          const SizedBox(
                                                                                                                            height: 20,
                                                                                                                          )
                                                                                                                        ],
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  );
                                                                                                                });
                                                                                                          }

                                                                                                        },
                                                                                                        child: Container(
                                                                                                          padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                          decoration: BoxDecoration(color: const Color(0xff1739E4), borderRadius: BorderRadius.circular(5)),
                                                                                                          height: 25,
                                                                                                          child: Row(
                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                            children: const [
                                                                                                              Text(
                                                                                                                "Upload",
                                                                                                                style: TextStyle(color: Colors.white),
                                                                                                              ),
                                                                                                              SizedBox(
                                                                                                                width: 10,
                                                                                                              ),
                                                                                                              Icon(
                                                                                                                Icons.cloud_upload,
                                                                                                                color: Colors.white,
                                                                                                              )
                                                                                                            ],
                                                                                                          ),
                                                                                                        ),
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        "Amount".toUpperCase(),
                                                                                                        style: const TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      const SizedBox()
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: SizedBox(
                                                                                                    height: 40,
                                                                                                    child: TextFormField(
                                                                                                      controller: controller.amountController,
                                                                                                      keyboardType: TextInputType.number,
                                                                                                      decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1))),
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        "Remark".toUpperCase(),
                                                                                                        style: const TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 10,
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Container(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: TextFormField(
                                                                                                    controller: controller.remarkController,
                                                                                                    keyboardType: TextInputType.text,
                                                                                                    maxLines: 4,
                                                                                                    decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 50,
                                                                                                ),
                                                                                                Container(
                                                                                                  margin: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 50,
                                                                                                  child: ElevatedButton(
                                                                                                      style: ElevatedButton.styleFrom(primary: const Color(0xff0B28BE)),
                                                                                                      onPressed: () {
                                                                                                        if (controller.amountController.text.isNotEmpty) {
                                                                                                          EasyLoading.show(status: "Loading");

                                                                                                          controller.updateIncomeMasterCall(controller.transcationList5[index]["detail_id"], controller.amountController.text, controller.remarkController.text, context).whenComplete(() {
                                                                                                            controller.incomeMasterDetailsCall();
                                                                                                            //Get.back();
                                                                                                          });
                                                                                                        } else {
                                                                                                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Fields are empty !!")));
                                                                                                        }
                                                                                                      },
                                                                                                      child: Text("Submit".toUpperCase())),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 20,
                                                                                                )
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                        ],
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ));
                                                                        });
                                                                  },
                                                                  child: SvgPicture
                                                                      .asset(
                                                                    "images/pencil.svg",
                                                                    width: 24,
                                                                  ),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap:()async{
                                                                          showDialog(context: context, builder: (context)=>Center(
                                                                            child: Container(
                                                                                width: 50,
                                                                                height:50,
                                                                                child: CircularProgressIndicator()),
                                                                          ));
                                                                          await Api.deleteDepreciationEntryApi("incomeMasterOther",controller.transcationList5[index]["detail_id"]).then((value){
                                                                            print("---delete Response----"+value.toString());
                                                                            Get.back();
                                                                            if(value["response_status"].toString() == "1"){
                                                                              Get.back();
                                                                            }
                                                                            controller.incomeMasterDetailsCall().then((value){
                                                                              setState((){});
                                                                            });
                                                                          });
                                                                        },
                                                                        child:Icon(Icons.delete)
                                                                    )),

                                                              ],
                                                            ),
                                                            SizedBox(height: 10,),
                                                          ],
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            ],
                                          ),
                                        ) : dataNotFound(context),
                                      );
                                    });
                              });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.visibility,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                    //End Other

                    //Start Agriculture
                    const SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const [
                          Text("AGRICULTURE"),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                              color: const Color(0xffC1CDFF), width: 1)),
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      child: Row(
                        children: [
                          Flexible(
                            child: TextField(
                              style: const TextStyle(fontSize: 13),
                              controller: controller.agricultureController,
                              enabled: false,
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(15),
                                disabledBorder:OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Colors.transparent)) ,
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xffC1CDFF))),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: const BorderSide(
                                        color: Color(0xff0B28BE))),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                      insetPadding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      backgroundColor: Colors.transparent,
                                      content: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Scrollbar(
                                          thumbVisibility: true,
                                          child: ListView(
                                            shrinkWrap: true,
                                            children: [
                                              Column(
                                                children: [
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    height: 430,
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10)),
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          alignment:
                                                              Alignment.center,
                                                          decoration:
                                                              const BoxDecoration(
                                                                  color: Color(
                                                                      0xff1739E4),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            10),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            10),
                                                                  )),
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          height: 40,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              const Text(""),
                                                              const Text(
                                                                "AGRICULTURE",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        18,
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                              InkWell(
                                                                  onTap: () {
                                                                    Get.back();
                                                                  },
                                                                  child:
                                                                      const Icon(
                                                                    Icons.clear,
                                                                    color: Colors
                                                                        .white,
                                                                  )),
                                                            ],
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              const Text(
                                                                "FILE UPLOAD",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        13),
                                                              ),
                                                              InkWell(
                                                                onTap: () async {
                                                                  if(Platform.isWindows) {
                                                                    FilePickerResult? filePickerResult = await FilePicker
                                                                        .platform.pickFiles(type: FileType.image);
                                                                    if(filePickerResult != null) {
                                                                      PlatformFile file = filePickerResult.files.single;
                                                                      controller.image = File(file.path.toString());
                                                                      print("File Path:"+controller.image.toString());
                                                                    }
                                                                  } else {
                                                                    showDialog(
                                                                        barrierDismissible:
                                                                        false,
                                                                        context:
                                                                        context,
                                                                        builder:
                                                                            (context) {
                                                                          return AlertDialog(
                                                                            contentPadding:
                                                                            EdgeInsets.zero,
                                                                            insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                            content:
                                                                            Container(
                                                                              height:
                                                                              130,
                                                                              child:
                                                                              Column(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                children: [
                                                                                  Container(
                                                                                    padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                    width: MediaQuery.of(context).size.width,
                                                                                    height: 50,
                                                                                    color: Colors.indigo,
                                                                                    child: Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                      children: [
                                                                                        const Text(""),
                                                                                        const Text(
                                                                                          "Choose Source",
                                                                                          style: TextStyle(color: Colors.white),
                                                                                        ),
                                                                                        InkWell(
                                                                                            onTap: () {
                                                                                              Get.back();
                                                                                            },
                                                                                            child: const Icon(Icons.clear, color: Colors.white))
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                  const SizedBox(height: 10),
                                                                                  Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                    children: [
                                                                                      Column(
                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          GestureDetector(
                                                                                              onTap: () {
                                                                                                controller.uploadFromGallery().whenComplete(() {
                                                                                                  Get.back();
                                                                                                });
                                                                                              },
                                                                                              child: Image.asset(
                                                                                                "images/gallery.png",
                                                                                                width: 30,
                                                                                              )),
                                                                                          const Text("Gallery")
                                                                                        ],
                                                                                      ),
                                                                                      Column(
                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          GestureDetector(
                                                                                              onTap: () {
                                                                                                controller.uploadFromCamera().whenComplete(() {
                                                                                                  Get.back();
                                                                                                });
                                                                                              },
                                                                                              child: Image.asset(
                                                                                                "images/camera.png",
                                                                                                width: 30,
                                                                                              )),
                                                                                          const Text("Camera")
                                                                                        ],
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                  const SizedBox(
                                                                                    height: 20,
                                                                                  )
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          );
                                                                        });
                                                                  }

                                                                },
                                                                child:
                                                                    Container(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      horizontal:
                                                                          10),
                                                                  decoration: BoxDecoration(
                                                                      color: const Color(
                                                                          0xff1739E4),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5)),
                                                                  height: 25,
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceEvenly,
                                                                    children: const [
                                                                      Text(
                                                                        "Upload",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.white),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            10,
                                                                      ),
                                                                      Icon(
                                                                        Icons
                                                                            .cloud_upload,
                                                                        color: Colors
                                                                            .white,
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                "Amount"
                                                                    .toUpperCase(),
                                                                style:
                                                                    const TextStyle(
                                                                        fontSize:
                                                                            13),
                                                              ),
                                                              const SizedBox()
                                                            ],
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: SizedBox(
                                                            height: 40,
                                                            child:
                                                                TextFormField(
                                                              keyboardType:
                                                                  TextInputType
                                                                      .number,
                                                              controller: controller
                                                                  .amountController6,
                                                              decoration: InputDecoration(
                                                                  border: OutlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      borderSide: const BorderSide(
                                                                          color: Color(
                                                                              0xffC1CDFF),
                                                                          width:
                                                                              1))),
                                                            ),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                "Remark"
                                                                    .toUpperCase(),
                                                                style:
                                                                    const TextStyle(
                                                                        fontSize:
                                                                            13),
                                                              ),
                                                              const SizedBox(
                                                                height: 10,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Container(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          child: TextFormField(
                                                            keyboardType:
                                                                TextInputType
                                                                    .text,
                                                            controller: controller
                                                                .remarkController6,
                                                            maxLines: 4,
                                                            decoration: InputDecoration(
                                                                border: OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                5),
                                                                    borderSide:
                                                                        const BorderSide(
                                                                            color:
                                                                                Color(0xffC1CDFF)))),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 50,
                                                        ),
                                                        Container(
                                                          margin:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      10),
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          height: 50,
                                                          child: ElevatedButton(
                                                              style: ElevatedButton
                                                                  .styleFrom(
                                                                      primary:
                                                                          const Color(
                                                                              0xff0B28BE)),
                                                              onPressed: () {
                                                                if(controller.amountController6.text.isEmpty) {
                                                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Amount is required !!")));
                                                                } else  if(controller.remarkController6.text.isEmpty) {
                                                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Remark is required !!")));
                                                                } else {
                                                                  EasyLoading
                                                                      .show();
                                                                  controller
                                                                      .incomeMasterCall(
                                                                      context,
                                                                      "Agriculture",
                                                                      controller
                                                                          .amountController6
                                                                          .text,
                                                                      controller
                                                                          .remarkController6
                                                                          .text)
                                                                      .whenComplete(
                                                                          () {
                                                                        controller
                                                                            .incomeMasterDetailsCall();
                                                                        Get.back();
                                                                      });
                                                                }
                                                              },
                                                              child: Text("Submit"
                                                                  .toUpperCase())),
                                                        ),
                                                        const SizedBox(
                                                          height: 20,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          InkWell(
                            onTap: () {
                              controller
                                  .incomeMasterDetailsCall()
                                  .whenComplete(() {
                                // print("---------incomeMasterResponse---------------" + controller.incomeMasterResponse.value.toString());
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        contentPadding: EdgeInsets.symmetric(horizontal: 3),
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const Text("Agriculture Details"),
                                            InkWell(
                                                onTap: () {
                                                  Get.back();
                                                },
                                                child: const Icon(
                                                  Icons.clear,
                                                  color: Colors.red,
                                                ))
                                          ],
                                        ),
                                        content: controller
                                            .transcationList6.isNotEmpty ?  Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 300,
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 20),
                                                child: Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment
                                                      .center,
                                                  children: [
                                                    Expanded(child: Text("S. No",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Head",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Amount",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("Reamrk",style: TextStyle(
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.bold),)),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),
                                                    Expanded(child: Text("")),

                                                  ],
                                                ),
                                              ),
                                              Expanded(
                                                child: ListView.builder(
                                                    itemCount: controller
                                                        .transcationList6
                                                        .length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return Padding(
                                                        padding: const EdgeInsets.symmetric(horizontal: 20),
                                                        child: Column(
                                                          children: [
                                                            SizedBox(height: 10,),
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Expanded(
                                                                    child: Text(
                                                                  "${index + 1}",
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller.transcationList6[
                                                                          index]
                                                                      ["income_head"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList6[
                                                                      index]["amount"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: Text(
                                                                  controller
                                                                          .transcationList6[
                                                                      index]["remark"],
                                                                  style:
                                                                      const TextStyle(
                                                                          fontSize:
                                                                              13),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap: () {
                                                                          showDialog(
                                                                              barrierDismissible:
                                                                                  false,
                                                                              context:
                                                                                  context,
                                                                              builder:
                                                                                  (context) {
                                                                                return AlertDialog(
                                                                                  backgroundColor:
                                                                                      Colors.transparent,
                                                                                  contentPadding:
                                                                                      EdgeInsets.zero,
                                                                                  insetPadding:
                                                                                  EdgeInsets.symmetric(horizontal: 50,vertical: 150),
                                                                                  content:
                                                                                      Container(
                                                                                        alignment: Alignment.topRight,
                                                                                    width: MediaQuery.of(context).size.width,
                                                                                    height: 400,
                                                                                    decoration: BoxDecoration(
                                                                                        color: Colors.black,
                                                                                        image: DecorationImage(
                                                                                          fit: BoxFit.fill,
                                                                                          image: NetworkImage(
                                                                                            "https://appexperts.net/btconnect/" + controller.transcationList6[index]["document_path"].toString(),
                                                                                          ),
                                                                                        )),
                                                                                        child: IconButton(onPressed: (){
                                                                                          Get.back();
                                                                                        },
                                                                                          icon: Image.asset("images/close.png",width: 30,),
                                                                                        ),
                                                                                  ),
                                                                                );
                                                                              });
                                                                        },
                                                                        child:
                                                                            SvgPicture
                                                                                .asset(
                                                                          "images/visibility.svg",
                                                                          width: 24,
                                                                        ))),
                                                                Expanded(
                                                                    child: InkWell(
                                                                  onTap: () {
                                                                    print(
                                                                        'Edit Agriculture working');
                                                                    controller
                                                                        .amountController
                                                                        .text = controller
                                                                            .transcationList6[
                                                                        index]["amount"];
                                                                    controller
                                                                        .remarkController
                                                                        .text = controller
                                                                            .transcationList6[
                                                                        index]["remark"];
                                                                    Get.back();

                                                                    showDialog(
                                                                        barrierDismissible:
                                                                            false,
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (context) {
                                                                          return AlertDialog(
                                                                              insetPadding: const EdgeInsets.symmetric(
                                                                                  horizontal:
                                                                                      10),
                                                                              backgroundColor:
                                                                                  Colors
                                                                                      .transparent,
                                                                              content:
                                                                                  Container(
                                                                                width: MediaQuery.of(context)
                                                                                    .size
                                                                                    .width,
                                                                                child:
                                                                                    Scrollbar(
                                                                                  thumbVisibility:
                                                                                      true,
                                                                                  child:
                                                                                      ListView(
                                                                                    shrinkWrap: true,
                                                                                    children: [
                                                                                      Column(
                                                                                        children: [
                                                                                          Container(
                                                                                            width: MediaQuery.of(context).size.width,
                                                                                            height: 420,
                                                                                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                                                                            child: Column(
                                                                                              children: [
                                                                                                Container(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  alignment: Alignment.center,
                                                                                                  decoration: const BoxDecoration(
                                                                                                      color: Color(0xff1739E4),
                                                                                                      borderRadius: BorderRadius.only(
                                                                                                        topLeft: Radius.circular(10),
                                                                                                        topRight: Radius.circular(10),
                                                                                                      )),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 40,
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    children: [
                                                                                                      const Text(""),
                                                                                                      const Text(
                                                                                                        "Agriculture",
                                                                                                        style: TextStyle(fontSize: 18, color: Colors.white),
                                                                                                      ),
                                                                                                      InkWell(
                                                                                                          onTap: () {
                                                                                                            Get.back();
                                                                                                          },
                                                                                                          child: const Icon(
                                                                                                            Icons.clear,
                                                                                                            color: Colors.white,
                                                                                                          )),
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      const Text(
                                                                                                        "FILE UPLOAD",
                                                                                                        style: TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      InkWell(
                                                                                                        onTap: () async {
                                                                                                          if(Platform.isWindows) {
                                                                                                            FilePickerResult? filePickerResult = await FilePicker
                                                                                                                .platform.pickFiles(type: FileType.image);
                                                                                                            if(filePickerResult != null) {
                                                                                                              PlatformFile file = filePickerResult.files.single;
                                                                                                              controller.image = File(file.path.toString());
                                                                                                              print("File Path:"+controller.image.toString());
                                                                                                            }
                                                                                                          } else {
                                                                                                            showDialog(
                                                                                                                barrierDismissible: false,
                                                                                                                context: context,
                                                                                                                builder: (context) {
                                                                                                                  return AlertDialog(
                                                                                                                    contentPadding: EdgeInsets.zero,
                                                                                                                    insetPadding: EdgeInsets.symmetric(horizontal: 50),
                                                                                                                    content: Container(
                                                                                                                      height: 130,
                                                                                                                      child: Column(
                                                                                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                        children: [
                                                                                                                          Container(
                                                                                                                            padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                                            width: MediaQuery.of(context).size.width,
                                                                                                                            height: 50,
                                                                                                                            color: Colors.indigo,
                                                                                                                            child: Row(
                                                                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                              children: [
                                                                                                                                const Text(""),
                                                                                                                                const Text(
                                                                                                                                  "Choose Source",
                                                                                                                                  style: TextStyle(color: Colors.white),
                                                                                                                                ),
                                                                                                                                InkWell(
                                                                                                                                    onTap: () {
                                                                                                                                      Get.back();
                                                                                                                                    },
                                                                                                                                    child: const Icon(Icons.clear, color: Colors.white))
                                                                                                                              ],
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                          const SizedBox(height: 10),
                                                                                                                          Row(
                                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                            children: [
                                                                                                                              Column(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  GestureDetector(
                                                                                                                                      onTap: () {
                                                                                                                                        controller.uploadFromGallery().whenComplete(() {
                                                                                                                                          Get.back();
                                                                                                                                        });
                                                                                                                                      },
                                                                                                                                      child: Image.asset(
                                                                                                                                        "images/gallery.png",
                                                                                                                                        width: 30,
                                                                                                                                      )),
                                                                                                                                  const Text("Gallery")
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                              Column(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                children: [
                                                                                                                                  GestureDetector(
                                                                                                                                      onTap: () {
                                                                                                                                        controller.uploadFromCamera().whenComplete(() {
                                                                                                                                          Get.back();
                                                                                                                                        });
                                                                                                                                      },
                                                                                                                                      child: Image.asset(
                                                                                                                                        "images/camera.png",
                                                                                                                                        width: 30,
                                                                                                                                      )),
                                                                                                                                  const Text("Camera")
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                            ],
                                                                                                                          ),
                                                                                                                          const SizedBox(
                                                                                                                            height: 20,
                                                                                                                          )
                                                                                                                        ],
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  );
                                                                                                                });
                                                                                                          }

                                                                                                        },
                                                                                                        child: Container(
                                                                                                          padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                          decoration: BoxDecoration(color: const Color(0xff1739E4), borderRadius: BorderRadius.circular(5)),
                                                                                                          height: 25,
                                                                                                          child: Row(
                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                            children: const [
                                                                                                              Text(
                                                                                                                "Upload",
                                                                                                                style: TextStyle(color: Colors.white),
                                                                                                              ),
                                                                                                              SizedBox(
                                                                                                                width: 10,
                                                                                                              ),
                                                                                                              Icon(
                                                                                                                Icons.cloud_upload,
                                                                                                                color: Colors.white,
                                                                                                              )
                                                                                                            ],
                                                                                                          ),
                                                                                                        ),
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        "Amount".toUpperCase(),
                                                                                                        style: const TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      const SizedBox()
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: SizedBox(
                                                                                                    height: 40,
                                                                                                    child: TextFormField(
                                                                                                      controller: controller.amountController,
                                                                                                      keyboardType: TextInputType.number,
                                                                                                      decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1))),
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        "Remark".toUpperCase(),
                                                                                                        style: const TextStyle(fontSize: 13),
                                                                                                      ),
                                                                                                      const SizedBox(
                                                                                                        height: 10,
                                                                                                      )
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 10,
                                                                                                ),
                                                                                                Container(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  child: TextFormField(
                                                                                                    controller: controller.remarkController,
                                                                                                    keyboardType: TextInputType.text,
                                                                                                    maxLines: 4,
                                                                                                    decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 50,
                                                                                                ),
                                                                                                Container(
                                                                                                  margin: const EdgeInsets.symmetric(horizontal: 10),
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  height: 50,
                                                                                                  child: ElevatedButton(
                                                                                                      style: ElevatedButton.styleFrom(primary: const Color(0xff0B28BE)),
                                                                                                      onPressed: () {
                                                                                                        if (controller.amountController.text.isNotEmpty) {
                                                                                                          EasyLoading.show(status: "Loading");

                                                                                                          controller.updateIncomeMasterCall(controller.transcationList6[index]["detail_id"], controller.amountController.text, controller.remarkController.text, context).whenComplete(() {
                                                                                                            controller.incomeMasterDetailsCall();
                                                                                                            //Get.back();
                                                                                                          });
                                                                                                        } else {
                                                                                                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Fields are empty !! !!")));
                                                                                                        }
                                                                                                      },
                                                                                                      child: Text("Submit".toUpperCase())),
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 20,
                                                                                                )
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                        ],
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ));
                                                                        });
                                                                  },
                                                                  child: SvgPicture
                                                                      .asset(
                                                                    "images/pencil.svg",
                                                                    width: 24,
                                                                  ),
                                                                )),
                                                                Expanded(
                                                                    child: InkWell(
                                                                        onTap:()async{
                                                                          showDialog(context: context, builder: (context)=>Center(
                                                                            child: Container(
                                                                                width: 50,
                                                                                height:50,
                                                                                child: CircularProgressIndicator()),
                                                                          ));
                                                                          await Api.deleteDepreciationEntryApi("incomeMasterAgriculture",controller.transcationList6[index]["detail_id"]).then((value){
                                                                            print("---delete Response----"+value.toString());
                                                                            Get.back();
                                                                            if(value["response_status"].toString() == "1"){
                                                                              Get.back();
                                                                            }
                                                                            controller.incomeMasterDetailsCall().then((value){
                                                                              setState((){});
                                                                            });
                                                                          });
                                                                        },
                                                                        child:Icon(Icons.delete)
                                                                    )),

                                                              ],
                                                            ),
                                                            SizedBox(height: 10,),
                                                          ],
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            ],
                                          ),
                                        ) : dataNotFound(context),
                                      );
                                    });
                              });
                            },
                            child: const CircleAvatar(
                                radius: 14,
                                backgroundColor: Color(0xffB5C5FF),
                                child: Icon(
                                  Icons.visibility,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ),
                          const SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                    //End Agriculture
                    const SizedBox(height: 20)
                  ],
                ),
              ),
            );
            // }
          },
        ));
  }
}

dataNotFound(BuildContext context) {
  return Container(
    alignment: Alignment.center,
    width:
    MediaQuery.of(context).size.width,
    height: 300,
    child: Text("Data Not Available !!"),
  );
}
