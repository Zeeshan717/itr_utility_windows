// ignore_for_file: file_names, deprecated_member_use, avoid_print, non_constant_identifier_names

import 'dart:io';

import 'package:accountx/app/services/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';

class IncomeMasterController extends GetxController {
  String? dropdownValue;
  String? dropdownValue2;
  String? dropdownValue3;
  String? dropdownValue4;
  String? dropdownValue5;
  String? dropdownValue6;

  GetStorage box = GetStorage();

  TextEditingController salaryController = TextEditingController();
  TextEditingController hpController = TextEditingController();
  TextEditingController capitalController = TextEditingController();
  TextEditingController bandpController = TextEditingController();
  TextEditingController otherController = TextEditingController();
  TextEditingController agricultureController = TextEditingController();

  TextEditingController amountController = TextEditingController();
  TextEditingController remarkController = TextEditingController();

  TextEditingController amountController2 = TextEditingController();
  TextEditingController remarkController2 = TextEditingController();

  TextEditingController amountController3 = TextEditingController();
  TextEditingController remarkController3 = TextEditingController();

  TextEditingController amountController4 = TextEditingController();
  TextEditingController remarkController4 = TextEditingController();

  TextEditingController amountController5 = TextEditingController();
  TextEditingController remarkController5 = TextEditingController();

  TextEditingController amountController6 = TextEditingController();
  TextEditingController remarkController6 = TextEditingController();

  File? image;
  String image1 = "";
  List<Map<String, dynamic>> transcationList = [];
  List<Map<String, dynamic>> transcationList2 = [];
  List<Map<String, dynamic>> transcationList3 = [];
  List<Map<String, dynamic>> transcationList4 = [];
  List<Map<String, dynamic>> transcationList5 = [];
  List<Map<String, dynamic>> transcationList6 = [];
  // var incomeMasterResponse ;

  // DateTime now = DateTime.now();
  //
  // late DateTime date1 = DateTime(now.year-1);
  // late DateTime date2 = DateTime(now.year);
  // late DateTime date3 = DateTime(now.year+1);
  //
  // late var dateList = [];
  //
  //   dateList = [
  //     Text(formatDate(date1, [yyyy])+" - "+formatDate(date2, [yyyy])),
  //     Text(formatDate(date2, [yyyy])+" - "+formatDate(date3, [yyyy])),
  //   ];

  incomeMasterControl() {
    dropdownValue = 'SALARY 1';
    dropdownValue2 = 'HP 1';
    dropdownValue3 = 'CAPITAL 1';
    dropdownValue4 = 'B&P 1';
    dropdownValue5 = 'OTHER 1';
    dropdownValue6 = 'AGRICULTURE 1';
    update();
  }

  Future uploadFromCamera() async {
    PickedFile? pickedFile = await ImagePicker().getImage(source: ImageSource.camera, imageQuality: 30);

    if (pickedFile != null) {
      image = File(pickedFile.path);
      image1 = image!.path.toString();
    }
  }

  Future uploadFromGallery() async {
    PickedFile? pickedFile = await ImagePicker().getImage(source: ImageSource.gallery, imageQuality: 30);
    if (pickedFile != null) {
      image = File(pickedFile.path);
      image1 = image!.path.toString();
    }
  }

  Future incomeMasterCall(BuildContext context, String incomeHead, String amount, String remark) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var finacialYear = box.read("finacial_year");
    print("-----financial Year-----" + finacialYear.toString());
    var adminId = box.read("loginValue")["admin_id"];
    EasyLoading.show(status: "Loading");
    await Api.incomeMasterApi(
            clientId.toString(), clientName.toString(), adminId.toString(), finacialYear.toString(), incomeHead, amount, image1.toString(), remark)
        .then((value) {
      if (value['success'] == 1) {
        print("Working");
        EasyLoading.dismiss();
        Navigator.pop(context);
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(value['message'].toString())),
        );
      } else {
        EasyLoading.dismiss();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(value['message'].toString())),
        );
      }
    });
  }

  Future incomeMasterDetailsCall() async {
    print("IncomeMasterDetailsCall working");
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read('finacial_year');
    await Api.getIcomeMasterApi(clientId.toString(), adminId.toString(), financialYear.toString()).then((value1) {
      // print("Not Working");
      // print(value1);

      salaryController.text = value1["salary"] ?? "0";
      hpController.text = value1["hp"] ?? "0";
      capitalController.text = value1["capital"] ?? "0";
      bandpController.text = value1["b_and_p"] ?? "0";
      otherController.text = value1["other"] ?? "0";
      agricultureController.text = value1["agriculture"] ?? "0";

      transcationList.clear();

      if (value1.toString().contains("detailed_response")) {
        value1["detailed_response"].forEach((element) {
          // print(element.toString());
          if (element["income_head"].toString() == 'Salary') {
            transcationList.add({
              "detail_id": element["detail_id"],
              "income_head": element["income_head"],
              "document_path": element["document_path"],
              "amount": element["amount"],
              "remark": element["remark"],
              "inserted_by": element["inserted_by"]
            });
          }
        });



      transcationList2.clear();
      value1["detailed_response"].forEach((element) {
        // print(element.toString());
        if (element["income_head"].toString() == 'HP') {
          transcationList2.add({
            "detail_id": element["detail_id"],
            "income_head": element["income_head"],
            "document_path": element["document_path"],
            "amount": element["amount"],
            "remark": element["remark"],
            "inserted_by": element["inserted_by"]
          });
        }
      });

      transcationList3.clear();
      value1["detailed_response"].forEach((element) {
        // print(element.toString());
        if (element["income_head"].toString() == 'Capital') {
          transcationList3.add({
            "detail_id": element["detail_id"],
            "income_head": element["income_head"],
            "document_path": element["document_path"],
            "amount": element["amount"],
            "remark": element["remark"],
            "inserted_by": element["inserted_by"]
          });
        }
      });

      transcationList4.clear();
      value1["detailed_response"].forEach((element) {
        // print(element.toString());
        if (element["income_head"].toString() == 'B and P') {
          transcationList4.add({
            "detail_id": element["detail_id"],
            "income_head": element["income_head"],
            "document_path": element["document_path"],
            "amount": element["amount"],
            "remark": element["remark"],
            "inserted_by": element["inserted_by"]
          });
        }
      });

      transcationList5.clear();
      value1["detailed_response"].forEach((element) {
        // print(element.toString());
        if (element["income_head"].toString() == 'Other') {
          transcationList5.add({
            "detail_id": element["detail_id"],
            "income_head": element["income_head"],
            "document_path": element["document_path"],
            "amount": element["amount"],
            "remark": element["remark"],
            "inserted_by": element["inserted_by"]
          });
        }
      });

      transcationList6.clear();
      value1["detailed_response"].forEach((element) {
        // print(element.toString());
        if (element["income_head"].toString() == 'Agriculture') {
          transcationList6.add({
            "detail_id": element["detail_id"],
            "income_head": element["income_head"],
            "document_path": element["document_path"],
            "amount": element["amount"],
            "remark": element["remark"],
            "inserted_by": element["inserted_by"]
          });
        }
      });
    }else{
      print("Value not available");
      }
      //  print("---------transctionList--------" + transcationList[1]["detail_id"].toString());

      if (value1['success'] == 1) {
        print("Working");
        // incomeMasterResponse.value = value1;
        EasyLoading.dismiss();
        // Navigator.pop(context);
        // ScaffoldMessenger.of(context).showSnackBar(
        //   SnackBar(content: Text(value['message'].toString())),
        // );
      } else {
        EasyLoading.dismiss();
        // ScaffoldMessenger.of(context).showSnackBar(
        //   SnackBar(content: Text(value['message'].toString())),
        // );
      }
    });
  }

  Future updateIncomeMasterCall(String detail_id, String amount, String remark,BuildContext context) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("loginValue")["admin_id"];
    var pdfFileTemp;
    if(image==null){
      pdfFileTemp= "";
    }else{
      pdfFileTemp=image!.path.toString();
    }
    await Api.updateIncomeMasterApi(
        clientId.toString(), clientName.toString(), adminId.toString(), detail_id, amount, pdfFileTemp, remark).then((value) {
          if(value["response_status"].toString()=="1"){
            print("UPdate Income Master@@@@@@@@");
            EasyLoading.dismiss();
            Get.back();
            showDialog(
                context: context,
                builder: (context) => Container(
                  child: AlertDialog(
                    title: Text("Alert"),
                    content: Text(value["response_msg"].toString()),
                    actions: [TextButton(onPressed: () => Get.back(), child: Text("OK"))],
                  ),
                ));
          }else{
            EasyLoading.dismiss();
          }
    });
  }
}
