// ignore_for_file: must_be_immutable

import 'package:accountx/app/modules/calculators_page/calculators_controller.dart';
import 'package:accountx/app/modules/depreciationCalPage/depreciation_view.dart';
import 'package:accountx/app/modules/emiCalPage/emiView.dart';
import 'package:accountx/app/modules/taxCalPage/taxView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CalculatorView extends GetResponsiveView<CalculatorsController> {

  var _calculatorsController = Get.put(CalculatorsController());

  CalculatorView({Key? key})
      : super(key: key, settings: const ResponsiveScreenSettings(desktopChangePoint: 800, tabletChangePoint: 700, watchChangePoint: 300));
/*  final CalculatorsController _calculatorsController = Get.find();*/
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(

            backgroundColor: const Color(0xff0B28BE),
            bottom: TabBar(
              indicatorColor: Colors.white,
              controller: _calculatorsController.tabController,
              tabs: const [
                Tab(
                  text: "Tax",
                ),
                Tab(
                  text: "Emi",
                ),
                Tab(
                  text: "Depreciation",
                ),
              ],
            ),
            centerTitle: true,
            title: const Text(
              "Calculator",
              style: TextStyle(fontSize: 16),
            ),
          ),
          body: TabBarView(
            controller: _calculatorsController.tabController,
            children: [const TaxView(), const EmiView(), DepreciationCalculator()],
          ),
        ));
  }
}
