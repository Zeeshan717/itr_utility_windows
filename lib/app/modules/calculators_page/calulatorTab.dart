// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CalculatorTabController extends GetxController with GetSingleTickerProviderStateMixin {
  final List<Tab> myTabs = <Tab>[
    const Tab(text: 'Tax Calculator',),
    const Tab(text: 'Emi Calculator',),
    const Tab(text: 'Depreciation Calculator',),
  ];

  TabController? tabController;

  @override
  void onInit() {
    tabController = TabController(length: myTabs.length, vsync: this);
    super.onInit();
  }

  @override
  void onClose() {
    tabController!.dispose();
    super.onClose();
  }

}