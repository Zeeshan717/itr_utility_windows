import 'package:accountx/app/modules/calculators_page/calculators_controller.dart';
import 'package:get/instance_manager.dart';

class CalculatorsBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<CalculatorsController>(CalculatorsController());
  }
}
