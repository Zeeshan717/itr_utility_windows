// ignore_for_file: file_names

import 'package:accountx/app/modules/taxCalPage/taxController.dart';
import 'package:get/instance_manager.dart';

class TaxBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<TaxController>(TaxController());
    // Get.lazyPut(() => TaxController());
  }
}
