// ignore_for_file: avoid_print, unrelated_type_equality_checks, non_constant_identifier_names, file_names

import 'package:accountx/app/services/api.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class TaxController extends GetxController {
  GetStorage box = GetStorage();
  var val = 1.obs;
  var val2 = 1.obs;
  var val80g = 1.obs;

  var totalIncomegetx = 0.0.obs;
  var totalDeductiongetx = 0.0.obs;
  var finalTax = 0.0.obs;

  var isVisible = false.obs;
  var isVisible1 = false.obs;

  // Incontroller Details
  TextEditingController salaryCntroller = TextEditingController();
  TextEditingController hpCntroller = TextEditingController();
  TextEditingController capitalCntroller = TextEditingController();
  TextEditingController bAndPController = TextEditingController();
  TextEditingController otherSController = TextEditingController(); // other in income
  TextEditingController agricultureController = TextEditingController();

// Total Deductions
  TextEditingController cntroller_80c = TextEditingController();
  // 80c sub heading
  TextEditingController epfController = TextEditingController();
  TextEditingController licController = TextEditingController();
  TextEditingController nationalSavingController = TextEditingController();
  TextEditingController tutionFeeForChildernController = TextEditingController();
  TextEditingController seniorCitizenSavingController = TextEditingController();
  TextEditingController repaymentOfPrincipal = TextEditingController();
  TextEditingController sukanyaSamridhiSchemeController = TextEditingController();
  TextEditingController registraionFees = TextEditingController();

  TextEditingController cntroller_80ccd = TextEditingController();

  TextEditingController cntroller_80d = TextEditingController();
  // 80d sub heading
  TextEditingController selfController80D = TextEditingController();
  TextEditingController parentsController80D = TextEditingController();

  TextEditingController cntroller_80g = TextEditingController();
// 80g sub heading

  TextEditingController donationOtherByChequeController = TextEditingController();
  TextEditingController donationInCashController = TextEditingController();

  TextEditingController cntroller_80tta = TextEditingController();
  TextEditingController othterDController = TextEditingController(); // other in deduction\

  //total computation controllers
  TextEditingController taxtotalincomecontroller=TextEditingController();
  TextEditingController  taxtotaldeductcontroller=TextEditingController();
  TextEditingController  taxtaxablecontroller=TextEditingController();
  TextEditingController  taxtotaltaxpayablecontroller=TextEditingController();


  Future incomeMasterDetailsCall() async {
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    await Api.getIcomeMasterApi(clientId.toString(), adminId.toString(), "2021-2022").then((value1) {
      print("TaxController");
      salaryCntroller.text = value1["salary"] == ""
          ? 0.0.toString()
          : double.parse(value1["salary"]==null?"0.0":value1["salary"]) <= 50000
              ? 0.0.toString()
              : (double.parse(value1["salary"]) - 50000).toString();
      hpCntroller.text = value1["hp"] == "" ? 0.0.toString() : (double.parse(value1["hp"]==null?"0.0" : value1["hp"]) - double.parse(value1["hp"]==null?"0.0" : value1["hp"]) * 30 / 100).toString();
      capitalCntroller.text = value1["capital"]==null ? "0.0" : value1["capital"];
      bAndPController.text = value1["b_and_p"]==null ? "0.0" : value1["b_and_p"];
      otherSController.text = value1["other"] == null ? "0.0" : value1["other"];
      agricultureController.text = value1["agriculture"] == null ? "0.0" : value1["agriculture"];
      totalInc();
    });
  }

  apiDeductionDetailsCall() {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    Api.getDeductionDetailsApi2(clientId.toString(), clientName.toString(), adminId.toString(), financialYear.toString()).then((value) {
      print(value["response_status"].toString() + " working");

      //-----------80c-----------
      cntroller_80c.text = value["80c"] == null ? "0.0" : value["80c"];
      epfController.text = value["epf_pf"] == null ? "0.0" : value["epf_pf"];
      licController.text = value["lic"] == null ? "0.0" : value["lic"];
      nationalSavingController.text = value["national_saving"] == null ? "0.0" : value["national_saving"];
      tutionFeeForChildernController.text = value["tuition_fees"]  == null ? "0.0" : value["tuition_fees"];
      seniorCitizenSavingController.text = value["senior_citizen_saving"]  == null ? "0.0" : value["senior_citizen_saving"];
      repaymentOfPrincipal.text = value["repayment_of_principle"]  == null ? "0.0" : value["repayment_of_principle"];
      sukanyaSamridhiSchemeController.text = value["sukanya_samridhi_scheme"]  == null ? "0.0" : value["sukanya_samridhi_scheme"];
      registraionFees.text = value["reg_fees_stamp_duty"]  == null ? "0.0" : value["reg_fees_stamp_duty"];
      //-------------80ccd------------
      cntroller_80ccd.text = value["80ccd"] == ""
          ? "0.0"
          : double.parse(value["80ccd"]??"0.0") >= 50000
              ? 50000.toString()
              : double.parse(value["80ccd"]??"0.0").toString();
      //------------80d----------------
      cntroller_80d.text = value["80d"]??"0.0";
      selfController80D.text = value["self"];
      parentsController80D.text = value["parents"];
      //-------------80g-----------------
      cntroller_80g.text = value["80g"];
      donationOtherByChequeController.text = value["other_by_cheque"];
      donationInCashController.text = value["in_cash"];
      //-------------80tt---------

      cntroller_80tta.text = value["80tta"] == ""
          ? "0.0"
          : double.parse(value["80tta"]) >= 10000
              ? 10000.toString()
              : double.parse(value["80tta"]).toString();
      othterDController.text = value["other"];

      total80c();
      total80d();
      total80g();

      //  cntroller_80ccd.text = value[];
      //  cntroller_80d.text = value[];

      // controller80C.text = value["80c"];
      // controller80CCD.text = value["80ccd"];
      // controller80D.text = value["80d"];
      // controller80G.text = value["80g"];
      // controller80TTA.text = value["80tta"];
      // controllerOther.text = value["other"];
    });
  }

  // String scheme = 'New';
  int age = 59;
  // Income Details
  double salary = 0.0;
  double hp = 0.0;
  double capital = 0.0;
  double bAndp = 0.0;
  double otherSalary = 0.0;
  double agriculture = 0.0;
  double totalIncome = 0.0;

  //Deduction Details
  double d_80c = 0.0;
  double d_80ccd = 0.0;
  double d_80d = 0.0;
  double d_80g = 0.0;
  double d_80tta = 0.0;
  double d_other = 0.0;
  double totalDeduction = 0.0;

  // Deduction 80c sub headings
  double d_epfController = 0.0;
  double d_licController = 0.0;
  double d_nationalSavingController = 0.0;
  double d_tutionFeeForChildernController = 0.0;
  double d_seniorCitizenSavingController = 0.0;
  double d_repaymentOfPrincipal = 0.0;
  double d_sukanyaSamridhiSchemeController = 0.0;
  double d_registraionFees = 0.0;
  double d_cntroller_80ccd = 0.0;
  double d_cntroller_80d = 0.0;

  // Calculation of ToTal Income
  double totalInc() {
    salary = salaryCntroller.text.toString().isNotEmpty ? double.parse(salaryCntroller.text.toString()) : 0.0;
    hp = hpCntroller.text.toString().isNotEmpty ? double.parse(hpCntroller.text.toString()) : 0.0;
    capital = capitalCntroller.text.toString().isNotEmpty ? double.parse(capitalCntroller.text.toString()) : 0.0;
    bAndp = bAndPController.text.toString().isNotEmpty ? double.parse(bAndPController.text.toString()) : 0.0;
    otherSalary = otherSController.text.toString().isNotEmpty ? double.parse(otherSController.text.toString()) : 0.0;
    agriculture = agricultureController.text.toString().isNotEmpty ? double.parse(agricultureController.text.toString()) : 0.0;

    totalIncome = salary + hp + capital + bAndp + otherSalary + agriculture;
    totalIncomegetx.value = totalIncome;
    taxtotalincomecontroller.text= totalIncome.toString();
   box.write("gross",taxtotalincomecontroller.text);
    return totalIncome;
  }

  double getTotalDeduction() {
    double d_80c = cntroller_80c.text.toString().isNotEmpty ? double.parse(cntroller_80c.text.toString()) : 0.0;
    double d_80ccd = cntroller_80ccd.text.toString().isNotEmpty ? double.parse(cntroller_80ccd.text.toString()) : 0.0;
    double d_80d = cntroller_80d.text.toString().isNotEmpty ? double.parse(cntroller_80d.text.toString()) : 0.0;
    double d_80g = cntroller_80g.text.toString().isNotEmpty ? double.parse(cntroller_80g.text.toString()) : 0.0;
    double d_80tta = cntroller_80tta.text.toString().isNotEmpty ? double.parse(cntroller_80tta.text.toString()) : 0.0;
    double d_other = othterDController.text.toString().isNotEmpty ? double.parse(othterDController.text.toString()) : 0.0;
    totalDeduction = (d_80c >= 150000 ? 150000.0 : d_80c) +
        (d_80ccd >= 50000 ? 50000.0 : d_80ccd) +
        d_80d +
        d_80g +
        (d_80tta >= 10000 ? 10000 : d_80tta) +
        d_other;
    totalDeductiongetx.value = totalDeduction;
    // taxtotaldeductcontroller.text=totalDeduction.toString();
    return totalDeduction;
  }

  void total80c() {
    double d_epfController = epfController.text.toString().isNotEmpty ? double.parse(epfController.text.toString()) : 0.0;
    double d_licController = licController.text.toString().isNotEmpty ? double.parse(licController.text.toString()) : 0.0;
    double d_nationalSavingController =
        nationalSavingController.text.toString().isNotEmpty ? double.parse(nationalSavingController.text.toString()) : 0.0;
    double d_tutionFeeForChildernController =
        tutionFeeForChildernController.text.toString().isNotEmpty ? double.parse(tutionFeeForChildernController.text.toString()) : 0.0;
    double d_seniorCitizenSavingController =
        seniorCitizenSavingController.text.toString().isNotEmpty ? double.parse(seniorCitizenSavingController.text.toString()) : 0.0;
    double d_repaymentOfPrincipal = repaymentOfPrincipal.text.toString().isNotEmpty ? double.parse(repaymentOfPrincipal.text.toString()) : 0.0;
    double d_sukanyaSamridhiSchemeController =
        sukanyaSamridhiSchemeController.text.toString().isNotEmpty ? double.parse(sukanyaSamridhiSchemeController.text.toString()) : 0.0;
    double d_registraionFees = registraionFees.text.toString().isNotEmpty ? double.parse(registraionFees.text.toString()) : 0.0;

    var total80c = d_epfController +
        d_licController +
        d_nationalSavingController +
        d_tutionFeeForChildernController +
        d_seniorCitizenSavingController +
        d_repaymentOfPrincipal +
        d_sukanyaSamridhiSchemeController +
        d_registraionFees;
    cntroller_80c.text = (total80c >= 150000 ? 150000 : total80c).toString();

    print(total80c.toString() + "-----");
    print(cntroller_80c.text.toString());
  }

  void total80d() {
    double d_selfController80D = selfController80D.text.toString().isNotEmpty ? double.parse(selfController80D.text.toString()) : 0.0;
    double d_parentsController80D = parentsController80D.text.toString().isNotEmpty ? double.parse(parentsController80D.text.toString()) : 0.0;

    var total80d = (d_selfController80D >= 50000 ? 50000 : d_selfController80D) + (d_parentsController80D >= 50000 ? 50000 : d_parentsController80D);
    cntroller_80d.text = total80d.toString();
    print(total80d.toString() + "-----");
    print(cntroller_80d.text.toString());
  }

  void total80g() {
    double d_donationOtherByChequeController =
        donationOtherByChequeController.text.toString().isNotEmpty ? double.parse(donationOtherByChequeController.text.toString()) : 0.0;
    double d_donationInCashController =
        donationInCashController.text.toString().isNotEmpty ? double.parse(donationInCashController.text.toString()) : 0.0;

    var total80g = (val80g == 1 ? d_donationOtherByChequeController : d_donationOtherByChequeController / 2) +
        (d_donationInCashController >= 2000 ? 2000 : d_donationInCashController);
    cntroller_80g.text = total80g.toString();
    print(total80g.toString() + "-----");
    print(cntroller_80g.text.toString());
  }

  // double totalExp() {
  //   totalExmption = exemption + interstPaidOnHome;
  //   return totalExmption;
  // }

  // void UpdateTotalIncome() {
  //   ifs = incomeFromSalary.text.isNotEmpty ? double.parse(incomeFromSalary.text.toString()) : 0.0; //Interest from Salary
  //   ifi = incomeFromInterest.text.isNotEmpty ? double.parse(incomeFromInterest.text.toString()) : 0.0; // Income  from Interest
  //   oi = otherIncome.text.isNotEmpty ? double.parse(otherIncome.text.toString()) : 0.0; // Other Income
  //   rir = rentalIncomeReceived.text.isNotEmpty ? double.parse(rentalIncomeReceived.text.toString()) : 0.0; // Rental Income Received
  //   exemption = exemptionsAndDeductions.text.isNotEmpty ? double.parse(exemptionsAndDeductions.text.toString()) : 0.0; //Exemptions & Deductions
  //   interstPaidOnHome =
  //       interestPaidOnHomeLoan.text.isNotEmpty ? double.parse(interestPaidOnHomeLoan.text.toString()) : 0.0; // Interest paid on Home Loan

  //   // //Deduction Details
  //   basicDed = basicDeductions.text.isNotEmpty ? double.parse(basicDeductions.text.toString()) : 0.0;
  //   intrestFromDeposit = interestFromDeposits.text.isNotEmpty ? double.parse(interestFromDeposits.text.toString()) : 0.0;
  //   medicalIns = medicalInsurance.text.isNotEmpty ? double.parse(medicalInsurance.text.toString()) : 0.0;
  //   charity = donationsToCharity.text.isNotEmpty ? double.parse(donationsToCharity.text.toString()) : 0.0;
  //   interestOnEdu = interstOnEducationalLoan.text.isNotEmpty ? double.parse(interstOnEducationalLoan.text.toString()) : 0.0;
  //   intrestOnHouseLone = interestOnHousingLoan.text.isNotEmpty ? double.parse(interestOnHousingLoan.text.toString()) : 0.0;
  //   nps = employeeContributionToNPS.text.isNotEmpty ? double.parse(employeeContributionToNPS.text.toString()) : 0.0;

  //   print("update totalIncome working");
  //   totalIncomegetx.value = ifs + ifi + oi + (rir * 70 / 100) + exemption + interstPaidOnHome as double;
  //   print(totalIncomegetx.value.toString());
  // }

  // void updateTotalDeduction() {
  //   ifs = incomeFromSalary.text.isNotEmpty ? double.parse(incomeFromSalary.text.toString()) : 0.0; //Interest from Salary
  //   ifi = incomeFromInterest.text.isNotEmpty ? double.parse(incomeFromInterest.text.toString()) : 0.0; // Income  from Interest
  //   oi = otherIncome.text.isNotEmpty ? double.parse(otherIncome.text.toString()) : 0.0; // Other Income
  //   rir = rentalIncomeReceived.text.isNotEmpty ? double.parse(rentalIncomeReceived.text.toString()) : 0.0; // Rental Income Received
  //   exemption = exemptionsAndDeductions.text.isNotEmpty ? double.parse(exemptionsAndDeductions.text.toString()) : 0.0; //Exemptions & Deductions
  //   interstPaidOnHome =
  //       interestPaidOnHomeLoan.text.isNotEmpty ? double.parse(interestPaidOnHomeLoan.text.toString()) : 0.0; // Interest paid on Home Loan

  //   // //Deduction Details
  //   basicDed = basicDeductions.text.isNotEmpty ? double.parse(basicDeductions.text.toString()) : 0.0;
  //   intrestFromDeposit = interestFromDeposits.text.isNotEmpty ? double.parse(interestFromDeposits.text.toString()) : 0.0;
  //   medicalIns = medicalInsurance.text.isNotEmpty ? double.parse(medicalInsurance.text.toString()) : 0.0;
  //   charity = donationsToCharity.text.isNotEmpty ? double.parse(donationsToCharity.text.toString()) : 0.0;
  //   interestOnEdu = interstOnEducationalLoan.text.isNotEmpty ? double.parse(interstOnEducationalLoan.text.toString()) : 0.0;
  //   intrestOnHouseLone = interestOnHousingLoan.text.isNotEmpty ? double.parse(interestOnHousingLoan.text.toString()) : 0.0;
  //   nps = employeeContributionToNPS.text.isNotEmpty ? double.parse(employeeContributionToNPS.text.toString()) : 0.0;

  //   totalDeductiongetx.value = basicDed + intrestFromDeposit + medicalIns + charity + interestOnEdu + intrestOnHouseLone + nps;
  // }

  // double totalDeduct() {
  //   basicDed = basicDed <= 150000 ? basicDed : 150000;
  //   intrestFromDeposit = val2.value == 1
  //       ? intrestFromDeposit <= 10000
  //           ? intrestFromDeposit
  //           : 10000
  //       : intrestFromDeposit <= 50000
  //           ? intrestFromDeposit
  //           : 50000;
  //   medicalIns = val2.value == 1 //age <= 60
  //       ? medicalIns <= 75000
  //           ? medicalIns
  //           : 75000
  //       : medicalIns <= 100000
  //           ? medicalIns
  //           : 100000;
  //   charity = charity <= 2000 ? charity : 2000;
  //   // interestOnEdu = interestOnEdu <= 50000 ? interestOnEdu : 50000;
  //   interestOnEdu = interestOnEdu;

  //   intrestOnHouseLone = intrestOnHouseLone <= 150000 ? intrestOnHouseLone : 150000;
  //   nps = nps <= 50000
  //       ? ifs * 10 / 100 >= nps
  //           ? nps
  //           : ifs * 10 / 100
  //       : 50000;
  //   totalDeduction = basicDed + intrestFromDeposit + medicalIns + charity + interestOnEdu + intrestOnHouseLone + nps;

  //   // print(basicDed.toString() + '--basicDed--');
  //   // print(intrestFromDeposit.toString() + '--intrestFromDeposit--');
  //   // print(medicalIns.toString() + '--medicalIns--');
  //   // print(charity.toString() + '--charity--');
  //   // print(interestOnEdu.toString() + '--interestOnEdu--');
  //   // print(intrestOnHouseLone.toString() + '--intrestOnHouseLone--');
  //   // print(nps.toString() + '--nps--');

  //   return totalDeduction;
  // }

  // double totalTaxNew() {
  //   ifs = incomeFromSalary.text.isNotEmpty ? double.parse(incomeFromSalary.text.toString()) : 0.0; //Interest from Salary
  //   ifi = incomeFromInterest.text.isNotEmpty ? double.parse(incomeFromInterest.text.toString()) : 0.0; // Income  from Interest
  //   oi = otherIncome.text.isNotEmpty ? double.parse(otherIncome.text.toString()) : 0.0; // Other Income
  //   rir = rentalIncomeReceived.text.isNotEmpty ? double.parse(rentalIncomeReceived.text.toString()) : 0.0; // Rental Income Received
  //   exemption = exemptionsAndDeductions.text.isNotEmpty ? double.parse(exemptionsAndDeductions.text.toString()) : 0.0; //Exemptions & Deductions
  //   interstPaidOnHome =
  //       interestPaidOnHomeLoan.text.isNotEmpty ? double.parse(interestPaidOnHomeLoan.text.toString()) : 0.0; // Interest paid on Home Loan

  //   // //Deduction Details
  //   basicDed = basicDeductions.text.isNotEmpty ? double.parse(basicDeductions.text.toString()) : 0.0;
  //   intrestFromDeposit = interestFromDeposits.text.isNotEmpty ? double.parse(interestFromDeposits.text.toString()) : 0.0;
  //   medicalIns = medicalInsurance.text.isNotEmpty ? double.parse(medicalInsurance.text.toString()) : 0.0;
  //   charity = donationsToCharity.text.isNotEmpty ? double.parse(donationsToCharity.text.toString()) : 0.0;
  //   interestOnEdu = interstOnEducationalLoan.text.isNotEmpty ? double.parse(interstOnEducationalLoan.text.toString()) : 0.0;
  //   intrestOnHouseLone = interestOnHousingLoan.text.isNotEmpty ? double.parse(interestOnHousingLoan.text.toString()) : 0.0;
  //   nps = employeeContributionToNPS.text.isNotEmpty ? double.parse(employeeContributionToNPS.text.toString()) : 0.0;

  //   var tax = 0.0;
  //   //age > 0 && age <= 60
  //   if (true) {
  //     //val2.value == 1
  //     double taxableIncome = totalInc();
  //     if (taxableIncome <= 250000) {
  //       tax = 0.0;
  //     }
  //     if (taxableIncome > 250000 && taxableIncome <= 500000) {
  //       var a1 = taxableIncome - 250000;
  //       tax = a1 * 5 / 100;
  //       tax = tax + tax * 4 / 100;
  //     }
  //     if (taxableIncome > 500000 && taxableIncome <= 750000) {
  //       var a1 = taxableIncome - 500000;
  //       tax = 250000 * 5 / 100 + a1 * 10 / 100;
  //       tax = tax + tax * 4 / 100;
  //     }
  //     if (taxableIncome > 750000 && taxableIncome <= 1000000) {
  //       var a1 = taxableIncome - 750000;
  //       tax = 250000 * 5 / 100 + 250000 * 10 / 100 + a1 * 15 / 100;
  //       tax = tax + tax * 4 / 100;
  //     }
  //     if (taxableIncome > 1000000 && taxableIncome <= 1250000) {
  //       var a1 = taxableIncome - 1000000;
  //       tax = 250000 * 5 / 100 + 250000 * 10 / 100 + 250000 * 15 / 100 + a1 * 20 / 100;
  //       tax = tax + tax * 4 / 100;
  //     }
  //     if (taxableIncome > 1250000 && taxableIncome <= 1500000) {
  //       var a1 = taxableIncome - 1250000;
  //       tax = 250000 * 5 / 100 + 250000 * 10 / 100 + 250000 * 15 / 100 + 250000 * 20 / 100 + a1 * 25 / 100;
  //       tax = tax + tax * 4 / 100;
  //     }
  //     if (taxableIncome > 1500000) {
  //       var a1 = taxableIncome - 1500000;
  //       tax = 250000 * 5 / 100 + 250000 * 10 / 100 + 250000 * 15 / 100 + 250000 * 20 / 100 + 250000 * 25 / 100 + a1 * 30 / 100;
  //       tax = tax + tax * 4 / 100;
  //     }
  //   }
  //   // else if (val2.value == 2) //age > 60 && age <= 80
  //   // {
  //   //   double taxableIncome = totalInc();
  //   //   if (taxableIncome <= 300000) {
  //   //     tax = 0.0;
  //   //   }
  //   //   if (taxableIncome > 300000 && taxableIncome <= 500000) {
  //   //     var a1 = taxableIncome - 300000;
  //   //     tax = a1 * 5 / 100;
  //   //     tax = tax + tax * 4 / 100;
  //   //   }
  //   //   if (taxableIncome > 500000 && taxableIncome <= 1000000) {
  //   //     var a1 = taxableIncome - 500000;
  //   //     tax = 200000 * 5 / 100 + a1 * 10 / 100;
  //   //     tax = tax + tax * 4 / 100;
  //   //   }
  //   //   if (taxableIncome > 1000000) {
  //   //     var a1 = taxableIncome - 1000000;
  //   //     tax = 200000 * 5 / 100 + 500000 * 20 / 100 + a1 * 30 / 100;
  //   //     tax = tax + tax * 4 / 100;
  //   //   }
  //   // } else {
  //   //   double taxableIncome = totalInc();
  //   //   if (taxableIncome <= 500000) {
  //   //     tax = 0.0;
  //   //   }
  //   //   if (taxableIncome > 500000 && taxableIncome <= 1000000) {
  //   //     var a1 = taxableIncome - 500000;
  //   //     tax = a1 * 20 / 100;
  //   //     tax = tax + tax * 4 / 100;
  //   //   }
  //   //   if (taxableIncome > 1000000) {
  //   //     var a1 = taxableIncome - 1000000;
  //   //     tax = 500000 * 20 / 100 + a1 * 30 / 100;
  //   //     tax = tax + tax * 4 / 100;
  //   //   }
  //   // }

  //   return tax;
  // }

  double totalTaxOld() {
    var tax = 0.0;
    // age > 0 && age <= 60
    if (val2.value == 1) {
      double taxableIncome = totalInc() - getTotalDeduction();

      print(taxableIncome);
      if (taxableIncome <= 250000) {
        tax = 0.0;
      }
      if (taxableIncome > 250000 && taxableIncome <= 500000) {
        var a1 = taxableIncome - 250000;
        tax = a1 * 5 / 100;
        tax = tax + tax * 4 / 100;
      }
      if (taxableIncome > 500000 && taxableIncome <= 1000000) {
        var a1 = taxableIncome - 500000;
        tax = 250000 * 5 / 100 + a1 * 20 / 100;
        tax = tax + tax * 4 / 100;
      }
      if (taxableIncome > 1000000) {
        var a1 = taxableIncome - 1000000;
        tax = 250000 * 5 / 100 + 500000 * 20 / 100 + a1 * 30 / 100;
        tax = tax + tax * 4 / 100;
      }
    } else if (val2.value == 2) //age > 60 && age <= 80
    {
      double taxableIncome = totalInc() - getTotalDeduction();
      if (taxableIncome <= 300000) {
        tax = 0.0;
      }
      if (taxableIncome > 300000 && taxableIncome <= 500000) {
        var a1 = taxableIncome - 300000;
        tax = a1 * 5 / 100;
        tax = tax + tax * 4 / 100;
      }
      if (taxableIncome > 500000 && taxableIncome <= 1000000) {
        var a1 = taxableIncome - 500000;
        tax = 200000 * 5 / 100 + a1 * 20 / 100;
        tax = tax + tax * 4 / 100;
      }
      if (taxableIncome > 1000000) {
        var a1 = taxableIncome - 1000000;
        tax = 200000 * 5 / 100 + 500000 * 20 / 100 + a1 * 30 / 100;
        tax = tax + tax * 4 / 100;
      }
    } else {
      double taxableIncome = totalInc() - getTotalDeduction();
      if (taxableIncome <= 500000) {
        tax = 0.0;
      }
      if (taxableIncome > 500000 && taxableIncome <= 1000000) {
        var a1 = taxableIncome - 500000;
        tax = a1 * 20 / 100;
        tax = tax + tax * 4 / 100;
      }
      if (taxableIncome > 1000000) {
        var a1 = taxableIncome - 1000000;
        tax = 500000 * 20 / 100 + a1 * 30 / 100;
        tax = tax + tax * 4 / 100;
      }
    }
    double taxableIncome = totalInc() - getTotalDeduction();

    finalTax.value = taxableIncome <= 500000 ? 0.0 : tax;

    return taxableIncome <= 500000 ? 0.0 : tax;
  }

}
