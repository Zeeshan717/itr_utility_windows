// ignore_for_file: avoid_print, sized_box_for_whitespace, file_names

import 'package:accountx/app/modules/taxCalPage/taxController.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class TaxView extends StatefulWidget {
  const TaxView({Key? key}) : super(key: key);

  @override
  State<TaxView> createState() => _TaxViewState();
}

class _TaxViewState extends State<TaxView> {
  // const TaxView({ Key? key }) : super(key: key);
  final TaxController _taxController = Get.put(TaxController());
  GetStorage box=GetStorage();
  NumberFormat numFormat = NumberFormat.decimalPattern('hi');
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  @override
  void initState() {
    _taxController.incomeMasterDetailsCall();
    _taxController.apiDeductionDetailsCall();
    // _taxController.totalInc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    print("Width: " + screenWidth.toString());
    print("Height: " + screenHeight.toString());
    return Scaffold(
        resizeToAvoidBottomInset: false,
        // appBar: AppBar(
        //   backgroundColor: Color(0xff0B28BE),
        // ),
        backgroundColor: Colors.white,
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Container(
            margin: const EdgeInsets.only(top: 20),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            margin: const EdgeInsets.only(left: 40, top: 10),
                            child: const Text(
                              "Age",
                              style: TextStyle(fontSize: 13),
                            )),
                      ],
                    ),
                    Obx(
                      () => Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            width: 30,
                          ),
                          Row(
                            children: [
                              Radio(
                                value: 1,
                                groupValue: _taxController.val2.value,
                                onChanged: (value) {
                                  _taxController.val2.value = value as int;
                                },
                                activeColor: const Color(0xff0B28BE),
                              ),
                              const Text(
                                "0 to 60",
                                style: TextStyle(fontSize: 12),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                value: 2,
                                groupValue: _taxController.val2.value,
                                onChanged: (value) {
                                  _taxController.val2.value = value as int;
                                },
                                activeColor: const Color(0xff0B28BE),
                              ),
                              const Text("60 to 80", style: TextStyle(fontSize: 12))
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                value: 3,
                                groupValue: _taxController.val2.value,
                                onChanged: (value) {
                                  _taxController.val2.value = value as int;
                                },
                                activeColor: const Color(0xff0B28BE),
                              ),
                              const Text("80 & above", style: TextStyle(fontSize: 12))
                            ],
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      decoration: const BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10))),
                      width: MediaQuery.of(context).size.width,
                      child: Obx(() => Column(
                            children: [
                              Container(
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                                  color: Color(0xff1739E4),
                                ),
                                margin: const EdgeInsets.symmetric(horizontal: 5),
                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                width: MediaQuery.of(context).size.width,
                                height: 50,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Text(
                                      "Income Details",
                                      style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Obx(
                                          () => Text(
                                            "Total Income:${_taxController.totalIncomegetx.value}",
                                            style: const TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        InkWell(
                                            onTap: () {
                                              _taxController.isVisible.value = !_taxController.isVisible.value;
                                            },
                                            child: const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.white,
                                            ))
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Visibility(
                                visible: _taxController.isVisible.value ? true : false,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                                      border: Border.all(
                                        width: 1,
                                        color: const Color(0xffC1CDFF),
                                      )),
                                  margin: const EdgeInsets.symmetric(horizontal: 10),
                                  padding: const EdgeInsets.symmetric(horizontal: 10),
                                  width: MediaQuery.of(context).size.width,
                                  child: Column(
                                    children: [
                                      //1
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 5),
                                        child: Row(
                                          children: [
                                            Flexible(
                                                child: SizedBox(
                                              height: 40,
                                              child: TextField(
                                                keyboardType: TextInputType.number,
                                                readOnly: true,
                                                controller: _taxController.salaryCntroller,
                                                decoration: InputDecoration(
                                                    enabledBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    focusedBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    labelText: "Salary",
                                                    labelStyle: const TextStyle(fontSize: 13)),
                                              ),
                                            )),
                                            const SizedBox(
                                              width: 20,
                                            ),
                                            Flexible(
                                                child: SizedBox(
                                              height: 40,
                                              child: TextField(
                                                keyboardType: TextInputType.number,
                                                readOnly: true,
                                                controller: _taxController.hpCntroller,
                                                decoration: InputDecoration(
                                                    enabledBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    focusedBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    labelText: "HP",
                                                    labelStyle: const TextStyle(fontSize: 13)),
                                              ),
                                            )),
                                          ],
                                        ),
                                      ),
                                      //2
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 5),
                                        child: Row(
                                          children: [
                                            Flexible(
                                                child: SizedBox(
                                              height: 40,
                                              child: TextField(
                                                keyboardType: TextInputType.number,
                                                readOnly: true,
                                                controller: _taxController.capitalCntroller,
                                                decoration: InputDecoration(
                                                    enabledBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    focusedBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    labelText: "Capital",
                                                    labelStyle: const TextStyle(fontSize: 13)),
                                              ),
                                            )),
                                            const SizedBox(
                                              width: 20,
                                            ),
                                            Flexible(
                                                child: SizedBox(
                                              height: 40,
                                              child: TextField(
                                                keyboardType: TextInputType.number,
                                                readOnly: true,
                                                controller: _taxController.bAndPController,
                                                decoration: InputDecoration(
                                                    enabledBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    focusedBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    labelText: "B&P",
                                                    labelStyle: const TextStyle(fontSize: 13)),
                                              ),
                                            )),
                                          ],
                                        ),
                                      ),
                                      //3
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 5),
                                        child: Row(
                                          children: [
                                            Flexible(
                                                child: SizedBox(
                                              height: 40,
                                              child: TextField(
                                                keyboardType: TextInputType.number,
                                                readOnly: true,
                                                controller: _taxController.otherSController,
                                                decoration: InputDecoration(
                                                    enabledBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    focusedBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    labelText: "Other",
                                                    labelStyle: const TextStyle(fontSize: 13)),
                                              ),
                                            )),
                                            const SizedBox(
                                              width: 20,
                                            ),
                                            Flexible(
                                                child: SizedBox(
                                              height: 40,
                                              child: TextField(
                                                keyboardType: TextInputType.number,
                                                readOnly: true,
                                                controller: _taxController.agricultureController,
                                                decoration: InputDecoration(
                                                    enabledBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    focusedBorder: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(5),
                                                        borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                                    labelText: "Agriculture",
                                                    labelStyle: const TextStyle(fontSize: 13)),
                                              ),
                                            )),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 50,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: const Color(0xff1739E4)),
                      margin: const EdgeInsets.symmetric(horizontal: 5),
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Text(
                            "Deduction Details",
                            style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                          ),
                          Obx(
                            () => Text(
                              "Total Deduction: ${_taxController.totalDeductiongetx.value}",
                              style:  TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: [
                          //1
                          Row(
                            children: [
                              Flexible(
                                child: Row(
                                  children: [
                                    Flexible(
                                        child: SizedBox(
                                      height: 40,
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        controller: _taxController.cntroller_80c,
                                        decoration: InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(5),
                                              borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                          focusedBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(5),
                                              borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                          labelText: "80C",
                                          labelStyle: const TextStyle(fontSize: 13),
                                        ),
                                        onChanged: (value) {
                                          _taxController.getTotalDeduction();
                                        },
                                      ),
                                    )),
                                    SizedBox(width: 5,),
                                    InkWell(
                                        onTap: () {
                                          showDialog(
                                              context: context,
                                              builder: (context) {
                                                return AlertDialog(
                                                  insetPadding: const EdgeInsets.only(top: 220),
                                                  contentPadding: EdgeInsets.zero,
                                                  shape: const RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
                                                  title: const Center(child: Text("80C")),
                                                  content: SingleChildScrollView(
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          height: MediaQuery.of(context).size.height - 300,
                                                          child: Column(
                                                            children: [
                                                              const SizedBox(
                                                                height: 10,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 20),
                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                  children: const [],
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                height: 5,
                                                              ),

                                                              //1
                                                              const SizedBox(
                                                                height: 20,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.epfController,
                                                                        decoration: InputDecoration(
                                                                            labelText: "EPF,PF",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                    const SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.licController,
                                                                        decoration: InputDecoration(
                                                                            labelText: "LIC",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                  ],
                                                                ),
                                                              ),
                                                              //2
                                                              const SizedBox(
                                                                height: 20,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.nationalSavingController,
                                                                        decoration: InputDecoration(
                                                                            labelText: "National Saving..",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                    const SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.tutionFeeForChildernController,
                                                                        decoration: InputDecoration(
                                                                            labelText: "Tution Fee for Children",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                  ],
                                                                ),
                                                              ),
                                                              //3
                                                              const SizedBox(
                                                                height: 20,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.seniorCitizenSavingController,
                                                                        decoration: InputDecoration(
                                                                            labelText: "Senior Citizen Saving..",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                    const SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.repaymentOfPrincipal,
                                                                        decoration: InputDecoration(
                                                                            labelText: "Repayment Of Principal Amt.",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                  ],
                                                                ),
                                                                //4
                                                              ),
                                                              //4
                                                              const SizedBox(
                                                                height: 20,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.sukanyaSamridhiSchemeController,
                                                                        decoration: InputDecoration(
                                                                            labelText: "Sukanya Samridhi Scheme",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                    const SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.registraionFees,
                                                                        decoration: InputDecoration(
                                                                            labelText: "Registration Fees ",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                  ],
                                                                ),
                                                                //4
                                                              ),
                                                              const SizedBox(
                                                                height: 20,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                      child: SizedBox(
                                                                        width: MediaQuery.of(context).size.width,
                                                                        height: 40,
                                                                        child: ElevatedButton(
                                                                          style: ElevatedButton.styleFrom(
                                                                              primary: Colors.white,
                                                                              shape: RoundedRectangleBorder(
                                                                                  borderRadius: BorderRadius.circular(5),
                                                                                  side: const BorderSide(color: Color(0xff1638E1)))),
                                                                          onPressed: () {
                                                                            Get.back();
                                                                          },
                                                                          child: SvgPicture.asset(
                                                                            "images/clear.svg",
                                                                            width: 30,
                                                                            
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    Flexible(
                                                                      child: SizedBox(
                                                                        width: MediaQuery.of(context).size.width,
                                                                        height: 40,
                                                                        child: ElevatedButton(
                                                                          style: ElevatedButton.styleFrom(primary: const Color(0xff1739E4)),
                                                                          onPressed: () {
                                                                            _taxController.total80c();
                                                                            _taxController.getTotalDeduction();
                                                                            Get.back();
                                                                            print(' new working');
                                                                          },
                                                                          child: SvgPicture.asset(
                                                                            "images/submit.svg",
                                                                            width: 30,
                                                                            
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              });
                                        },
                                        child: SvgPicture.asset("images/info.svg",
                                        width: 20,
                                        )
                                    )
                                  ],
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                child: Row(
                                  children: [
                                    Flexible(
                                        child: SizedBox(
                                      height: 40,
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        controller: _taxController.cntroller_80ccd,
                                        decoration: InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(5),
                                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                            focusedBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(5),
                                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                            labelText: "80CCD",
                                            labelStyle: const TextStyle(fontSize: 13)),
                                        onChanged: (value) {
                                          _taxController.getTotalDeduction();
                                        },
                                      ),
                                    )),
                                    SizedBox(width: 5,),
                                    InkWell(
                                      onTap: null,
                                      child: SvgPicture.asset("",
                                        width: 20,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          //2
                          Row(
                            children: [
                              Flexible(
                                child: Row(
                                  children: [
                                    Flexible(
                                        child: SizedBox(
                                      height: 40,
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        controller: _taxController.cntroller_80d,
                                        decoration: InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(5),
                                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                            focusedBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(5),
                                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                            labelText: "80D",
                                            labelStyle: const TextStyle(fontSize: 13)),
                                        onChanged: (value) {
                                          _taxController.getTotalDeduction();
                                        },
                                      ),
                                    )),
                                    SizedBox(width: 5,),
                                    InkWell(
                                        onTap: () {
                                          showDialog(
                                              context: context,
                                              builder: (context) {
                                                return AlertDialog(
                                                  insetPadding: const EdgeInsets.only(top: 300),
                                                  contentPadding: EdgeInsets.zero,
                                                  shape: const RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
                                                  title: const Center(child: Text("80D")),
                                                  content: SingleChildScrollView(
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          height: MediaQuery.of(context).size.height - 300,
                                                          child: Column(
                                                            children: [
                                                              //1
                                                              const SizedBox(
                                                                height: 20,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.selfController80D,
                                                                        decoration: InputDecoration(
                                                                            labelText: "Self",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                  ],
                                                                ),
                                                              ),
                                                              //2
                                                              const SizedBox(
                                                                height: 20,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.parentsController80D,
                                                                        decoration: InputDecoration(
                                                                            labelText: "Parents",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                  ],
                                                                ),
                                                              ),

                                                              const SizedBox(
                                                                height: 10,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                      child: SizedBox(
                                                                        width: MediaQuery.of(context).size.width,
                                                                        height: 40,
                                                                        child: ElevatedButton(
                                                                          style: ElevatedButton.styleFrom(
                                                                              primary: Colors.white,
                                                                              shape: RoundedRectangleBorder(
                                                                                  borderRadius: BorderRadius.circular(5),
                                                                                  side: const BorderSide(color: Color(0xff1638E1)))),
                                                                          onPressed: () {
                                                                            Get.back();
                                                                          },
                                                                          child: SvgPicture.asset(
                                                                            "images/clear.svg",
                                                                            width: 30,
                                                                            
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    Flexible(
                                                                      child: SizedBox(
                                                                        width: MediaQuery.of(context).size.width,
                                                                        height: 40,
                                                                        child: ElevatedButton(
                                                                          style: ElevatedButton.styleFrom(primary: const Color(0xff1739E4)),
                                                                          onPressed: () {
                                                                            print('working');
                                                                            _taxController.total80d();
                                                                            _taxController.getTotalDeduction();
                                                                            Get.back();
                                                                          },
                                                                          child: SvgPicture.asset(
                                                                            "images/submit.svg",
                                                                            width: 30,
                                                                            
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                height: 10,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              });
                                        },
                                        child: SvgPicture.asset
                                          ("images/info.svg",
                                          width: 20,
                                        )
                                    )
                                  ],
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                child: Row(
                                  children: [
                                    Flexible(
                                        child: SizedBox(
                                      height: 40,
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        controller: _taxController.cntroller_80g,
                                        maxLength: 10,
                                        decoration: InputDecoration(
                                            counterText: "",
                                            enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(5),
                                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                            focusedBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(5),
                                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                            labelText: "80G",
                                            labelStyle: const TextStyle(fontSize: 13)),
                                        onChanged: (value) {
                                          _taxController.getTotalDeduction();
                                        },
                                      ),
                                    )),
                                    SizedBox(width: 5,),
                                    InkWell(
                                        onTap: () {
                                          showDialog(
                                              context: context,
                                              builder: (context) {
                                                return AlertDialog(
                                                  insetPadding: const EdgeInsets.only(top: 300),
                                                  contentPadding: EdgeInsets.zero,
                                                  shape: const RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
                                                  title: const Center(child: Text("80G")),
                                                  content: SingleChildScrollView(
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          height: MediaQuery.of(context).size.height - 300,
                                                          child: Column(
                                                            children: [
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 4),
                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: [
                                                                    Obx(
                                                                      () => Row(
                                                                        children: [
                                                                          Radio(
                                                                            value: 1,
                                                                            groupValue: _taxController.val80g.value,
                                                                            onChanged: (value) {
                                                                              _taxController.val80g.value = value as int;
                                                                            },
                                                                            activeColor: const Color(0xff0B28BE),
                                                                          ),
                                                                          const Text(
                                                                            "100 %",
                                                                            style: TextStyle(fontSize: 12),
                                                                          )
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Obx(
                                                                      () => Row(
                                                                        children: [
                                                                          Radio(
                                                                            value: 2,
                                                                            groupValue: _taxController.val80g.value,
                                                                            onChanged: (value) {
                                                                              _taxController.val80g.value = value as int;
                                                                            },
                                                                            activeColor: const Color(0xff0B28BE),
                                                                          ),
                                                                          const Text("50%", style: TextStyle(fontSize: 12))
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              //2
                                                              const SizedBox(
                                                                height: 20,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.donationOtherByChequeController,
                                                                        decoration: InputDecoration(
                                                                            labelText: "Other(By Cheque)",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                    const SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    Flexible(
                                                                        child: SizedBox(
                                                                      height: 40,
                                                                      child: TextField(
                                                                        keyboardType: TextInputType.number,
                                                                        controller: _taxController.donationInCashController,
                                                                        decoration: InputDecoration(
                                                                            labelText: "In Cash",
                                                                            labelStyle: const TextStyle(
                                                                              fontSize: 13,
                                                                            ),
                                                                            enabledBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF))),
                                                                            focusedBorder: OutlineInputBorder(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                borderSide: const BorderSide(color: Color(0xffC1CDFF)))),
                                                                      ),
                                                                    )),
                                                                  ],
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                height: 30,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                                child: Row(
                                                                  children: [
                                                                    Flexible(
                                                                      child: SizedBox(
                                                                        width: MediaQuery.of(context).size.width,
                                                                        height: 40,
                                                                        child: ElevatedButton(
                                                                          style: ElevatedButton.styleFrom(
                                                                              primary: Colors.white,
                                                                              shape: RoundedRectangleBorder(
                                                                                  borderRadius: BorderRadius.circular(5),
                                                                                  side: const BorderSide(color: Color(0xff1638E1)))),
                                                                          onPressed: () {
                                                                            Get.back();
                                                                          },
                                                                          child: SvgPicture.asset(
                                                                            "images/clear.svg",
                                                                            width: 30,
                                                                            
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    Flexible(
                                                                      child: SizedBox(
                                                                        width: MediaQuery.of(context).size.width,
                                                                        height: 40,
                                                                        child: ElevatedButton(
                                                                          style: ElevatedButton.styleFrom(primary: const Color(0xff1739E4)),
                                                                          onPressed: () {
                                                                            _taxController.total80g();
                                                                            _taxController.getTotalDeduction();
                                                                            Get.back();
                                                                          },
                                                                          child: SvgPicture.asset(
                                                                            "images/submit.svg",
                                                                            width: 30,
                                                                            
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              });
                                        },
                                        child: SvgPicture.asset("images/info.svg",
                                          width: 20,
                                        )
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          //3
                          Row(
                            children: [
                              Flexible(
                                child: Row(
                                  children: [
                                    Flexible(
                                        child: SizedBox(
                                      height: 40,
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        controller: _taxController.cntroller_80tta,
                                        decoration: InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(5),
                                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                            focusedBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(5),
                                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                            labelText: "80TTA",
                                            labelStyle: const TextStyle(fontSize: 13)),
                                        onChanged: (value) {
                                          _taxController.getTotalDeduction();
                                        },
                                      ),
                                    )),
                                    SizedBox(width: 5,),
                                    SvgPicture.asset("",
                                      width: 20,
                                    )
                                  ],
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                child: Row(
                                  children: [
                                    Flexible(
                                        child: SizedBox(
                                      height: 40,
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        controller: _taxController.othterDController,
                                        decoration: InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(5),
                                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                            focusedBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(5),
                                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                            labelText: "Other",
                                            labelStyle: const TextStyle(fontSize: 13)),
                                        onChanged: (value) {
                                          _taxController.getTotalDeduction();
                                        },
                                      ),
                                    )),
                                    SizedBox(width: 5,),
                                    SvgPicture.asset("",
                                      width: 20,
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Text(
                                      "Tax Payable",
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                                    ),
                                    Obx(
                                      () => Text(
                                        "₹ " + numFormat.format(_taxController.finalTax.value),
                                        style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Flexible(
                                      child: SizedBox(
                                        width: MediaQuery.of(context).size.width,
                                        height: 40,
                                        child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            primary: const Color(0xff1638E1),
                                          ),
                                          onPressed: () {
                                            _taxController.totalTaxOld();

                                            box.write("deductionvalue", _taxController.totalDeductiongetx.value.toString() );
                                            box.write("taxablevalue", _taxController.finalTax.value.toString() );


                                              //   print("--------ddddddd-------"+_taxController.salaryCntroller.text);

                                            // _taxController.taxtotaldeductcontroller.text=_taxController.getTotalDeduction().toString();
                                            // print("de------------"+ _taxController.taxtotaldeductcontroller.text);
                                          },
                                          child: const Text(
                                            "CALCULATE",
                                            style: TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
