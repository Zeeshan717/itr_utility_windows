// ignore_for_file: non_constant_identifier_names

import 'dart:io';

import 'package:accountx/app/modules/homePage/homeView.dart';
import 'package:accountx/app/modules/investment_page/investment_details.dart';
import 'package:accountx/app/services/api.dart';
import 'package:date_format/date_format.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';

import '../../provider/investment_provider.dart';
import 'investment_view.dart';

class InvestmentController extends GetxController {
  GetStorage box = GetStorage();
  String? dropDownValue;
  TextEditingController nameController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController amountController = TextEditingController();
  TextEditingController remarkController = TextEditingController();
  File? pdf_file;

  String? editDropDownValue;
  TextEditingController editNameController = TextEditingController();
  TextEditingController editDateController = TextEditingController();
  TextEditingController editAmountController = TextEditingController();
  TextEditingController editRemarkController = TextEditingController();

  @override
  void onInit() {
    dateController.text = formatDate(DateTime(DateTime.now().year - 1, 4, 1), [yyyy, '-', mm, '-', dd]);
    super.onInit();
  }

  Future uploadPdf() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
    );

    if (result != null) {
      pdf_file = File(result.files.single.path.toString());
    } else {
      // User canceled the picker
    }
  }

//-----------insert Investment Api Call ------------------------
 Future<void> insertInvestmentApiCall(BuildContext context) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    var pdfFileTemp;
    if(pdf_file==null){
      pdfFileTemp= "";
    }else{
      pdfFileTemp=pdf_file!.path.toString();
    }
    EasyLoading.show(status: "Loading...");
    await Api.insertInvestmentApi(
        context,
        clientId, clientName, adminId, financialYear, dropDownValue!, nameController.text, dateController.text,
            pdfFileTemp, amountController.text, remarkController.text)

        .then((value) async{
      if (value["response_status"] == "1") {
        EasyLoading.dismiss();
        await Get.defaultDialog(
          barrierDismissible: false,
          content: Text(value["message"]),
          actions: [
            TextButton(onPressed: () {
              nameController.clear();
              amountController.clear();
              remarkController.clear();
              pdf_file = null;
              Get.back();
            }, child: Text("OK"))
          ]
        );
        Provider.of<InvestmentProvider>(context,listen: false).updateValue();

      } else {
        EasyLoading.dismiss();
        Get.defaultDialog(
            barrierDismissible: false,
            content: Text(value["message"]),
            actions: [
              TextButton(onPressed: () {
                Get.back();
              }, child: Text("OK"))
            ]
        );
      }
    });
  }

  //--------------Get Investment Api Call---------------------
  Future getInvestmentApiCall(int index) async {
    var clientId = box.read("client_id");
    var adminId = box.read("admin_id");
    var financialYear = box.read("finacial_year");
    Api.getInvestmentApi2(clientId.toString(), financialYear.toString(), adminId.toString()).then((value) {
      print("Working");
      if(value["response_status"].toString() == "1") {
        editDropDownValue = value["response_investment_list"][index]["investment_type"];
        editNameController.text = value["response_investment_list"][index]["name"];
        editDateController.text = value["response_investment_list"][index]["date"];
        editAmountController.text = value["response_investment_list"][index]["amount"];
        editRemarkController.text = value["response_investment_list"][index]["remark"];
      }

    });
  }

  //----------------Edit Investment Api Call ------------------

Future updateInvestmentApiCall(BuildContext context,String investmentId) async {
  var clientId = box.read("client_id");
  var clientName = box.read("client_name");
  var adminId = box.read("admin_id");
  var financialYear = box.read("finacial_year");
   print("Working (updateInvestmentApiCall)");
   var pdfFileTemp;
   if(pdf_file==null || pdf_file==""){
     pdfFileTemp="";
   }else{
     pdfFileTemp=pdf_file!.path.toString();
   }

  Api.editInvestmentApi(clientId.toString(), clientName.toString(), adminId.toString(),
      financialYear.toString(), editDropDownValue.toString(),
      editNameController.text, editDateController.text, pdfFileTemp,
       editAmountController.text,
      editRemarkController.text, investmentId).then((value) {
        if(value["response_status"].toString() == "1") {
          EasyLoading.dismiss();
          Get.back();
          Provider.of<InvestmentProvider>(context,listen: false).updateValue();
          //Get.to(HomeView());
         // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(value["message"])));
        } else {
          EasyLoading.dismiss();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(value["message"])));
        }
   });

}

}
