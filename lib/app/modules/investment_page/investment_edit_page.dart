import 'package:accountx/app/modules/investment_page/investment_controller.dart';
import 'package:accountx/app/modules/investment_page/investment_details.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class InvestmentEditView extends StatefulWidget {
  InvestmentEditView(
      {Key? key,
      required this.currentindex,
      required this.investmentId,
      required this.investmentType,
      required this.name,
      required this.date,
      required this.amount,
      required this.remark})
      : super(key: key);
  int currentindex;
  String investmentId;
  String investmentType;
  String name;
  String date;
  String amount;
  String remark;

  @override
  State<InvestmentEditView> createState() => _InvestmentEditViewState();
}

class _InvestmentEditViewState extends State<InvestmentEditView> {
  var controller = Get.put(InvestmentController());
  DateTime? now;

  @override
  void initState() {
    controller.getInvestmentApiCall(widget.currentindex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff1739E4),
        centerTitle: true,
        title: const Text(
          "Investment Edit",
          style: TextStyle(fontSize: 16, color: Colors.white),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              //Investment Type
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Investment Type",
                    style: TextStyle(color: Color(0xff656A87), fontSize: 16),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                            color: const Color(0xffC1CDFF), width: 1)),
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    width: MediaQuery.of(context).size.width,
                    height: 40,
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: controller.editDropDownValue,
                      icon: SvgPicture.asset(
                        "images/dropdownicon.svg",
                        width: 30,
                      ),
                      elevation: 16,
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                      ),
                      underline: const SizedBox(),
                      onChanged: (String? newValue) {
                        setState(() {
                          controller.editDropDownValue = newValue!;
                        });
                      },
                      items: <String>[
                        "Short Term Investment",
                        "Long Term Investment"
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                ],
              ),
              //Name
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Name",
                    style: TextStyle(color: Color(0xff656A87), fontSize: 16),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.editNameController,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: const BorderSide(
                                width: 1, color: Color(0xffC1CDFF))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: const BorderSide(
                                width: 1, color: Color(0xffC1CDFF))),
                      ),
                    ),
                  )
                ],
              ),
              //Date
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Date",
                    style: TextStyle(color: Color(0xff656A87), fontSize: 16),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  InkWell(
                    onTap: () {
                      editDateSelected(context);
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                              width: 1, color: const Color(0xffC1CDFF))),
                      height: 40,
                      child: TextFormField(
                        keyboardType: TextInputType.datetime,
                        style: const TextStyle(fontSize: 13),
                        enabled: false,
                        controller: controller.editDateController,
                        decoration: InputDecoration(
                          suffixIcon: SvgPicture.asset(
                            "images/calender.svg",
                            width: 30,
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: const BorderSide(
                                  width: 1, color: Color(0xffC1CDFF))),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: const BorderSide(
                                  width: 1, color: Color(0xffC1CDFF))),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              //Amount
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Amount",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.editAmountController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: const BorderSide(
                                width: 1, color: Color(0xffC1CDFF))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: const BorderSide(
                                width: 1, color: Color(0xffC1CDFF))),
                      ),
                    ),
                  )
                ],
              ),
              //Remark
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Remark",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.editRemarkController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: const BorderSide(
                                width: 1, color: Color(0xffC1CDFF))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: const BorderSide(
                                width: 1, color: Color(0xffC1CDFF))),
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              //Attachment
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    "Attachment",
                    style: TextStyle(fontSize: 16, color: Color(0xffC1CDFF)),
                  ),
                  InkWell(
                    onTap: () {
                      controller.uploadPdf();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: const Color(0xff1739E4),
                          borderRadius: BorderRadius.circular(5)),
                      height: 40,
                      child: Row(
                        children: const [
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Upload",
                            style: TextStyle(color: Colors.white),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.cloud_upload,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),

              const SizedBox(
                height: 50,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 50,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: const Color(0xff1739E4),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5))),
                    onPressed: () {
                      if (controller.editDropDownValue != "" &&
                          controller.editAmountController.text.isNotEmpty &&
                          controller.editNameController.text.isNotEmpty) {
                        EasyLoading.show();
                        controller.updateInvestmentApiCall(
                            context, widget.investmentId);
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text("Fields are empty !!")));
                      }
                    },
                    child: const Text("Update")),
              )
            ],
          ),
        ),
      ),
    );
  }

  editDateSelected(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: DateTime(DateTime.now().year - 1, 4, 1),
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 31));
    if (now != null) {
      controller.editDateController.text =
          formatDate(now!, [yyyy, '-', mm, '-', dd]);
    }
  }
}
