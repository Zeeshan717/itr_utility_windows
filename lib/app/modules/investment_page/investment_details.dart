import 'package:accountx/app/model/investment_model.dart';
import 'package:accountx/app/modules/investment_page/investment_edit_page.dart';
import 'package:accountx/app/modules/pdf_view.dart';
import 'package:accountx/app/provider/investment_provider.dart';
import 'package:accountx/app/services/api.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';

class InvestmentDetails extends StatefulWidget {
  const InvestmentDetails({Key? key}) : super(key: key);

  @override
  State<InvestmentDetails> createState() => _InvestmentDetailsState();
}

class _InvestmentDetailsState extends State<InvestmentDetails> {

  GetStorage storage = GetStorage();
  Future<InvestmentModel>? investmentMode;
  DownloadNOpen pdfOpen = DownloadNOpen();

  initState(){
    Future<InvestmentModel> investmentMode = Api.getInvestmentApi(storage.read("client_id").toString(),
        storage.read("finacial_year").toString(), storage.read("admin_id").toString());
  //  Provider.of<InvestmentProvider>(context).updateValue();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white24,
        title: Text("Investment Details",style: TextStyle(fontSize: 16,
        color: Colors.black),),
        centerTitle: true,
      ),
      body: Consumer<InvestmentProvider>(
        builder:(context,investmentProvider,child)=> FutureBuilder<InvestmentModel>(
          future: investmentProvider.investmentModel,
           builder: (context,snapshot){
             if(snapshot.hasData) {
               EasyLoading.dismiss();
               return SingleChildScrollView(
                 child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.white,
          child: Column(children: [
            DataTable2(
              columnSpacing: 20,
              horizontalMargin: 20,
              minWidth: 800,
              columns: const [  DataColumn2(
                  label: Center(child: Text('Financial \nYear',textAlign: TextAlign.center,)),
              ),
              DataColumn(
                  label: Center(child: Text('Investment \nType',textAlign: TextAlign.center,)),
              ),
                 DataColumn(
                  label: Center(child: Text('Name \n',textAlign: TextAlign.center,)),
              ),
              DataColumn(
                  label: Center(child: Text('Date \n',textAlign: TextAlign.center,)),
              ),
                  DataColumn(
                  label: Center(child: Text('Amount \n',textAlign: TextAlign.center,)),
              ),
              DataColumn(
                  label: Center(child: Text('Remark \n',textAlign: TextAlign.center,)),
              ),
                   DataColumn(
                  label: Center(child: Text('Attachment \n',textAlign: TextAlign.center,)),
              ),
                  DataColumn(
                      label: Center(child: Text('Edit \n'))),
                DataColumn(label: Center(child: Text('Delete \n',
                  textAlign: TextAlign.center,)))
              ], rows: List.generate(snapshot.data!.responseInvestmentList.length,
                 (index) => DataRow(
                   cells: [
                 DataCell(Center(
                       child: Text(snapshot.data!.responseInvestmentList[index].financialYear,
                       style: TextStyle(fontSize: 12),
                       textAlign: TextAlign.center,
                       ),
                     )),
                     DataCell(Center(
                       child: Text(snapshot.data!.responseInvestmentList[index].investmentType,
                       style: TextStyle(fontSize: 12),
                       textAlign: TextAlign.center,
                       ),
                     )),
                     DataCell(Center(
                       child: Text(snapshot.data!.responseInvestmentList[index].name,
                       style: TextStyle(fontSize: 12),
                       ),
                     )),
                     DataCell(Center(
                       child: Text(snapshot.data!.responseInvestmentList[index]
                       .date.toString().split(" ")[0],
                       style: TextStyle(fontSize: 12),
                       textAlign: TextAlign.center,
                       ),
                     )),
                     DataCell(Text(snapshot.data!.responseInvestmentList[index].amount,
                     style: TextStyle(fontSize: 12),
                       textAlign: TextAlign.center,
                     )),
                     DataCell(Center(
                       child: Text(snapshot.data!.responseInvestmentList[index].remark,
                       style: TextStyle(fontSize: 12),
                       textAlign: TextAlign.center,
                       ),
                     )),
                       DataCell(Center(
                       child: TextButton(onPressed: ()  {
                         pdfOpen.openFile(url: "https://appexperts.net/btconnect/${snapshot.data!
                         .responseInvestmentList[index].attachment.toString()}",
                          fileName: "${storage.read("client_name")}_investment_details.pdf");
                       }, child: snapshot.data!
                           .responseInvestmentList[index].attachment.toString().contains(".pdf")? const Text("View PDF",
                         style: TextStyle(fontSize: 12),
                         textAlign: TextAlign.center,
                       ):Text("")),
                     )),
                     DataCell(Center(
                       child: InkWell(
                           onTap: () {
                             Get.to(InvestmentEditView(
                               currentindex: index,
                               investmentId: snapshot.data!.responseInvestmentList[index].investmentId,
                               investmentType: snapshot.data!.responseInvestmentList[index].investmentType,
                               name: snapshot.data!.responseInvestmentList[index].name,
                               date: snapshot.data!.responseInvestmentList[index].date.toString().split(" ")[0],
                               amount: snapshot.data!.responseInvestmentList[index].remark,
                               remark: snapshot.data!.responseInvestmentList[index].remark,
                             ));
                           },
                           child: SvgPicture.asset("images/pencil.svg",width: 20,))
                     )),
                     DataCell(Center(
                       child: InkWell(
                           onTap:()async{
                             showDialog(context: context, builder: (context)=>Center(
                               child: Container(
                                   width: 50,
                                   height:50,
                                   child: CircularProgressIndicator()),
                             ));
                             await Api.deleteDepreciationEntryApi("Investment",snapshot.data!.responseInvestmentList[index].investmentId.toString()).then((value){
                               print("---delete Response----"+value.toString());
                               Get.back();
                               if(value["response_status"].toString() == "1"){
                                 Provider.of<InvestmentProvider>(context,listen: false).updateValue();
                               }

                             });
                           },
                           child: Icon(Icons.delete)),
                     )),
                   ]
                 )))
          ]),
        ),
               );
             }
             else if(snapshot.connectionState == ConnectionState.waiting) {
               EasyLoading.show(status: "Loading...");
               return Container(alignment: Alignment.center,);
             } else {
               EasyLoading.dismiss();
               return Container(
                 alignment: Alignment.center,
                 width: MediaQuery.of(context).size.width,
                 height: MediaQuery.of(context).size.height,
                 child: Text("Data Not Available !!"),
               );
             }
           },
        ),
      ) );
  }
}