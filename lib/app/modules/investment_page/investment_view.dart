import 'dart:io';

import 'package:accountx/app/modules/investment_page/investment_controller.dart';
import 'package:accountx/app/modules/investment_page/investment_details.dart';
import 'package:date_format/date_format.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class InvestmentView extends StatefulWidget {
  const InvestmentView({Key? key}) : super(key: key);
  @override
  State<InvestmentView> createState() => _InvestmentViewState();
 void updateUi(){
   _InvestmentViewState().clearUiState();
 }
}

class _InvestmentViewState extends State<InvestmentView> {

  bool isSelected = false;
  var controller = Get.put(InvestmentController());
  DateTime? now;
  var ddValue = "";

  void clearUiState(){
    setState(() {
    });
  }

  @override
  void dispose() {
    controller.nameController.clear();
    controller.amountController.clear();
    controller.remarkController.clear();
    controller.pdf_file = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text(
          "Investment",
          style: TextStyle(fontSize: 16, color: Colors.black),
        ),
        actions: [
          Tooltip(
            message: 'Info',
            child: InkWell(
              onTap: () {
                setState((){
                  isSelected =!isSelected;
                });
              /*  Get.to(const InvestmentDetails());*/
              },
              child: isSelected ? Icon(Icons.home,color: Colors.black,) : Image.asset("images/history.png",width: 30,color: Colors.black,) ),
          ),
            SizedBox(width: 20,)
        ],
      ),
      body: isSelected ? InvestmentDetails() : Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              //Investment Type
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Investment Type",
                    style: TextStyle(color: Color(0xff656A87), fontSize: 16),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: const Color(0xffC1CDFF), width: 1)),
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    width: MediaQuery.of(context).size.width,
                    height: 40,
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: controller.dropDownValue,
                      icon: SvgPicture.asset(
                        "images/dropdownicon.svg",
                        width: 15,
                      ),
                      elevation: 16,
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                      ),
                      underline: const SizedBox(),
                      onChanged: (String? newValue) {
                        setState(() {
                          controller.dropDownValue = newValue!;
                        });
                        ddValue = newValue.toString();
                        print("DD Value : "+ddValue);
                      },
                      items: <String>["Short Term Investment", "Long Term Investment"].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                ],
              ),
              //Name
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Name",
                    style: TextStyle(color: Color(0xff656A87), fontSize: 16),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.nameController,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        enabledBorder:
                            OutlineInputBorder(borderRadius: BorderRadius.circular(5),
                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                        focusedBorder:
                            OutlineInputBorder(borderRadius: BorderRadius.circular(5),
                                borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                      ),
                    ),
                  )
                ],
              ),
              //Date
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Date",
                    style: TextStyle(color: Color(0xff656A87), fontSize: 16),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  InkWell(
                    onTap: () {
                      dateSelected(context);
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(width: 1, color: const Color(0xffC1CDFF))),
                      height: 40,
                      child: TextFormField(
                        keyboardType: TextInputType.datetime,
                        style: const TextStyle(fontSize: 13),
                        enabled: false,
                        controller: controller.dateController,
                        decoration: InputDecoration(
                          suffixIcon: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5),
                            child: SvgPicture.asset(
                              "images/calender.svg",
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Colors.transparent)),
                          enabledBorder:
                              OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                          focusedBorder:
                              OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              //Amount
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Amount",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.amountController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        enabledBorder:
                            OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                        focusedBorder:
                            OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                      ),
                    ),
                  )
                ],
              ),
              //Remark
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Remark",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.remarkController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        enabledBorder:
                            OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                        focusedBorder:
                            OutlineInputBorder(borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              //Attachment
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    "Attachment",
                    style: TextStyle(fontSize: 16, color: Color(0xffC1CDFF)),
                  ),
                  Row(
                    children: [
                      InkWell(
                        onTap: () async {
                          print("-----b-----------"+controller.pdf_file.toString());
                           await controller.uploadPdf().then((value) => setState((){}));
                          print("------a----------"+controller.pdf_file.toString());

                        },
                        child:
                        Container(
                          decoration: BoxDecoration(color: controller.pdf_file == null?Color(0xff1739E4):Colors.green, borderRadius: BorderRadius.circular(5)),
                          height: 40,
                          child: Row(
                            children:  [
                              SizedBox(
                                width: 10,
                              ),
                              controller.pdf_file == null?Text(
                                "Upload PDF",
                                style: TextStyle(color: Colors.white),
                              ):Text(
                                "Uploaded",
                                style: TextStyle(color: Colors.white,fontWeight: FontWeight.w500,fontSize: 16,letterSpacing: 0.5),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Icon(
                                controller.pdf_file == null? Icons.cloud_upload:Icons.done_outline,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 10,
                              )
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                        visible:controller.pdf_file != null ,
                        child: InkWell(
                          onTap: (){
                            controller.pdf_file = null;
                            setState(() {
                            });
                          },
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text("Clear File",style: TextStyle(
                          decoration: TextDecoration.underline,
                          letterSpacing: 0.6,
                          color: Colors.blueAccent,
                          fontSize: 16
                        ),),
                            )),
                      )
                    ],
                  )
                ],
              ),

              const SizedBox(
                height: 50,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 50,
                child: ElevatedButton(
                    style:
                        ElevatedButton.styleFrom(primary: const Color(0xff1739E4),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5))),
                    onPressed: () async{
                      if(ddValue.isEmpty) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Please select investment type !!")));
                      } else if(controller.nameController.text.isEmpty) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Name is required !!")));
                      } else if(controller.dateController.text.isEmpty) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Date is required !!")));
                      } else if(controller.amountController.text.isEmpty) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Amount is required !!")));
                      } else if(controller.remarkController.text.isEmpty) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Remark is required !!")));
                      } else {
                       await controller.insertInvestmentApiCall(context).then((value) => setState((){}));
                      }
                    },
                    child: const Text("Submit")),
              )
            ],
          ),
        ),
      ) ,
    );
  }

  dateSelected(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: DateTime(DateTime.now().year - 1, 4, 1),
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 31));
    if(now !=null) {
      controller.dateController.text = formatDate(now!, [yyyy, '-', mm, '-', dd]);
    }
  }
}
