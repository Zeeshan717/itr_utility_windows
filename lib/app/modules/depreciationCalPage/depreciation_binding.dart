import 'package:accountx/app/modules/depreciationCalPage/depreciation_controller.dart';
import 'package:get/get.dart';

class DepreciationBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<DepreciationController>(DepreciationController());
  }
}
