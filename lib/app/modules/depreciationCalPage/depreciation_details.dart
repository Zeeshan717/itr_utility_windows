import 'dart:developer';
import 'dart:ffi';

import 'package:accountx/app/model/depreciation_details_model.dart';
import 'package:accountx/app/modules/depreciationCalPage/depreciation_controller.dart';
import 'package:accountx/app/utils/excel_api.dart';
import 'package:accountx/app/utils/pdf_api.dart';
import 'package:data_table_2/paginated_data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../../services/api.dart';

class DepreciationDetails extends StatefulWidget {
  const DepreciationDetails({Key? key}) : super(key: key);

  @override
  State<DepreciationDetails> createState() => _DepreciationDetailsState();
}

class _DepreciationDetailsState extends State<DepreciationDetails> {
  DepreciationController controller = Get.put(DepreciationController());
  var depreciationList = Get.arguments;
  @override
  void initState() {
    // TODO: implement initState

    String clientId = controller.box.read("client_id").toString();
    // depreciationList = controller.box.read("DepList${clientId.toString()}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(" : @@@@@@@@@@@" + depreciationList.toString());

    String? dropdownValue;
    return Scaffold(
      appBar: AppBar(
        title: Text("Depreciation Details"),
        backgroundColor: Color(0xff0B28BE),
        actions: [
          DropdownButton<String>(
            value: dropdownValue,
            icon: const Icon(
              Icons.more_vert,
              color: Colors.white,
            ),
            elevation: 16,
            // style: const TextStyle(color: Colors.deepPurple),
            underline: SizedBox(),
            onChanged: (String? newValue) async {
              dropdownValue = newValue!;
              if (dropdownValue == 'Get PDF') {
                final pdfFile = await PdfApi.generatedTable();
                PdfApi.openFile(pdfFile);
              }
              if (dropdownValue == 'Get Excel Sheet') {
                ExcelApi excelApi = ExcelApi();
                excelApi.createExcel();
              }
            },
            items: <String>['Get PDF', 'Get Excel Sheet'].map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
          SizedBox(
            width: 5,
          )
        ],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Center(
                child: Text(
                  "FIXED ASSETS",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              DataTable2(
                border: TableBorder.all(width: 1),
                columnSpacing: 3,
                horizontalMargin: 2,
                minWidth: 750,
                columns: const [
                  DataColumn(
                      label: Center(
                    child: Text(
                      "Particulars",
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                    ),
                  )),
                  DataColumn(
                      label: Center(
                    child: Text(
                      "Rate",
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                    ),
                  )),
                  DataColumn(
                      label: Center(
                    child: Text(
                      "WDV as \non 01/04/2021",
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                    ),
                  )),
                  DataColumn(
                      label: Center(
                    child: Text(
                      "Addition \n More Than 180 \n days",
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                    ),
                  )),
                  DataColumn(
                    label: Center(
                      child: Text(
                        "Addition \n Less than 180 \ndays",
                        style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  DataColumn(
                      label: Center(
                          child: Text(
                    "Deduction",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  ))),
                  DataColumn(
                      label: Center(
                          child: Text(
                    "Total",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  ))),
                  DataColumn(
                      label: Center(
                    child: Text(
                      "Depreciation \nfor the year",
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                    ),
                  )),
                  DataColumn(
                      label: Center(
                    child: Text(
                      "WDV as \non \n31/03/2022",
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                    ),
                  )),
                  DataColumn(
                      label: Center(
                        child: Text(
                          "         ",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        ),
                      )),
                ],
                rows: List.generate(
                    depreciationList.length,
                    (index) => DataRow(cells: [
                          DataCell(Center(child: Text(depreciationList[index]["asset_name"]))),
                          DataCell(Center(child: Text(depreciationList[index]["depreciation_rate"]))),
                          DataCell(Center(child: Text(depreciationList[index]["opening_balance"]))),
                          DataCell(Center(child: Text(""))),
                          DataCell(Center(child: Text(""))),
                          DataCell(Center(child: Text(""))),
                          DataCell(Center(child: Text(""))),
                          DataCell(Center(child: Text(depreciationList[index]["depreciation_amount"]))),
                          DataCell(Center(child: Text(depreciationList[index]["closing_balance"]))),
                          DataCell(Center(child: InkWell(
                              onTap:()async{
                                showDialog(context: context, builder: (context)=>Center(
                                  child: Container(
                                      width: 50,
                                      height:50,
                                      child: CircularProgressIndicator()),
                                ));
                                await Api.deleteDepreciationEntryApi("depreciation",depreciationList[index]["depreciation_id"]).then((value)async{
                                  print(value.toString());
                                  Get.back();
                                  if(value["response_status"].toString() == "1"){
                                     await controller.getDepreciationApiCall().then((value) {
                                       print("depreciation value ++++++"+value.toString());
                                       depreciationList = value;
                                       setState(() {
                                       });

                                    });
                                  }else{

                                  }

                                });
                              },child: Icon(Icons.delete,color: Colors.red,)))),
                        ])),
              ),
              SizedBox(
                height: 20,
              )
            ],
          ),
        ),
      ),
    );
  }
}
