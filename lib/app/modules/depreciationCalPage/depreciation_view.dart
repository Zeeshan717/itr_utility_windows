// ignore_for_file: prefer_const_constructors_in_immutables, sized_box_for_whitespace, avoid_print

import 'package:accountx/app/database/database_helper.dart';
import 'package:accountx/app/model/depreciation_model.dart';
import 'package:accountx/app/modules/depreciationCalPage/depreciation_controller.dart';
import 'package:accountx/app/modules/depreciationCalPage/depreciation_details.dart';
import 'package:accountx/app/utils/pdf_api.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sqflite/sqlite_api.dart';

class DepreciationCalculator extends StatefulWidget {
  DepreciationCalculator({Key? key}) : super(key: key);

  final depreciationCalculatorState = _DepreciationCalculatorState();
  @override
  State<DepreciationCalculator> createState() => _DepreciationCalculatorState();
  void callState(){
    depreciationCalculatorState.disposeMethodForTextfield();
  }
}

class _DepreciationCalculatorState extends State<DepreciationCalculator> {
  // var controller = Get.find<DepreciationController>();
  DepreciationController controller = Get.put(DepreciationController());

  final dbHelper = DatabaseHelper.instance;

  DateTime? pdate;
  DateTime? sDate;

  void disposeMethodForTextfield(){
    controller.assetName.clear();
    controller.openingBalance.clear();
    controller.purchaseAmt.clear();
    controller.saleAmt.clear();
  }

  @override
  Widget build(BuildContext context) {

    String clientId = controller.box.read("client_id").toString();
    controller.purchaseDate.text = DateTime.now().toString().split(" ")[0];

    return Scaffold(
      body: Container(
        color: const Color(0xffFFFEFE),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                elevation: 6,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 60,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const Text(
                            "HEAD CATEGORY",
                            style: TextStyle(fontSize: 13, color: Color(0xff656A87)),
                          ),
                          Flexible(
                            child: Container(
                                alignment: Alignment.center,
                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                width: 200,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5), border: Border.all(color: const Color(0xffC1CDFF), width: 1)),
                                child: Obx(
                                  () => DropdownButton<String>(
                                    isExpanded: true,
                                    value: controller.selectedHead.value,
                                    icon: SvgPicture.asset("images/dropdownicon.svg"),
                                    elevation: 16,
                                    style: const TextStyle(color: Colors.black, fontSize: 13, fontWeight: FontWeight.bold),
                                    underline: const SizedBox(),
                                    onChanged: (newValue) {
                                      controller.setSelectedHead(newValue!);
                                      controller.depCal();
                                    },
                                    items: <String>['Computer & Laptop', 'Land', 'Building', 'Plant and Machinery', 'furniture and fixtures']
                                        .map<DropdownMenuItem<String>>((String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  ),
                                )),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                           Text(
                            "Asset Name".toUpperCase(),
                            style: TextStyle(color: Color(0xff656A87)),
                          ),
                          SizedBox(
                            height: 40,
                            child: TextFormField(
                              controller: controller.assetName,
                              decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: const BorderSide(
                                        width: 1,
                                        color: Color(0xffC1CDFF),
                                      )),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: const BorderSide(
                                        width: 1,
                                        color: Color(0xffC1CDFF),
                                      ))),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Opening Balance".toUpperCase(),
                            style: const TextStyle(color: Color(0xff656A87)),
                          ),
                          SizedBox(
                            height: 40,
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              onChanged: (value) => controller.depCal(),
                              controller: controller.openingBalance,
                              decoration: InputDecoration(
                                  labelText: "₹",
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: const BorderSide(
                                        width: 1,
                                        color: Color(0xffC1CDFF),
                                      )),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: const BorderSide(
                                        width: 1,
                                        color: Color(0xffC1CDFF),
                                      ))),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Purchase Date".toUpperCase(),
                            style: const TextStyle(color: Color(0xff656A87)),
                          ),
                          GestureDetector(
                            onTap: () {
                              _purchaseDatePicker(context);
                            },
                            child: SizedBox(
                              height: 40,
                              child: Container(
                                decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: const Color(0xffC1CDFF))),
                                child: TextFormField(
                                  style: const TextStyle(fontSize: 13),
                                  controller: controller.purchaseDate,
                                  enabled: false,
                                  decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.symmetric(horizontal: 8,vertical: 8),
                                      suffixIcon: Padding(
                                        padding: const EdgeInsets.symmetric(vertical: 5),
                                        child: SvgPicture.asset(
                                          "images/calender.svg",
                                          width: 30,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5),
                                          borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF),
                                          )),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5),
                                          borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF),
                                          ))),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Purchase Amt.".toUpperCase(),
                            style: const TextStyle(color: Color(0xff656A87)),
                          ),
                          SizedBox(
                            height: 40,
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              onChanged: (value) => controller.depCal(),
                              controller: controller.purchaseAmt,
                              decoration: InputDecoration(
                                  labelText: "₹",
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: const BorderSide(
                                        width: 1,
                                        color: Color(0xffC1CDFF),
                                      )),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: const BorderSide(
                                        width: 1,
                                        color: Color(0xffC1CDFF),
                                      ))),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Sale Date".toUpperCase(),
                            style: const TextStyle(color: Color(0xff656A87)),
                          ),
                          GestureDetector(
                            onTap: () {
                              slDate(context);
                            },
                            child: SizedBox(
                              height: 40,
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5), border: Border.all(width: 1, color: const Color(0xffC1CDFF))),
                                child: TextFormField(
                                  style: const TextStyle(fontSize: 13),
                                  enabled: false,
                                  controller: controller.saleDate,
                                  decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.fromLTRB(2, 2, 0, 0),
                                      suffixIcon: Padding(
                                        padding: const EdgeInsets.symmetric(vertical: 5),
                                        child: SvgPicture.asset(
                                          "images/calender.svg",
                                          width: 30,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5),
                                          borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF),
                                          )),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5),
                                          borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF),
                                          ))),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Sale Amt.".toUpperCase(),
                            style: const TextStyle(color: Color(0xff656A87)),
                          ),
                          SizedBox(
                            height: 40,
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              onChanged: (value) => controller.depCal(),
                              controller: controller.saleAmt,
                              decoration: InputDecoration(
                                  labelText: "₹",
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: const BorderSide(
                                        width: 1,
                                        color: Color(0xffC1CDFF),
                                      )),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: const BorderSide(
                                        width: 1,
                                        color: Color(0xffC1CDFF),
                                      ))),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Financial Year".toUpperCase(),
                            style: const TextStyle(color: Color(0xff656A87)),
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            decoration:
                                BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: const Color(0xffC1CDFF), width: 1)),
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            height: 40,
                            child: Obx(
                              () => DropdownButton<String>(
                                isExpanded: true,
                                value: controller.selectedYear.value,
                                icon: SvgPicture.asset("images/dropdownicon.svg"),
                                elevation: 16,
                                style: const TextStyle(color: Colors.black, fontSize: 13, fontWeight: FontWeight.bold),
                                underline: const SizedBox(),
                                onChanged: (newValue) {
                                  controller.setSelectedYear(newValue!);
                                },
                                items: <String>['2021-2022', '2022-2023'].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: const Color(0xffC1CDFF), width: 1),
                ),
                child: Column(
                  children: [
                    //1
                    const SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Dep Rate.".toUpperCase(),
                            style: const TextStyle(color: Color(0xff656A87)),
                          ),
                          Obx(
                            () => Text(
                              controller.depRate.value.toString(),
                              style: const TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                           Text(
                            "Total Asset".toUpperCase(),
                            style: TextStyle(color: Color(0xff656A87)),
                          ),
                          Obx(
                            () => Text(
                              "₹${controller.totalAssets.value.toString()}",
                              style: const TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    //2
                    const SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Dep. Amt.".toUpperCase()),
                          Obx(
                            () => Text(
                              "₹${controller.totalDep.value.toString()}",
                              style: const TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Text("Closing Bal.".toUpperCase()),
                          Obx(
                            () => Text(
                              "₹${controller.closingBalance.value.toString()}",
                              style: const TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 15,
              ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: const Color(0xff0B28BE), shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6))),
                        onPressed: () {
                          if(controller.assetName.text.isEmpty) {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Asset Name is required !!")));
                          } else if(controller.openingBalance.text.isEmpty) {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Opening Balance is required !!")));
                          }
                          else{
                            controller.insertDepreciationApiCall(context);
                          }

                        },
                        child: Text(
                          "Submit".toUpperCase(),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Color(0xff0B28BE), shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6))),
                        child: Text("View Table"),
                        onPressed: () async {
                          var depList = await controller.getDepreciationApiCall()??[];
                          controller.box.write("DepList", depList);
                          print("-----------depList-----------" + depList.toString());
                          if (depList.isNotEmpty) {
                            Get.to(DepreciationDetails(), arguments: depList);
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Data Not Available")));
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            ],
          ),
        ),
      ),
    );
  }

  void _insert(DepreciationModel depreciationModel) async {
    // row to insert
    Map<String, dynamic> row = {
      DatabaseHelper.columnParticulars: depreciationModel.assetName,
      DatabaseHelper.columnRate: depreciationModel.depRate,
      DatabaseHelper.columnWdv1: depreciationModel.openingAmount,
      DatabaseHelper.columnAdd1: depreciationModel.purchaseAmountMore180Days,
      DatabaseHelper.columnAdd2: depreciationModel.purchaseAmountLess180Days,
      DatabaseHelper.columnDeduction: depreciationModel.deduction,
      DatabaseHelper.columnTotal: depreciationModel.total,
      DatabaseHelper.columnDepreciation: depreciationModel.depreciation,
      DatabaseHelper.columnWdv2: depreciationModel.closingBalance,
      DatabaseHelper.columnClientId: depreciationModel.clientId,
    };
    final id = await dbHelper.insert(row);
    print('inserted row id: $id');
  }

  void _query() async {
    final allRows = await dbHelper.queryAllRows();
    print('query all rows:');
    allRows.forEach(print);
  }

  // void _update() async {
  //   // row to update
  //   Map<String, dynamic> row = {
  //     DatabaseHelper.columnId   : 1,
  //     DatabaseHelper.column : 'Mary',
  //     DatabaseHelper.columnAge  : 32
  //   };
  //   final rowsAffected = await dbHelper.update(row);
  //   print('updated $rowsAffected row(s)');
  // }

  void _delete() async {
    // Assuming that the number of rows is the id for the last row.
    final id = await dbHelper.queryRowCount();
    final rowsDeleted = await dbHelper.delete(id!);
    print('deleted $rowsDeleted row(s): row $id');
  }

  Future<List<DepreciationModel>> queries() async {
    Database db = await DatabaseHelper.instance.database;

    List<Map> result = await db.query(DatabaseHelper.table);

    // result.forEach((row) => print(row));
    List<DepreciationModel> depList = [];

    result.forEach((value) {
      if (value["clientId"] == controller.box.read("client_id")) {
        depList.add(DepreciationModel(value["particulars"], value["rate"], value["wdv1"], value["add1"], value["add2"], value["deduction"],
            value["total"], value["depreciation"], value["wdv2"], value["clientId"]));
        // DepList
        controller.box.write("DepList", depList);
      }
    });
    print("Sqlite" + depList.toString());
    return depList;
  }

  void _purchaseDatePicker(BuildContext context) async {
    int firstYear = int.parse(controller.box.read("selected_finacila_year").toString().split("-")[0]);
    int lastyear = int.parse(controller.box.read("selected_finacila_year").toString().split("-")[1]);
   // DateTime _initialDate = controller.purchaseDate.text != null ? DateTime.parse(controller.purchaseDate.text):DateTime(firstYear, 4, 1);
    DateTime _initialDate = DateTime(firstYear, 4, 1);
    pdate = await showDatePicker(context: context, initialDate: _initialDate, firstDate: DateTime(firstYear, 4, 1), lastDate: DateTime(lastyear, 3, 31));
    if(pdate!=null){
      controller.purchaseDate.text = pdate.toString().split(" ")[0];
    }
    controller.depCal();
  }

  slDate(BuildContext context) async {
    sDate = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(2021, 4, 1), lastDate: DateTime(2023, 3, 31));
    if(sDate!=null) {
      controller.saleDate.text = sDate.toString().split(" ")[0];
    }
    controller.depCal();
  }
}
