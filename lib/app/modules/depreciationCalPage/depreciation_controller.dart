import 'package:accountx/app/model/depreciation_details_model.dart';
import 'package:accountx/app/model/depreciation_model.dart';
import 'package:accountx/app/services/api.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DepreciationController extends GetxController {
  GetStorage box = GetStorage();
  TextEditingController assetName = TextEditingController();
  TextEditingController openingBalance = TextEditingController();
  TextEditingController purchaseDate = TextEditingController();
  TextEditingController purchaseAmt = TextEditingController();
  TextEditingController saleDate = TextEditingController();
  TextEditingController saleAmt = TextEditingController();

  var totalDep = 0.0.obs;
  var depRate = 40.0.obs;
  var closingBalance = 0.0.obs;
  var totalAssets = 0.0.obs;
  double ob = 0.0;
  double pa = 0.0;
  double sa = 0.0;

  double dr = 0.0;
  double da = 0.0;
  final selectedHead = "Computer & Laptop".obs;
  final selectedYear = "2021-2022".obs;

  List<DepreciationModel> depreciationList = [];

  void setSelectedHead(String value) {
    selectedHead.value = value;
  }

  void setSelectedYear(String value) {
    selectedYear.value = value;
  }

  bool isDepDurationFull() {
    var pDate = purchaseDate.text.split('-')[1].toString();
    int pDate1 = int.parse(purchaseDate.text.split('-')[2].toString());
    print("-----isDepDurationFull()---pDate----"+pDate.toString());
    print("-----isDepDurationFull()----pDate1---"+pDate1.toString());
    if (pDate == '04' || pDate == '05' || pDate == '06' || pDate == '07' || pDate == '08' || pDate == '09' || (pDate == '10' && pDate1 <= 3)) {
      return true;
    }
    return false;
  }

  void depCal() {
    ob = openingBalance.text.isNotEmpty ? double.parse(openingBalance.text.toString()) : 0.0;
    pa = purchaseAmt.text.isNotEmpty ? double.parse(purchaseAmt.text.toString()) : 0.0;
    sa = saleAmt.text.isNotEmpty ? double.parse(saleAmt.text.toString()) : 0.0;
    print("---------------"+isDepDurationFull().toString());
    if (selectedHead.value == "Computer & Laptop") {

      depRate.value = 40;
      totalDep.value = (ob - sa) * depRate.value / 100 + (pa * depRate.value / 100) / (isDepDurationFull() ? 1 : 2);
      closingBalance.value = (ob + pa - sa) - totalDep.value;
      totalAssets.value = ob + pa - sa;
    }
    if (selectedHead.value == "Land") {
      depRate.value = 0;
      totalDep.value = (ob - sa) * depRate.value / 100 + (pa * depRate.value / 100) / (isDepDurationFull() ? 1 : 2);
      closingBalance.value = (ob + pa - sa) - totalDep.value;
      totalAssets.value = ob + pa - sa;
    }
    if (selectedHead.value == "Building") {
      depRate.value = 10;
      totalDep.value = (ob - sa) * depRate.value / 100 + (pa * depRate.value / 100) / (isDepDurationFull() ? 1 : 2);
      closingBalance.value = (ob + pa - sa) - totalDep.value;
      totalAssets.value = ob + pa - sa;
    }
    if (selectedHead.value == "Plant and Machinery") {
      depRate.value = 15;
      totalDep.value = (ob - sa) * depRate.value / 100 + (pa * depRate.value / 100) / (isDepDurationFull() ? 1 : 2);
      closingBalance.value = (ob + pa - sa) - totalDep.value;
      totalAssets.value = ob + pa - sa;
    }
    if (selectedHead.value == "furniture and fixtures") {
      depRate.value = 10;
      totalDep.value = (ob - sa) * depRate.value / 100 + (pa * depRate.value / 100) / (isDepDurationFull() ? 1 : 2);
      closingBalance.value = (ob + pa - sa) - totalDep.value;
      totalAssets.value = ob + pa - sa;
    }
  }

  @override
  void onClose() {
    String clientId = box.read("client_id").toString();
    print("box " + box.read("DepList${clientId.toString()}").toString());
    super.onClose();
  }

  void insertDepreciationApiCall(BuildContext context) async {
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    await Api.InsertDepreciationApi(
            clientId.toString(),
            adminId.toString(),
            financialYear.toString(),
            selectedHead.value.toString(),
            assetName.text.toString(),
            openingBalance.text.toString(),
            purchaseAmt.text.isEmpty ? "" : purchaseDate.text.toString(),
            purchaseAmt.text.toString(),
            saleDate.text.toString(),
            saleAmt.text.toString(),
            depRate.value.toString(),
            totalAssets.value.toString(),
            totalDep.value.toString(),
            closingBalance.value.toString())
        .then((value) {
      if (value["response_status"].toString() == "1") {
        showDialog(
            context: context,
            builder: (context) => Container(
                  child: AlertDialog(
                    title: Text("Alert"),
                    content: Text(value["message"].toString()),
                    actions: [TextButton(onPressed: () => Get.back(), child: Text("OK"))],
                  ),
                ));
        print(value["message"].toString());
        assetName.clear();
        openingBalance.clear();
        purchaseAmt.clear();
        saleAmt.clear();
        depCal();
      }
      if (value["response_status"].toString() == "0") {
        if(assetName.text.isEmpty) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Asset Name Required !!")));
        } else  if(openingBalance.text.isEmpty) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Opening Balance Required !!")));
        }
      else {
          showDialog(
              context: context,
              builder: (context) => Container(
                child: AlertDialog(
                  title: Text("Alert"),
                  content: Text(value["message"].toString()),
                  actions: [TextButton(onPressed: () => Get.back(), child: Text("OK"))],
                ),
              ));
        }
        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(value["message"].toString())));
      }
    });
  }

  Future<List<dynamic>> getDepreciationApiCall() async {
    print("Depreciation Api Called");
    var clientId = box.read("client_id");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    var value1;
    var depList = await Api.getDepreciationApi(clientId.toString(), financialYear.toString(), adminId.toString()).then((value) {
      value1 = value["response_depreciation_list"];
    });
    print("-------deplist--------"+depList.toString());
    return value1 == null ? [] : value1;
  }

}
