import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MenuPage extends StatelessWidget {
  const MenuPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff0B28BE),
        centerTitle: true,
        title: const Text(
          "AccountX",
          style: TextStyle(fontSize: 16),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    Get.toNamed("/");
                  },
                  child: Card(
                    elevation: 6,
                    child: Container(
                      alignment: Alignment.center,
                      width: 100,
                      height: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset("images/calculator.png"),
                          const Text(
                            "Calculator",
                            style: TextStyle(color: Color(0xff656A87)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Get.toNamed("/incomeMaster");
                  },
                  child: Card(
                      elevation: 5,
                      child: Container(
                          alignment: Alignment.center,
                          height: 100,
                          width: 100,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Image.asset("images/income.png"),
                                const Text(
                                  'Income Master',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Color(0xff656A87)),
                                ),
                              ],
                            ),
                          ))),
                ),
                InkWell(
                  onTap: () {
                    Get.toNamed("/bankStatement");
                  },
                  child: Card(
                    elevation: 5,
                    child: Container(
                      alignment: Alignment.center,
                      width: 100,
                      height: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset("images/bank.png"),
                          const Text(
                            "Bank Satement",
                            style: TextStyle(color: Color(0xff656A87)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    Get.toNamed("/ppfaccount");
                  },
                  child: Card(
                    elevation: 5,
                    child: Container(
                      alignment: Alignment.center,
                      width: 100,
                      height: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset("images/ppf.png"),
                          const Text(
                            "PPF Account",
                            style: TextStyle(color: Color(0xff656A87)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Get.toNamed("/healthinsurance");
                  },
                  child: Card(
                    elevation: 5,
                    child: Container(
                      alignment: Alignment.center,
                      width: 100,
                      height: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset("images/health.png"),
                          const Text(
                            "Health Insurance",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Color(0xff656A87)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Get.toNamed("/bankstatement");
                  },
                  child: Card(
                    elevation: 0,
                    child: Container(
                      alignment: Alignment.center,
                      width: 100,
                      height: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: const [
                          // Image.asset("images/health.png"),
                          // Text("Bank Statement",textAlign: TextAlign.center,
                          //   style: TextStyle(color: Color(0xff656A87)),),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
