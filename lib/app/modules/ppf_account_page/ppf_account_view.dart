// ignore_for_file: sized_box_for_whitespace, avoid_print

import 'package:accountx/app/model/ppf_account_model.dart';
import 'package:accountx/app/modules/ppf_account_page/create_ppf_account.dart';
import 'package:accountx/app/modules/ppf_account_page/ppf_account_controller.dart';
import 'package:accountx/app/provider/ppf_provider.dart';
import 'package:accountx/app/services/api.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class PPFAccount extends StatefulWidget {
  const PPFAccount({Key? key}) : super(key: key);
  @override
  State<PPFAccount> createState() => _PPFAccountState();
}

class _PPFAccountState extends State<PPFAccount> {
  PPFAccountController controller = Get.put(PPFAccountController());
  bool isselected=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white24,
        title: const Text("PPF ACCOUNT",style: TextStyle(fontSize: 16,color: Colors.black),),
        actions: [
          Tooltip(
            message: "Add",
            child: InkWell(
                onTap: () {
                  setState((){isselected=!isselected;});
                },
                child: isselected==false ? Image.asset("images/add.png",width: 25,color: Colors.black,):Icon(Icons.home,color: Colors.black,)),
          ),
          const SizedBox(width: 20,)
        ],
      ),
      body: isselected? CreatePPFAccount(): Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child:Consumer<PpfProvider>(
          builder: (context,ppfProvider,child)=>
           FutureBuilder<PpfAccountModel>(
            future:ppfProvider.investmentModel,
            builder: (context,snapshot){
              print("Snapshot Data: "+snapshot.data.toString());
             if(snapshot.hasData) {
               EasyLoading.dismiss();
               return Padding(
                 padding: const EdgeInsets.fromLTRB(16, 16, 16, 70),
                 child: DataTable2(
                     columnSpacing: 10,
                     horizontalMargin: 2,
                     minWidth: 560,
                    columns: const [
                   DataColumn(label: Center(child: Text("Account Number")),),
                   DataColumn(label: Center(child: Text("Statement Date"))),
                   DataColumn(label: Center(child: Text("Deposits"))),
                   DataColumn(label: Center(child: Text("Remark"))),
                      DataColumn(label: Center(child: Text("Delete"))),
                 ], rows: List.generate(snapshot.data!.responsePpfList!.length, (index) =>   DataRow(cells: [
                   DataCell(Center(child: Text(snapshot.data!.responsePpfList![index].accountNo.toString()))),
                   DataCell(Center(child: Text(snapshot.data!.responsePpfList![index].statementDate.toString().split(" ")[0]))),
                   DataCell(Center(child: Text(snapshot.data!.responsePpfList![index].deposite.toString()))),
                   DataCell(Center(child: Text(snapshot.data!.responsePpfList![index].remark.toString()))),
                   DataCell(Center(
                     child: InkWell(
                         onTap:()async{
                           showDialog(context: context, builder: (context)=>Center(
                             child: Container(
                                 width: 50,
                                 height:50,
                                 child: CircularProgressIndicator()),
                           ));
                           await Api.deleteDepreciationEntryApi("ppfAccount",snapshot.data!.responsePpfList![index].id.toString()).then((value){
                             print("---delete Response----"+value.toString());
                             Get.back();
                             if(value["response_status"].toString() == "1"){
                               Provider.of<PpfProvider>(context,listen: false).updateValue();
                             }
                           });
                         },
                         child: Icon(Icons.delete)),
                   )),
                 ]))),
               );
             } else if(snapshot.connectionState == ConnectionState.waiting) {
               EasyLoading.show(status: "Loading...");
               return Container();
             } else {
               EasyLoading.dismiss();
               return const Center(
                 child: Text("Data Not Available !!"),
               );
             }
            },
          ),
        ),
      ),
    );
  }
}
