import 'package:accountx/app/modules/ppf_account_page/ppf_account_controller.dart';
import 'package:get/get.dart';

class PPFAccountBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<PPFAccountController>(PPFAccountController());
  }
}