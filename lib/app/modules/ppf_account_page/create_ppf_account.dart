// ignore_for_file: must_be_immutable

import 'package:accountx/app/modules/ppf_account_page/ppf_account_controller.dart';
import 'package:accountx/app/modules/ppf_account_page/ppf_account_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class CreatePPFAccount extends StatelessWidget {
  CreatePPFAccount({Key? key}) : super(key: key);

  DateTime? now;
  DateTime initialDate = DateTime(DateTime.now().year-1,4,1);
  DateTime firstDate = DateTime(DateTime.now().year-1,4,1);
  DateTime lastDate = DateTime(DateTime.now().year,3,31);


  PPFAccountController ppfAccountController = Get.put(PPFAccountController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    /*  appBar: AppBar(
        backgroundColor: const Color(0xff0B28BE),
        centerTitle: true,
        title: const Text("Create PPF Account",style: TextStyle(fontSize: 16),),
      ),*/
      backgroundColor: Colors.white,
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 20,),
              Center(child: Text("Create PPF Account",style: TextStyle(fontSize: 16),)),
              //Account Number
              SizedBox(height: 20,),
              Text("Account Number".toUpperCase(),style: const TextStyle(color: Color(0xff656A87)),),
              SizedBox(
                height: 40,
                child: TextFormField(
                  controller: ppfAccountController.accountNumberController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: const BorderSide(
                              color: Color(0xffC1CDFF)
                          )
                      )
                  ),
                ),
              ),
              const SizedBox(height: 15,),
              //Statement Date
              SizedBox(child: Text("Statement Date".toUpperCase(),style: const TextStyle(color: Color(0xff656A87)),)),
              InkWell(
                onTap: () {
                  selectedDateShow(context);
                },
                child: SizedBox(
                  height: 40,
                  child: TextFormField(
                    controller: ppfAccountController.statementDateController,
                    enabled: false,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: const BorderSide(
                                color: Color(0xffC1CDFF)
                            )
                        )
                    ),
                  ),
                ),
              ),
              //Deposits
              const SizedBox(height: 15,),
              Text("Deposits".toUpperCase(),style: const TextStyle(color: Color(0xff656A87)),),
              SizedBox(
                height: 40,
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  controller: ppfAccountController.depositsController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: const BorderSide(
                              color: Color(0xffC1CDFF)
                          )
                      )
                  ),
                ),
              ),
              const SizedBox(height: 15,),
              Text("Remark".toUpperCase(),style: const TextStyle(color: Color(0xff656A87)),),
              TextFormField(
                controller: ppfAccountController.remarkController,
                maxLines: 5,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                        borderSide: const BorderSide(
                            color: Color(0xffC1CDFF)
                        )
                    )
                ),
              ),
              const SizedBox(height: 150,),
              Center(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 40,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: const Color(0xff0B28BE)
                      ),
                      onPressed: () {
                        if(ppfAccountController.accountNumberController.text.isEmpty) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Account Number is required !!",)));
                        } else if(ppfAccountController.statementDateController.text.isEmpty) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Statement date is required !!",)));
                        } else if(ppfAccountController.depositsController.text.isEmpty) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Deposits is required !!",)));
                        } else if(ppfAccountController.remarkController.text.isEmpty) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Remark is required !!",)));
                        } else {
                          EasyLoading.show(status: "Loading...");
                          ppfAccountController.ppfAccountCall();
                        }

                      }, child: const Text("SUBMIT")),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  selectedDateShow(BuildContext context) async {
   now = await showDatePicker(context: context,
        initialDate: initialDate,
        firstDate: firstDate,
        lastDate: lastDate);
   ppfAccountController.statementDateController.text = firstDate.toString().split(" ")[0];
   if(now != null) {
     ppfAccountController.statementDateController.text = now.toString().split(" ")[0];


   }

  }
}
