// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:accountx/app/services/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class PPFAccountController extends GetxController {

  GetStorage box = GetStorage();

  TextEditingController accountNumberController = TextEditingController();
  TextEditingController statementDateController = TextEditingController();
  TextEditingController depositsController = TextEditingController();
  TextEditingController remarkController = TextEditingController();


  var distibutorId;
  var adminId;
  var financialYear;

    ppfAccountCall() {
    distibutorId = box.read("client_id");
    adminId = box.read("admin_id");
      financialYear = box.read("finacial_year");

       Api.ppfAccountApi(box.read("client_id").toString(),
          box.read("admin_id").toString(),
         box.read("finacial_year").toString(),
        accountNumberController.text,
        statementDateController.text,
      depositsController.text,
      remarkController.text
    ).then((value) {
      print(" Response Status : "+value["response_status"].toString());

      if(value["response_status"].toString() == '1') {
        EasyLoading.dismiss();
        Get.defaultDialog(
          barrierDismissible: false,
          content: Text(value["message"]),
          actions: [
            TextButton(onPressed: () {
              Get.back();
              accountNumberController.clear();
              depositsController.clear();
              remarkController.clear();
            }, child: Text("OK"))
          ]
        );
      } else {
        Get.defaultDialog(
            barrierDismissible: false,
            content: Text(value["message"]),
            actions: [
              TextButton(onPressed: () => Get.back(), child: Text("OK"))
            ]
        );
      }

      // if(value["response_status"].toString() == "1") {
      // Get.defaultDialog(
      //   barrierDismissible: false,
      //   content: Text(value["response_msg"]),
      //   actions: [
      //     TextButton(onPressed: () => Get.back(), child: Text("OK"))
      //   ]
      // );
      // } else {
      //   Get.defaultDialog(
      //       barrierDismissible: false,
      //       content: Text(value["response_msg"]),
      //       actions: [
      //         TextButton(onPressed: () => Get.back(), child: Text("OK"))
      //       ]
      //   );
      // }
    });
  }




}