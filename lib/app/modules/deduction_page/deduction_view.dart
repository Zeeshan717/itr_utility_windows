// ignore_for_file: sized_box_for_whitespace, prefer_const_literals_to_create_immutables

import 'dart:io';

import 'package:accountx/app/model/deduction_model.dart';
import 'package:accountx/app/modules/deduction_page/deduction_controller.dart';
import 'package:accountx/app/modules/pdf_view.dart';
import 'package:accountx/app/services/api.dart';
import 'package:date_format/date_format.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DeductionView extends StatefulWidget {
  const DeductionView({Key? key}) : super(key: key);

  @override
  State<DeductionView> createState() => _DeductionViewState();
  void updateUi() {
    _DeductionViewState().clearUiState();
  }
}

class _DeductionViewState extends State<DeductionView> {
  bool isSelected = false;
  GetStorage storage = GetStorage();
  var controller = Get.put(DeductionController());
  DateTime? now;
  DateTime selectedDate = DateTime(DateTime.now().year - 1, 4, 1);
  DownloadNOpen openPDF = DownloadNOpen();

  @override
  void initState() {
    controller.apiDeductionDetailsCall();
    controller.getDeducationData();
    super.initState();
  }

  void clearUiState() {
    controller.apiDeductionDetailsCall();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white24,
        centerTitle: true,
        title: const Text(
          "Deduction",
          style: TextStyle(fontSize: 16, color: Colors.black),
        ),
        actions: [
          Tooltip(
            message: "Info",
            child: InkWell(
                onTap: () {
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          backgroundColor: Colors.white,
                          insetPadding:
                              const EdgeInsets.symmetric(horizontal: 10),
                          contentPadding: EdgeInsets.zero,
                          content: Container(
                            height: 510,
                            child: Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  color: const Color(0xff1739E4),
                                  width: MediaQuery.of(context).size.width,
                                  height: 50,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      const Text(""),
                                      const Text(
                                        "Deduction History",
                                        style: TextStyle(
                                            fontSize: 16, color: Colors.white),
                                      ),
                                      InkWell(
                                          onTap: () {
                                            Get.back();
                                          },
                                          child: const Icon(Icons.clear,
                                              color: Colors.white))
                                    ],
                                  ),
                                ),
                                //80C
                                const SizedBox(
                                  height: 5,
                                ),
                                InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return AlertDialog(
                                            alignment: Alignment.center,
                                            backgroundColor: Colors.white,
                                            insetPadding: EdgeInsets.symmetric(
                                                horizontal: 10, vertical: 100),
                                            contentPadding: EdgeInsets.zero,
                                            content: Column(
                                              children: [
                                                Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 400,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 10),
                                                        width: MediaQuery.of(
                                                                context)
                                                            .size
                                                            .width,
                                                        color: const Color(
                                                            0xff1739E4),
                                                        height: 50,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            const Text(""),
                                                            const Text(
                                                              "80C Details",
                                                              style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            InkWell(
                                                                onTap: () {
                                                                  Get.back();
                                                                },
                                                                child:
                                                                    const Icon(
                                                                  Icons.clear,
                                                                  color: Colors
                                                                      .white,
                                                                ))
                                                          ],
                                                        ),
                                                      ),
                                                      FutureBuilder<
                                                              DeductionModel>(
                                                          future:
                                                          Api.getDeductionDetailsApi(
                                                              storage
                                                                  .read(
                                                                      "client_id")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "client_name")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "admin_id")
                                                                  .toString(),
                                                              storage.read(
                                                                  "finacial_year")),
                                                          builder: (BuildContext context, AsyncSnapshot<DeductionModel> snapshot) {
                                                            print("--%%%%%%%%>>>>>-----snapshot------------" +
                                                                snapshot.data.
                                                                    toString());
                                                            if (snapshot
                                                                .hasData) {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                                child: Column(
                                                                  children: [
                                                                    //EPF,PF
                                                                    const SizedBox(
                                                                      height:
                                                                          20,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "EPF,PF: ",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .epfPf,
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //LIC
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                            "LIC: ",
                                                                            style:
                                                                                TextStyle(fontSize: 13)),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.lic,
                                                                            style:
                                                                                const TextStyle(fontSize: 13))
                                                                      ],
                                                                    ),
                                                                    //National Saving
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                            "National Saving: ",
                                                                            style:
                                                                                TextStyle(fontSize: 13)),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.nationalSaving,
                                                                            style:
                                                                                const TextStyle(fontSize: 13))
                                                                      ],
                                                                    ),
                                                                    //Tuition Fee for Children
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                            "Tuition Fee for Children: ",
                                                                            style:
                                                                                TextStyle(fontSize: 13)),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.tuitionFees,
                                                                            style:
                                                                                const TextStyle(fontSize: 13))
                                                                      ],
                                                                    ),
                                                                    //Senior Citizen Saving
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                            "Senior Citizen Saving: ",
                                                                            style:
                                                                                TextStyle(fontSize: 13)),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.seniorCitizenSaving,
                                                                            style:
                                                                                const TextStyle(fontSize: 13))
                                                                      ],
                                                                    ),
                                                                    //Repayment of Principal
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                            "Repayment of Principal: ",
                                                                            style:
                                                                                TextStyle(fontSize: 13)),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.repaymentOfPrinciple,
                                                                            style:
                                                                                const TextStyle(fontSize: 13))
                                                                      ],
                                                                    ),
                                                                    //Sukanya Samridhi Scheme
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                            "Sukanya Samridhi Scheme: ",
                                                                            style:
                                                                                TextStyle(fontSize: 13)),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.sukanyaSamridhiScheme,
                                                                            style:
                                                                                const TextStyle(fontSize: 13))
                                                                      ],
                                                                    ),
                                                                    //Registration Fees
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                            "Registration Fees: ",
                                                                            style:
                                                                                TextStyle(fontSize: 13)),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.regFeesStampDuty,
                                                                            style:
                                                                                const TextStyle(fontSize: 13))
                                                                      ],
                                                                    ),
                                                                    //Date
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                            "Date: ",
                                                                            style:
                                                                                TextStyle(fontSize: 13)),
                                                                        Text(
                                                                            snapshot.data!.date80C.toString().split(" ")[
                                                                                0],
                                                                            style:
                                                                                const TextStyle(fontSize: 13)),
                                                                      ],
                                                                    ),
                                                                    //Remark
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                            "Remark: ",
                                                                            style:
                                                                                TextStyle(fontSize: 13)),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.remark80C,
                                                                            style:
                                                                                const TextStyle(fontSize: 13)),
                                                                      ],
                                                                    ),

                                                                    //Attachment
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                            "Attachment: ",
                                                                            style:
                                                                                TextStyle(fontSize: 13)),
                                                                        TextButton(
                                                                            onPressed:
                                                                                () async {
                                                                              await openPDF.openFile(url: "https://appexperts.net/btconnect/${snapshot.data!.attachmentPath80C.toString()}", fileName: "${storage.read("client_name")}_80cdetails.pdf");
                                                                            },
                                                                            child:
                                                                                const Text(
                                                                              "View PDF",
                                                                              style: TextStyle(fontSize: 13, color: Color(0xff1739E4)),
                                                                            ))
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              );
                                                            } else if (snapshot
                                                                    .connectionState ==
                                                                ConnectionState
                                                                    .waiting) {
                                                              EasyLoading.show(
                                                                  status:
                                                                      "Loading...");
                                                              return Container();
                                                            } else {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Expanded(
                                                                child:
                                                                    Container(
                                                                  width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                                  height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height,
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Text(
                                                                        "Data Not Available !!",
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              );
                                                            }
                                                          })
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        });
                                  },
                                  child: const Card(
                                    child: ListTile(
                                      title: Text("80C"),
                                    ),
                                  ),
                                ),
                                //80CCD
                                const SizedBox(
                                  height: 10,
                                ),
                                InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return AlertDialog(
                                            backgroundColor: Colors.white,
                                            insetPadding:
                                                const EdgeInsets.symmetric(
                                                    horizontal: 10,
                                                    vertical: 100),
                                            contentPadding: EdgeInsets.zero,
                                            content: Column(
                                              children: [
                                                Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 400,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 10),
                                                        color: const Color(
                                                            0xff1739E4),
                                                        height: 50,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            const Text(""),
                                                            const Text(
                                                              "80CCD Details",
                                                              style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            InkWell(
                                                                onTap: () {
                                                                  Get.back();
                                                                },
                                                                child:
                                                                    const Icon(
                                                                  Icons.clear,
                                                                  color: Colors
                                                                      .white,
                                                                ))
                                                          ],
                                                        ),
                                                      ),
                                                      FutureBuilder<
                                                              DeductionModel>(
                                                          future: Api.getDeductionDetailsApi(
                                                              storage
                                                                  .read(
                                                                      "client_id")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "client_name")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "admin_id")
                                                                  .toString(),
                                                              storage.read(
                                                                  "finacial_year")),
                                                          builder: (context,
                                                              snapshot) {
                                                            print("%%%%%%%%% : " +
                                                                snapshot
                                                                    .toString());
                                                            /*      print('${storage.read("client_id").toString()},${
                                                                storage.read("client_name").toString()},${
                                                                storage.read("admin_id").toString()},${
                                                                storage.read("finacial_year")}');*/
                                                            print("**** priyanshu-----***" +
                                                                snapshot.data
                                                                    .toString());
                                                            if (snapshot
                                                                .hasData) {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                                child: Column(
                                                                  children: [
                                                                    //Donation
                                                                    const SizedBox(
                                                                      height:
                                                                          20,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Donation",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .donation,
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Date
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Date:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                            snapshot.data!.date80Ccd.toString().split(" ")[
                                                                                0],
                                                                            style:
                                                                                const TextStyle(fontSize: 13))
                                                                      ],
                                                                    ),
                                                                    //Remark
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Date:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.remark80Ccd,
                                                                            style:
                                                                                const TextStyle(fontSize: 13)),
                                                                      ],
                                                                    ),
                                                                    //Attachment
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Attachment:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        TextButton(
                                                                            onPressed:
                                                                                () async {
                                                                              await openPDF.openFile(url: "https://appexperts.net/btconnect/${snapshot.data!.attachmentPath80Ccd.toString()}", fileName: "${storage.read("client_name")}_80ccddetails.pdf");
                                                                            },
                                                                            child:
                                                                                const Text(
                                                                              "View PDF",
                                                                              style: TextStyle(fontSize: 13, color: Color(0xff1739E4)),
                                                                            ))
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              );
                                                            } else if (snapshot
                                                                    .connectionState ==
                                                                ConnectionState
                                                                    .waiting) {
                                                              EasyLoading.show(
                                                                  status:
                                                                      "Loading...");
                                                              return Container();
                                                            } else {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Expanded(
                                                                child:
                                                                    Container(
                                                                  width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                                  height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height,
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Text(
                                                                        "Data Not Available !!",
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              );
                                                            }
                                                          })
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        });
                                  },
                                  child: const Card(
                                    child: ListTile(
                                      title: Text("80CCD"),
                                    ),
                                  ),
                                ),
                                //80D
                                const SizedBox(
                                  height: 10,
                                ),
                                InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return AlertDialog(
                                            alignment: Alignment.center,
                                            backgroundColor: Colors.white,
                                            insetPadding:
                                                const EdgeInsets.symmetric(
                                                    horizontal: 10,
                                                    vertical: 100),
                                            contentPadding: EdgeInsets.zero,
                                            content: Column(
                                              children: [
                                                Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 400,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 10),
                                                        width: MediaQuery.of(
                                                                context)
                                                            .size
                                                            .width,
                                                        color: const Color(
                                                            0xff1739E4),
                                                        height: 50,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            const Text(""),
                                                            const Text(
                                                              "80D Details",
                                                              style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            InkWell(
                                                                onTap: () {
                                                                  Get.back();
                                                                },
                                                                child:
                                                                    const Icon(
                                                                  Icons.clear,
                                                                  color: Colors
                                                                      .white,
                                                                ))
                                                          ],
                                                        ),
                                                      ),
                                                      FutureBuilder<
                                                              DeductionModel>(
                                                          future: Api.getDeductionDetailsApi(
                                                              storage
                                                                  .read(
                                                                      "client_id")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "client_name")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "admin_id")
                                                                  .toString(),
                                                              storage.read(
                                                                  "finacial_year")),
                                                          builder: (context,
                                                              snapshot) {
                                                            if (snapshot
                                                                .hasData) {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                                child: Column(
                                                                  children: [
                                                                    //Parents
                                                                    const SizedBox(
                                                                      height:
                                                                          20,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Parents",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .parents,
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Self
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Self:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.self,
                                                                            style:
                                                                                const TextStyle(fontSize: 13))
                                                                      ],
                                                                    ),
                                                                    //Date
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Date:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                            snapshot.data!.date80D.toString().split(" ")[
                                                                                0],
                                                                            style:
                                                                                const TextStyle(fontSize: 13)),
                                                                      ],
                                                                    ),
                                                                    //Remark
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Remark:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .remark80D,
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Attachment
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Attachment:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        TextButton(
                                                                            onPressed:
                                                                                () async {
                                                                              await openPDF.openFile(url: "https://appexperts.net/btconnect/${snapshot.data!.attachmentPath80D.toString()}", fileName: "${storage.read("client_name")}_80ddetails.pdf");
                                                                            },
                                                                            child:
                                                                                const Text(
                                                                              "View PDF",
                                                                              style: TextStyle(fontSize: 13, color: Color(0xff1739E4)),
                                                                            ))
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              );
                                                            } else if (snapshot
                                                                    .connectionState ==
                                                                ConnectionState
                                                                    .waiting) {
                                                              EasyLoading.show(
                                                                  status:
                                                                      "Loading...");
                                                              return Container();
                                                            } else {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Expanded(
                                                                child:
                                                                    Container(
                                                                  width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                                  height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height,
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Text(
                                                                        "Data Not Available !!",
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              );
                                                            }
                                                          })
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        });
                                  },
                                  child: const Card(
                                    child: ListTile(
                                      title: Text("80D"),
                                    ),
                                  ),
                                ),
                                //80G
                                const SizedBox(
                                  height: 10,
                                ),
                                InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return AlertDialog(
                                            backgroundColor: Colors.white,
                                            insetPadding:
                                                const EdgeInsets.symmetric(
                                                    horizontal: 10,
                                                    vertical: 100),
                                            contentPadding: EdgeInsets.zero,
                                            content: Column(
                                              children: [
                                                Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 400,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 10),
                                                        color: const Color(
                                                            0xff1739E4),
                                                        height: 50,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            const Text(""),
                                                            const Text(
                                                              "80G Details",
                                                              style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            InkWell(
                                                                onTap: () {
                                                                  Get.back();
                                                                },
                                                                child:
                                                                    const Icon(
                                                                  Icons.clear,
                                                                  color: Colors
                                                                      .white,
                                                                ))
                                                          ],
                                                        ),
                                                      ),
                                                      FutureBuilder<
                                                              DeductionModel>(
                                                          future: Api.getDeductionDetailsApi(
                                                              storage
                                                                  .read(
                                                                      "client_id")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "client_name")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "admin_id")
                                                                  .toString(),
                                                              storage.read(
                                                                  "finacial_year")),
                                                          builder: (context,
                                                              snapshot) {
                                                            if (snapshot
                                                                .hasData) {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                                child: Column(
                                                                  children: [
                                                                    //Range
                                                                    const SizedBox(
                                                                      height:
                                                                          20,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Range",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .range,
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Other(By Cheque)
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Other(By Cheque):",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.otherByCheque,
                                                                            style:
                                                                                const TextStyle(fontSize: 13))
                                                                      ],
                                                                    ),
                                                                    //In Cash
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "In Cash:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                            snapshot
                                                                                .data!.inCash,
                                                                            style:
                                                                                const TextStyle(fontSize: 13)),
                                                                      ],
                                                                    ),
                                                                    //Date
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Date:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .date80G
                                                                              .toString()
                                                                              .split(" ")[0],
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Remark
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Remark:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .remark80G,
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Attachment
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Attachment:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        TextButton(
                                                                            onPressed:
                                                                                () async {
                                                                              await openPDF.openFile(url: "https://appexperts.net/btconnect/${snapshot.data!.attachmentPath80G.toString()}", fileName: "${storage.read("client_name")}_80gdetails.pdf");
                                                                            },
                                                                            child:
                                                                                const Text(
                                                                              "View PDF",
                                                                              style: TextStyle(fontSize: 13, color: Color(0xff1739E4)),
                                                                            ))
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              );
                                                            } else if (snapshot
                                                                    .connectionState ==
                                                                ConnectionState
                                                                    .waiting) {
                                                              EasyLoading.show(
                                                                  status:
                                                                      "Loading...");
                                                              return Container();
                                                            } else {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Expanded(
                                                                child:
                                                                    Container(
                                                                  width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                                  height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height,
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Text(
                                                                        "Data Not Available !!",
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              );
                                                            }
                                                          })
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        });
                                  },
                                  child: const Card(
                                    child: ListTile(
                                      title: Text("80G"),
                                    ),
                                  ),
                                ),
                                //80TTA
                                const SizedBox(
                                  height: 10,
                                ),
                                InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return AlertDialog(
                                            backgroundColor: Colors.white,
                                            insetPadding:
                                                const EdgeInsets.symmetric(
                                                    horizontal: 10,
                                                    vertical: 100),
                                            contentPadding: EdgeInsets.zero,
                                            content: Column(
                                              children: [
                                                Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 400,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 10),
                                                        color: const Color(
                                                            0xff1739E4),
                                                        height: 50,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            const Text(""),
                                                            const Text(
                                                              "80TTA Details",
                                                              style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            InkWell(
                                                                onTap: () {
                                                                  Get.back();
                                                                },
                                                                child:
                                                                    const Icon(
                                                                  Icons.clear,
                                                                  color: Colors
                                                                      .white,
                                                                ))
                                                          ],
                                                        ),
                                                      ),
                                                      FutureBuilder<
                                                              DeductionModel>(
                                                          future: Api.getDeductionDetailsApi(
                                                              storage
                                                                  .read(
                                                                      "client_id")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "client_name")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "admin_id")
                                                                  .toString(),
                                                              storage.read(
                                                                  "finacial_year")),
                                                          builder: (context,
                                                              snapshot) {
                                                            if (snapshot
                                                                .hasData) {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                                child: Column(
                                                                  children: [
                                                                    //80TTA
                                                                    const SizedBox(
                                                                      height:
                                                                          20,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "80TTA",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .val80Tta,
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Date
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Date:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .date80Tta
                                                                              .toString()
                                                                              .split(" ")[0],
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Remark
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Remark:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .remark80Tta,
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Attachment
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Attachment:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        TextButton(
                                                                            onPressed:
                                                                                () async {
                                                                              await openPDF.openFile(url: "https://appexperts.net/btconnect/${snapshot.data!.attachmentPath80Tta.toString()}", fileName: "${storage.read("client_name")}_80ttadetails.pdf");
                                                                            },
                                                                            child:
                                                                                const Text(
                                                                              "View PDF",
                                                                              style: TextStyle(fontSize: 13, color: Color(0xff1739E4)),
                                                                            ))
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              );
                                                            } else if (snapshot
                                                                    .connectionState ==
                                                                ConnectionState
                                                                    .waiting) {
                                                              EasyLoading.show(
                                                                  status:
                                                                      "Loading...");
                                                              return Container();
                                                            } else {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Expanded(
                                                                child:
                                                                    Container(
                                                                  width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                                  height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height,
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Text(
                                                                        "Data Not Available !!",
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              );
                                                            }
                                                          })
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        });
                                  },
                                  child: const Card(
                                    child: ListTile(
                                      title: Text("80TTA"),
                                    ),
                                  ),
                                ),
                                //Other
                                const SizedBox(
                                  height: 10,
                                ),
                                InkWell(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return AlertDialog(
                                            backgroundColor: Colors.white,
                                            insetPadding:
                                                const EdgeInsets.symmetric(
                                                    horizontal: 10,
                                                    vertical: 100),
                                            contentPadding: EdgeInsets.zero,
                                            content: Column(
                                              children: [
                                                Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 400,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 10),
                                                        color: const Color(
                                                            0xff1739E4),
                                                        height: 50,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            const Text(""),
                                                            const Text(
                                                              "Other Details",
                                                              style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            InkWell(
                                                                onTap: () {
                                                                  Get.back();
                                                                },
                                                                child:
                                                                    const Icon(
                                                                  Icons.clear,
                                                                  color: Colors
                                                                      .white,
                                                                ))
                                                          ],
                                                        ),
                                                      ),
                                                      FutureBuilder<
                                                              DeductionModel>(
                                                          future: Api.getDeductionDetailsApi(
                                                              storage
                                                                  .read(
                                                                      "client_id")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "client_name")
                                                                  .toString(),
                                                              storage
                                                                  .read(
                                                                      "admin_id")
                                                                  .toString(),
                                                              storage.read(
                                                                  "finacial_year")),
                                                          builder: (context,
                                                              snapshot) {
                                                            if (snapshot
                                                                .hasData) {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        10),
                                                                child: Column(
                                                                  children: [
                                                                    //Other
                                                                    const SizedBox(
                                                                      height:
                                                                          20,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Other",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .other,
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Date
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Date:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .dateOther
                                                                              .toString()
                                                                              .split(" ")[0],
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Remark
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Remark:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        Text(
                                                                          snapshot
                                                                              .data!
                                                                              .remarkOther,
                                                                          style:
                                                                              const TextStyle(fontSize: 13),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    //Attachment
                                                                    const SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        const Text(
                                                                          "Attachment:",
                                                                          style:
                                                                              TextStyle(fontSize: 13),
                                                                        ),
                                                                        TextButton(
                                                                            onPressed:
                                                                                () async {
                                                                              await openPDF.openFile(url: "https://appexperts.net/btconnect/${snapshot.data!.attachmentPathOther}", fileName: "${storage.read("client_name")}_otherDetails.pdf");
                                                                            },
                                                                            child:
                                                                                const Text(
                                                                              "View PDF",
                                                                              style: TextStyle(fontSize: 13, color: Color(0xff1739E4)),
                                                                            ))
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              );
                                                            } else if (snapshot
                                                                    .connectionState ==
                                                                ConnectionState
                                                                    .waiting) {
                                                              EasyLoading.show(
                                                                  status:
                                                                      "Loading...");
                                                              return Container();
                                                            } else {
                                                              EasyLoading
                                                                  .dismiss();
                                                              return Expanded(
                                                                child:
                                                                    Container(
                                                                  width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                                  height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .height,
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Text(
                                                                        "Data Not Available !!",
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              );
                                                            }
                                                          })
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        });
                                  },
                                  child: const Card(
                                    child: ListTile(
                                      title: Text("Other"),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      });
                },
                child: Image.asset(
                  "images/history.png",
                  width: 30,
                  color: Colors.black,
                )),
          ),
          const SizedBox(
            width: 20,
          )
        ],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 40,
              ),
              Form(
                  child: Column(
                children: [
                  //1 80C
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "80C",
                        style: TextStyle(fontSize: 16),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Flexible(
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              height: 40,
                              child: TextFormField(
                                readOnly: true,
                                keyboardType: TextInputType.number,
                                controller: controller.controller80C,
                                autofocus: false,
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF))),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF)))),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          InkWell(
                              onTap: () {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        backgroundColor: Colors.transparent,
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        contentPadding: EdgeInsets.zero,
                                        content: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 600,
                                          child: SingleChildScrollView(
                                            child: Column(
                                              children: [
                                                Container(
                                                    alignment: Alignment.center,
                                                    decoration:
                                                        const BoxDecoration(
                                                            color: Color(
                                                                0xff1739E4),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              topLeft: Radius
                                                                  .circular(10),
                                                              topRight: Radius
                                                                  .circular(10),
                                                            )),
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    height: 50,
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 10),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          const Text(""),
                                                          const Text(
                                                            "80C",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                          InkWell(
                                                              onTap: () {
                                                                Get.back();
                                                              },
                                                              child: const Icon(
                                                                Icons.clear,
                                                                color: Colors
                                                                    .white,
                                                              ))
                                                        ],
                                                      ),
                                                    )),
                                                //EPF,PF and LIC
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    children: [
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controller_epf_pf,
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText: "EPF,PF",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                      const SizedBox(
                                                        width: 10,
                                                      ),
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controller_lic,
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText: "LIC",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                                //National Saving and Tuition Fee for Children
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    children: [
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controller_national_saving,
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText:
                                                                "National Saving...",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                      const SizedBox(
                                                        width: 10,
                                                      ),
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controller_tuition_fees,
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText:
                                                                "Tuition Fee for Children",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                                //Senior Citizen Saving and Repayment of Principal
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    children: [
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controller_senior_citizen_saving,
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText:
                                                                "Senior Citizen Saving...",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                      const SizedBox(
                                                        width: 10,
                                                      ),
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controller_repayment_of_principle,
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText:
                                                                "Repayment of Principal",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                                //Sukanya Samridhi Scheme and Registration Fees
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    children: [
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controller_sukanya_samridhi_scheme,
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText:
                                                                "Sukanya Samridhi Scheme",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                      const SizedBox(
                                                        width: 10,
                                                      ),
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controller_reg_fees_stamp_duty,
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText:
                                                                "Registration Fees",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                                //Date
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Date"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          dateSelected(context);
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: const Color(
                                                                      0xffC1CDFF))),
                                                          child: TextFormField(
                                                            style:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13),
                                                            controller: controller
                                                                .controllerDate,
                                                            enabled: false,
                                                            decoration:
                                                                InputDecoration(
                                                                    suffixIcon:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                              .symmetric(
                                                                          vertical:
                                                                              5),
                                                                      child: SvgPicture
                                                                          .asset(
                                                                        "images/calender.svg",
                                                                      ),
                                                                    ),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        borderSide: const BorderSide(
                                                                            width:
                                                                                1,
                                                                            color:
                                                                                Color(0xffC1CDFF)))),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                //Remark
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Remark"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                        height: 40,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            border: Border.all(
                                                                width: 1,
                                                                color: const Color(
                                                                    0xffC1CDFF))),
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controllerRemark,
                                                          decoration: InputDecoration(
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      const BorderSide(
                                                                          width:
                                                                              1,
                                                                          color:
                                                                              Color(0xffC1CDFF)))),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      const Text(
                                                        "Attachment",
                                                        style: TextStyle(
                                                            color: Color(
                                                                0xff656A87)),
                                                      ),
                                                      InkWell(
                                                        onTap: () async {
                                                          if (Platform
                                                              .isWindows) {
                                                            FilePickerResult?
                                                                filePickerResult =
                                                                await FilePicker
                                                                    .platform
                                                                    .pickFiles(
                                                                        type: FileType
                                                                            .any);
                                                            if (filePickerResult !=
                                                                null) {
                                                              PlatformFile
                                                                  file =
                                                                  filePickerResult
                                                                      .files
                                                                      .single;
                                                              controller.image =
                                                                  File(file.path
                                                                      .toString());
                                                              print("File Path:" +
                                                                  controller
                                                                      .image
                                                                      .toString());
                                                            }
                                                          } else {
                                                            controller
                                                                .uploadPdf();
                                                          }
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              color: const Color(
                                                                  0xff1739E4),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                          child: Row(
                                                            children: [
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              const Text(
                                                                "Upload PDF",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              const Icon(
                                                                Icons
                                                                    .cloud_upload,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  margin: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 50,
                                                  child: ElevatedButton(
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                            primary: const Color(
                                                                0xff1739E4)),
                                                    onPressed: () {
                                                      controller
                                                          .api80CCall(context)
                                                          .whenComplete(() {
                                                        controller
                                                            .apiDeductionDetailsCall();
                                                      });
                                                    },
                                                    child: Text(
                                                        "Submit".toUpperCase()),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    });
                              },
                              child: SvgPicture.asset(
                                "images/info.svg",
                                width: 20,
                              )),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  //2 80CCD
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("80CCD"),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Flexible(
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                readOnly: true,
                                keyboardType: TextInputType.number,
                                controller: controller.controller80CCD,
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF))),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF)))),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          InkWell(
                              onTap: () {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        backgroundColor: Colors.transparent,
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        contentPadding: EdgeInsets.zero,
                                        content: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 480,
                                          child: SingleChildScrollView(
                                            child: Column(
                                              children: [
                                                Container(
                                                    alignment: Alignment.center,
                                                    decoration:
                                                        const BoxDecoration(
                                                            color: Color(
                                                                0xff1739E4),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              topLeft: Radius
                                                                  .circular(10),
                                                              topRight: Radius
                                                                  .circular(10),
                                                            )),
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    height: 50,
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 10),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          const Text(""),
                                                          const Text(
                                                            "80CCD",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                          InkWell(
                                                              onTap: () {
                                                                Get.back();
                                                              },
                                                              child: const Icon(
                                                                Icons.clear,
                                                                color: Colors
                                                                    .white,
                                                              ))
                                                        ],
                                                      ),
                                                    )), //Donation
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    children: [
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          controller: controller
                                                              .controller_donetion,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText: "NPS",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                                //Date
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Date"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          dateSelected2(
                                                              context);
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: const Color(
                                                                      0xffC1CDFF))),
                                                          child: TextFormField(
                                                            style:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13),
                                                            controller: controller
                                                                .controllerDate2,
                                                            enabled: false,
                                                            decoration:
                                                                InputDecoration(
                                                                    suffixIcon:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                              .symmetric(
                                                                          vertical:
                                                                              5),
                                                                      child: SvgPicture
                                                                          .asset(
                                                                        "images/calender.svg",
                                                                      ),
                                                                    ),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        borderSide: const BorderSide(
                                                                            width:
                                                                                1,
                                                                            color:
                                                                                Color(0xffC1CDFF)))),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                //Remark
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Remark"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                        height: 40,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            border: Border.all(
                                                                width: 1,
                                                                color: const Color(
                                                                    0xffC1CDFF))),
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controllerRemark,
                                                          decoration: InputDecoration(
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      const BorderSide(
                                                                          width:
                                                                              1,
                                                                          color:
                                                                              Color(0xffC1CDFF)))),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      const Text(
                                                        "Attachment",
                                                        style: TextStyle(
                                                            color: Color(
                                                                0xff656A87)),
                                                      ),
                                                      InkWell(
                                                        onTap: () async {
                                                          if (Platform
                                                              .isWindows) {
                                                            FilePickerResult?
                                                                filePickerResult =
                                                                await FilePicker
                                                                    .platform
                                                                    .pickFiles(
                                                                        type: FileType
                                                                            .any);
                                                            if (filePickerResult !=
                                                                null) {
                                                              PlatformFile
                                                                  file =
                                                                  filePickerResult
                                                                      .files
                                                                      .single;
                                                              controller.image =
                                                                  File(file.path
                                                                      .toString());
                                                              controller
                                                                      .pdf_file =
                                                                  File(file.path
                                                                      .toString());
                                                              print("File Path:" +
                                                                  controller
                                                                      .image
                                                                      .toString());
                                                            }
                                                          } else {
                                                            controller
                                                                .uploadPdf();
                                                          }
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              color: const Color(
                                                                  0xff1739E4),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                          child: Row(
                                                            children: [
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              const Text(
                                                                "Upload PDF",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              const Icon(
                                                                Icons
                                                                    .cloud_upload,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Container(
                                                  margin: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 50,
                                                  child: ElevatedButton(
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                            primary: const Color(
                                                                0xff1739E4)),
                                                    onPressed: () {
                                                      controller
                                                          .api80ccdCall(context)
                                                          .whenComplete(() {
                                                        controller
                                                            .apiDeductionDetailsCall();
                                                      });
                                                    },
                                                    child: Text(
                                                        "Submit".toUpperCase()),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    });
                              },
                              child: SvgPicture.asset(
                                "images/info.svg",
                                width: 20,
                              ))
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  //3 80D
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "80D",
                        style: TextStyle(fontSize: 16),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Flexible(
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                readOnly: true,
                                keyboardType: TextInputType.number,
                                controller: controller.controller80D,
                                autofocus: false,
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF))),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF)))),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          InkWell(
                              onTap: () {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        backgroundColor:
                                            const Color(0xff1739E4),
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        contentPadding: EdgeInsets.zero,
                                        content: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 480,
                                          child: SingleChildScrollView(
                                            child: Column(
                                              children: [
                                                Container(
                                                    alignment: Alignment.center,
                                                    decoration:
                                                        const BoxDecoration(
                                                            color: Color(
                                                                0xff1739E4),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              topLeft: Radius
                                                                  .circular(10),
                                                              topRight: Radius
                                                                  .circular(10),
                                                            )),
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    height: 50,
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 10),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          const Text(""),
                                                          const Text(
                                                            "80D",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                          InkWell(
                                                              onTap: () {
                                                                Get.back();
                                                              },
                                                              child: const Icon(
                                                                Icons.clear,
                                                                color: Colors
                                                                    .white,
                                                              ))
                                                        ],
                                                      ),
                                                    )),
                                                //Parents and Self
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    children: [
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          controller: controller
                                                              .controller_parents,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText:
                                                                "Parents",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                      const SizedBox(
                                                        width: 10,
                                                      ),
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          controller: controller
                                                              .controller_self,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText: "Self",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                                //Date
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Date"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          dateSelected3(
                                                              context);
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: const Color(
                                                                      0xffC1CDFF))),
                                                          child: TextFormField(
                                                            style:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13),
                                                            controller: controller
                                                                .controllerDate3,
                                                            enabled: false,
                                                            decoration:
                                                                InputDecoration(
                                                                    suffixIcon:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                              .symmetric(
                                                                          vertical:
                                                                              5),
                                                                      child: SvgPicture
                                                                          .asset(
                                                                        "images/calender.svg",
                                                                      ),
                                                                    ),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        borderSide: const BorderSide(
                                                                            width:
                                                                                1,
                                                                            color:
                                                                                Color(0xffC1CDFF)))),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                //Remark
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Remark"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                        height: 40,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            border: Border.all(
                                                                width: 1,
                                                                color: const Color(
                                                                    0xffC1CDFF))),
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controllerRemark,
                                                          decoration: InputDecoration(
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      const BorderSide(
                                                                          width:
                                                                              1,
                                                                          color:
                                                                              Color(0xffC1CDFF)))),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      const Text(
                                                        "Attachment",
                                                        style: TextStyle(
                                                            color: Color(
                                                                0xff656A87)),
                                                      ),
                                                      InkWell(
                                                        onTap: () async {
                                                          if (Platform
                                                              .isWindows) {
                                                            FilePickerResult?
                                                                filePickerResult =
                                                                await FilePicker
                                                                    .platform
                                                                    .pickFiles(
                                                                        type: FileType
                                                                            .any);
                                                            if (filePickerResult !=
                                                                null) {
                                                              PlatformFile
                                                                  file =
                                                                  filePickerResult
                                                                      .files
                                                                      .single;
                                                              controller.image =
                                                                  File(file.path
                                                                      .toString());
                                                              print("File Path:" +
                                                                  controller
                                                                      .image
                                                                      .toString());
                                                            }
                                                          } else {
                                                            controller
                                                                .uploadPdf();
                                                          }
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              color: const Color(
                                                                  0xff1739E4),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                          child: Row(
                                                            children: [
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              const Text(
                                                                "Upload PDF",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              const Icon(
                                                                Icons
                                                                    .cloud_upload,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Container(
                                                  margin: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 50,
                                                  child: ElevatedButton(
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                            primary: const Color(
                                                                0xff1739E4)),
                                                    onPressed: () {
                                                      controller
                                                          .api80dCall(context)
                                                          .whenComplete(() {
                                                        controller
                                                            .apiDeductionDetailsCall();
                                                      });
                                                    },
                                                    child: Text(
                                                        "Submit".toUpperCase()),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    });
                              },
                              child: SvgPicture.asset(
                                "images/info.svg",
                                width: 20,
                              ))
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  //4 80G
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "80G",
                        style: TextStyle(fontSize: 16),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Flexible(
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                readOnly: true,
                                keyboardType: TextInputType.number,
                                controller: controller.controller80G,
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF))),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF)))),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          InkWell(
                              onTap: () {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        backgroundColor: Colors.transparent,
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        contentPadding: EdgeInsets.zero,
                                        content: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 540,
                                          child: SingleChildScrollView(
                                            child: Column(
                                              children: [
                                                Container(
                                                    alignment: Alignment.center,
                                                    decoration:
                                                        const BoxDecoration(
                                                            color: Color(
                                                                0xff1739E4),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              topLeft: Radius
                                                                  .circular(10),
                                                              topRight: Radius
                                                                  .circular(10),
                                                            )),
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    height: 50,
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 10),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          const Text(""),
                                                          const Text(
                                                            "80G",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                          InkWell(
                                                              onTap: () {
                                                                Get.back();
                                                              },
                                                              child: const Icon(
                                                                Icons.clear,
                                                                color: Colors
                                                                    .white,
                                                              ))
                                                        ],
                                                      ),
                                                    )),
                                                Row(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Obx(
                                                          () => Radio(
                                                              activeColor:
                                                                  const Color(
                                                                      0xff1739E4),
                                                              value: "100%",
                                                              groupValue:
                                                                  controller
                                                                      .radioValue
                                                                      .value,
                                                              onChanged:
                                                                  (newValue) {
                                                                controller
                                                                    .setRadioValue(
                                                                        newValue);
                                                              }),
                                                        ),
                                                        const SizedBox(
                                                          width: 2,
                                                        ),
                                                        const Text("100%")
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        Obx(
                                                          () => Radio(
                                                              activeColor:
                                                                  const Color(
                                                                      0xff1739E4),
                                                              value: "50%",
                                                              groupValue:
                                                                  controller
                                                                      .radioValue
                                                                      .value,
                                                              onChanged:
                                                                  (newValue) {
                                                                controller
                                                                    .setRadioValue(
                                                                        newValue);
                                                              }),
                                                        ),
                                                        const SizedBox(
                                                          width: 2,
                                                        ),
                                                        const Text("50%")
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                //Other(By Cheque) and In Cash
                                                const SizedBox(
                                                  height: 20,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    children: [
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText:
                                                                "Other(By Cheque)",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                      const SizedBox(
                                                        width: 10,
                                                      ),
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: InkWell(
                                                          onTap: () {
                                                            dateSelected5(
                                                                context);
                                                          },
                                                          child: TextFormField(
                                                            keyboardType:
                                                                TextInputType
                                                                    .number,
                                                            decoration:
                                                                InputDecoration(
                                                              labelText:
                                                                  "In Cash",
                                                              labelStyle:
                                                                  const TextStyle(
                                                                      fontSize:
                                                                          13,
                                                                      color: Color(
                                                                          0xff656A87)),
                                                              enabledBorder: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      const BorderSide(
                                                                          width:
                                                                              1,
                                                                          color:
                                                                              Color(0xffC1CDFF))),
                                                              focusedBorder: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      const BorderSide(
                                                                          width:
                                                                              1,
                                                                          color:
                                                                              Color(0xffC1CDFF))),
                                                            ),
                                                          ),
                                                        ),
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                                //Date
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Date"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          dateSelected4(
                                                              context);
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: const Color(
                                                                      0xffC1CDFF))),
                                                          child: TextFormField(
                                                            style:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13),
                                                            controller: controller
                                                                .controllerDate4,
                                                            enabled: false,
                                                            decoration:
                                                                InputDecoration(
                                                                    suffixIcon:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                              .symmetric(
                                                                          vertical:
                                                                              5),
                                                                      child: SvgPicture
                                                                          .asset(
                                                                        "images/calender.svg",
                                                                      ),
                                                                    ),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        borderSide: const BorderSide(
                                                                            width:
                                                                                1,
                                                                            color:
                                                                                Color(0xffC1CDFF)))),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                //Remark
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Remark"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                        height: 40,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            border: Border.all(
                                                                width: 1,
                                                                color: const Color(
                                                                    0xffC1CDFF))),
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controllerRemark,
                                                          decoration: InputDecoration(
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      const BorderSide(
                                                                          width:
                                                                              1,
                                                                          color:
                                                                              Color(0xffC1CDFF)))),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      const Text(
                                                        "Attachment",
                                                        style: TextStyle(
                                                            color: Color(
                                                                0xff656A87)),
                                                      ),
                                                      InkWell(
                                                        onTap: () async {
                                                          if (Platform
                                                              .isWindows) {
                                                            FilePickerResult?
                                                                filePickerResult =
                                                                await FilePicker
                                                                    .platform
                                                                    .pickFiles(
                                                                        type: FileType
                                                                            .any);
                                                            if (filePickerResult !=
                                                                null) {
                                                              PlatformFile
                                                                  file =
                                                                  filePickerResult
                                                                      .files
                                                                      .single;
                                                              controller.image =
                                                                  File(file.path
                                                                      .toString());
                                                              print("File Path:" +
                                                                  controller
                                                                      .image
                                                                      .toString());
                                                            }
                                                          } else {
                                                            controller
                                                                .uploadPdf();
                                                          }
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              color: const Color(
                                                                  0xff1739E4),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                          child: Row(
                                                            children: const [
                                                              SizedBox(
                                                                width: 10,
                                                              ),
                                                              Text(
                                                                "Upload PDF",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                              SizedBox(
                                                                width: 10,
                                                              ),
                                                              Icon(
                                                                Icons
                                                                    .cloud_upload,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              SizedBox(
                                                                width: 10,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Container(
                                                  margin: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 50,
                                                  child: ElevatedButton(
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                            primary: const Color(
                                                                0xff1739E4)),
                                                    onPressed: () {
                                                      controller
                                                          .api80GCall(context)
                                                          .whenComplete(() {
                                                        controller
                                                            .apiDeductionDetailsCall();
                                                      });
                                                    },
                                                    child: Text(
                                                        "Submit".toUpperCase()),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    });
                              },
                              child: SvgPicture.asset(
                                "images/info.svg",
                                width: 20,
                              ))
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  //5 80TTA
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "80TTA",
                        style: TextStyle(fontSize: 16),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Flexible(
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                readOnly: true,
                                keyboardType: TextInputType.number,
                                controller: controller.controller80TTA,
                                autofocus: false,
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF))),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF)))),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          InkWell(
                              onTap: () {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        backgroundColor: Colors.transparent,
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        contentPadding: EdgeInsets.zero,
                                        content: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 480,
                                          child: SingleChildScrollView(
                                            child: Column(
                                              children: [
                                                Container(
                                                    alignment: Alignment.center,
                                                    decoration:
                                                        const BoxDecoration(
                                                            color: Color(
                                                                0xff1739E4),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              topLeft: Radius
                                                                  .circular(10),
                                                              topRight: Radius
                                                                  .circular(10),
                                                            )),
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    height: 50,
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 10),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          const Text(""),
                                                          const Text(
                                                            "80TTA",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                          InkWell(
                                                              onTap: () {
                                                                Get.back();
                                                              },
                                                              child: const Icon(
                                                                Icons.clear,
                                                                color: Colors
                                                                    .white,
                                                              ))
                                                        ],
                                                      ),
                                                    )),
                                                //80TTA
                                                const SizedBox(
                                                  height: 20,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    children: [
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          controller: controller
                                                              .deduction_80tta_controller,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText: "80TTA",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                                //Date
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Date"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          dateSelected5(
                                                              context);
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: const Color(
                                                                      0xffC1CDFF))),
                                                          child: TextFormField(
                                                            controller: controller
                                                                .controllerDate5,
                                                            enabled: false,
                                                            decoration:
                                                                InputDecoration(
                                                                    suffixIcon:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                              .symmetric(
                                                                          vertical:
                                                                              5),
                                                                      child: SvgPicture
                                                                          .asset(
                                                                        "images/calender.svg",
                                                                      ),
                                                                    ),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        borderSide: const BorderSide(
                                                                            width:
                                                                                1,
                                                                            color:
                                                                                Color(0xffC1CDFF)))),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                //Remark
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Remark"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                        height: 40,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            border: Border.all(
                                                                width: 1,
                                                                color: const Color(
                                                                    0xffC1CDFF))),
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controllerRemark,
                                                          decoration: InputDecoration(
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      const BorderSide(
                                                                          width:
                                                                              1,
                                                                          color:
                                                                              Color(0xffC1CDFF)))),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      const Text(
                                                        "Attachment",
                                                        style: TextStyle(
                                                            color: Color(
                                                                0xff656A87)),
                                                      ),
                                                      InkWell(
                                                        onTap: () async {
                                                          if (Platform
                                                              .isWindows) {
                                                            FilePickerResult?
                                                                filePickerResult =
                                                                await FilePicker
                                                                    .platform
                                                                    .pickFiles(
                                                                        type: FileType
                                                                            .any);
                                                            if (filePickerResult !=
                                                                null) {
                                                              PlatformFile
                                                                  file =
                                                                  filePickerResult
                                                                      .files
                                                                      .single;
                                                              controller.image =
                                                                  File(file.path
                                                                      .toString());
                                                              print("File Path:" +
                                                                  controller
                                                                      .image
                                                                      .toString());
                                                            }
                                                          } else {
                                                            controller
                                                                .uploadPdf();
                                                          }
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              color: const Color(
                                                                  0xff1739E4),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                          child: Row(
                                                            children: [
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              const Text(
                                                                "Upload PDF",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              const Icon(
                                                                Icons
                                                                    .cloud_upload,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Container(
                                                  margin: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 50,
                                                  child: ElevatedButton(
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                            primary: const Color(
                                                                0xff1739E4)),
                                                    onPressed: () {
                                                      controller
                                                          .api80TTACall(context)
                                                          .whenComplete(() {
                                                        controller
                                                            .apiDeductionDetailsCall();
                                                      });
                                                    },
                                                    child: Text(
                                                        "Submit".toUpperCase()),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    });
                              },
                              child: SvgPicture.asset(
                                "images/info.svg",
                                width: 20,
                              ))
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  //6 Other
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Other"),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Flexible(
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                readOnly: true,
                                keyboardType: TextInputType.number,
                                controller: controller.controllerOther,
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF))),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xffC1CDFF)))),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          InkWell(
                              onTap: () {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        backgroundColor: Colors.transparent,
                                        insetPadding:
                                            const EdgeInsets.symmetric(
                                                horizontal: 10),
                                        contentPadding: EdgeInsets.zero,
                                        content: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 480,
                                          child: SingleChildScrollView(
                                            child: Column(
                                              children: [
                                                Container(
                                                    alignment: Alignment.center,
                                                    decoration:
                                                        const BoxDecoration(
                                                            color: Color(
                                                                0xff1739E4),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              topLeft: Radius
                                                                  .circular(10),
                                                              topRight: Radius
                                                                  .circular(10),
                                                            )),
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    height: 50,
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 10),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          const Text(""),
                                                          const Text(
                                                            "Other",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                          InkWell(
                                                              onTap: () {
                                                                Get.back();
                                                              },
                                                              child: const Icon(
                                                                Icons.clear,
                                                                color: Colors
                                                                    .white,
                                                              ))
                                                        ],
                                                      ),
                                                    )),
                                                //Other
                                                const SizedBox(
                                                  height: 20,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    children: [
                                                      Flexible(
                                                          child: SizedBox(
                                                        height: 40,
                                                        child: TextFormField(
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          controller: controller
                                                              .deductionOtherController,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText: "Other",
                                                            labelStyle:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    color: Color(
                                                                        0xff656A87)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                borderSide: const BorderSide(
                                                                    width: 1,
                                                                    color: Color(
                                                                        0xffC1CDFF))),
                                                          ),
                                                        ),
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                                //Date
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Date"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          dateSelected5(
                                                              context);
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: const Color(
                                                                      0xffC1CDFF))),
                                                          child: TextFormField(
                                                            style:
                                                                const TextStyle(
                                                                    fontSize:
                                                                        13),
                                                            controller: controller
                                                                .controllerDate5,
                                                            enabled: false,
                                                            decoration:
                                                                InputDecoration(
                                                                    suffixIcon:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                              .symmetric(
                                                                          vertical:
                                                                              5),
                                                                      child: SvgPicture
                                                                          .asset(
                                                                        "images/calender.svg",
                                                                      ),
                                                                    ),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        borderSide: const BorderSide(
                                                                            width:
                                                                                1,
                                                                            color:
                                                                                Color(0xffC1CDFF)))),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                //Remark
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text("Remark"),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                        height: 40,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            border: Border.all(
                                                                width: 1,
                                                                color: const Color(
                                                                    0xffC1CDFF))),
                                                        child: TextFormField(
                                                          controller: controller
                                                              .controllerRemark,
                                                          decoration: InputDecoration(
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  borderSide:
                                                                      const BorderSide(
                                                                          width:
                                                                              1,
                                                                          color:
                                                                              Color(0xffC1CDFF)))),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      const Text(
                                                        "Attachment",
                                                        style: TextStyle(
                                                            color: Color(
                                                                0xff656A87)),
                                                      ),
                                                      InkWell(
                                                        onTap: () async {
                                                          if (Platform
                                                              .isWindows) {
                                                            FilePickerResult?
                                                                filePickerResult =
                                                                await FilePicker
                                                                    .platform
                                                                    .pickFiles(
                                                                        type: FileType
                                                                            .any);
                                                            if (filePickerResult !=
                                                                null) {
                                                              PlatformFile
                                                                  file =
                                                                  filePickerResult
                                                                      .files
                                                                      .single;
                                                              controller.image =
                                                                  File(file.path
                                                                      .toString());
                                                              print("File Path:" +
                                                                  controller
                                                                      .image
                                                                      .toString());
                                                            }
                                                          } else {
                                                            controller
                                                                .uploadPdf();
                                                          }
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              color: const Color(
                                                                  0xff1739E4),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                          child: Row(
                                                            children: [
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              const Text(
                                                                "Upload PDF",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              const Icon(
                                                                Icons
                                                                    .cloud_upload,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                Container(
                                                  margin: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  height: 50,
                                                  child: ElevatedButton(
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                            primary: const Color(
                                                                0xff1739E4)),
                                                    onPressed: () {
                                                      controller
                                                          .apiOtherDeductionCall(
                                                              context)
                                                          .whenComplete(() {
                                                        controller
                                                            .apiDeductionDetailsCall();
                                                      });
                                                    },
                                                    child: Text(
                                                        "Submit".toUpperCase()),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    });
                              },
                              child: SvgPicture.asset(
                                "images/info.svg",
                                width: 20,
                              ))
                        ],
                      ),
                    ],
                  ),
                ],
              ))
            ],
          ),
        ),
      ),
    );
  }

  dateSelected(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 1));

    if (now != null) {
      controller.controllerDate.text =
          formatDate(now!, [yyyy, '-', mm, '-', dd]);
    }
  }

  dateSelected2(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 1));

    if (now != null) {
      controller.controllerDate2.text =
          formatDate(now!, [yyyy, '-', mm, '-', dd]);
    }
  }

  dateSelected3(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 1));

    if (now != null) {
      controller.controllerDate3.text =
          formatDate(now!, [yyyy, '-', mm, '-', dd]);
    }
  }

  dateSelected4(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 1));

    if (now != null) {
      controller.controllerDate4.text =
          formatDate(now!, [yyyy, '-', mm, '-', dd]);
    }
  }

  dateSelected5(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 1));

    if (now != null) {
      controller.controllerDate5.text =
          formatDate(now!, [yyyy, '-', mm, '-', dd]);
    }
  }

  dateSelected6(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 1));

    if (now != null) {
      controller.controllerDate6.text =
          formatDate(now!, [yyyy, '-', mm, '-', dd]);
    }
  }
}
