// ignore_for_file: non_constant_identifier_names, avoid_print

import 'dart:io';

import 'package:accountx/app/services/api.dart';
import 'package:date_format/date_format.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../model/deduction_model.dart';

class DeductionController extends GetxController {
  GetStorage box = GetStorage();
  TextEditingController controller80C = TextEditingController();
  TextEditingController controller80CCD = TextEditingController();
  TextEditingController controller80D = TextEditingController();
  TextEditingController controller80G = TextEditingController();
  TextEditingController controller80TTA = TextEditingController();
  TextEditingController controllerOther = TextEditingController();
  TextEditingController controllerDate = TextEditingController();
  TextEditingController controllerDate2 = TextEditingController();
  TextEditingController controllerDate3 = TextEditingController();
  TextEditingController controllerDate4 = TextEditingController();
  TextEditingController controllerDate5 = TextEditingController();
  TextEditingController controllerDate6 = TextEditingController();
  TextEditingController controllerRemark = TextEditingController();

  TextEditingController controller_epf_pf = TextEditingController();
  TextEditingController controller_lic = TextEditingController();
  TextEditingController controller_national_saving = TextEditingController();
  TextEditingController controller_tuition_fees = TextEditingController();
  TextEditingController controller_senior_citizen_saving = TextEditingController();
  TextEditingController controller_repayment_of_principle = TextEditingController();
  TextEditingController controller_sukanya_samridhi_scheme = TextEditingController();
  TextEditingController controller_reg_fees_stamp_duty = TextEditingController();

  TextEditingController controller_donetion = TextEditingController();

  TextEditingController controller_parents = TextEditingController();
  TextEditingController controller_self = TextEditingController();

  TextEditingController deduction_80g_other_by_cheque_contoller = TextEditingController();
  TextEditingController deduction_80g_in_cash_contoller = TextEditingController();

  TextEditingController deduction_80tta_controller = TextEditingController();

  TextEditingController deductionOtherController = TextEditingController();

  var radioValue = "bankGroup".obs;

  void setRadioValue(var value) {
    radioValue.value = value;
  }

  File? image;
  File? pdf_file;

  @override
  void onInit() {
    // controller80C.text = "0.0";
    // controller80CCD.text = "0.0";
    // controller80D.text = "0.0";
    // controller80G.text = "0.0";
    // controller80TTA.text = "0.0";
    // controllerOther.text = "0.0";

    controllerDate.text = formatDate(DateTime(DateTime.now().year - 1, 4, 1), [yyyy, '-', mm, '-', dd]);
    controllerDate2.text = formatDate(DateTime(DateTime.now().year - 1, 4, 1), [yyyy, '-', mm, '-', dd]);
    controllerDate3.text = formatDate(DateTime(DateTime.now().year - 1, 4, 1), [yyyy, '-', mm, '-', dd]);
    controllerDate4.text = formatDate(DateTime(DateTime.now().year - 1, 4, 1), [yyyy, '-', mm, '-', dd]);
    controllerDate5.text = formatDate(DateTime(DateTime.now().year - 1, 4, 1), [yyyy, '-', mm, '-', dd]);
    controllerDate6.text = formatDate(DateTime(DateTime.now().year - 1, 4, 1), [yyyy, '-', mm, '-', dd]);

    super.onInit();
  }
  // Future uploadFromCamera() async {
  //   PickedFile? pickedFile = await ImagePicker().getImage(source: ImageSource.camera, imageQuality: 30);

  //   if (pickedFile != null) {
  //     image = File(pickedFile.path);
  //     update();
  //   } else {
  //     print('No image selected.');
  //   }
  // }

  // Future uploadFromGallery() async {
  //   PickedFile? pickedFile = await ImagePicker().getImage(source: ImageSource.gallery, imageQuality: 30);
  //   if (pickedFile != null) {
  //     image = File(pickedFile.path);
  //     update();
  //   } else {
  //     print('No image selected.');
  //   }
  // }

  Future uploadPdf() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
    );

    if (result != null) {
      pdf_file = File(result.files.single.path.toString());
    } else {
      // User canceled the picker
    }
  }

//--------------80c Api Call Function--------------------
  Future api80CCall(BuildContext context) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("admin_id");
    var financialYear = box.read("finacial_year");
    var pdfFileTemp;
    if(pdf_file == null) {
     Get.defaultDialog(
         barrierDismissible: false,
         content: Text("Please Upload File !!"),
       actions: [
         TextButton(onPressed: () => Get.back(), child: Text("OK"))
       ]
     );
    }else{
      EasyLoading.show(status: "Loading...");
      pdfFileTemp=pdf_file!.path.toString();
      await Api.insertDeduction80CApi(
          clientId.toString(),
          clientName.toString(),
          adminId.toString(),
          financialYear.toString(),
          controllerDate.text,
          controllerRemark.text,
          pdfFileTemp,
          '80c',
          controller_epf_pf.text,
          controller_lic.text,
          controller_national_saving.text,
          controller_tuition_fees.text,
          controller_senior_citizen_saving.text,
          controller_repayment_of_principle.text,
          controller_sukanya_samridhi_scheme.text,
          controller_reg_fees_stamp_duty.text)
          .then((value) {
        if (value["response_status"].toString() == "1") {

          EasyLoading.dismiss();
          Get.defaultDialog(
              barrierDismissible: false,
              content: Text(value["message"]),
          actions: [
            TextButton(onPressed: () {
              controller_epf_pf.clear();
              controller_lic.clear();
              controller_national_saving.clear();
              controller_tuition_fees.clear();
              controller_senior_citizen_saving.clear();
              controller_repayment_of_principle.clear();
              controller_sukanya_samridhi_scheme.clear();
              controller_reg_fees_stamp_duty.clear();
              controllerRemark.clear();
              Get.back();
            }, child: Text("OK"))
          ]
          );

        } else {
          EasyLoading.dismiss();
          Get.defaultDialog(
              barrierDismissible: false,
              content: Text(value["message"]),
              actions: [
                TextButton(onPressed: () => Get.back(), child: Text("OK"))
              ]
          );
        }
      });
    }

  }

  //--------------80ccd Api Call Funcation--------------------
  Future api80ccdCall(BuildContext context) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    if(pdf_file == null) {
      Get.defaultDialog(
          barrierDismissible: false,
          content: Text("Please Upload File !!"),
          actions: [
            TextButton(onPressed: () => Get.back(), child: Text("OK"))
          ]
      );
    } else {
      EasyLoading.show(status: "Loading...");
      print("------controllerDate2--------------" + controllerDate2.text);
      await Api.insertDeduction80CCDApi(clientId, clientName, adminId, financialYear, controllerDate2.text, controllerRemark.text, File(pdf_file!.path),
          '80ccd', controller_donetion.text)
          .then((value) {
        if (value["response_status"] == "1") {
          EasyLoading.dismiss();
          Get.defaultDialog(
            barrierDismissible: false,
            content: Text(value["message"]),
            actions: [
              TextButton(onPressed: () {
                controller_donetion.clear();
                controllerRemark.clear();
                Get.back();
              }, child: Text("OK"))
            ]
          );
        } else {
          EasyLoading.dismiss();
          Get.defaultDialog(
              barrierDismissible: false,
              content: Text(value["message"]),
              actions: [
                TextButton(onPressed: () => Get.back(), child: Text("OK"))
              ]
          );        }
      });
    }

  }

  Future api80dCall(BuildContext context) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    if(pdf_file == null) {
      Get.defaultDialog(
          barrierDismissible: false,
          content: Text("Please Upload File !!"),
          actions: [
            TextButton(onPressed: () => Get.back(), child: Text("OK"))
          ]
      );
    } else {
      EasyLoading.show(status: "Loading...");
      print("------controllerDate2--------------" + controllerDate2.text);
      await Api.insertDeduction80DApi(clientId, clientName, adminId, financialYear, controllerDate2.text, controllerRemark.text, File(pdf_file!.path),
          '80d', controller_parents.text, controller_self.text)
          .then((value) {
        if (value["response_status"] == "1") {
          EasyLoading.dismiss();
         Get.defaultDialog(
           barrierDismissible: false,
           content: Text(value["message"]),
           actions: [
             TextButton(onPressed: () {
               controller_parents.clear();
               controller_self.clear();
               controllerRemark.clear();
               Get.back();
             }, child: Text("OK"))
           ]
         );
        } else {
          EasyLoading.dismiss();
          Get.defaultDialog(
              barrierDismissible: false,
              content: Text(value["message"]),
              actions: [
                TextButton(onPressed: () => Get.back(), child: Text("OK"))
              ]
          );
        }
      });
    }
  }

  //Deduction 80G Api

  Future api80GCall(BuildContext context) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    if(pdf_file == null) {
      Get.defaultDialog(
          barrierDismissible: false,
          content: Text("Please Upload File !!"),
          actions: [
            TextButton(onPressed: () => Get.back(), child: Text("OK"))
          ]
      );
    } else {
      EasyLoading.show(status: "Loading...");
      await Api.insertDeduction80GApi(clientId, clientName, adminId, financialYear, controllerDate4.text, controllerRemark.text, File(pdf_file!.path),
          "80g", radioValue.value, deduction_80g_other_by_cheque_contoller.text, deduction_80g_in_cash_contoller.text)
          .then((value) {
        if (value["response_status"] == "1") {
          EasyLoading.dismiss();
          Get.defaultDialog(
            barrierDismissible: false,
            content: Text(value["message"]),
            actions: [
              TextButton(onPressed: () {
                deduction_80g_other_by_cheque_contoller.clear();
                deduction_80g_in_cash_contoller.clear();
                controllerRemark.clear();
              }, child: Text("OK"))
            ]
        );
        } else {
          EasyLoading.dismiss();
          Get.defaultDialog(
              barrierDismissible: false,
              content: Text(value["message"]),
              actions: [
                TextButton(onPressed: () => Get.back(), child: Text("OK"))
              ]
          );
        }
      });
    }
  }

  //Deduction 80TTA Api

  Future api80TTACall(BuildContext context) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    if(pdf_file == null) {
      Get.defaultDialog(
          barrierDismissible: false,
          content: Text("Please Upload File !!"),
          actions: [
            TextButton(onPressed: () => Get.back(), child: Text("OK"))
          ]
      );
    } else {
      EasyLoading.show(status: "Loading...");
      await Api.insertDeduction80TTAApi(clientId, clientName, adminId, financialYear, controllerDate5.text, controllerRemark.text, File(pdf_file!.path),
          "80tta", deduction_80tta_controller.text)
          .then((value) {
        if (value["response_status"] == "1") {
          EasyLoading.dismiss();
          Get.defaultDialog(
              barrierDismissible: false,
              content: Text(value["message"]),
              actions: [
                TextButton(onPressed: () {
                  deduction_80tta_controller.clear();
                  controllerRemark.clear();
                  Get.back();
                }, child: Text("OK"))
              ]
          );
        } else {
          EasyLoading.dismiss();
          Get.defaultDialog(
              barrierDismissible: false,
              content: Text(value["message"]),
              actions: [
                TextButton(onPressed: () => Get.back(), child: Text("OK"))
              ]
          );
        }
      });
    }
  }

  //Deduction Other Api
  Future apiOtherDeductionCall(BuildContext context) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    if(pdf_file == null) {
      Get.defaultDialog(
          barrierDismissible: false,
          content: Text("Please Upload File !!"),
          actions: [
            TextButton(onPressed: () => Get.back(), child: Text("OK"))
          ]
      );
    } else {
      EasyLoading.show(status: "Loading...");
      await Api.insertDeductionOtherApi(clientId, clientName, adminId, financialYear, controllerDate6.text, controllerRemark.text, File(pdf_file!.path),
          "other", deductionOtherController.text)
          .then((value) {
        if (value["response_status"] == "1") {
          EasyLoading.dismiss();
          Get.defaultDialog(
              barrierDismissible: false,
              content: Text(value["message"]),
              actions: [
                TextButton(onPressed: () {
                  deductionOtherController.clear();
                  controllerRemark.clear();
                  Get.back();
                }, child: Text("OK"))
              ]
          );
        } else {
          EasyLoading.dismiss();
          Get.defaultDialog(
              barrierDismissible: false,
              content: Text(value["message"]),
              actions: [
                TextButton(onPressed: () => Get.back(), child: Text("OK"))
              ]
          );
        }
      });
    }
  }


  apiDeductionDetailsCall() {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    Api.getDeductionDetailsApi2(clientId, clientName, adminId, financialYear).then((value) {
      print(value["response_status"].toString() + " working");
      controller80C.text = value["80c"]==null?"0":value["80c"];
      controller80CCD.text = value["80ccd"]==null?"0":value["80ccd"];
      controller80D.text = value["80d"]==null?"0":value["80d"];
      controller80G.text = value["80g"]==null?"0":value["80g"];
      controller80TTA.text = value["80tta"]==null?"0":value["80tta"];
      controllerOther.text = value["other"]==null?"0":value["other"];
    });
  }
  GetStorage storage = GetStorage();
  Future getDeducationData()async{
    DeductionModel deductionModel = await Api.getDeductionDetailsApi(
        storage
            .read(
            "client_id")
            .toString(),
        storage
            .read(
            "client_name")
            .toString(),
        storage
            .read(
            "admin_id")
            .toString(),
        storage.read(
            "finacial_year"));

    print("----(((( 6 feb data-----"+deductionModel.toString());
  }
}
