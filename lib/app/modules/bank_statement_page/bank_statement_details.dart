// ignore_for_file: avoid_print, duplicate_ignore

import 'package:flutter/material.dart';

// ignore: must_be_immutable
class BankStatementDetails extends StatefulWidget {

  String accountNo;
  String bankName;
  String type;
  String fromdate;
  String todate;
  String financialyear;
  String baseurl;
  String bankstatement;
  String interestpaid;
  String interestreceived;
  String charges;
  String other;


   BankStatementDetails({Key? key,required this.accountNo,required this.bankName,required this.type,

   required this.fromdate,
     required this.todate,
     required this.financialyear,
     required this.baseurl,
     required this.bankstatement,
     required this.interestpaid,
     required this.interestreceived,
     required this.charges,
     required this.other,
   }) : super(key: key);

  @override
  State<BankStatementDetails> createState() => _BankStatementDetailsState();
}

class _BankStatementDetailsState extends State<BankStatementDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            SizedBox(height: 20,),
            Center(child: Text("Bank Statement Details",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),)),
            SizedBox(height: 20,),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              margin: const EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(width: 1,color: const Color(0xffC1CDFF))
              ),
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: [
                  //Bank Name
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Bank Name: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        Text(widget.bankName,style: const TextStyle(fontSize: 16),),
                      ],
                    ),
                  ),
                  //Account Number
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Account Number: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        Text(widget.accountNo,style: const TextStyle(fontSize: 16),),
                      ],
                    ),
                  ),
                  //Account Type
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Account Type: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        Text(widget.type,style: const TextStyle(fontSize: 16),),
                      ],
                    ),
                  ),
                  //From Date
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("From Date: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        Text(widget.fromdate,style: const TextStyle(fontSize: 16),),
                      ],
                    ),
                  ),
                  //To Date
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("To Date: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        Text(widget.todate,style: const TextStyle(fontSize: 16),),
                      ],
                    ),
                  ),
                  //Financial Year
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Financial Year: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        Text(widget.financialyear,style: const TextStyle(fontSize: 16),),
                      ],
                    ),
                  ),
                  //Bank Statement
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Bank Statement: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        widget.bankstatement.split("/")[2].toString().isNotEmpty ?
                        Image.network(
                        widget.baseurl+widget.bankstatement ,
                        width: 100,
                          height: 100,
                        ) :  const Text("")
                      ],
                    ),
                  ),
                  //Interest Paid
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Interest Paid: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        Text(widget.interestpaid),
                      ],
                    ),
                  ),
                  //Interest Received
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Interest Received: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        Text(widget.interestreceived),
                      ],
                    ),
                  ),
                  //Charges
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Charges: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        Text(widget.charges),
                      ],
                    ),
                  ),
                  //Other
                  const SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Other: ",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        Text(widget.other),
                      ],
                    ),
                  ),

                  const SizedBox(height: 10,)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
