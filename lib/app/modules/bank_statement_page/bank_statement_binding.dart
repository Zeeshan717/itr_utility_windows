import 'package:accountx/app/modules/bank_statement_page/bank_statement_controller.dart';
import 'package:get/get.dart';

class BankStatementBinding extends Bindings {

  @override
  void dependencies() {
    Get.put<BankStatementController>(BankStatementController());
  }
}