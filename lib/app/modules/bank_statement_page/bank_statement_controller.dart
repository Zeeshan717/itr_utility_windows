import 'dart:io';

import 'package:accountx/app/services/api.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../provider/bank_statement_provider.dart';

class BankStatementController extends GetxController {
  GetStorage box = GetStorage();
  var bankValue = "Axis Bank Ltd.".obs;
  var accountValue = "SALARY".obs;
  var radioValue = "bankGroup".obs;
  File? image;

  void setBankValue(String value) {
    bankValue.value = value;
  }

  void setSalaryValue(String value) {
    accountValue.value = value;
  }

  void setRadioValue(var value) {
    radioValue.value = value;
  }

  TextEditingController toDateController = TextEditingController();
  TextEditingController fromDateController = TextEditingController();

  @override
  void onInit() {
    getBankStatementCall();

    fromDateController.text = formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd]);

    toDateController.text = formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd]);
    super.onInit();
  }

  Future uploadFromCamera() async {
    // ignore: deprecated_member_use
    PickedFile? pickedFile = await ImagePicker().getImage(source: ImageSource.camera, imageQuality: 30);

    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {}
  }

  Future uploadFromGallery() async {
    // ignore: deprecated_member_use
    PickedFile? pickedFile = await ImagePicker().getImage(source: ImageSource.gallery, imageQuality: 30);
    if (pickedFile != null) {
      image = File(pickedFile.path);
    }
  }

  // TextEditingController bankStatusController = TextEditingController();
  // TextEditingController bankNameController = TextEditingController();
  TextEditingController accountNoController = TextEditingController();
  TextEditingController accountTypeController = TextEditingController();
  TextEditingController fromDate1Controller = TextEditingController();
  TextEditingController toDate1Controller = TextEditingController();
  TextEditingController fileNameController = TextEditingController();
  TextEditingController interestPaidController = TextEditingController();
  TextEditingController interestReceivedController = TextEditingController();
  TextEditingController chargesController = TextEditingController();
  TextEditingController otherController = TextEditingController();

  // ignore: non_constant_identifier_names
  Future InsertBankMasterDetailsApiCall(
      String accountNo, String fromDate, String toDate, String interestPaid, String interestReceived, String charges, String other,BuildContext context) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("admin_id");
    var financialYear = box.read("finacial_year");

    // ignore: avoid_print
    // print(other.toString());

    if (image == null || image!.path == "") {
      Get.defaultDialog(content: Text("Please upload image"), actions: [TextButton(onPressed: () => Get.back(), child: Text("OK"))]);
/*      await Api.InsertBankMasterDetailsApiNoFile(clientId, clientName, adminId, financialYear, radioValue.value, bankValue.value, accountNo,
              accountValue.value, fromDate, toDate, "", interestPaid, interestReceived, charges, other)
          .then((value) {
        if (value["response_status"] == "1") {
          Get.snackbar("", "",
              messageText: Text(
                value["message"],
                textAlign: TextAlign.center,
              ),
              snackPosition: SnackPosition.BOTTOM);
        } else {
          Get.snackbar("", "",
              messageText: Text(
                value["message"],
                textAlign: TextAlign.center,
              ),
              snackPosition: SnackPosition.BOTTOM);
        }
      });*/
    } else {
      EasyLoading.show(status: "Loading...");
      await Api.InsertBankMasterDetailsApi(clientId.toString(), clientName.toString(), adminId.toString(), financialYear.toString(), radioValue.value,
              bankValue.value, accountNo, accountValue.value, fromDate, toDate, File(image!.path), interestPaid, interestReceived, charges, other)
          .then((value) async{
        if (value["response_status"] == "1"){
          EasyLoading.dismiss();
       await   Get.defaultDialog(
              content: Text(
                value["message"],
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      accountNoController.clear();
                      interestPaidController.clear();
                      interestReceivedController.clear();
                      chargesController.clear();
                      otherController.clear();
                      image = null;
                      Get.back();
                    },
                    child: Text("OK"))
              ]);
          Provider.of<BankStatementProvider>(context,listen: false).updateValue();
        } else {
          EasyLoading.dismiss();
          Get.defaultDialog(
              content: Text(
                value["message"],
              ),
              actions: [TextButton(onPressed: () => Get.back(), child: Text("OK"))]);
        }
      });
    }
  }

  Future getBankStatementCall() async {
    var clientId = box.read("client_id").toString();
    var adminId = box.read("loginValue")["admin_id"].toString();
    var financialYear = box.read("finacial_year").toString();
     Api.getBankStatementApi(clientId, adminId, financialYear);
  }

  List<String> banks = [
    'Axis Bank Ltd.',
    'Bandhan Bank Ltd.',
    'CSB Bank Limited',
    'City Union Bank Ltd.',
    'DCB Bank Ltd.',
    'Dhanlaxmi Bank Ltd.',
    'Federal Bank Ltd.',
    'HDFC Bank Ltd',
    'ICICI Bank Ltd.',
    'IndusInd Bank Ltd',
    'IDFC FIRST Bank Limited',
    'Jammu & Kashmir Bank Ltd.',
    'Karnataka Bank Ltd.',
    'Karur Vysya Bank Ltd.',
    'Kotak Mahindra Bank Ltd',
    'Nainital bank Ltd.',
    'RBL Bank Ltd.',
    'South Indian Bank Ltd.',
    'Tamilnad Mercantile Bank Ltd.',
    'YES Bank Ltd.',
    'IDBI Bank Limited',
    'Coastal Local Area Bank Ltd',
    'Krishna Bhima Samruddhi LAB Ltd',
    'Subhadra Local Bank Ltd',
    'Au Small Finance Bank Ltd.',
    'Capital Small Finance Bank Ltd',
    'Fincare Small Finance Bank Ltd.',
    'Equitas Small Finance Bank Ltd',
    'ESAF Small Finance Bank Ltd.',
    'Suryoday Small Finance Bank Ltd.',
    'Ujjivan Small Finance Bank Ltd.',
    'Utkarsh Small Finance Bank Ltd.',
    'North East Small finance Bank Ltd',
    'Jana Small Finance Bank Ltd',
    'Shivalik Small Finance Bank Ltd',
    'Unity Small Finance Bank Ltd',
    'Airtel Payments Bank Ltd',
    'India Post Payments Bank Ltd',
    'FINO Payments Bank Ltd',
    'Paytm Payments Bank Ltd',
    'Jio Payments Bank Ltd',
    'NSDL Payments Bank Limited',
    'Bank of Baroda',
    'Bank of India',
    'Bank of Maharashtra',
    'Canara Bank',
    'Central Bank of India',
    'Indian Bank',
    'Indian Overseas Bank',
    'Punjab & Sind Bank',
    'Punjab National Bank',
    'State Bank of India',
    'UCO Bank',
    'Union Bank of India',
    'National Bank for Agriculture and Rural Development',
    'Export-Import Bank of India',
    'National Housing Bank',
    'Small Industries Development Bank of India',
    'Assam Gramin Vikash Bank',
    'Andhra Pradesh Grameena Vikas Bank',
    'Andhra Pragathi Grameena Bank',
    'Arunachal Pradesh Rural Bank',
    'Aryavart Bank',
    'Bangiya Gramin Vikash Bank',
    'Baroda Gujarat Gramin Bank',
    'Baroda Rajasthan Kshetriya Gramin Bank',
    'Baroda UP Bank',
    'Chaitanya Godavari GB',
    'Chhattisgarh Rajya Gramin Bank',
    'Dakshin Bihar Gramin Bank',
    'Ellaquai Dehati Bank',
    'Himachal Pradesh Gramin Bank',
    'J&K Grameen Bank',
    'Jharkhand Rajya Gramin Bank',
    'Karnataka Gramin Bank',
    'Karnataka Vikas Gramin Bank',
    'Kerala Gramin Bank',
    'Madhya Pradesh Gramin Bank',
    'Madhyanchal Gramin Bank',
    'Maharashtra Gramin Bank',
    'Manipur Rural Bank',
    'Meghalaya Rural Bank',
    'Mizoram Rural Bank',
    'Nagaland Rural Bank',
    'Odisha Gramya Bank',
    'Paschim Banga Gramin Bank',
    'Prathama U.P. Gramin Bank',
    'Puduvai Bharathiar Grama Bank',
    'Punjab Gramin Bank',
    'Rajasthan Marudhara Gramin Bank',
    'Saptagiri Grameena Bank',
    'Sarva Haryana Gramin Bank',
    'Saurashtra Gramin Bank',
    'Tamil Nadu Grama Bank',
    'Telangana Grameena Bank',
    'Tripura Gramin Bank',
    'Uttar Bihar Gramin Bank',
    'Utkal Grameen Bank',
    'Uttarbanga Kshetriya Gramin Bank',
    'Vidharbha Konkan Gramin Bank',
    'Uttarakhand Gramin Bank',
    'Australia and New Zealand Banking Group Ltd.',
    'National Australia Bank',
    'Westpac Banking Corporation',
    'Bank of Bahrain & Kuwait BSC',
    'AB Bank Ltd.',
    'Sonali Bank Ltd.',
    'Bank of Nova Scotia',
    'Industrial & Commercial Bank of China Ltd. ',
    'BNP Paribas',
    'Credit Agricole Corporate & Investment Bank ',
    'Societe Generale',
    'Deutsche Bank',
    'HSBC Ltd ',
    'PT Bank Maybank Indonesia TBK',
    'Mizuho Bank Ltd.',
    'Sumitomo Mitsui Banking Corporation',
    'MUFG Bank, Ltd.',
    'Cooperatieve Rabobank U.A.',
    'Doha Bank',
    'Qatar National Bank SAQ',
    'JSC VTB Bank',
    'Sberbank',
    'DBS Bank Ltd.',
    'United Overseas Bank Ltd.',
    'FirstRand Bank Ltd.',
    'Shinhan Bank',
    'Woori Bank',
    'KEB Hana Bank',
    'Industrial Bank of Korea ',
    'Bank of Ceylon',
    'Credit Suisse A.G',
    'CTBC Bank Co., Ltd',
    'Krung Thai Bank Public Co. Ltd.',
    'Abu Dhabi Commercial Bank Ltd.',
    'Mashreq Bank PSC',
    'First Abu Dhabi Bank PJSC',
    'Emirates NBD Bank PJSC',
    'Barclays Bank Plc.',
    'Standard Chartered Bank',
    'NatWest Markets plc',
    'American Express Banking Corp.',
    'Bank of America',
    'Citibank N.A.',
    'J.P. Morgan Chase Bank N.A.'
  ];
}
