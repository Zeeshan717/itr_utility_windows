// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace

import 'package:accountx/app/model/bank_statement_model.dart';
import 'package:accountx/app/modules/bank_statement_page/bank_statement_controller.dart';
import 'package:accountx/app/modules/bank_statement_page/bank_statement_details.dart';
import 'package:accountx/app/provider/update_provider.dart';
import 'package:accountx/app/services/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../provider/bank_statement_provider.dart';

// ignore: must_be_immutable
class BankStatementHistory extends StatefulWidget {
  BankStatementHistory({Key? key}) : super(key: key);

  @override
  State<BankStatementHistory> createState() => _BankStatementHistoryState();
}

class _BankStatementHistoryState extends State<BankStatementHistory> {
  var controller = Get.put(BankStatementController());
  bool ispressed =false;
  int i=0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: AppBar(
         backgroundColor: Colors.white,
        title: Text("Bank Statement History",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
      ),*/
      backgroundColor: Colors.white,
      body: Consumer<BankStatementProvider>(
        builder:(context,bankStatementProvider,child)=> FutureBuilder<BankStatementModel>(
          future:bankStatementProvider.bankStatementModel,
          builder: (context,AsyncSnapshot<BankStatementModel> snapshot){
            if(snapshot.hasData) {
              EasyLoading.dismiss();
              return Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: [
                    Expanded(
                      child: ListView.builder(
                          itemCount: snapshot.data!.responseBankList.length,
                          itemBuilder: (context,index){

                            return Column(
                              children: [
                                ispressed==false?
                                Card(
                                  child: ListTile(
                                    onTap:(){
                                        setState((){ispressed=true;
                                        i=index;
                                        });

                                      if(snapshot.hasData) {

                                      } else {
                                        Get.snackbar("", "",messageText: Text("Data Not Available !!"),
                                        snackPosition: SnackPosition.BOTTOM
                                        );
                                      }

                                    },
                                    title: Text(snapshot.data!.responseBankList[index].bankName),
                                    trailing: IconButton(onPressed:()async{
                            showDialog(context: context, builder: (context)=>Center(
                            child: Container(
                            width: 50,
                            height:50,
                            child: CircularProgressIndicator()),
                            ));
                            await Api.deleteDepreciationEntryApi("bankStatement",(snapshot.data!.responseBankList[index].bankId.toString())).then((value){
                            print("---delete Response----"+value.toString());
                            Get.back();
                            if(value["response_status"].toString() == "1"){
                            Provider.of<BankStatementProvider>(context,listen: false).updateValue();
                            }
                            });
                            },icon: Icon(Icons.delete),),
                                  ),
                                ):Container(
                                  width:MediaQuery.of(context).size.width,
                                  height:MediaQuery.of(context).size.height,
                                  child: BankStatementDetails(
                            accountNo: snapshot.data!.responseBankList[i].accountNo,
                            bankName: snapshot.data!.responseBankList[i].bankName,
                            type: snapshot.data!.responseBankList[i].accountType,
                            fromdate: snapshot.data!.responseBankList[i].fromDate.toString().split(' ')[0],
                            todate: snapshot.data!.responseBankList[i].toDate.toString().split(' ')[0],
                            financialyear: snapshot.data!.responseBankList[i].financialYear,
                            baseurl: snapshot.data!.responseBankList[i].baseUrl.toString().isNotEmpty ?
                            snapshot.data!.responseBankList[i].baseUrl.toString() : "",
                            bankstatement: snapshot.data!.responseBankList[i].bankStatement.toString()
                                  .trim().isNotEmpty ?
                            snapshot.data!.responseBankList[i].bankStatement.toString() : "",
                            interestpaid: snapshot.data!.responseBankList[i].interestPaid,
                            interestreceived: snapshot.data!.responseBankList[i].interestReceived,
                            charges: snapshot.data!.responseBankList[i].charges,
                            other: snapshot.data!.responseBankList[i].other,
                            ),
                                ),
                                SizedBox(height: 10,)
                              ],
                            );
                          })
                    )
                  ],
                ),
              );
            } else if(snapshot.connectionState == ConnectionState.waiting) {
              EasyLoading.show(status: "Loading...");
              return Container();
            } else {
              EasyLoading.dismiss();
              return Center(
                child: Text("Data Not Available !!"),
              );
            }
          },
        ),
      ),
    );
  }
}
