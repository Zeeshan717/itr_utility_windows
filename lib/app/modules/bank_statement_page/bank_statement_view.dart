// ignore_for_file: must_be_immutable, sized_box_for_whitespace

import 'package:accountx/app/modules/bank_statement_page/bank_statement_controller.dart';
import 'package:accountx/app/modules/bank_statement_page/bank_statement_history.dart';
import 'package:date_format/date_format.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BankStatementView extends StatefulWidget {
  BankStatementView({Key? key}) : super(key: key);

  @override
  State<BankStatementView> createState() => _BankStatementViewState();
}

class _BankStatementViewState extends State<BankStatementView> {

  bool isSelected = false;

  DateTime? fromDateSelect;

  DateTime? toDateSelect;

  DateTime firstDate = DateTime(DateTime.now().year - 1, 1, 1);

  DateTime lastDate = DateTime(DateTime.now().year + 10, 12, 31);

  var controller = Get.put(BankStatementController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        actions: [
          Tooltip(
            message: "Info",
            child: InkWell(
                onTap: () {
                  setState((){
                    isSelected =!isSelected;
                  });
                /*  Get.to(() => );*/
                },
                child: isSelected==false  ? Image.asset('images/history.png',width: 30,color: Colors.black,): Icon(Icons.home,color: Colors.black,)),
          ),
          SizedBox(width: 20,)
        ],
        backgroundColor: Colors.white,
        centerTitle: true,
        title: isSelected == true ? Text("Bank Statement History",style: TextStyle(fontSize: 16,color: Colors.black)) :const Text(
          "Bank Statement",
          style: TextStyle(fontSize: 16,color: Colors.black),
        ),
      ),

      body: isSelected == false ? Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(6), border: Border.all(width: 1,
                    color: const Color(0xffC1CDFF))),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    //1
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Row(
                                  children: [
                                    Obx(
                                      () => Radio(
                                          activeColor: const Color(0xff0B28BE),
                                          value: "Existing",
                                          groupValue: controller.radioValue.value,
                                          onChanged: (newValue) {
                                            controller.setRadioValue(newValue);
                                          }),
                                    ),
                                  ],
                                ),
                                Text("Existing".toUpperCase())
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                Row(
                                  children: [
                                    Obx(
                                      () => Radio(
                                          activeColor: const Color(0xff0B28BE),
                                          value: "Closed",
                                          groupValue: controller.radioValue.value,
                                          onChanged: (value) {
                                            controller.setRadioValue(value);
                                          }),
                                    ),
                                    Text("Closed".toUpperCase())
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    //2
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Obx(
                                  () => Radio(
                                      activeColor: const Color(0xff0B28BE),
                                      value: "New",
                                      groupValue: controller.radioValue.value,
                                      onChanged: (value) {
                                        controller.setRadioValue(value);
                                      }),
                                ),
                                Text("New".toUpperCase())
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                Row(
                                  children: [
                                    Obx(
                                      () => Radio(
                                          activeColor: const Color(0xff0B28BE),
                                          value: "Newly \nIntroduced",
                                          groupValue: controller.radioValue.value,
                                          onChanged: (value) {
                                            controller.setRadioValue(value);
                                          }),
                                    ),
                                    Text("Newly \nIntroduced".toUpperCase())
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Text(
                      "Bank Name",
                      style: TextStyle(
                        color: Color(0xff656A87),
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(color: Color(0xffC1CDFF))
                      ),
                      child:
                          DropdownSearch<String>(
                        popupProps: const PopupProps.dialog(
                            showSearchBox: true),
                        items: controller.banks,
                        onChanged: (value) {
                          controller.setBankValue(value!);
                          //print("-----Drop down Value on Bank master--------" + value.toString());
                        },
                      ),

                      // ),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Account Number".toUpperCase(),
                      style: const TextStyle(color: Color(0xff656A87)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: SizedBox(
                  height: 40,
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    controller: controller.accountNoController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(6),
                      borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF)),
                    )),
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Type of Account".toUpperCase(),
                      style: const TextStyle(color: Color(0xff656A87)),
                    ),
                  ],
                ),
              ),
              Container(
                height: 40,
                padding: const EdgeInsets.symmetric(horizontal: 20),
                margin: const EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(6), border: Border.all(width: 1, color: const Color(0xffC1CDFF))),
                width: MediaQuery.of(context).size.width,
                child: Obx(() => DropdownButton(
                    isExpanded: true,
                    underline: const SizedBox(),
                    value: controller.accountValue.value,
                    onChanged: (String? value) {
                      controller.accountValue.value = value!;
                    },
                    items: <String>["CURRENT", "SAVING", "JOINT", "SALARY", "CC", "OD"].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem(value: value, child: Text(value));
                    }).toList())),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(6), border: Border.all(width: 1, color: const Color(0xffC1CDFF))),
                margin: const EdgeInsets.symmetric(horizontal: 10),
                padding: const EdgeInsets.symmetric(horizontal: 20),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("Bank Statement"),
                        Row(
                          children: [
                            InkWell(
                              onTap: () async{
                               await showDialog(
                                    barrierDismissible:
                                    false,
                                    context:
                                    context,
                                    builder:
                                        (context) {
                                     // print("----mediaquery width size------"+MediaQuery.of(context).size.width * 0.35);
                                      return AlertDialog(
                                        titlePadding: EdgeInsets.zero,
                                        contentPadding: EdgeInsets.zero,
                                        insetPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.35),
                                        content: Container(
                                          height: 200,
                                        //  color: Colors.red,
                                          child: Column(
                                           // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          //  crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                                width: MediaQuery.of(context).size.width,
                                                height: 50,
                                                color: Colors.indigo,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    const Text(""),
                                                    const Text(
                                                      "Choose Source",
                                                      style: TextStyle(color: Colors.white,fontWeight: FontWeight.w500,letterSpacing: 0.6),
                                                    ),
                                                    InkWell(
                                                        onTap: () {
                                                          Get.back();
                                                        },
                                                        child: const Icon(Icons.clear, color: Colors.white))
                                                  ],
                                                ),
                                              ),
                                              const SizedBox(height: 20),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Column(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      GestureDetector(
                                                          onTap: () {
                                                            controller.uploadFromGallery().whenComplete(() {
                                                              Get.back();
                                                            });
                                                          },
                                                          child: Image.asset(
                                                            "images/gallery.png",
                                                            width: 64,
                                                          )),
                                                       Padding(
                                                         padding: const EdgeInsets.all(6.0),
                                                         child: Text("Gallery",style:TextStyle(
                                                          fontSize: 24,letterSpacing: 0.6,
                                                           fontWeight: FontWeight.w500
                                                      ),),
                                                       )
                                                    ],
                                                  ),
                                                  Column(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      GestureDetector(
                                                          onTap: () {
                                                            controller.uploadFromCamera().whenComplete(() {
                                                              Get.back();
                                                            });
                                                          },
                                                          child: Image.asset(
                                                            "images/camera.png",
                                                            width: 64,
                                                          )),
                                                       Padding(
                                                         padding: const EdgeInsets.all(6.0),
                                                         child: Text("Camera",style:TextStyle(
                                          fontSize: 24,letterSpacing: 0.6
                                          ),),
                                                       )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              const SizedBox(
                                                height: 20,
                                              )
                                            ],
                                          ),
                                        ),
                                      );
                                    });
                               setState(() {
                               });
                              },
                              child:
                              Container(
                                decoration: BoxDecoration(color: controller.image == null?Color(0xff1739E4):Colors.green, borderRadius: BorderRadius.circular(5)),
                                height: 40,
                                child: Row(
                                  children:  [
                                    SizedBox(
                                      width: 10,
                                    ),
                                    controller.image == null?Text(
                                      "Upload PDF",
                                      style: TextStyle(color: Colors.white),
                                    ):Text(
                                      "Uploaded",
                                      style: TextStyle(color: Colors.white,fontWeight: FontWeight.w500,fontSize: 16,letterSpacing: 0.5),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Icon(
                                      controller.image == null? Icons.cloud_upload:Icons.done_outline,
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Visibility(
                              visible:controller.image != null ,
                              child: InkWell(
                                  onTap: (){
                                    controller.image = null;
                                    setState(() {
                                    });
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 8),
                                    child: Text("Clear File",style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        letterSpacing: 0.6,
                                        color: Colors.blueAccent,
                                        fontSize: 16
                                    ),),
                                  )),
                            )
                          ],
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      "Date",
                      style: TextStyle(color: Color(0xff656A87)),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("From",style: TextStyle(color: Color(0xff656A87)),),
                              SizedBox(height: 10,),
                              GestureDetector(
                                onTap: () {
                                  fromDate(context);
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5), border: Border.all(width: 1, color: const Color(0xffC1CDFF))),
                                  height: 40,
                                  child: TextFormField(
                                    style: const TextStyle(fontSize: 13),
                                    controller: controller.fromDateController,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.all(15),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(6),
                                        borderSide: const BorderSide(
                                          width: 1,
                                          color: Color(0xffC1CDFF),
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(6),
                                        borderSide: const BorderSide(
                                          width: 1,
                                          color: Color(0xffC1CDFF),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("To",style: TextStyle(color: Color(0xff656A87)),),
                              SizedBox(height: 10,),
                              GestureDetector(
                                onTap: () {
                                  toDate(context);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5), border: Border.all(width: 1, color: const Color(0xffC1CDFF))),
                                  height: 40,
                                  child: TextFormField(
                                    style: const TextStyle(fontSize: 13),
                                    controller: controller.toDateController,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.all(15),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(6), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(6), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                            child: Text(
                          "Interest Paid".toUpperCase(),
                          style: const TextStyle(color: Color(0xff656A87)),
                        )),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            "Interest Received".toUpperCase(),
                            style: const TextStyle(color: Color(0xff656A87)),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                            child: SizedBox(
                          height: 40,
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            controller: controller.interestPaidController,
                            decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF)))),
                          ),
                        )),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: SizedBox(
                          height: 40,
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            controller: controller.interestReceivedController,
                            decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(6), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF)))),
                          ),
                        )),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Charges".toUpperCase(),style: TextStyle(color: Color(0xff656A87)),),
                            SizedBox(height: 10,),
                            SizedBox(
                                height: 40,
                                child: TextFormField(
                                  controller: controller.chargesController,
                                  keyboardType: TextInputType.number,
                                  decoration: const InputDecoration(
                                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(width: 1, color: Color(0xffC1CDFF)))),
                                )),
                          ],
                        )),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Other".toUpperCase(),style: TextStyle(color: Color(0xff656A87)),),
                            SizedBox(height: 10,),
                            SizedBox(
                              height: 40,
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                controller: controller.otherController,
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF))),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(width: 1, color: Color(0xffC1CDFF)))),
                              ),
                            )
                          ],
                        ))
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: const Color(0xff0B28BE), shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7))),
                      onPressed: () {
                        if (controller.accountNoController.text.isEmpty) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Account Number is required !!")));
                        }
                        else if(controller.fromDateController.text.isEmpty) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("From Date is required !!")));
                        } else if(controller.toDateController.text.isEmpty) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("To Date is required !!")));
                        } else if(controller.interestPaidController.text.isEmpty) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Interest Paid is required !!")));
                        } else if(controller.interestReceivedController.text.isEmpty) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Interest Received is required !!")));
                        }
                        else {
                          controller.InsertBankMasterDetailsApiCall(
                              controller.accountNoController.text,
                              controller.fromDateController.text,
                              controller.toDateController.text,
                              controller.interestPaidController.text,
                              controller.interestReceivedController.text,
                              controller.chargesController.text,
                              controller.otherController.text,context).then((value) => setState((){}));
                        }
                      },
                      child: Text(
                        "Submit".toUpperCase(),
                        style: const TextStyle(fontSize: 18),
                      )),
                ),
              ),
              const SizedBox(
                height: 70,
              )
            ],
          ),
        ),
      ) : BankStatementHistory(),
    );
  }

  fromDate(BuildContext context) async {
    fromDateSelect = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: firstDate, lastDate: lastDate);
    controller.fromDateController.text = formatDate(fromDateSelect!, [yyyy, '-', mm, '-', dd]);
  }

  toDate(BuildContext context) async {
    toDateSelect = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: firstDate, lastDate: lastDate);
    controller.toDateController.text = formatDate(toDateSelect!, [yyyy, '-', mm, '-', dd]);
  }
}
