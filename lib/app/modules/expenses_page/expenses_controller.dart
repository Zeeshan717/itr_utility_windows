import 'dart:io';

import 'package:accountx/app/modules/expenses_page/expenses_details.dart';
import 'package:accountx/app/modules/homePage/homeView.dart';
import 'package:accountx/app/services/api.dart';
import 'package:date_format/date_format.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';

import '../../provider/update_provider.dart';

class ExpensesController extends GetxController {
  GetStorage box = GetStorage();

  TextEditingController nameController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController amountController = TextEditingController();
  TextEditingController remarkController = TextEditingController();
  File? pdf_file;

  TextEditingController editNameController = TextEditingController();
  TextEditingController editDateController = TextEditingController();
  TextEditingController editAmountController = TextEditingController();
  TextEditingController editRemarkController = TextEditingController();

  @override
  void onInit() {
    dateController.text = formatDate(DateTime(DateTime.now().year - 1, 4, 1), [yyyy, '-', mm, '-', dd]);
    super.onInit();
  }

  Future uploadPdf() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
    );

    if (result != null) {
      pdf_file = File(result.files.single.path.toString());
    } else {
      // User canceled the picker
    }
  }

//-----------insert Expense Api Call ------------------------
  Future insertExpensesApiCall(BuildContext context) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("loginValue")["admin_id"];
    var financialYear = box.read("finacial_year");
    var pdfFileTemp;
    EasyLoading.show(status: "Loading...");
    if(pdf_file == null) {
      pdfFileTemp = "";
    } else {
      pdfFileTemp = pdf_file!.path.toString();
    }
    await Api.insertExpensesApi(clientId, clientName, adminId, financialYear,
        nameController.text, dateController.text, pdfFileTemp,
            amountController.text, remarkController.text)
        .then((value) async{
      if (value["response_status"] == "1") {
        EasyLoading.dismiss();
      await  Get.defaultDialog(
         barrierDismissible: false,
         content: Text(value["message"]),
         actions: [
           TextButton(onPressed: () {
             nameController.clear();
             amountController.clear();
             remarkController.clear();
             pdf_file = null;
             Get.back();
           }, child: Text("OK"))
         ]
       );
      Provider.of<UpdateProvider>(context,listen: false).updateValue();
      } else {
        EasyLoading.dismiss();
        Get.defaultDialog(
            barrierDismissible: false,
            content: Text(value["message"]),
            actions: [
              TextButton(onPressed: () {
                nameController.clear();
                amountController.clear();
                remarkController.clear();
                Get.back();
              }, child: Text("OK"))
            ]
        );      }
    });
  }

//-------------Get Expense Api Call--------------
  Future getExpenseApiCall(int index) async {
    var clientId = box.read("client_id");
    var adminId = box.read("admin_id");
    var financialYear = box.read("finacial_year");
    Api.getExpenseApi2(clientId.toString(), financialYear.toString(), adminId.toString()).then((value) {
      print("Working");
      if (value["response_status"].toString() == "1") {
        editNameController.text = value["response_expense_list"][index]["name"];
        editDateController.text = value["response_expense_list"][index]["date"];
        editAmountController.text = value["response_expense_list"][index]["amount"];
        editRemarkController.text = value["response_expense_list"][index]["remark"];
      }
    });
  }

  //----------------Edit Expense Api Call ------------------

  Future updateExpenseApiCall(BuildContext context, String expenseId) async {
    var clientId = box.read("client_id");
    var clientName = box.read("client_name");
    var adminId = box.read("admin_id");
    var financialYear = box.read("finacial_year");
    print("Working (updateInvestmentApiCall)");
   /* var pdfFileTemp;
    if(pdf_file==null && pdf_file==""){
      pdfFileTemp="";
    }else{
      pdfFileTemp=pdf_file!.path.toString();
    }*/
    Api.editExpenseApi(clientId.toString(), clientName.toString(), adminId.toString(), financialYear.toString(), editNameController.text,
            editDateController.text,pdf_file.toString(), editAmountController.text, editRemarkController.text, expenseId)
        .then((value) {
      if (value["response_status"] == "1") {
        EasyLoading.dismiss();
        Get.back();
        Provider.of<UpdateProvider>(context,listen: false).updateValue();
       // Get.offAll(HomeView());
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(value["message"])));
      } else {
        EasyLoading.dismiss();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(value["message"])));
      }
    });
  }
}
