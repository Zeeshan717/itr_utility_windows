import 'package:accountx/app/model/expense_model.dart';
import 'package:accountx/app/model/investment_model.dart';
import 'package:accountx/app/modules/expenses_page/expenses_edit_page.dart';
import 'package:accountx/app/modules/pdf_view.dart';
import 'package:accountx/app/provider/update_provider.dart';
import 'package:accountx/app/services/api.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';

class ExpenseDetails extends StatefulWidget {
  const ExpenseDetails({Key? key}) : super(key: key);

  @override
  State<ExpenseDetails> createState() => _ExpenseDetailsState();
  callBackOuter(){
    _ExpenseDetailsState().callBack();
  }

}

class _ExpenseDetailsState extends State<ExpenseDetails> {

  GetStorage storage = GetStorage();

  DownloadNOpen pdfOpen = DownloadNOpen();
  Future<ExpenseModel>? expenseModel;
  @override
  void initState() {
    // TODO: implement initState
    expenseModel = Api.getExpenseApi(storage.read("client_id").toString(),
        storage.read("finacial_year").toString(), storage.read("admin_id").toString());
    print("------>> callBackFuncation -----${expenseModel.toString()}");
    super.initState();
  }
  void callBack()async{
    expenseModel = Api.getExpenseApi(storage.read("client_id").toString(),
        storage.read("finacial_year").toString(), storage.read("admin_id").toString());
    print("------>> callBackFuncation 1-----${await expenseModel.toString()}");

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
        appBar: AppBar(

          elevation: 0,
          backgroundColor: Colors.white,
          title: Text(
            "Expense Details",
            style: TextStyle(fontSize: 16, color: Colors.black),
          ),
          centerTitle: true,
        ),
        body: Consumer<UpdateProvider>(
          builder:(contex,updateProvider,child) => FutureBuilder<ExpenseModel>(
            future:updateProvider.getExpenseApiResponse
                ,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                EasyLoading.dismiss();
                return Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: DataTable2(
                        columnSpacing: 15,
                        horizontalMargin: 15,
                        minWidth: 710,
                        columns: const [
                          DataColumn(
                            label: Center(child: Text('Financial Year',textAlign: TextAlign.center,)),
                          ),
                          DataColumn(
                            label: Center(child: Text('Name',textAlign: TextAlign.center,)),
                          ),
                          DataColumn(
                            label: Center(child: Text('Date',textAlign: TextAlign.center,)),
                          ),
                          DataColumn(
                            label: Center(child: Text('Attachment',textAlign: TextAlign.center,)),
                          ),
                          DataColumn(
                            label: Center(child: Text('Amount',textAlign: TextAlign.center,)),
                          ),
                          DataColumn(
                            label: Center(child: Text('Remark',textAlign: TextAlign.center,)),
                            numeric: true,
                          ),
                          DataColumn(label: Center(child: Text('Edit',
                          textAlign: TextAlign.center,))),
                          DataColumn(label: Center(child: Text('Delete',
                            textAlign: TextAlign.center,)))
                        ],
                        rows: List.generate(
                            snapshot.data!.responseExpenseList.length,
                            (index) => DataRow(cells: [
                                  DataCell(Center(
                                    child: Text(
                                      snapshot.data!.responseExpenseList[index].financialYear.toString(),
                                      style: TextStyle(fontSize: 13),
                                      textAlign: TextAlign.center,
                                    ),
                                  )),
                                  DataCell(Center(
                                    child: Text(
                                      snapshot.data!.responseExpenseList[index].name.toString(),
                                      style: TextStyle(fontSize: 13),
                                       textAlign: TextAlign.center,
                                    ),
                                  )),
                                  DataCell(Center(
                                    child: Text(
                                      snapshot.data!.responseExpenseList[index].date.toString().split(" ")[0],
                                      style: TextStyle(fontSize: 13),
                                       textAlign: TextAlign.center,
                                    ),
                                  )),
                                  DataCell(Center(
                                    child: TextButton(
                                        onPressed: () {
                                          pdfOpen.openFile(
                                              url:
                                                  "https://appexperts.net/btconnect/${snapshot.data!.responseExpenseList[index].attachment.toString()}",
                                              fileName: "${storage.read("client_name")}_expense_details.pdf");
                                        },
                                        child: snapshot.data!.responseExpenseList[index].attachment.toString().contains(".pdf") ?const Text(
                                          "View PDF",
                                          style: TextStyle(fontSize: 13),
                                           textAlign: TextAlign.center,
                                        ):Text("")),
                                  )),
                                  DataCell(Center(
                                    child: Text(
                                      snapshot.data!.responseExpenseList[index].amount.toString(),
                                      style: TextStyle(fontSize: 13),
                                       textAlign: TextAlign.center,
                                    ),
                                  )),
                                  DataCell(Center(
                                    child: Text(
                                      snapshot.data!.responseExpenseList[index].remark.toString(),
                                      style: TextStyle(fontSize: 13),
                                       textAlign: TextAlign.center,
                                    ),
                                  )),
                                  DataCell(Center(
                                      child: InkWell(
                                          onTap: () {
                                            Get.to(ExpenseEditView(
                                              currentindex: index,
                                              expenseId: snapshot.data!.responseExpenseList[index].expenseId.toString(),
                                              name: snapshot.data!.responseExpenseList[index].name.toString(),
                                              date: snapshot.data!.responseExpenseList[index].date.toString().split(" ")[0],
                                              amount: snapshot.data!.responseExpenseList[index].amount.toString(),
                                              remark: snapshot.data!.responseExpenseList[index].remark.toString(),
                                            ));
                                          },
                                          child: SvgPicture.asset("images/pencil.svg",width: 20,
                                          )
                                          ))),
                              DataCell(Center(
                                child: InkWell(
                                    onTap:()async{
                                      showDialog(context: context, builder: (context)=>Center(
                                        child: Container(
                                            width: 50,
                                            height:50,
                                            child: CircularProgressIndicator()),
                                      ));
                                      await Api.deleteDepreciationEntryApi("expenses",snapshot.data!.responseExpenseList[index].expenseId.toString()).then((value){
                                        print("---delete Response----"+value.toString());
                                        Get.back();
                                        if(value["response_status"].toString() == "1"){
                                          Provider.of<UpdateProvider>(context,listen: false).updateValue();
                                        }

                                      });
                                    },
                                    child: Icon(Icons.delete)),
                              )),
                                ]))),
                  ),
                );
              }
              else if (snapshot.connectionState == ConnectionState.waiting) {
                EasyLoading.show(status: "Loading...");
                return Container();
              } else {
                EasyLoading.dismiss();
                return Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: Text("Data Not Available !!",),
                );
              }
            },
          ),
        ));
  }
}
