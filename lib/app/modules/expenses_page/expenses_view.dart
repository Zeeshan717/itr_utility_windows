import 'package:accountx/app/modules/expenses_page/expenses_controller.dart';
import 'package:accountx/app/modules/expenses_page/expenses_details.dart';
import 'package:accountx/app/modules/investment_page/investment_details.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class ExpensesView extends StatefulWidget {
  const ExpensesView({Key? key}) : super(key: key);

  @override
  State<ExpensesView> createState() => _ExpensesViewState();
}

class _ExpensesViewState extends State<ExpensesView> {

  bool isSelected= false;

  var controller = Get.put(ExpensesController());

  DateTime? now;

  String? dropdownValue;
  @override
  void dispose() {
    controller.nameController.clear();
    controller.amountController.clear();
    controller.remarkController.clear();
    controller.pdf_file = null;
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text(
          "Expenses",
          style: TextStyle(color: Colors.black, fontSize: 16),
        ),
        actions: [
          Tooltip(
            message: 'Info',
            child: InkWell(
                onTap: () {
                  setState((){
                    isSelected =!isSelected;
                  });
                /*  Get.to(const );*/
                },
                child: isSelected ?  Icon(Icons.home,color: Colors.black,) :  Image.asset("images/history.png",width: 30,color: Colors.black,)) ,
          ),
          SizedBox(
            width: 20,
          )
        ],
      ),
      body: isSelected ? ExpenseDetails() : Container(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              //Expenses Date

              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [],
              ),
              //Name
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Name",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.nameController,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 2)),
                      ),
                    ),
                  )
                ],
              ),
              //Date
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Date",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  InkWell(
                    onTap: () {
                      dateSelected(context);
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: const Color(0xffC1CDFF), width: 1)),
                      height: 40,
                      child: TextFormField(
                        style: const TextStyle(fontSize: 13),
                        keyboardType: TextInputType.datetime,
                        enabled: false,
                        controller: controller.dateController,
                        decoration: InputDecoration(
                          suffixIcon: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5),
                            child: SvgPicture.asset(
                              "images/calender.svg",
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Colors.transparent)),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 0)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 0)),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              //Amount
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Amount",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.amountController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1)),
                      ),
                    ),
                  )
                ],
              ),
              //Remark
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Remark",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.remarkController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1)),
                      ),
                    ),
                  )
                ],
              ),
              //Attachment
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    "Attachment",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  Row(
                    children: [
                      InkWell(
                        onTap: () async{
                        await  controller.uploadPdf().then((value) => setState((){}));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            color: controller.pdf_file == null? Color(0xff1739E4): Colors.green,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          height: 40,
                          child: Row(
                            children:  [
                              SizedBox(
                                width: 10,
                              ),
                              controller.pdf_file == null ? Text(
                                "Upload",
                                style: TextStyle(color: Colors.white,fontWeight: FontWeight.w500,fontSize: 16,letterSpacing: 0.5),
                              ):Text(
                                "Uploaded",
                                style: TextStyle(color: Colors.white,fontWeight: FontWeight.w500,fontSize: 16,letterSpacing: 0.5),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Icon(
                                controller.pdf_file == null? Icons.cloud_upload:Icons.done_outline,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 10,
                              )
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                        visible:controller.pdf_file != null ,
                        child: InkWell(
                            onTap: (){
                              controller.pdf_file = null;
                              setState(() {
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text("Clear File",style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  letterSpacing: 0.6,
                                  color: Colors.blueAccent,
                                  fontSize: 16
                              ),),
                            )),
                      )
                    ],
                  )
                ],
              ),
              //Submit Button
              const SizedBox(
                height: 50,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: const Color(0xff1739E4)),
                  onPressed: () {
                    if(controller.nameController.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Name is required !!")));
                    }
                    if(controller.amountController.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Amount is required !!")));
                    } else if(controller.remarkController.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Remark is required !!")));
                    }
                    else {
                      controller.insertExpensesApiCall(context).whenComplete(() {
                        setState(() {
                        });
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Inserted successfully")));
                      });
                    }
                  },
                  child: const Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  dateSelected(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: DateTime(DateTime.now().year - 1, 4, 1),
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 31));

    if(now != null) {
      controller.dateController.text = formatDate(now!, [yyyy, '-', mm, '-', dd]);
    }
  }
}
