import 'package:accountx/app/modules/expenses_page/expenses_controller.dart';
import 'package:accountx/app/modules/expenses_page/expenses_details.dart';
import 'package:accountx/app/modules/investment_page/investment_details.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class ExpenseEditView extends StatefulWidget {
  ExpenseEditView(
      {Key? key,
      required this.currentindex,
      required this.expenseId,
      required this.name,
      required this.date,
      required this.amount,
      required this.remark})
      : super(key: key);

  int currentindex;
  String expenseId;
  String name;
  String date;
  String amount;
  String remark;
  @override
  State<ExpenseEditView> createState() => _ExpenseEditViewState();
}

class _ExpenseEditViewState extends State<ExpenseEditView> {
  var controller = Get.put(ExpensesController());

  DateTime? now;

  String? dropdownValue;

  @override
  void initState() {
    controller.getExpenseApiCall(widget.currentindex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff1739E4),
        centerTitle: true,
        title: const Text(
          "Expenses",
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
        actions: [
          InkWell(
              onTap: () {
                Get.to(const ExpenseDetails());
              },
              child: const Icon(
                Icons.more_vert,
                color: Colors.white,
              )),
          SizedBox(
            width: 20,
          )
        ],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              //Expenses Date

              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [],
              ),
              //Name
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Name",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.editNameController,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 2)),
                      ),
                    ),
                  )
                ],
              ),
              //Date
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Date",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  InkWell(
                    onTap: () {
                      dateSelected(context);
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: const Color(0xffC1CDFF), width: 1)),
                      height: 40,
                      child: TextFormField(
                        style: const TextStyle(fontSize: 13),
                        keyboardType: TextInputType.datetime,
                        enabled: false,
                        controller: controller.editDateController,
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 0)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 0)),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              //Amount
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Amount",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.editAmountController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1)),
                      ),
                    ),
                  )
                ],
              ),
              //Remark
              const SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Remark",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: controller.editRemarkController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5), borderSide: const BorderSide(color: Color(0xffC1CDFF), width: 1)),
                      ),
                    ),
                  )
                ],
              ),
              //Attachment
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    "Attachment",
                    style: TextStyle(fontSize: 16, color: Color(0xff656A87)),
                  ),
                  InkWell(
                    onTap: () {
                      controller.uploadPdf();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: const Color(0xff1739E4),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      height: 40,
                      child: Row(
                        children: const [
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Upload PDF",
                            style: TextStyle(color: Colors.white),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.cloud_upload,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              //Submit Button
              const SizedBox(
                height: 50,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: const Color(0xff1739E4)),
                  onPressed: () {

                    if(controller.editNameController.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Updated Name empty !!")));
                    }
                    else if(controller.editDateController.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Update Date empty !!")));
                    }
                    else if(controller.editAmountController.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Updated Amount empty !!")));
                    } else if(controller.editRemarkController.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Updated Reamrk empty !!")));
                    } else {
                      EasyLoading.show();
                      controller.updateExpenseApiCall(context, widget.expenseId).whenComplete(() {
                      //  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Updated successfully")));
                      });
                    }
                  },
                  child: const Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  dateSelected(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: DateTime(DateTime.now().year - 1, 4, 1),
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 31));

    controller.editDateController.text = formatDate(now!, [yyyy, '-', mm, '-', dd]);
  }
}
