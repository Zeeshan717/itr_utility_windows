// ignore_for_file: file_names

import 'dart:math';

import 'package:accountx/app/model/emi_details_model.dart';
import 'package:accountx/app/modules/emiCalPage/emiView.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class EmiController extends GetxController {

  TextEditingController loanAmount = TextEditingController();
  TextEditingController rateOfInterestController = TextEditingController();
  TextEditingController loanPeriodController = TextEditingController();

  TextEditingController dateController = TextEditingController();

  double emiValue = 0.0;

  double getEmi(double p, double r, int t) {
    EmiView.emiDetailsList = [];
    double scheduledEmi = p * (r / 12 / 100) * pow((1 + (r / 12 / 100)), t) / (pow((1 + (r / 12 / 100)), t) - 1);
    double bBalance = p;
    double scheduledPayment = scheduledEmi;
    double totalPayment = 0.0;
    double principal = 0.0;
    double interest = 0.0;
    double endingBalance = -1;
    var count = 1;
    while (count < t) {
      if (endingBalance == -1) {
        bBalance = p;
        scheduledPayment = scheduledEmi;
        totalPayment = bBalance < scheduledEmi?bBalance:scheduledEmi;
        interest = bBalance * r / 12 / 100;
        principal =  (bBalance < scheduledEmi?bBalance:scheduledEmi) - interest;
        endingBalance = bBalance - (bBalance < scheduledEmi?bBalance:principal);
        EmiView.emiDetailsList.add(EmiDetailsModel(bBalance, scheduledPayment, totalPayment, principal, interest, endingBalance));
      } else {
        bBalance = endingBalance;
        scheduledPayment = scheduledEmi;
        totalPayment =  bBalance < scheduledEmi?bBalance:scheduledEmi;;
        interest = bBalance * r / 12 / 100;
        principal = (bBalance < scheduledEmi?bBalance:scheduledEmi) - interest;
        endingBalance =bBalance - (bBalance < scheduledEmi?bBalance:principal);
        EmiView.emiDetailsList.add(EmiDetailsModel(bBalance, scheduledPayment, totalPayment, principal, interest, endingBalance));
        count++;
      }
      // print(endingBalance);
    }
//   print(emiDetailsList[0].endingBalance);
    return scheduledEmi;
  }


}