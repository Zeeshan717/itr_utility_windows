// ignore_for_file: file_names, avoid_print

import 'package:accountx/app/model/emi_details_model.dart';
import 'package:accountx/app/modules/emiCalPage/emiController.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EmiView extends StatefulWidget {
  const EmiView({Key? key}) : super(key: key);
  static List<EmiDetailsModel> emiDetailsList = [];

  @override
  State<EmiView> createState() => _EmiViewState();
}

class _EmiViewState extends State<EmiView> {
  var controller = Get.put(EmiController());
  bool isTableShow = false;
  DateTime? now;



  @override
  Widget build(BuildContext context) {




    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Scrollbar(
          thickness: 4,
          thumbVisibility: true,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Loan Amount".toUpperCase(),
                            style: const TextStyle(
                                fontSize: 12, color: Color(0xff656A87)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 40,
                        child: TextFormField(
                          controller: controller.loanAmount,
                          style: const TextStyle(fontSize: 12),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            prefixIcon: Image.asset("images/rupess.png",width: 10,scale: 20,),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: const BorderSide(
                                    color: Color(0xffC1CDFF), width: 1)),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: const BorderSide(
                                    color: Color(0xffC1CDFF), width: 1)),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Rate of Interest".toUpperCase(),
                            style: const TextStyle(
                                fontSize: 12, color: Color(0xff656A87)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 40,
                        child: TextFormField(
                          controller: controller.rateOfInterestController,
                          style: const TextStyle(fontSize: 12),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: const BorderSide(
                                      color: Color(0xffC1CDFF), width: 1)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: const BorderSide(
                                      color: Color(0xffC1CDFF), width: 1))),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Loan Period".toUpperCase(),
                            style: const TextStyle(
                                fontSize: 12, color: Color(0xff656A87)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 40,
                        child: TextFormField(
                          controller: controller.loanPeriodController,
                          style: const TextStyle(fontSize: 12),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: const BorderSide(
                                      color: Color(0xffC1CDFF), width: 1)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: const BorderSide(
                                      color: Color(0xffC1CDFF), width: 1))),
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),


                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: const Color(0xff0B28BE)),
                          onPressed: () {
                            if(controller.loanAmount.text.isEmpty) {
                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Loan Amount Required")));
                            } else if(controller.rateOfInterestController.text.isEmpty){
                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Rate of Interest Required")));
                            } else if(controller.loanPeriodController.text.isEmpty){
                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Loan Period Required")));
                            } else{
                              setState(() {
                                controller.emiValue = controller.getEmi(double.parse(controller.loanAmount.text),
                                    double.parse(controller.rateOfInterestController.text),
                                    int.parse(controller.loanPeriodController.text));
                                print('Emi Details '+EmiView.emiDetailsList[0].toString());
                              });
                            }

                            FocusScope.of(context).requestFocus(FocusNode());
                          },
                          child: Text("Get Details".toUpperCase(),
                              style: const TextStyle(fontSize: 16))),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
                Visibility(
                  visible: isTableShow || controller.loanAmount.text.isEmpty || controller.rateOfInterestController.text.isEmpty
                      || controller.loanPeriodController.text.isEmpty ? false : true,
                  child: Padding(padding: const EdgeInsets.all(16),
                  child: DataTable2(
                    columnSpacing: 2,
                    horizontalMargin: 2,
                    minWidth: 550,
                    columns: const [
                      DataColumn(label: Text('Payment. \nNo',style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,)),
                      DataColumn(label: Text('Payment \nDate',style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,)),
                      DataColumn(label: Text('Beginning \nBalance',style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,)),
                      DataColumn(label: Text('Scheduled \nPayment',style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,)),
                      DataColumn(label: Text('Total \nPayment',style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,)),
                      DataColumn(label: Text('Principle',style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,)),
                      DataColumn(label: Text('Interest',style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,)),
                      DataColumn(label: Text('Ending Balance',style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,)),
                    ],
                    rows: List.generate(EmiView.emiDetailsList.length, (index) => DataRow(cells: [
                      DataCell(Center(child: Text("${index+1}",
                        style: const TextStyle(fontSize: 12),textAlign: TextAlign.center,))),
                      DataCell(Text(formatDate(DateTime(DateTime.now().year,DateTime.now().month+index,
                      DateTime.now().day,), [dd,'-',mm,'-',yyyy],),style: const TextStyle(fontSize: 11),)),
                      DataCell(Text(EmiView.emiDetailsList[index].bBalance.toStringAsFixed(2),
                        style: const TextStyle(fontSize: 12),)),
                      DataCell(Text(EmiView.emiDetailsList[index].scheduledPayment.toStringAsFixed(2),
                        style: const TextStyle(fontSize: 12),)),
                      DataCell(Text(EmiView.emiDetailsList[index].totalPayment.toStringAsFixed(2),
                          style: const TextStyle(fontSize: 12),
                      )),
                      DataCell(Text(EmiView.emiDetailsList[index].principal.toStringAsFixed(2),
                        style: const TextStyle(fontSize: 12),)),
                      DataCell(Text(EmiView.emiDetailsList[index].interst.toStringAsFixed(2),
                        style: const TextStyle(fontSize: 12),)),
                      DataCell(Text(EmiView.emiDetailsList[index].endingBalance.toStringAsFixed(2),
                        style: const TextStyle(fontSize: 12),)),
                    ])),
                  ),
                  ),
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}


