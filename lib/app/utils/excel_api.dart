import 'dart:io';

import 'package:accountx/app/model/depreciation_model.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart';

class ExcelApi {
  GetStorage box = GetStorage();

  final Workbook workbook = Workbook();

  void createExcel() async {
    final Worksheet sheet = workbook.worksheets[0];
    String clientId = box.read("client_id").toString();
    var depreciationList = box.read("DepList");
    List<DepreciationModel> dep = [];
    depreciationList.forEach((e) {
      dep.add(DepreciationModel(e["asset_name"], e["depreciation_rate"], e["opening_balance"], e["purchase_amount"], "", "", "",
          e["depreciation_amount"], e["closing_balance"], ""));
    });
    sheet.name = "Depreciation Report";

    sheet.getRangeByName('E1').setText("Fixed Assets");
    sheet.getRangeByName('E1').cellStyle.bold = true;
    sheet.getRangeByName('A3').setText("Particular");
    sheet.getRangeByName('A3').cellStyle.bold = true;
    sheet.getRangeByName('B3').setText("Rate");
    sheet.getRangeByName('B3').cellStyle.bold = true;
    sheet.getRangeByName('C3').setText("WDV as\n on");
    sheet.getRangeByName('C3').cellStyle.bold = true;
    sheet.getRangeByName('D3').setText("Addition More");
    sheet.getRangeByName('D3').cellStyle.bold = true;
    sheet.getRangeByName('E3').setText("Addition Less");
    sheet.getRangeByName('E3').cellStyle.bold = true;
    sheet.getRangeByName('F3').setText("Deduction");
    sheet.getRangeByName('F3').cellStyle.bold = true;
    sheet.getRangeByName('G3').setText("Total");
    sheet.getRangeByName('G3').cellStyle.bold = true;
    sheet.getRangeByName('H3').setText("Dep for \n ther year");
    sheet.getRangeByName('H3').cellStyle.bold = true;
    sheet.getRangeByName('I3').setText("WDV as\n on");
    sheet.getRangeByName('I3').cellStyle.bold = true;

    for (int i = 0, j = 4; i < dep.length; i++, j++) {
      sheet.getRangeByName('A$j').setText(dep[i].assetName);

      sheet.getRangeByName('B$j').setText(dep[i].depRate);

      sheet.getRangeByName('C$j').setText(dep[i].openingAmount);

      sheet.getRangeByName('D$j').setText(dep[i].purchaseAmountMore180Days);

      sheet.getRangeByName('E$j').setText(dep[i].purchaseAmountLess180Days);

      sheet.getRangeByName('F$j').setText(dep[i].deduction);

      sheet.getRangeByName('G$j').setText(dep[i].total);

      sheet.getRangeByName('H$j').setText(dep[i].depreciation);

      sheet.getRangeByName('I$j').setText(dep[i].closingBalance);
    }

    // int i;
    // int k = 0;
    // for(i=0;i<snapshot.data!.payments.length;i++) {

    // sheet.getRangeByName('A${11+k}').setText("${i+1}.");
    // sheet.getRangeByName('A${11+k}').cellStyle.bold = false;

    // sheet.getRangeByName('B${11+k}').setText("Date: ${snapshot.data!.payments[i].date.toString()}");
    // sheet.getRangeByName('C${11+k}').setText("Total Count: ${snapshot.data!.payments[i].details.length.toString()}");
    // sheet.getRangeByName('D${11+k}').setText("Total Collection: ?${snapshot.data!.payments[i].amount}");

    // /*int j;
    //                                             for(j=0;j<snapshot.data!.payments[i].details.length;j++) {
    //                                               sheet.getRangeByName('B${13+i+j}').setText(snapshot.data!.payments[i].details[j].residentName);
    //                                               sheet.getRangeByName('C${13+i+j}').setText(snapshot.data!.payments[i].details[j].receiptNo);
    //                                               sheet.getRangeByName('D${13+i+j}').setText(snapshot.data!.payments[i].details[j].amount);
    //                                             }*/

    // sheet.getRangeByName('B${12+k+1}').setText("Resident Name");
    // sheet.getRangeByName('B${12+k+1}').cellStyle.bold = true;

    // sheet.getRangeByName('C${12+k+1}').setText("Receipt No");
    // sheet.getRangeByName('C${12+k+1}').cellStyle.bold = true;

    // sheet.getRangeByName('D${12+k+1}').setText("Amount");
    // sheet.getRangeByName('D${12+k+1}').cellStyle.bold = true;

    // int j;
    // for(j=0;j<snapshot.data!.payments[i].details.length;j++) {
    // sheet.getRangeByName('B${13+k+j+1}').setText(snapshot.data!.payments[i]
    //     .details[j].residentName);
    // sheet.getRangeByName('C${13+k+j+1}').setText(snapshot.data!.payments[i]
    //     .details[j].receiptNo);
    // sheet.getRangeByName('D${13+k+j+1}').setText(snapshot.data!.payments[i]
    //     .details[j].amount);
    // }
    // k=k+j+5;
    // }

    // sheet.getRangeByName('B${15+k}').setText("Powered by Zucol Group");
    // sheet.getRangeByName('B${16+k}').setText("Report Generated at: "+
    // formatDate(DateTime.now(),
    // [dd,'-',mm,'-',yyyy," ",HH,':',nn,':',ss])
    // ,);

    final List<int> bytes = workbook.saveAsStream();
    workbook.dispose();
    workbook.dispose();

    final String path = (await getApplicationSupportDirectory()).path;
    final String fileName = "$path/depreciationReport.xlsx";
    final File file = File(fileName);
    await file.writeAsBytes(bytes, flush: true);
    OpenFile.open(fileName);
  }
}
