import 'dart:io';
import 'package:accountx/app/model/depreciation_model.dart';
import 'package:get_storage/get_storage.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';

// class Dep {
//   final String particulars;
//   final String rate;
//   final String wdv1;
//   final String add1;
//   final String add2;
//   final String deduction;
//   final String total;
//   final String depreciation;
//   final String wdv2;
//   const Dep({required this.particulars,required this.rate,required this.wdv1,required this.add1,
//   required this.add2,required this.deduction,required this.total,required this.depreciation,required this.wdv2});
// }

class PdfApi {
  static Future<File> generatedTable() async {
    final pdf = Document();

    final headers = [
      "Particulars",
      "Rate",
      "WDV as on 01/04/2021",
      "Addition \n More than 180 days",
      "Addition \n Less than 180 days",
      "Deduction",
      "Total",
      "Depreciation for the year",
      "WDV as on 31/03/2022"
    ];

    GetStorage box = GetStorage();
    String clientId = box.read("client_id").toString();
    var depreciationList = box.read("DepList");
    var dep = [];
    depreciationList.forEach((e) {
      dep.add(DepreciationModel(e["asset_name"], e["depreciation_rate"], e["opening_balance"], e["purchase_amount"], "", "", "",
          e["depreciation_amount"], e["closing_balance"], ""));
    });

    final data = dep
        .map((dep) => [
              dep.assetName,
              dep.depRate,
              dep.openingAmount,
              dep.purchaseAmountMore180Days,
              dep.purchaseAmountLess180Days,
              dep.deduction,
              dep.total,
              dep.depreciation,
              dep.closingBalance
            ])
        .toList();

    pdf.addPage(
      Page(
        pageFormat: PdfPageFormat.a4,
        build: (context) => Table.fromTextArray(
          headerAlignment: Alignment.center,
          cellAlignment: Alignment.center,
          headerStyle: TextStyle(
            fontSize: 6,
          ),
          cellStyle: TextStyle(fontSize: 5),
          cellPadding: EdgeInsets.symmetric(horizontal: 3),
          tableWidth: TableWidth.max,
          headers: headers,
          data: data,
        ),
      ),
    );

    return saveDocument(name: 'depreciation_details.pdf', pdf: pdf);
  }

  static Future<File> saveDocument({
    required String name,
    required Document pdf,
  }) async {
    final bytes = await pdf.save();
    final dir = await getApplicationDocumentsDirectory();
    final file = File('${dir.path}/$name');
    await file.writeAsBytes(bytes);
    return file;
  }

  static Future openFile(File file) async {
    final url = file.path;
    await OpenFile.open(url);
  }
}
