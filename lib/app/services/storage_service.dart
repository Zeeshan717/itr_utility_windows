import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get_storage/get_storage.dart';

class StorageService extends GetxService {
  final GetStorage _box = GetStorage();
  // Future<StorageService> init() async {
  //   _box = GetStorage();
  //   return this;
  // }

  void writeData(String key, dynamic value) async {
    await _box.write(key, value);
    await _box.read('name');
  }
}
