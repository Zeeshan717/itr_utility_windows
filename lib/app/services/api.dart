// ignore_for_file: avoid_print, non_constant_identifier_names, duplicate_ignore

import 'dart:convert';
import 'dart:io';

import 'package:accountx/app/model/annexure_json_model.dart';
import 'package:accountx/app/model/bank_statement_model.dart';
import 'package:accountx/app/model/ca_details_model.dart';
import 'package:accountx/app/model/deduction_model.dart';
import 'package:accountx/app/model/expense_model.dart';
import 'package:accountx/app/model/investment_model.dart';
import 'package:accountx/app/model/ppf_account_model.dart';
import 'package:accountx/app/model/user_client_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;

class Api {

  static Future sendMailApi(String gross_total_income_new,
      String gross_total_income_old,
      String total_other_income_new,
      String total_other_income_old,
      String total_deduction_new,
      String total_deduction_old,
      String total_taxable_income_new,
      String total_taxable_income_old,
      String total_tax_due_new,
      String total_tax_due_old,
      String total_house_property_new,
      String total_house_property_old,
      String salary_income_new,
      String salary_income_old,
      String tax_file_session,
      String user_email,) async {
    final response = await http.post(
      Uri.parse(
          'https://testbthawk.appsmarche.com/income_tax_calculator/api/send_mail_api.php'),
      body: {
        "gross_total_income_new": gross_total_income_new,
        "gross_total_income_old": gross_total_income_old,
        "total_other_income_new": total_other_income_new,
        "total_other_income_old": total_other_income_old,
        "total_deduction_new": '0',
        "total_deduction_old": total_deduction_old,
        "total_taxable_income_new": total_taxable_income_new,
        "total_taxable_income_old": total_taxable_income_old,
        "total_tax_due_new": total_tax_due_new,
        "total_tax_due_old": total_tax_due_old,
        "total_house_property_new": '0',
        "total_house_property_old": total_house_property_old,
        "salary_income_new": salary_income_new,
        "salary_income_old": salary_income_old,
        "tax_file_session": tax_file_session,
        "mail": user_email,
      },
    );

    if (response.statusCode == 200) {
      print(jsonDecode(response.body));
      return jsonDecode(response.body);
    } else {
      print(jsonDecode(response.body));
      throw Exception('Failed to send Email.');
    }
  }

  //*********Login Api************** */
  static Future loginApi(String email_id, String password, String imei_no,
      String ip_address, String carrier_name, String app_version,
      String phone_model, String phone_manufacturer, String sdk_phone_version,
      String device_id) async {
    final response = await http.post(
      Uri.parse('https://appexperts.net/btconnect/api/userLogin.php'),
      body: {
        "password": password,
        "email_id": email_id,
        "imei_no": imei_no,
        "ip_address": ip_address,
        "carrier_name": carrier_name,
        "app_version": app_version,
        "phone_model": phone_model,
        "phone_manufacturer": phone_manufacturer,
        "sdk_phone_version": sdk_phone_version,
        "device_id": device_id,
      },
    );
    if (response.statusCode == 200) {
      print(jsonDecode(response.body));
      return jsonDecode(response.body);
    } else {
      print(jsonDecode(response.body));
      throw Exception('Failed to send Email.');
    }
  }

  //****GetUserList Api***** */

  static Future<UserClientList> UserClientListApi(String admin_id, String type,
      String imei_no, String user_role, String manager_detail) async {
    final response = await http.post(
      Uri.parse('https://appexperts.net/btconnect/api/getUserClientList.php'),
      body: {
        "admin_id": admin_id,
        "type": type,
        "user_role": user_role,
        "manager_detail": manager_detail,
      },
    );
    if (response.statusCode == 200) {
      // print(jsonDecode(response.body));
      // return jsonDecode(response.body);
      return userClientListFromJson(response.body);
    } else {
      // print(jsonDecode(response.body));
      throw Exception('Failed to send Email.');
    }
  }

//**********CaDetailModel***************** */
  static Future<CaDetailModel> CaDetailsApi(String admin_id) async {

    final response = await http.post(
      Uri.parse('https://appexperts.net/btconnect/api/getCADetail.php'),
      body: {
        "admin_id": admin_id,
      },
    );
    if (response.statusCode == 200) {
      return caDetailModelFromJson(response.body);
    } else {
      print(jsonDecode(response.body));
      throw Exception('Failed to send Email.');
    }
  }

//** incomeMasterApi Api*** */

  static Future incomeMasterApi(String distributorId, String distributorName,
      String adminId, String financialYear, String incomeHead, String amount,
      String fileName, String remark) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertIncomeMasterDetail.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributorId;
    request.fields['distributor_name'] = distributorName;
    request.fields['admin_id'] = adminId;
    request.fields['financial_year'] = financialYear;
    request.fields['income_head'] = incomeHead;
    request.fields['amount'] = amount;
    try {
      request.files.add(
          await http.MultipartFile.fromPath("file_name", fileName.toString()));
    } catch (e) {
      request.fields['file_name'] = "";
    }

    request.fields['remark'] = remark;

    // var response = await request.send();
    //
    // print(response.statusCode);
    //
    // response.stream.transform(utf8.decoder).listen((value) {
    //   print(value);
    // });

    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  //**** getIcomeMasterApi Api***** */
  static Future getIcomeMasterApi(String distributorId, String adminId,
      String financialYear) async {
    var response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/getIncomeMaster.php"),
        body: {"distributor_id": distributorId.toString(),
          "admin_id": adminId.toString(),
          "financial_year": financialYear.toString()});
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

  //**** updateIncomeMasterApi Api******/
  static Future updateIncomeMasterApi(String distributorId,
      String distributorName, String adminId, String detail_id, String amount,
      String fileName, String remark) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/updateIncomeMaster.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributorId;
    request.fields['distributor_name'] = distributorName;
    request.fields['admin_id'] = adminId;
    request.fields['detail_id'] = detail_id;
    request.fields['amount'] = amount;
    if (fileName == "") {
      request.fields['file_name'] = "";
    } else {
      request.files.add(await http.MultipartFile.fromPath(
          "file_name", File(fileName).path.toString()));
    }

    request.fields['remark'] = remark;

    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

//**** InsertBankMasterDetails Api File******/
  static Future InsertBankMasterDetailsApi(String distributorId,
      String distributorName,
      String adminId,
      String financialYear,
      String bankStatus,
      String bankName,
      String accountNo,
      String accountType,
      String fromDate,
      String toDate,
      File fileName,
      String interestPaid,
      String interestReceived,
      String charges,
      String other) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertBankMasterDetail.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributorId;
    request.fields['distributor_name'] = distributorName;
    request.fields['admin_id'] = adminId;
    request.fields['financial_year'] = financialYear;
    request.fields['bank_status'] = bankStatus;
    request.fields['bank_name'] = bankName;
    request.fields['account_no'] = accountNo;
    request.fields['account_type'] = accountType;
    request.fields['from_date'] = fromDate;
    request.fields['to_date'] = toDate;
    try {
      if (fileName.path.isEmpty) {
        print("File is empty");
        request.fields['bank_statement'] = "";
      } else {
        print("File have path");
        request.files.add(
            await http.MultipartFile.fromPath('bank_statement', fileName.path));
      }
    } on Exception catch (e) {
      print(e);
    }

    request.fields['interest_paid'] = interestPaid;
    request.fields['interest_received'] = interestReceived;
    request.fields['charges'] = charges;
    request.fields['other'] = other;
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  //**** InsertBankMasterDetails Api No File******/
  static Future InsertBankMasterDetailsApiNoFile(String distributorId,
      String distributorName,
      String adminId,
      String financialYear,
      String bankStatus,
      String bankName,
      String accountNo,
      String accountType,
      String fromDate,
      String toDate,
      String fileName,
      String interestPaid,
      String interestReceived,
      String charges,
      String other) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertBankMasterDetail.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributorId;
    request.fields['distributor_name'] = distributorName;
    request.fields['admin_id'] = adminId;
    request.fields['financial_year'] = financialYear;
    request.fields['bank_status'] = bankStatus;
    request.fields['bank_name'] = bankName;
    request.fields['account_no'] = accountNo;
    request.fields['account_type'] = accountType;
    request.fields['from_date'] = fromDate;
    request.fields['to_date'] = toDate;
    request.fields['bank_statement'] = "";

    request.fields['interest_paid'] = interestPaid;
    request.fields['interestReceived'] = interestReceived;
    request.fields['charges'] = charges;
    request.fields['other'] = other;
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

//**** getBankStatementApi Api******/
  static Future<BankStatementModel> getBankStatementApi(String distributorId,
      String adminId, String financialYear) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/getBankMaster.php"),
        body: {
          "distributor_id": distributorId,
          "admin_id": adminId,
          "financial_year": financialYear
        });

    if (response.statusCode == 200) {
      return bankStatementModelFromJson(response.body);
    }
    return bankStatementModelFromJson(response.body);
  }

//**** logoutApi Api******/
  static Future logoutApi(String adminId) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/userLogout.php"),
        body: {"admin_id": adminId});
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

//**** ppfAccountApi Api******/
  static Future ppfAccountApi(String distributorId, String adminId,
      String financialYear, String accountNo, String statementDate,
      String deposit, String remark) async {
    final response = await http.post(Uri.parse(
        "https://appexperts.net/btconnect/api/insertPPFAccountMaster.php"),
        body: {
          "distributor_id": distributorId,
          "admin_id": adminId,
          "financial_year": financialYear,
          "account_no": accountNo,
          "statement_date": statementDate,
          "deposite": deposit,
          "remark": remark
        });
    if (response.statusCode.toString() == 200) {
      print("PPF Account Details -----:" + response.body);
      return jsonDecode(response.body);
    }
    print("PPF Account Details -----:" + response.body);
    return jsonDecode(response.body);
  }

//**** getPPFAccount Api******/
  static Future<PpfAccountModel> getPPFAccount(String distributorId,
      String adminId, String financialYear) async {
    final response = await http.post(Uri.parse(
        "https://appexperts.net/btconnect/api/getPPFAccountMaster.php"),
        body: {
          "distributor_id": distributorId,
          "admin_id": adminId,
          "financial_year": financialYear
        });
    if (response.statusCode == 200) {
      return ppfAccountModelFromJson(response.body);
    }
    return ppfAccountModelFromJson(response.body);
  }

  //****** 80C ******
  static Future insertDeduction80CApi(String distributor_id,
      String distributor_name,
      String admin_id,
      String financial_year,
      String date,
      String remark,
      String file_name,
      String deduction_head,
      String epf_pf,
      String lic,
      String national_saving,
      String tuition_fees,
      String senior_citizen_saving,
      String repayment_of_principle,
      String sukanya_samridhi_scheme,
      String reg_fees_stamp_duty) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertDeductionMasterDeatil.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['distributor_name'] = distributor_name;
    request.fields['admin_id'] = admin_id;
    request.fields['financial_year'] = financial_year;
    request.fields['date'] = date;
    request.fields['remark'] = remark;
    request.fields['deduction_head'] = deduction_head;
    request.fields['epf_pf'] = epf_pf;
    request.fields['lic'] = lic;
    request.fields['national_saving'] = national_saving;
    request.fields['tuition_fees'] = tuition_fees;
    request.fields['senior_citizen_saving'] = senior_citizen_saving;
    request.fields['repayment_of_principle'] = repayment_of_principle;
    request.fields['sukanya_samridhi_scheme'] = sukanya_samridhi_scheme;
    request.fields['reg_fees_stamp_duty'] = reg_fees_stamp_duty;
    try {
      /* if (file_name == null || file_name == "") {
        print("File is empty");
        request.fields['file_name'] = "";
      } else {*/
      print("File have path");
      request.files.add(await http.MultipartFile.fromPath(
          'file_name', File(file_name).path.toString()));
      //  }
    } on Exception catch (e) {
      print(e);
    }
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  //****** 80CCD ******
  static Future insertDeduction80CCDApi(String distributor_id,
      String distributor_name, String admin_id, String financial_year,
      String date,
      String remark, File file_name, String deduction_head,
      String donation) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertDeductionMasterDeatil.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['distributor_name'] = distributor_name;
    request.fields['admin_id'] = admin_id;
    request.fields['financial_year'] = financial_year;
    request.fields['date'] = date;
    request.fields['remark'] = remark;
    request.fields['deduction_head'] = deduction_head;
    request.fields['donation'] = donation;
    try {
      if (file_name.path.isEmpty) {
        print("File is empty");
        request.fields['file_name'] = "";
      } else {
        print("File have path");
        print("File Path"+file_name.path.toString());
        request.files.add(await http.MultipartFile.fromPath(
            'file_name', file_name.path));
      }
    } on Exception catch (e) {
      print(e);
    }
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  //****** 80D ******
  static Future insertDeduction80DApi(String distributor_id,
      String distributor_name, String admin_id, String financial_year,
      String date,
      String remark, File file_name, String deduction_head, String parents,
      String self) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertDeductionMasterDeatil.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['distributor_name'] = distributor_name;
    request.fields["admin_id"] = admin_id;
    request.fields['financial_year'] = financial_year;
    request.fields['date'] = date;
    request.fields['remark'] = remark;
    request.fields['deduction_head'] = deduction_head;
    request.fields['parents'] = parents;
    request.fields['self'] = self;
    try {
      if (file_name.path.isEmpty) {
        print("File is empty");
        request.fields['bank_statement'] = "";
      } else {
        print("File have path");
        request.files.add(await http.MultipartFile.fromPath(
            'bank_statement', file_name.path));
      }
    } on Exception catch (e) {
      print(e);
    }
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  //****** 80G ******
  static Future insertDeduction80GApi(String distributor_id,
      String distributor_name, String admin_id, String financial_year,
      String date,
      String remark, File file_name, String deduction_head, String range,
      String other_by_cheque, String in_cash) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertDeductionMasterDeatil.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['distributor_name'] = distributor_name;
    request.fields["admin_id"] = admin_id;
    request.fields['financial_year'] = financial_year;
    request.fields['date'] = date;
    request.fields['remark'] = remark;
    request.fields['deduction_head'] = deduction_head;
    request.fields['range'] = range;
    request.fields['other_by_cheque'] = other_by_cheque;
    request.fields['in_cash'] = in_cash;
    try {
      if (file_name.path.isEmpty) {
        print("File is empty");
        request.fields['bank_statement'] = "";
      } else {
        print("File have path");
        request.files.add(await http.MultipartFile.fromPath(
            'bank_statement', file_name.path));
      }
    } on Exception catch (e) {
      print(e);
    }
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  //****** 80TTA ******
  static Future insertDeduction80TTAApi(String distributor_id,
      String distributor_name, String admin_id, String financial_year,
      String date,
      String remark, File file_name, String deduction_head,
      String tta_80) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertDeductionMasterDeatil.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['distributor_name'] = distributor_name;
    request.fields["admin_id"] = admin_id;
    request.fields['financial_year'] = financial_year;
    request.fields['date'] = date;
    request.fields['remark'] = remark;
    request.fields['deduction_head'] = deduction_head;
    request.fields['tta_80'] = tta_80;

    try {
      if (file_name.path.isEmpty) {
        print("File is empty");
        request.fields['bank_statement'] = "";
      } else {
        print("File have path");
        request.files.add(await http.MultipartFile.fromPath(
            'bank_statement', file_name.path));
      }
    } on Exception catch (e) {
      print(e);
    }
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  //****** Oter ******
  // ignore: non_constant_identifier_names
  static Future insertDeductionOtherApi(String distributor_id,
      String distributor_name,
      String admin_id,
      String financial_year,
      String date,
      // ignore: non_constant_identifier_names
      String remark,
      File file_name,
      String deduction_head,
      String other) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertDeductionMasterDeatil.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['distributor_name'] = distributor_name;
    request.fields['admin_id'] = admin_id;
    request.fields['financial_year'] = financial_year;
    request.fields['date'] = date;
    request.fields['remark'] = remark;
    request.fields['deduction_head'] = deduction_head;
    request.fields['other'] = other;

    try {
      if (file_name.path.isEmpty) {
        print("File is empty");
        request.fields['bank_statement'] = "";
      } else {
        print("File have path");
        request.files.add(await http.MultipartFile.fromPath(
            'bank_statement', file_name.path));
      }
    } on Exception catch (e) {
      print(e);
    }
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  static Future<DeductionModel> getDeductionDetailsApi(String distributorId,
      String ditributorName, String adminId, String financialYear) async {
    print('${distributorId.toString()},${
        ditributorName.toString()},${
        adminId.toString()},${
        financialYear}');
    final response = await http.post(Uri.parse(
        "https://appexperts.net/btconnect/api/getDeductionMaster.php"),
        body: {
          "distributor_id": distributorId,
          "distributor_name": ditributorName,
          "admin_id": adminId,
          "financial_year": financialYear
        });

    print("-------*****Data Response Deduction Details:*****------ " + response.body);

    if (response.statusCode == 200) {
     // print("Data Response Deduction Details: " + response.body);
      return deductionModelFromJson(response.body);
    }
    return deductionModelFromJson(response.body);
  }

  static Future getDeductionDetailsApi2(String distributorId,
      String ditributorName, String adminId, String financialYear) async {
    final response = await http.post(Uri.parse(
        "https://appexperts.net/btconnect/api/getDeductionMaster.php"),
        body: {
          "distributor_id": distributorId,
          "distributor_name": ditributorName,
          "admin_id": adminId,
          "financial_year": financialYear
        });

    if (response.statusCode == 200) {
      print("Data Response Deduction Details: " + response.body);
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

  /**Investment Insert Api***** */
  static Future insertInvestmentApi(BuildContext context,
      String distributor_id,
      String distributor_name,
      String admin_id,
      String financial_year,
      String investment_type,
      String name,
      String date,
      String file_name,
      String amount,
      String remark,) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertInvestment.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['distributor_name'] = distributor_name;
    request.fields['admin_id'] = admin_id;
    request.fields['financial_year'] = financial_year;
    request.fields['investment_type'] = investment_type;
    request.fields['name'] = name;
    request.fields['date'] = date;
    request.fields['amount'] = amount;
    request.fields['remark'] = remark;
    if (file_name == null || file_name == "") {
      request.fields['attachment'] = "";
    } else {
      request.files.add(await http.MultipartFile.fromPath(
          'attachment', File(file_name).path.toString()));
    }
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  //**********Get Investment Details*************

  static Future<InvestmentModel> getInvestmentApi(String distributorID,
      String financialYear, String adminId) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/getInvestment.php"),
        body: {
          "distributor_id": distributorID,
          "financial_year": financialYear,
          "admin_id": adminId
        });
    if (response.statusCode == 200) {
      // print("Data Response of Investment: " + response.body);
      return investmentModelFromJson(response.body);
    }
    return investmentModelFromJson(response.body);
  }

  static Future getInvestmentApi2(String distributorID, String financialYear,
      String adminId) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/getInvestment.php"),
        body: {
          "distributor_id": distributorID,
          "financial_year": financialYear,
          "admin_id": adminId
        });
    if (response.statusCode == 200) {
      // print("Data Response of Investment: " + response.body);
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

  //***** Investment Edit Api*****
  static Future editInvestmentApi(String distributor_id,
      String distributor_name, String admin_id, String financial_year,
      String investment_type,
      String name, String date, String file_name, String amount, String remark,
      String investmentId) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/updateInvestment.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['distributor_name'] = distributor_name;
    request.fields['admin_id'] = admin_id;
    request.fields['financial_year'] = financial_year;
    request.fields['investment_type'] = investment_type;
    request.fields['name'] = name;
    request.fields['date'] = date;
    request.fields['amount'] = amount;
    request.fields['remark'] = remark;
    request.fields['investment_id'] = investmentId;

    try {
      if (file_name == "") {
        print("File is empty");
        request.fields['attachment'] = "";
      } else {
        print("File have path");
        request.files.add(await http.MultipartFile.fromPath(
            'attachment', File(file_name).path.toString()));
      }
    } on Exception catch (e) {
      print(e);
    }
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  /**Insert Expenser Api******/

  static Future insertExpensesApi(String distributor_id,
      String distributor_name,
      String admin_id,
      String financial_year,
      String name,
      String date,
      String file_name,
      String amount,
      String remark,) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertExpenses.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['distributor_name'] = distributor_name;
    request.fields['admin_id'] = admin_id;
    request.fields['financial_year'] = financial_year;

    request.fields['name'] = name;
    request.fields['date'] = date;
    request.fields['amount'] = amount;
    request.fields['remark'] = remark;

    try {
      if (file_name.isEmpty) {
        print("File is empty");
        request.fields['attachment'] = "";
      } else {
        print("File have path");
        request.files.add(await http.MultipartFile.fromPath(
            'attachment', File(file_name).path.toString()));
      }
    } on Exception catch (e) {
      print(e);
    }
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  //**********Get Expense Details*************

  static Future<ExpenseModel> getExpenseApi(String distributorID,
      String financialYear, String adminId) async {
    EasyLoading.show(status: "");
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/getExpenses.php"),
        body: {
          "distributor_id": distributorID,
          "financial_year": financialYear,
          "admin_id": adminId
        });
     print("Data Response of Investment: " + response.body);
    if (response.statusCode == 200) {
      EasyLoading.dismiss();
      // print("Data Response of Investment: " + response.body);
      return expenseModelFromJson(response.body);
    }
    EasyLoading.dismiss();
    return expenseModelFromJson(response.body);
  }

  static Future getExpenseApi2(String distributorID, String financialYear,
      String adminId) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/getExpenses.php"),
        body: {
          "distributor_id": distributorID,
          "financial_year": financialYear,
          "admin_id": adminId
        });
    if (response.statusCode == 200) {
      // print("Data Response of Investment: " + response.body);
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

//*******Investment Edit Api******************/

  static Future editExpenseApi(String distributor_id, String distributor_name,
      String admin_id, String financial_year, String name, String date,
      String file_name, String amount, String remark, String expenseId) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/updateExpense.php");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['distributor_name'] = distributor_name;
    request.fields['admin_id'] = admin_id;
    request.fields['financial_year'] = financial_year;
    request.fields['name'] = name;
    request.fields['date'] = date;
    request.fields['amount'] = amount;
    request.fields['remark'] = remark;
    request.fields['expense_id'] = expenseId;

    try {
      if (file_name == "") {
        print("File is empty");
        request.fields['attachment'] = "";
      } else {
        print("File have path");
        request.files.add(
            await http.MultipartFile.fromPath('attachment', file_name));
      }
    } on Exception catch (e) {
      print(e);
    }
    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  static Future InsertDepreciationApi(String distributor_id,
      String admin_id,
      String financial_year,
      String head_category,
      String asset_name,
      String opening_balance,
      String purchase_date,
      String purchase_amount,
      String sale_date,
      String sale_amount,
      String depreciation_rate,
      String total_assets,
      String depreciation_amount,
      String closing_balance) async {
    var postUri = Uri.parse(
        "https://appexperts.net/btconnect/api/insertDepreciation.php");

    var request = http.MultipartRequest("POST", postUri);
    request.fields['distributor_id'] = distributor_id;
    request.fields['admin_id'] = admin_id;
    request.fields['financial_year'] = financial_year;
    request.fields['head_category'] = head_category;
    request.fields['asset_name'] = asset_name;
    request.fields['opening_balance'] = opening_balance;
    request.fields['purchase_date'] = purchase_date;
    request.fields['purchase_amount'] = purchase_amount;
    request.fields['sale_date'] = sale_date;
    request.fields['sale_amount'] = sale_amount;
    request.fields['depreciation_rate'] = depreciation_rate;
    request.fields['total_assets'] = total_assets;
    request.fields['depreciation_amount'] = depreciation_amount;
    request.fields['closing_balance'] = closing_balance;

    http.Response response = await http.Response.fromStream(
        await request.send());
    print("Result: ${response.statusCode}");
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  static Future getDepreciationApi(String distributorID, String financialYear,
      String adminId) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/getDepreciation.php"),
        body: {
          "distributor_id": distributorID,
          "financial_year": financialYear,
          "admin_id": adminId
        });
    if (response.statusCode == 200) {
      // print("Data Response of Investment: " + response.body);
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

  //**** ProfitandLoss Api******/
  static Future profitAndLossApi(String distributorId,
      String adminId,
      String financialYear,
      String opening_stock,
      String sale,
      String purchase,
      String closing_stock,
      String gross_profit,
      String expenses,
      String commission,
      String interest_on_tds,
      String net_profit,
      String depreciation,
      String head_json,

      String commission_of_trading_ac,
      String commission_paid_to_retailer) async {
    final response = await http.post(Uri.parse(
        "https://appexperts.net/btconnect/api/insertProfitAndLoss.php"), body: {
      "distributor_id": distributorId,
      "admin_id": adminId,
      "financial_year": financialYear,
      "opening_stock": opening_stock,
      "sale": sale,
      "purchase": purchase,
      "closing_stock": closing_stock,
      "gross_profit": gross_profit,
      "expenses": expenses,
      "commission": commission,
      "interest_on_tds": interest_on_tds,
      "net_profit": net_profit,
      "depreciation": depreciation,
      "head_json": head_json,

      "commission_of_trading_ac": commission_of_trading_ac,
      "commission_paid_to_retailer": commission_paid_to_retailer
    });
    if (response.statusCode.toString() == 200) {
      print("Profit and Loss -----:" + response.body);
      return jsonDecode(response.body);
    }
    print("Profit and Loss -----:" + response.body);
    return jsonDecode(response.body);
  }

//**** GetProfitandLoss Api******/
  static Future getProfitAndLossApi(String distributorID, String financialYear,
      String adminId) async {
    print("distributorID" + distributorID.toString());
    print("financial_year" + financialYear.toString());
    print("adminId" + adminId.toString());

    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/getProfitAndLoss.php"),
        body: {
          "distributor_id": distributorID,
          "financial_year": financialYear,
          "admin_id": adminId
        });

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

  // complete computation data api
  static Future jsonComputationApi(String distributorID, String financialYear,
      String adminId, String computationData) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/insertComputation.php"),
        body: {
          "distributor_id": distributorID,
          "financial_year": financialYear,
          "admin_id": adminId,
          "computation_data": computationData
        });
    if (response.statusCode == 200) {
      print(response.body);
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

  // Annexure Api

  static Future jsonAnnexureApi(String distributorID, String financialYear,
      String adminId, String annexureData) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/insertAnnexure.php"),
        body: {
          "distributor_id": distributorID,
          "financial_year": financialYear,
          "admin_id": adminId,
          "annexure_data": annexureData
        });
    if (response.statusCode == 200) {
      print(response.body);
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

  static Future<AnnexureModel> getAnnexurApi(String distributorID,
      String financialYear, String adminId) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/getAnnexure.php"),
        body: {
          "distributor_id": distributorID,
          "financial_year": financialYear,
          "admin_id": adminId
        });
    if (response.statusCode == 200) {
      print("-------AnnexureModel-------"+response.body.toString());
      return annexureModelFromJson(response.body);
    }
    return annexureModelFromJson(response.body);
  }

  static Future getComputationApi(String distributorID, String financialYear,
      String adminId) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/getComputation.php"),
        body: {
          "distributor_id": distributorID,
          "financial_year": financialYear,
          "admin_id": adminId
        });
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }

  static Future deleteDepreciationEntryApi(String head,String id) async {
    final response = await http.post(
        Uri.parse("https://appexperts.net/btconnect/api/deleteRecord.php"),
        body: {
          "head": head,
          "id": id,
        });
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    return jsonDecode(response.body);
  }
}
