import 'package:accountx/app/modules/loginPage/loginController.dart';
import 'package:accountx/app/modules/loginPage/loginView.dart';
import 'package:accountx/app/services/api.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SideBar extends StatelessWidget {
  const SideBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GetStorage box4 = GetStorage();

    var loginData = box4.read("loginValue");

    return Drawer(
      child: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(40),
          ),
        ),
        child: Column(
          children: [
            DrawerHeader(
              decoration: const BoxDecoration(
                color: Color(0xff0B28BE),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const Center(
                    child: Icon(
                      Icons.person,
                      color: Colors.white,
                      size: 50.0,
                    ),
                  ),
                  Center(
                    child: Text(
                      loginData['admin_name'],
                      textAlign: TextAlign.center,
                      style: const TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ),
                ],
              ),
            ),
            ListTile(
              leading: const Icon(Icons.read_more),
              title: const Text("Logout"),
              onTap: () {
                Get.defaultDialog(
                    title: "Logout !!",titlePadding: EdgeInsets.only(top: 20),
                    content: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Center(
                            child: Text(
                          "Are you sure ?",
                          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                          textAlign: TextAlign.start,
                        )),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            TextButton(
                                onPressed: () {
                                  Get.back();
                                },
                                child: const Text(
                                  "Cancel",
                                  style: TextStyle(color: Color(0xff0B28BE)),
                                )),
                            TextButton(
                                onPressed: () {
                                  Api.logoutApi(box4.read("loginValue")["admin_id"]).then((value) {
                                    Get.snackbar(
                                      "",
                                      "",
                                      messageText: Text(
                                        value["details"],
                                        textAlign: TextAlign.center,
                                      ),
                                      snackPosition: SnackPosition.BOTTOM,
                                    );
                                    Get.put(LoginController());
                                    Get.offAll(LoginView());
                                    box4.write('isLogined', false);
                                  });
                                  //  Get.put(LoginController());
                                  //  Get.off(LoginView());
                                  //  box4.write('isLogined', false);
                                  //  Get.snackbar("", "",
                                  //      maxWidth: 200,
                                  //      backgroundColor: Colors.black,
                                  //  colorText: Colors.white,duration: const Duration(seconds: 1),
                                  //    snackPosition: SnackPosition.BOTTOM,
                                  //    messageText: const Text("Logout Successfully",
                                  //      style: TextStyle(color: Colors.white),
                                  //      textAlign: TextAlign.center,)
                                  //  );
                                },
                                child: const Text(
                                  "OK",
                                  style: TextStyle(color: Color(0xff0B28BE)),
                                )),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        )
                      ],
                    ));
              },
            ),
          ],
        ),
      ),
    );
  }
}
