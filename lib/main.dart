import 'dart:io';

import 'package:accountx/app/modules/bank_statement_page/bank_statement_binding.dart';
import 'package:accountx/app/modules/bank_statement_page/bank_statement_view.dart';
import 'package:accountx/app/modules/calculators_page/calculators_binding.dart';
import 'package:accountx/app/modules/calculators_page/calculators_view.dart';
import 'package:accountx/app/modules/deduction_page/deduction_view.dart';
import 'package:accountx/app/modules/depreciationCalPage/depreciation_view.dart';
import 'package:accountx/app/modules/expenses_page/expenses_view.dart';
import 'package:accountx/app/modules/financial_page/balance_sheet_page/balance_sheet_view.dart';

import 'package:accountx/app/modules/homePage/homeView.dart';
import 'package:accountx/app/modules/incomeMasterPage/incomeMasterBinding.dart';
import 'package:accountx/app/modules/incomeMasterPage/incomeMasterView.dart';
import 'package:accountx/app/modules/investment_page/investment_view.dart';
import 'package:accountx/app/modules/loginPage/loginBinding.dart';
import 'package:accountx/app/modules/loginPage/loginView.dart';
import 'package:accountx/app/modules/ppf_account_page/ppf_account_view.dart';
import 'package:accountx/app/modules/splash_screen.dart';
import 'package:accountx/app/modules/taxCalPage/taxView.dart';
import 'package:accountx/app/provider/ppf_provider.dart';
import 'package:accountx/app/provider/update_provider.dart';

import 'package:accountx/widgets/sidebar.dart';
import 'package:desktop_window/desktop_window.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:window_manager/window_manager.dart';
import 'app/modules/emiCalPage/emiView.dart';
import 'app/modules/financial_page/computation_page/computation_view.dart';
import 'app/modules/homePage/homeBinding.dart';
import 'package:get_storage/get_storage.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'app/provider/bank_statement_provider.dart';
import 'app/provider/investment_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if ( Platform.isWindows)  {
    await DesktopWindow.setMinWindowSize(const Size(720, 900));
  }
 /* if (Platform.isWindows) {
    await WindowManager.instance.ensureInitialized();

    // Use it only after calling `hiddenWindowAtLaunch`
    WindowManager.instance.waitUntilReadyToShow().then((_) async {
      // Set to frameless window
      *//* await WindowManager.instance.setSize(Size(370, 720));
    await WindowManager.instance.setPosition(Offset.zero);
    await WindowManager.instance.unmaximize();
    await WindowManager.instance.setFullScreen(false

    );
    await WindowManager.instance.restore();*//*

      await WindowManager.instance.center();
      await WindowManager.instance.setResizable(false);
      await WindowManager.instance.show();
      // WindowManager.instance.show();
    });
  }*/

  await GetStorage.init();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => UpdateProvider()),
      ChangeNotifierProvider(create: (_) => InvestmentProvider()),
      ChangeNotifierProvider(create: (_) => PpfProvider()),
      ChangeNotifierProvider(create: (_) => BankStatementProvider()),
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
  /*  GetStorage box1 = GetStorage();
    bool isLogined = false;
    isLogined = box1.read('isLogined') ?? false;
*/
    return
      GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'AccountX',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      builder: EasyLoading.init(),
      // initialRoute: isLogined ? "/" : "/login",
      initialRoute: "/spl",

      initialBinding: HomeBinding(),
      getPages: [
        GetPage(name: "/", page: () => HomeView(), binding: HomeBinding()),
        GetPage(name: "/spl", page: () => const SplashScreen()),
        GetPage(name: "/taxcal", page: () => const TaxView()),
        GetPage(name: "/emical", page: () => const EmiView()),
        GetPage(name: "/depcal", page: () => DepreciationCalculator()),
        GetPage(name: "/login", page: () => LoginView(), binding: LoginBinding()),
        GetPage(name: "/incomemaster", page: () => const IncomeMaster(), binding: IncomeMasterBinding()),
        GetPage(name: "/bankstatement", page: () => BankStatementView(), binding: BankStatementBinding()),
        GetPage(name: "/ppfaccount", page: () => const PPFAccount()),
        GetPage(name: "/deduction", page: () => const DeductionView()),
        GetPage(name: "/investment", page: () => const InvestmentView()),
        GetPage(name: "/expenses", page: () => const ExpensesView()),
        GetPage(name: "/CalculatorView", page: () => CalculatorView(), binding: CalculatorsBinding()),
        GetPage(name: "/sidebar", page: () => const SideBar()),
        GetPage(
          name: "/computation",
          page: () => ComputationView(),
        ),
        GetPage(
          name: "/balancesheet",
          page: () => BalanceSheetView(),
        ),
      ],
    );
  }
}
